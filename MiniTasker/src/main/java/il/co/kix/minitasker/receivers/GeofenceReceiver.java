package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.ActivityUtils;
import il.co.kix.minitasker.utils.GeofenceUtils;

/**
 * geofence broadcast receiver
 * <p/>
 * Created by phantom on 20/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class GeofenceReceiver extends BackgroundBroadcastReceiver {
    public void onReceiveBackground(@NotNull Context context, @NotNull Intent intent) {
        String action = intent.getAction();

        // Intent contains information about errors in adding or removing geofences
        if (TextUtils.equals(action, GeofenceUtils.ACTION_GEOFENCE_ERROR)) {

            handleGeofenceError(context, intent);

            // Intent contains information about successful addition or removal of geofences
        } else if (
                TextUtils.equals(action, GeofenceUtils.ACTION_GEOFENCES_ADDED)
                        ||
                        TextUtils.equals(action, GeofenceUtils.ACTION_GEOFENCES_REMOVED)) {

            handleGeofenceStatus(context, intent);

            // Intent contains information about a geofence transition
        } else if (TextUtils.equals(action, GeofenceUtils.ACTION_GEOFENCE_TRANSITION)) {

            handleGeofenceTransition(context, intent);

            // The Intent contained an invalid action
//        } else {
////            Log.e(GeofenceUtils.APPTAG, getString(R.string.invalid_action_detail, action));
//            //huh ? this should be a Context and Context has getString, wtf?
////            ToastHandler mToastHandler = new ToastHandler(context);
////            mToastHandler.showToast(R.string.invalid_action);
        }


    }

    /**
     * If you want to display a UI message about adding or removing geofences, put it here.
     *
     * @param context A Context for this component
     * @param intent  The received broadcast Intent
     */
    private void handleGeofenceStatus(Context context, Intent intent) {

    }

    /**
     * Report geofence transitions to the UI
     *
     * @param context A Context for this component
     * @param intent  The Intent containing the transition
     */
    private void handleGeofenceTransition(Context context, Intent intent) {
            /*
             * If you want to change the UI when a transition occurs, put the code
             * here. The current design of the app uses a notification to inform the
             * user that a transition has occurred.
             */
    }

    /**
     * Report addition or removal errors to the UI, using a Toast
     *
     * @param intent A broadcast Intent sent by ReceiveTransitionsIntentService
     */
    private void handleGeofenceError(@NotNull Context context, @NotNull Intent intent) {
        String msg = intent.getStringExtra(GeofenceUtils.EXTRA_GEOFENCE_STATUS);
//        Log.e(GeofenceUtils.APPTAG, msg);
        Context appContext = context.getApplicationContext();
        assert appContext != null;
        SharedPreferences mPrefs = appContext.getSharedPreferences
                (ActivityUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(BackgroundBroadcastReceiver.KEY_SHOW_TOAST, msg);
        editor.commit();

    }

}
