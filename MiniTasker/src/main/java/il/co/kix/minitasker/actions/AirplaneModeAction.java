package il.co.kix.minitasker.actions;

import android.annotation.SuppressLint;
import android.os.Build;
import android.provider.Settings;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.AirplaneModeEnabler;
import il.co.kix.minitasker.ExecuteAsRootBase;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * airplane mode action
 * <p/>
 * Created by sphantom on 7/28/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class AirplaneModeAction extends StatesAction {
    @SuppressLint("InlinedApi")
    @SuppressWarnings("deprecation")
    final String AIRPLANE_MODE_ON =
            (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 ?
                    android.provider.Settings.System.AIRPLANE_MODE_ON :
                    Settings.Global.AIRPLANE_MODE_ON);

    @Override
    public boolean save() {
        boolean result = super.save();
        ExecuteAsRootBase.canRunRootCommands();
        return result;
    }

    @NotNull
    @Override
    public String getDeviceName() {
        return "airplane";
    }

    @Override
    protected boolean perform_init() {
        return true;
    }

    @NotNull
    @SuppressLint("NewApi")
    @Override
    protected StateType perform_get_current_state() {
        int airplane_enabled =
                (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 ?
                        Settings.System.getInt(context.getContentResolver(), AIRPLANE_MODE_ON, 0) :
                        Settings.Global.getInt(context.getContentResolver(), AIRPLANE_MODE_ON, 0)
                );
        switch (airplane_enabled) {
            case 1:
                return StateType.On;
            case 0:
            default:
                return StateType.Off;
        }
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.AIRPLANE;
    }

    @Override
    protected boolean perform_on() {
        return new AirplaneModeEnabler(context, true).perform();
    }

    @Override
    protected boolean perform_off() {
        return new AirplaneModeEnabler(context, false).perform();
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.AIRPLANE_ROOT_FAILED);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_airplane_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_airplane_individual_on);
            case Off:
                return context.getString(R.string.action_airplane_individual_off);
            case Toggle:
                return context.getString(R.string.action_airplane_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_airplane_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_airplane_description_on);
            case Off:
                return context.getString(R.string.action_airplane_description_off);
            case Toggle:
                return context.getString(R.string.action_airplane_description_toggle);
        }
        return "";
    }
}
