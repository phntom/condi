package il.co.kix.minitasker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.common.collect.ImmutableList;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import il.co.kix.minitasker.conditions.CalendarCondition;
import il.co.kix.minitasker.conditions.TimeCondition;
import il.co.kix.minitasker.receivers.PhoneReceiver;
import il.co.kix.minitasker.services.ConditionIntentService;
import il.co.kix.minitasker.services.LauncherIntentService;
import il.co.kix.minitasker.ui.MainActivity;
import il.co.kix.minitasker.ui.MiniTaskerActivity;

import static android.provider.CalendarContract.Attendees;
import static android.provider.CalendarContract.Events;
import static android.provider.CalendarContract.Instances;
import static il.co.kix.minitasker.SqlDataType.BOOLEAN;
import static il.co.kix.minitasker.SqlDataType.ENUM;
import static il.co.kix.minitasker.SqlDataType.INTEGER;
import static il.co.kix.minitasker.SqlDataType.INTEGER_KEY;
import static il.co.kix.minitasker.SqlDataType.PRIMARY;
import static il.co.kix.minitasker.SqlDataType.SET;
import static il.co.kix.minitasker.SqlDataType.TEXT;

/**
 * handles the database
 * <p/>
 * DatabaseHandler
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    public static final String APPTAG = "cond.im";
    // Database Name
    public static final String DATABASE_NAME = "minitaskerdb";
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 51;
    private static final String V = "_v1";
    private static final int DEFAULT_LOOK_AHEAD_HOURS = 48;
    private static final int HOUR_MILLIS = 3600000;
    private static final String[] emptyStringArray = new String[]{};
    private static final String JSON_TAG_CONDITION = "condition_";
    private static final String JSON_TAG_ACTION = "action_";
    @NotNull
    private static final List<SqlTable> mTables = ImmutableList.<SqlTable>builder()
            .add(
                    new SqlTable("tasks" + V, 38)
                            .addColumn("task_id", PRIMARY, null, 38)
                            .addColumn("enabled", BOOLEAN, true, 38)
                            .addColumn("selected", BOOLEAN, false, 38)
                            .addColumn("description", TEXT, "", 38)
                            .addColumn("when_id", INTEGER, 0, 38)
                            .addColumn("what_id", INTEGER, 0, 38)
                            .addColumn("start_type", ENUM, StartType.UNKNOWN.ordinal(), 38)
                            .addColumn("label", TEXT, "", 40)
                            .addColumn("idx", INTEGER, 0, 40)
                            .addColumn("undo", INTEGER, 0, 40)
                            .addColumn("expanded", BOOLEAN, false, 40)
                            .addColumn("last_ran", INTEGER, 0, 40)
                            .addColumn("copy", INTEGER, 0, 40)
                            .addColumn("link_hash", TEXT, "", 45)
            )
            .add(
                    new SqlTable("when" + V, 38)
                            .addColumn("when_id", INTEGER_KEY, null, 38)
                            .addColumn("when_item", INTEGER, 0, 38)
                            .addColumn("when_type", TEXT, "", 38)
                            .addColumn("enabled", BOOLEAN, true, 38)
                            .addColumn("description", TEXT, "", 38)
                            .addColumn("taskdesc", TEXT, "", 38)
                            .addColumn("running", BOOLEAN, false, 38)
                            .addColumn("start_type", ENUM, StartType.UNKNOWN.ordinal(), 38)
                            .addColumn("undo", INTEGER, 0, 40)
                            .addColumn("idx", INTEGER, 0, 40)
                            .addColumn("flag", SET, 0, 40)
                            .addColumn("last_ran", INTEGER, 0, 40))
            .add(
                    new SqlTable("what" + V, 38)
                            .addColumn("what_id", INTEGER_KEY, null, 38)
                            .addColumn("what_item", INTEGER, 0, 38)
                            .addColumn("what_type", TEXT, "", 38)
                            .addColumn("enabled", BOOLEAN, true, 38)
                            .addColumn("description", TEXT, "", 38)
                            .addColumn("taskdesc", TEXT, "", 38)
                            .addColumn("start_type", ENUM, StartType.UNKNOWN.ordinal(), 38)
                            .addColumn("start", BOOLEAN, true, 40)
                            .addColumn("restore", BOOLEAN, false, 40)
                            .addColumn("undo", INTEGER, 0, 40)
                            .addColumn("idx", INTEGER, 0, 40)
                            .addColumn("flag", SET, 0, 40)
                            .addColumn("last_ran", INTEGER, 0, 40)
            )

            .add(
                    new SqlTable("last" + V, 38)
                            .addColumn("last_id", PRIMARY, null, 38)
                            .addColumn("value", INTEGER, 0, 38)
                            .addColumn("count", INTEGER, 1, 38)
            )
            .add(
                    new SqlTable("settings" + V, 38)
                            .addColumn("key", TEXT, "", 38)
                            .addColumn("value", TEXT, "", 38)
            )
            .add(
                    new SqlTable("stats" + V, 38)
                            .addColumn("kind", INTEGER, 0, 38)
                            .addColumn("value", INTEGER, 0, 38)
            )
            .add(
                    new SqlTable("log" + V, 40)
                            .addColumn("timestamp", INTEGER, 0, 40)
                            .addColumn("entity", TEXT, "", 40)
                            .addColumn("content", TEXT, "", 40)
            )
            .add(
                    new SqlTable("states" + V, 40)
                            .addColumn("device", TEXT, "", 40)
                            .addColumn("state", INTEGER, 0, 40)
            )
            .add(
                    new SqlTable("last_tasks" + V, 44)
                            .addColumn("last_id", INTEGER_KEY, 0, 44)
                            .addColumn("task_id", INTEGER_KEY, 0, 44)
            )
            .add(
                    new SqlTable("sms_queue" + V, 47)
                            .addColumn("queue_id", PRIMARY, 0, 47)
                            .addColumn("target", TEXT, "", 47)
                            .addColumn("message", TEXT, "", 47)
                            .addColumn("parts", INTEGER, 0, 47)
                            .addColumn("sent", INTEGER, 0, 47)
                            .addColumn("failed", INTEGER, 0, 47)
                            .addColumn("attempt", INTEGER, 0, 47)
                            .addColumn("decay", INTEGER, 0, 47)
            )
            .add(
                    new SqlTable("when_time_quick" + V, 51)
                            .addColumn("condition_id", INTEGER_KEY, 0, 51)
                            .addColumn("time", INTEGER_KEY, 0, 51)
            )
            .build();


    @NotNull
    private static final String[] mAllTables = new String[]{
            "tasks_v1",
            "when_v1",
            "what_v1",
            "last_v1",
            "settings_v1",
            "stats_v1",
            "log_v1",
            "states_v1",
            "last_tasks_v1",
            "sms_queue_v1",
            "when_time_quick_v1",
            "what_message_v1",
            "when_time_v1",
            "what_states_v1",
            "what_media_v1",
            "what_brightness_v1",
            "what_volumecontrol_v1",
            "when_media_v1",
            "when_dock_v1",
            "when_headset_v1",
            "when_charger_v1",
            "when_geofence_v1",
            "when_activity_v1",
            "when_location_v1",
            "when_calendar_v1",
            "when_phone_v1",
            "what_phone_v1",
            "what_navigation_v1",
            "what_launch_v1",
            "what_terminate_v1",
            "when_settings_v1",
            "what_sms_v1",
            "when_battery_v1",
            "when_boot_v1",
            "when_wifi_v1",
            "when_shortcut_v1",
            "when_hdmi_v1",
            "when_bluetooth_v1",
    };

    public boolean debugMode = false;

    private static final String QUERY_START_TYPE_START = "IN (" + StartType.START_WITH_END.ordinal() + "," + StartType.START_ONLY.ordinal() + ")";
    private static final String QUERY_START_TYPE_END = "IN (" + StartType.END.ordinal() + ")";

    @NotNull
    static private SecureRandom random = new SecureRandom();
    private final Context context;
    public int runActionCounter = 0;
    public int matchConditionCounter = 0;
    boolean upgradeChecked = false;

    public DatabaseHandler(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
        this.context = context;
    }

    static public void putColumnIntoContentValue(@NotNull Cursor cursor, int column, @NotNull ContentValues values) {
        final String columnName = cursor.getColumnName(column);
        switch (cursor.getType(column)) {
            case Cursor.FIELD_TYPE_NULL:
                values.putNull(columnName);
                break;
            case Cursor.FIELD_TYPE_BLOB:
                values.put(columnName, cursor.getBlob(column));
                break;
            case Cursor.FIELD_TYPE_FLOAT:
                values.put(columnName, cursor.getFloat(column));
                break;
            case Cursor.FIELD_TYPE_INTEGER:
                values.put(columnName, cursor.getInt(column));
                break;
            case Cursor.FIELD_TYPE_STRING:
                values.put(columnName, cursor.getString(column));
                break;
        }
    }

    @NotNull
    public static String randomString(int length) {
        char[] chars = new char[length];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }
        return new String(chars);
    }

    // Creating Tables
    @Override
    public void onCreate(@NotNull SQLiteDatabase db) {
        final String[] CREATE_TABLES = new String[]{
                "CREATE TABLE `what_message_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `message` TEXT\n" +
                        ");",
                "CREATE TABLE `when_time_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `repeat` INTEGER DEFAULT NULL,\n" +
                        "  `starttime` INTEGER DEFAULT NULL,\n" +
                        "  `endtime` INTEGER DEFAULT NULL,\n" +
                        "  `ranges` TEXT NOT NULL DEFAULT '',\n" +
                        "  `type` INTEGER NOT NULL DEFAULT " + TimeCondition.Type.WEEKLY.ordinal() + "\n" +
                        ");",
                "CREATE TABLE `what_states_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `device` TEXT,\n" +
                        "  `state` TEXT\n" +
                        ");",
                "CREATE TABLE `what_media_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `type` INT NOT NULL,\n" +
                        "  `playlist` TEXT NOT NULL DEFAULT '',\n" +
                        "  `playlist_id` INT NOT NULL DEFAULT 0,\n" +
                        "  `song_name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `song_id` INT NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `what_brightness_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `level` INT NOT NULL\n" +
                        ");",
                "CREATE TABLE `what_volumecontrol_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `volume_media` INTEGER NOT NULL,\n" +
                        "  `link_ringtone_notification` INTEGER NOT NULL,\n" +
                        "  `volume_ringtone` INTEGER NOT NULL,\n" +
                        "  `volume_notification` INTEGER NOT NULL,\n" +
                        "  `volume_alarm` INTEGER NOT NULL,\n" +
                        "  `vibrate` INTEGER NOT NULL,\n" +
                        "  `ignore` INTEGER NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `when_media_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `button` INTEGER NOT NULL,\n" +
                        "  `held` INTEGER NOT NULL\n" +
                        ");",
                "CREATE TABLE `when_dock_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");",
                "CREATE TABLE `when_headset_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `state` INTEGER NOT NULL\n" +
                        ");",
                "CREATE TABLE `when_charger_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `state` INTEGER NOT NULL\n" +
                        ");",
                "CREATE TABLE `when_geofence_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `latitude` REAL NOT NULL,\n" +
                        "  `longitude` REAL NOT NULL,\n" +
                        "  `radius` REAL NOT NULL,\n" +
                        "  `expiration` INTEGER NOT NULL,\n" +
                        "  `transition` INTEGER NOT NULL,\n" +
                        "  `friendly_name` TEXT NOT NULL,\n" +
                        "  `friendly_type` INTEGER NOT NULL\n" +
                        ");",
                "CREATE TABLE `when_activity_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `activity` INTEGER NOT NULL\n" +
                        ");",
                "CREATE TABLE `when_location_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `lat` REAL NOT NULL,\n" +
                        "  `lng` REAL NOT NULL,\n" +
                        "  `rad` REAL NOT NULL,\n" +
                        "  `inverse` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `description` TEXT NOT NULL DEFAULT ''\n" +
                        ");",
                "CREATE TABLE `when_calendar_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `title` TEXT NOT NULL,\n" +
                        "  `available` INT NOT NULL,\n" +
                        "  `desc` TEXT NOT NULL,\n" +
                        "  `identifier` INT NOT NULL,\n" +
                        "  `cal_name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `location` TEXT NOT NULL,\n" +
                        "  `last_sync` INT NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `when_phone_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `phone` TEXT NOT NULL DEFAULT '',\n" +
                        "  `private` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `unrecognized` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `kind` TEXT NOT NULL DEFAULT '',\n" +
                        "  `contact` TEXT NOT NULL DEFAULT '',\n" +
                        "  `inverse` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `contents` TEXT NOT NULL DEFAULT '%'\n" +
                        ");",
                "CREATE TABLE `what_phone_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `target` TEXT NOT NULL DEFAULT '',\n" +
                        "  `contact` TEXT NOT NULL DEFAULT ''\n" +
                        ");",
                "CREATE TABLE `what_navigation_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `address` TEXT NOT NULL DEFAULT ''" +
                        ");",
                "CREATE TABLE `what_launch_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `package` TEXT NOT NULL DEFAULT '',\n" +
                        "  `display` TEXT NOT NULL DEFAULT '',\n" +
                        "  `root` INTEGER NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `what_terminate_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `package` TEXT NOT NULL DEFAULT '',\n" +
                        "  `display` TEXT NOT NULL DEFAULT '',\n" +
                        "  `root` INTEGER NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `when_settings_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `statistics` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `activity_timeout` INTEGER NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `what_sms_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `target` TEXT NOT NULL DEFAULT '',\n" +
                        "  `target_name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `message` TEXT NOT NULL DEFAULT ''\n" +
                        ");",
                "CREATE TABLE `when_battery_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");",
                "CREATE TABLE `when_boot_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");",
                "CREATE TABLE `when_wifi_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `ssid` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `bssid` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `ip` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `inverse` INTEGER NOT NULL DEFAULT 0\n" +
                        ");",
                "CREATE TABLE `when_shortcut_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `appname` TEXT NOT NULL DEFAULT '',\n" +
                        "  `appactionid` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `title` TEXT NOT NULL DEFAULT ''\n" +
                        ");",
                "CREATE TABLE `when_hdmi_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");",
                "CREATE TABLE `when_bluetooth_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `mac` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `inverse` INTEGER NOT NULL DEFAULT 0\n" +
                        ");",

        };
        for (SqlTable sqlTable : mTables) {
            db.execSQL(sqlTable.toString());
        }

        for (String query : CREATE_TABLES) {
            db.execSQL(query);
        }

//        DatabaseUtils.createDbFromSqlStatements();

        /*
        for (Class<? extends EntityBase> aClass : EntityUtils.fields.keySet()) {
            try {
                final EntityBase instance = aClass.newInstance();
                final String tableName = instance.getTableName() + V;
                SqlTable sqlTable = new SqlTable(tableName, 0);
                Map<String, SqlDataType> fields = EntityUtils.fields.get(aClass);
                for (String fieldKey : fields.keySet()) {
                    final SqlDataType fieldType = fields.get(fieldKey);
                    Object fieldDefault = null;
                    switch (fieldType) {
                        case BLOB:
                        case TEXT:
                            fieldDefault = "";
                            break;
                        case INTEGER:
                        case INTEGER_KEY:
                        case PRIMARY:
                        case ENUM:
                        case SET:
                            fieldDefault = 0;
                            break;
                        case BOOLEAN:
                            fieldDefault = false;
                            break;
                        case REAL:
                            fieldDefault = 0.0;
                            break;
                    }
                    sqlTable.addColumn(fieldKey, fieldType, fieldDefault, 0);
                }

            } catch (InstantiationException | IllegalAccessException ignore) {
            }
        }
        */

        ContentValues contentValues = new ContentValues();
        contentValues.put("key", "ANDROID_ID");
        contentValues.put("value", Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID));
        db.insert("settings" + V, null, contentValues);
        contentValues.clear();
        contentValues.put("key", "INSTALL_VERSION");
        contentValues.put("value", "" + DATABASE_VERSION);
        db.insert("settings" + V, null, contentValues);
    }

    @Override
    public void onUpgrade(@NotNull SQLiteDatabase db, int oldVersion, int newVersion) {

        List<String> sql = new ArrayList<>();
        boolean passed_33 = false;
        switch (oldVersion) {
            case 9:
                sql.add("CREATE TABLE `what_media_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `type` VARCHAR NOT NULL,\n" +
                        "  `playlist` VARCHAR NOT NULL\n" +
                        ");");
            case 10:
                sql.add("ALTER TABLE `when_v1` ADD COLUMN `taskdesc` text NOT NULL DEFAULT ''");
                sql.add("ALTER TABLE `what_v1` ADD COLUMN `taskdesc` text NOT NULL DEFAULT ''");
            case 11:
                sql.add("CREATE TABLE `what_brightness_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `level` REAL NOT NULL\n" +
                        ");");
            case 15:
                sql.add("CREATE TABLE `when_geofence_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `latitude` REAL NOT NULL,\n" +
                        "  `longitude` REAL NOT NULL,\n" +
                        "  `radius` REAL NOT NULL,\n" +
                        "  `expiration` INTEGER NOT NULL,\n" +
                        "  `transition` INTEGER NOT NULL,\n" +
                        "  `friendly_name` TEXT NOT NULL,\n" +
                        "  `friendly_type` INTEGER NOT NULL\n" +
                        ");");
                sql.add("CREATE TABLE `when_activity_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `activity` INTEGER NOT NULL\n" +
                        ");");
            case 16:
                sql.add("ALTER TABLE `when_v1` ADD COLUMN `running` INT NOT NULL DEFAULT 0");
            case 17:
                sql.add("CREATE TABLE `when_location_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `lat` INTEGER NOT NULL,\n" +
                        "  `lng` INTEGER NOT NULL,\n" +
                        "  `rad` INTEGER NOT NULL\n" +
                        ");");
            case 18:
                sql.add("DROP TABLE `when_location_v1`;");
                sql.add("DELETE FROM `when_v1` WHERE when_type = 'location';");
                sql.add("CREATE TABLE `when_location_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `lat` REAL NOT NULL,\n" +
                        "  `lng` REAL NOT NULL,\n" +
                        "  `rad` REAL NOT NULL\n" +
                        ");");
            case 19:
                sql.add("CREATE TABLE `when_calendar_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `title` TEXT NOT NULL,\n" +
                        "  `available` INT NOT NULL,\n" +
                        "  `desc` TEXT NOT NULL,\n" +
                        "  `identifier` INT NOT NULL,\n" +
                        "  `location` TEXT NOT NULL\n" +
                        ");");
            case 20:
                sql.add("ALTER TABLE `when_calendar_v1` ADD COLUMN `cal_name` text NOT NULL DEFAULT ''");
                sql.add("ALTER TABLE `when_calendar_v1` ADD COLUMN `last_sync` INT NOT NULL DEFAULT 0");
            case 22:
                sql.add("CREATE TABLE `when_phone_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `phone` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("CREATE TABLE `what_phone_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");");
            case 23:
                sql.add("CREATE TABLE `what_navigation_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `address` TEXT NOT NULL DEFAULT ''" +
                        ");");
            case 24:
                sql.add("ALTER TABLE `what_phone_v1` ADD COLUMN `private` INTEGER NOT NULL DEFAULT 0");
                sql.add("ALTER TABLE `what_phone_v1` ADD COLUMN `unrecognized` INTEGER NOT NULL DEFAULT 0");
            case 25:
                sql.add("DROP TABLE `what_phone_v1`;");
                sql.add("DROP TABLE `when_phone_v1`;");
                sql.add("DELETE FROM `when_v1` WHERE when_type = 'phone';");
                sql.add("DELETE FROM `what_v1` WHERE what_type = 'phone';");
                sql.add("CREATE TABLE `when_phone_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `phone` TEXT NOT NULL DEFAULT '',\n" +
                        "  `private` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `unrecognized` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `kind` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("CREATE TABLE `what_phone_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `target` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
            case 26:
                sql.add("CREATE TABLE `what_launch_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `package` TEXT NOT NULL DEFAULT ''," +
                        "  `display` TEXT NOT NULL DEFAULT ''" +
                        ");");
                sql.add("CREATE TABLE `what_terminate_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `package` TEXT NOT NULL DEFAULT ''," +
                        "  `display` TEXT NOT NULL DEFAULT ''" +
                        ");");
            case 27:
                sql.add("ALTER TABLE `when_v1` ADD COLUMN `start_type` INTEGER NOT NULL DEFAULT " + StartType.UNKNOWN.ordinal());
                sql.add("ALTER TABLE `tasks_v1` ADD COLUMN `start_type` INTEGER NOT NULL DEFAULT " + StartType.UNKNOWN.ordinal());
            case 28:
                sql.add("ALTER TABLE `what_media_v1` ADD COLUMN `playlist_id` INT NOT NULL DEFAULT 0");
                sql.add("ALTER TABLE `what_media_v1` ADD COLUMN `song_name` TEXT NOT NULL DEFAULT ''");
                sql.add("ALTER TABLE `what_media_v1` ADD COLUMN `song_id` INT NOT NULL DEFAULT 0");
            case 29:
                sql.add("CREATE TABLE `when_settings_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `statistics` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `activity_timeout` INTEGER NOT NULL DEFAULT 0\n" +
                        ");");
            case 30:
                sql.add("ALTER TABLE what_phone_v1 RENAME TO tmp_what_phone_v1");
                sql.add("ALTER TABLE what_navigation_v1 RENAME TO tmp_what_navigation_v1");
                sql.add("ALTER TABLE what_launch_v1 RENAME TO tmp_what_launch_v1");
                sql.add("ALTER TABLE what_terminate_v1 RENAME TO tmp_what_terminate_v1");
                sql.add("CREATE TABLE `what_phone_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `target` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("CREATE TABLE `what_navigation_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `address` TEXT NOT NULL DEFAULT ''" +
                        ");");
                sql.add("CREATE TABLE `what_launch_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `package` TEXT NOT NULL DEFAULT '',\n" +
                        "  `display` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("CREATE TABLE `what_terminate_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `package` TEXT NOT NULL DEFAULT '',\n" +
                        "  `display` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("INSERT INTO what_phone_v1(what_id, enabled, target)\n" +
                        "SELECT when_id, enabled, target \n" +
                        "FROM tmp_what_phone_v1;\n");
                sql.add("INSERT INTO what_navigation_v1(what_id, enabled, address)\n" +
                        "SELECT when_id, enabled, address \n" +
                        "FROM tmp_what_navigation_v1;\n");
                sql.add("INSERT INTO what_launch_v1(what_id, enabled, package, display)\n" +
                        "SELECT when_id, enabled, package, display \n" +
                        "FROM tmp_what_launch_v1;\n");
                sql.add("INSERT INTO what_terminate_v1(what_id, enabled, package, display)\n" +
                        "SELECT when_id, enabled, package, display \n" +
                        "FROM tmp_what_terminate_v1;\n");
                sql.add("DROP TABLE tmp_what_phone_v1;");
                sql.add("DROP TABLE tmp_what_navigation_v1;");
                sql.add("DROP TABLE tmp_what_launch_v1;");
                sql.add("DROP TABLE tmp_what_terminate_v1;");
                sql.add("ALTER TABLE `what_phone_v1` ADD COLUMN `contact` TEXT NOT NULL DEFAULT '';");
            case 31:
                sql.add("ALTER TABLE what_volumecontrol_v1 RENAME TO tmp_what_volumecontrol_v1");
                sql.add("CREATE TABLE `what_volumecontrol_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `volume_media` INTEGER NOT NULL,\n" +
                        "  `link_ringtone_notification` INTEGER NOT NULL,\n" +
                        "  `volume_ringtone` INTEGER NOT NULL,\n" +
                        "  `volume_notification` INTEGER NOT NULL,\n" +
                        "  `volume_alarm` INTEGER NOT NULL,\n" +
                        "  `vibrate` INTEGER NOT NULL,\n" +
                        "  `ignore` INTEGER NOT NULL DEFAULT 0\n" +
                        ");");
                sql.add("INSERT INTO what_volumecontrol_v1 (what_id, enabled, volume_media, link_ringtone_notification, volume_ringtone, volume_notification, volume_alarm, vibrate) \n" +
                        "SELECT what_id, enabled, mediavol, ringtonestate, ringtonevol, notificationsvol, alarmvol, ringtonevib \n" +
                        "FROM tmp_what_volumecontrol_v1;\n");

                sql.add("CREATE TABLE `what_sms_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `target` TEXT NOT NULL DEFAULT '',\n" +
                        "  `target_name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `message` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("ALTER TABLE `what_v1` ADD COLUMN `start_type` INTEGER NOT NULL DEFAULT " + StartType.UNKNOWN.ordinal());
                sql.add("ALTER TABLE `when_phone_v1` ADD COLUMN `inverse` INTEGER NOT NULL DEFAULT 1");
                sql.add("CREATE TABLE `when_battery_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");");
                sql.add("UPDATE `when_time_v1` SET repeat = 127 WHERE repeat = 0");
            case 32:
                sql.add("CREATE TABLE `settings_v1` (\n" +
                        "  `key` TEXT NOT NULL,\n" +
                        "  `value` TEXT NOT NULL\n" +
                        ");");
                sql.add("INSERT INTO `settings_v1` VALUES ('INSTALL_VERSION', '" + DATABASE_VERSION + "');");
            case 33:
                passed_33 = true;
                sql.add("CREATE TABLE `stats_v1` (\n" +
                        "  `kind` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `value` INTEGER NOT NULL\n" +
                        ");");
                sql.add("DELETE FROM `settings_v1`;");
            case 34:
                sql.add("ALTER TABLE what_sms_v1 RENAME TO tmp_what_sms_v1");
                sql.add("CREATE TABLE `what_sms_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `target` TEXT NOT NULL DEFAULT '',\n" +
                        "  `target_name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `message` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                sql.add("INSERT INTO what_sms_v1 (what_id, enabled, target, target_name, message) \n" +
                        "SELECT when_id, enabled, target, target_name, message \n" +
                        "FROM tmp_what_sms_v1;\n");
                sql.add("DROP TABLE tmp_what_sms_v1;");
                sql.add("ALTER TABLE `when_location_v1` ADD COLUMN `inverse` INTEGER NOT NULL DEFAULT 0");
                sql.add("CREATE TABLE `when_boot_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");");
            case 35:
                sql.add("CREATE TABLE `when_wifi_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `ssid` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `bssid` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `ip` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `inverse` INTEGER NOT NULL DEFAULT 0\n" +
                        ");");
            case 36:
            case 37:
                sql.add("CREATE TABLE `when_shortcut_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `appname` TEXT NOT NULL DEFAULT '',\n" +
                        "  `appactionid` INTEGER NOT NULL DEFAULT 0,\n" +
                        "  `title` TEXT NOT NULL DEFAULT ''\n" +
                        ");");
                oldVersion = 38;
            case 38:
            case 39:
                // change brightness column from real to in
                sql.add("ALTER TABLE what_brightness_v1 RENAME TO tmp_what_brightness_v1;");
                sql.add("CREATE TABLE `what_brightness_v1` (\n" +
                        "  `what_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `level` INT NOT NULL\n" +
                        ");");
                sql.add("INSERT INTO what_brightness_v1 (what_id, enabled, level) \n" +
                        "SELECT what_id, enabled, CAST(255.0 * level AS INT) \n" +
                        "FROM tmp_what_brightness_v1;\n");
                sql.add("DROP TABLE tmp_what_brightness_v1;");
                // location description
                sql.add("ALTER TABLE `when_location_v1` ADD COLUMN `description` TEXT NOT NULL DEFAULT ''");

                // split actions into 2 with restore
                sql.add("INSERT INTO what_v1 (what_id, what_item, what_type, enabled, description, taskdesc, start_type, start, restore) \n" +
                        "SELECT what_id, what_item, what_type, enabled, '%%RESTORE%%', '%%RESTORE%%', start_type, 0, 1 \n" +
                        "FROM what_v1 \n" +
                        "WHERE (what_type IN ('states', 'brightness', 'media', 'volumecontrol', 'phone'));\n");
                //add restore only to phone with target = ''
                sql.add("DELETE FROM what_v1 WHERE what_type = 'phone' AND restore = 1 AND what_item IN (SELECT what_id FROM what_phone_v1 WHERE coalesce(target, '') != '');");

            case 40:
                sql.add("ALTER TABLE `when_phone_v1` ADD COLUMN `contents` TEXT NOT NULL DEFAULT '%';\n");
            case 41:
                sql.add("CREATE TABLE `when_hdmi_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1\n" +
                        ");");
            case 42:
                sql.add("ALTER TABLE `what_launch_v1` ADD COLUMN `root` INTEGER NOT NULL DEFAULT 0;\n");
                sql.add("ALTER TABLE `what_terminate_v1` ADD COLUMN `root` INTEGER NOT NULL DEFAULT 0;\n");
            case 43:
                sql.add("INSERT INTO `settings_v1` VALUES ('DISPLAY_LANGUAGE', 'en');");
                // for translation to be updated on first run

                sql.add("UPDATE `last_v1` SET count = 0;");
                // reset for the new last mechanism

            case 45:
                sql.add("UPDATE `last_v1` SET count = 0;");
                sql.add("DELETE FROM `last_tasks_v1`;");
            case 46:
            case 47:
                sql.add("CREATE TABLE `when_bluetooth_v1` (\n" +
                        "  `when_id` INTEGER PRIMARY KEY,\n" +
                        "  `enabled` INTEGER NOT NULL DEFAULT 1,\n" +
                        "  `mac` TEXT NOT NULL DEFAULT '%',\n" +
                        "  `name` TEXT NOT NULL DEFAULT '',\n" +
                        "  `inverse` INTEGER NOT NULL DEFAULT 0\n" +
                        ");");
                sql.add("UPDATE `last_v1` SET count = 0;");
                sql.add("DELETE FROM `last_tasks_v1`;");
            case 48:
                sql.add("UPDATE `settings_v1` SET value = 'XX' WHERE key = 'DISPLAY_LANGUAGE' AND value IN ('en','it','de','ru','id','iw');");
            case 49:
                sql.add("UPDATE `settings_v1` SET value = 'XX' WHERE key = 'DISPLAY_LANGUAGE' AND value IN ('en','de','id','it','iw','nb','ru');");
            case 50:
                sql.add("ALTER TABLE `when_time_v1` ADD COLUMN `ranges` TEXT NOT NULL DEFAULT '';\n");
                sql.add("ALTER TABLE `when_time_v1` ADD COLUMN `type` INTEGER NOT NULL DEFAULT " + TimeCondition.Type.WEEKLY.ordinal() + ";\n");

        }
        for (SqlTable sqlTable : mTables) {
            boolean upgrade = false;
            try {
                if (sqlTable.getVersion_created() > oldVersion) {
                    // new table is to be created
                    db.execSQL(sqlTable.toString());
                } else {
                    upgrade = true;
                }
            } catch (SQLiteException ignored) {
                upgrade = true;
            }
            if (upgrade) {
                // columns to be appended
                for (String line : sqlTable.toUpgradeString(oldVersion)) {
                    db.execSQL(line);
                }
            }
        }
        try {
            db.beginTransaction();
            for (String query : sql) {
                db.execSQL(query);
            }
            if (passed_33) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("key", "ANDROID_ID");
                contentValues.put("value", Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID));
                db.insert("settings" + V, null, contentValues);
            }
            db.delete("tasks" + V, "when_id NOT IN (SELECT when_id FROM when" + V + ")", emptyStringArray);
            db.delete("tasks" + V, "what_id NOT IN (SELECT what_id FROM what" + V + ")", emptyStringArray);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public void autoCleanup() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        db.beginTransaction();
        try {
            db.delete("tasks" + V, "when_id NOT IN (SELECT when_id FROM when" + V + ")", emptyStringArray);
            db.delete("tasks" + V, "what_id NOT IN (SELECT what_id FROM what" + V + ")", emptyStringArray);
            db.delete("when" + V, "when_id NOT IN (SELECT when_id FROM tasks" + V + ")", emptyStringArray);
            db.delete("what" + V, "what_id NOT IN (SELECT what_id FROM tasks" + V + ")", emptyStringArray);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public boolean upgradeNeeded() {
        if (upgradeChecked) return false;
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;

        final String curr_locale = Locale.getDefault().getLanguage();
        if (curr_locale == null) return false;

        Cursor c = null;
        try {
            upgradeChecked = true;
            c = db.query("what" + V, new String[]{"rowid"}, "description = '%%RESTORE%%'",
                    null, null, null, null);
            if (c != null && c.moveToFirst()) return true;
            if (c != null) c.close();
            c = db.query("when_time" + V, new String[]{"rowid"}, "ranges != ''", null, null, null, null, null);
            if (c != null && c.moveToFirst()) return true;
            final String prev_locale = getPrivateSetting("DISPLAY_LANGUAGE", curr_locale);
            setPrivateSetting("DISPLAY_LANGUAGE", curr_locale);
            return !prev_locale.equals(curr_locale);
        } finally {
            if (c != null) c.close();
        }
    }

    long lastAffectedRow(@NotNull SQLiteDatabase db) {
        return DatabaseUtils.longForQuery(db, "SELECT CHANGES()", null);
    }

    long lastRowID(@NotNull SQLiteDatabase db) {
        return DatabaseUtils.longForQuery(db, "SELECT last_insert_rowid()", null);
    }

    synchronized public long saveEntity(long primary, @NotNull String tableName, @NotNull ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        if (primary != 0) {
            db.update(tableName + V, values, "rowid = ?", new String[]{Long.toString(primary)});
            if (lastAffectedRow(db) == 0)
                primary = 0;
        }
        if (primary == 0) {
            try {
                db.insert(tableName + V, null, values);
                primary = lastRowID(db);
            } catch (SQLiteConstraintException e) {
                // this is for when_settings
                return 0;
            }
        }
        return primary;
    }

    @Nullable
    public ContentValues loadEntity(long primary, @NotNull String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        ContentValues ret = null;
        Cursor result = null;
        if (primary == 0) return null;
        try {
            result = db.rawQuery("SELECT * FROM " + tableName + V + " WHERE rowid = ? LIMIT 1",
                    new String[]{Long.toString(primary)});
            if (result.moveToNext()) {
                ret = new ContentValues();
                for (int i = 0; i < result.getColumnCount(); i++) {
                    switch (result.getType(i)) {
                        case Cursor.FIELD_TYPE_BLOB:
                            ret.put(result.getColumnName(i), result.getBlob(i));
                            break;
                        case Cursor.FIELD_TYPE_FLOAT:
                            ret.put(result.getColumnName(i), result.getFloat(i));
                            break;
                        case Cursor.FIELD_TYPE_INTEGER:
                            ret.put(result.getColumnName(i), result.getInt(i));
                            break;
                        case Cursor.FIELD_TYPE_NULL:
                            ret.put(result.getColumnName(i), 0);
                            break;
                        case Cursor.FIELD_TYPE_STRING:
                            ret.put(result.getColumnName(i), result.getString(i));
                            break;
                    }
                }
            }
            return ret;
        } finally {
            if (result != null) result.close();
        }
    }

    @NotNull
    public int[] getActionValues(long primary) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query(
                    "what" + V,
                    new String[]{
                            "what_id",
                            "start",
                            "restore",
                    },
                    "rowid = ?",
                    new String[]{Long.toString(primary)},
                    null,
                    null,
                    null
            );
            if (!result.moveToNext()) return new int[]{0, 0, 0};
            return new int[]{
                    result.getInt(0),
                    result.getInt(1),
                    result.getInt(2),
            };
        } finally {
            if (result != null) result.close();
        }
    }

    public int getIdByItem(long primary, @NotNull String entity) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query(entity + V, new String[]{entity + "_id"}, "rowid = ?",
                    new String[]{Long.toString(primary)}, null, null, null);
            if (!result.moveToNext()) return 0;
            return result.getInt(0);
        } finally {
            if (result != null) result.close();
        }
    }

    @Nullable
    public String getTypeById(long primary, @NotNull String entity) {
        return getFieldByPrimary(entity, primary, entity + "_type");
    }

    @Nullable
    public String getFieldByPrimary(@NotNull String table, long primary, @NotNull String field) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query(table + V, new String[]{field}, "rowid = ?",
                    new String[]{Long.toString(primary)}, null, null, null);
            if (!result.moveToNext()) return null;
            return result.getString(0);
        } finally {
            if (result != null) result.close();
        }
    }

    public int getTaskByEntity(long primary, @NotNull String entity) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query("tasks" + V, new String[]{"task_id"}, entity + "_id = ?",
                    new String[]{Long.toString(primary)}, null,
                    null, null);
            if (!result.moveToNext()) return 0;
            return result.getInt(0);
        } finally {
            if (result != null) result.close();
        }
    }

    public int getItemByEntity(long primary, @NotNull String entity) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query(entity + V, new String[]{entity + "_item"}, "rowid = ?",
                    new String[]{Long.toString(primary)}, null, null, null);
            if (!result.moveToNext()) return 0;
            return result.getInt(0);
        } finally {
            if (result != null) result.close();
        }
    }

    public long saveWhenWhat(long primary, int id, int task_id, @NotNull String tableName, @NotNull ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor res = null;
        try {
            if (id == 0 && task_id != 0) {
                res = db.query("tasks" + V, new String[]{tableName + "_id"}, "task_id = ?",
                        new String[]{Integer.toString(task_id)}, null, null, null);
                if (res.moveToNext()) {
                    id = res.getInt(0);
                    values.put(tableName + "_id", id);
                }
            }
        } finally {
            if (res != null) res.close();
        }
        if (id == 0 && primary != 0) {
            id = (int) primary;
            values.put(tableName + "_id", id);
        }
        primary = saveEntity(primary, tableName, values);
        if (id == 0 && primary != 0) {
            id = (int) primary;
            values.put(tableName + "_id", id);
            db.update(tableName + V, values, "rowid = ?", new String[]{Long.toString(primary)});
        }
        return primary;
    }

    public int startNewTask() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        int task_id;

        ContentValues values = new ContentValues();
        values.put("when_id", 0);
        values.put("what_id", 0);
        values.put("enabled", false);

        db.insert("tasks" + V, null, values);

        task_id = (int) this.lastRowID(db);

        return task_id;
    }

    public void enableTask(final int task_id, boolean enabled, boolean shutdown) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;

        ContentValues values = new ContentValues();

        if (!enabled && shutdown) {
            Cursor task_running = null;
            values.put("start_type", StartType.END.ordinal());
            try {
                final String query = "SELECT t.task_id FROM tasks" + V + " AS t "
                        + "WHERE t.task_id = ? AND t.start_type IN (?, ?)";
                task_running = db.rawQuery(query, new String[]{
                        "" + task_id,
                        "" + StartType.START_WITH_END.ordinal(),
                        "" + StartType.START_ONLY.ordinal()
                });
                if (task_running.moveToFirst()) {
                    scheduleActionsForLauncher(task_id, false);
                }
            } finally {
                if (task_running != null) task_running.close();
            }
        }
        values.put("enabled", enabled);
        db.update("tasks" + V, values, "task_id = ?", new String[]{Integer.toString(task_id)});
        if (enabled && shutdown) {
            scheduleMatchingTasks(StartType.START_WITH_END);
        }
        if (!enabled) {
            clearLastTasks(task_id);
        }
        final Uri baseUri = Uri.parse("content://" + BackgroundProvider.BACKGROUND_PROVIDER);
        final Uri runUri = Uri.withAppendedPath(baseUri, "/on_boot/run");
        if (runUri != null) {
            context.getContentResolver().query(runUri, null, null, null, null);
        }

        try {
            MiniTaskerApp miniTaskerApp = ((MiniTaskerApp) context.getApplicationContext());
            if (miniTaskerApp == null) return;
            MiniTaskerActivity activity = (MiniTaskerActivity) miniTaskerApp.getCurrentActivity();
            if (activity == null) return;
            if (activity.getClass() == MainActivity.class) {
                final MainActivity mainActivity = (MainActivity) activity;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mainActivity.refreshAdapter();
                    }
                });
            }
        } catch (ClassCastException ignore) {
        }
    }

    public boolean isEnabled(int task_id, int wh_id, String wh_type) {
        if (task_id == 0 || wh_id == 0) return true;
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor res = null;
        try {
            res = db.rawQuery("SELECT 1 FROM tasks" + V + " AS t, " + wh_type + V + " AS w "
                            + "WHERE t." + wh_type + "_id = w." + wh_type + "_id AND t.task_id = ? AND "
                            + "w." + wh_type + "_item = ? AND t.enabled = 1 AND w.enabled = 1",
                    new String[]{Integer.toString(task_id), Integer.toString(wh_id)}
            );
            return res.moveToNext();
        } finally {
            if (res != null) res.close();
        }
    }

    boolean rawQueryExecute(String query, String[] args) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.rawQuery(query, args);
            return result.moveToFirst();
        } finally {
            if (result != null) result.close();
        }
    }

    boolean hasConditionTypeEnabled(String type) {
        return rawQueryExecute("SELECT w.when_id FROM tasks" + V + " AS t, when" + V + " AS w, when_" + type + V
                        + " AS z WHERE t.when_id = w.when_id AND t.enabled = 1 AND w.enabled = 1 AND "
                        + "w.when_type = '" + type + "' AND w.when_item = z.when_id AND z.enabled = 1",
                emptyStringArray
        );
    }

    public void updateTimeConditionQuickLookup(long condition_id, Collection<Integer> times) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        final String table = "when_time_quick" + V;
        try {
            db.beginTransaction();
            db.delete(table, "condition_id = ?", new String[]{"" + condition_id});
            if (times != null) {
                ContentValues values = new ContentValues();
                values.put("condition_id", condition_id);
                for (Integer time : times) {
                    values.put("time", time);
                    db.insert(table, null, values);
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void scheduleNextTimeRangeWhen() {
        final SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Calendar calendar = Calendar.getInstance();
        scheduleNextTimeRangeWhen(db, calendar, context);
    }


    public static void scheduleNextTimeRangeWhen(@NotNull final SQLiteDatabase db,
                                                 @NotNull Calendar calendar,
                                                 @NotNull final Context context) {
        final long now = calendar.getTimeInMillis();
        //noinspection ResourceType
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1 - calendar.get(Calendar.DAY_OF_WEEK));
        final long startOfWeek = calendar.getTimeInMillis();
        final long minutesSinceStartOfWeek = TimeUnit.MILLISECONDS.toMinutes(now - startOfWeek);

        final String innerQuery = "SELECT condition_id, time FROM when_time_quick" + V
                + " WHERE time > ? UNION SELECT condition_id, time + 10080 AS time "
                + " FROM when_time_quick" + V + " WHERE time <= ?";
        final String query = String.format("SELECT condition_id, time FROM (%s) ORDER BY time ASC",
                innerQuery);
        final String[] queryArguments = new String[]{
                "" + minutesSinceStartOfWeek,
                "" + minutesSinceStartOfWeek,
        };

        Cursor result = null;
        try {
            result = db.rawQuery(query, queryArguments);
            if (result == null || !result.moveToNext()) return;
            int time = result.getInt(result.getColumnIndexOrThrow("time"));
            calendar.add(Calendar.MINUTE, time);
            List<Long> condition_ids = new ArrayList<>();
            condition_ids.add(result.getLong(result.getColumnIndexOrThrow("condition_id")));
            while (result.moveToNext()
                    && time == result.getInt(result.getColumnIndexOrThrow("time"))) {
                condition_ids.add(result.getLong(result.getColumnIndexOrThrow("condition_id")));
            }

            Intent alarmIntent = new Intent(context, PhoneReceiver.class);
            alarmIntent.putExtra("type", "time");
            alarmIntent.putExtra("condition_ids", TextUtils.join(",", condition_ids));

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            PendingIntent timePendingIntent = PendingIntent.getBroadcast(context, 8850, alarmIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), timePendingIntent);

            long seconds = TimeUnit.MILLISECONDS.toSeconds(calendar.getTimeInMillis() -
                    Calendar.getInstance().getTimeInMillis());

            Log.w(APPTAG, "next time condition is in " + seconds + " seconds");
        } finally {
            if (result != null) result.close();
        }
    }

    public void scheduleBuildCalendarQuery(@NotNull SQLiteDatabase db, @NotNull StringBuilder condition,
                                           @NotNull List<String> conditionArgs) {
        Cursor when = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) return;
        try {
            condition.append("(");
            when = db.rawQuery("SELECT w.when_id, title, available, desc, identifier, "
                    + "location FROM when_calendar" + V + " AS c," +
                    "when" + V + " AS w, tasks" + V + " AS t WHERE t.when_id = w.when_id AND " +
                    "w.when_type = 'calendar' AND w.when_item = c.rowid AND t.enabled = 1 AND " +
                    "w.enabled = 1 AND c.enabled = 1", emptyStringArray);
            while (when.moveToNext()) {
                if (condition.length() > 1)
                    condition.append(") OR (");
                condition.append("(");
                condition.append(Events.CALENDAR_ID);
                condition.append(" = ?)");
                final String calendar_id = when.getString(4);
                conditionArgs.add(calendar_id);
                final String titleWildcard = when.getString(1);
                final String descWildcard = when.getString(3);
                final String locWildcard = when.getString(5);
                final CalendarCondition.AvailabilityType availabilityType
                        = CalendarCondition.AvailabilityType.values()[when.getInt(2)];
                assert titleWildcard != null && descWildcard != null && locWildcard != null;
                if (!titleWildcard.equals("*")) {
                    final String titleCondition = titleWildcard.replace("%", "\\%").replace("*", "%");
                    condition.append(" AND (");
                    condition.append(Events.TITLE);
                    condition.append(" LIKE ?)");
                    conditionArgs.add(titleCondition);
                }
                if (!descWildcard.equals("*")) {
                    final String descCondition = descWildcard.replace("%", "\\%").replace("*", "%");
                    condition.append(" AND (");
                    condition.append(Events.DESCRIPTION);
                    condition.append(" LIKE ?)");
                    conditionArgs.add(descCondition);
                }
                if (!locWildcard.equals("*")) {
                    final String locCondition = locWildcard.replace("%", "\\%").replace("*", "%");
                    condition.append(" AND (");
                    condition.append(Events.EVENT_LOCATION);
                    condition.append(" LIKE ?)");
                    conditionArgs.add(locCondition);
                }
                if (availabilityType != CalendarCondition.AvailabilityType.ANY) {
                    condition.append(" AND (");
                    condition.append(Events.AVAILABILITY);
                    condition.append(" = ");
                    if (availabilityType == CalendarCondition.AvailabilityType.AVAILABLE)
                        condition.append(Events.AVAILABILITY_FREE);
                    else if (availabilityType == CalendarCondition.AvailabilityType.UNAVAILABLE)
                        condition.append(Events.AVAILABILITY_BUSY);
                    condition.append(")");
                }
            }
            condition.append(")");
        } finally {
            if (when != null) when.close();
        }
    }

    public void scheduleNextCalendarEvent() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) return;

        long now = Long.valueOf("0" + getPrivateSetting("LAST_CALENDAR_TIME", "")) + 1;
        if (now == 1) now = Calendar.getInstance().getTimeInMillis();


        Cursor events = null;
        Cursor instances = null;
        try {
            StringBuilder condition = new StringBuilder();
            List<String> conditionArgs = new ArrayList<>();
            StringBuilder instance_ids = new StringBuilder();

            scheduleBuildCalendarQuery(db, condition, conditionArgs);
            if (condition.length() == 2) return;

//            Log.d(APPTAG, "calendar query: "+ condition);

            events = context.getContentResolver().query(
                    Events.CONTENT_URI,
                    new String[]{Events._ID},
                    condition.toString(),
                    conditionArgs.toArray(new String[conditionArgs.size()]),
                    null
            );
            if (events == null || !events.moveToFirst()) return;
            instance_ids.append(events.getString(0));
            while (events.moveToNext()) {
                instance_ids.append(",").append(events.getString(0));
            }

            final String event_condition = Instances.ALL_DAY + " = 0 AND "
                    + Instances.SELF_ATTENDEE_STATUS
                    + "!=" + Attendees.ATTENDEE_STATUS_DECLINED
                    + " AND " + Instances.STATUS + "!="
                    + Instances.STATUS_CANCELED + " AND "
                    + Instances.EVENT_ID + " IN (" + instance_ids + ")";

            //noinspection ConstantConditions
            instances = context.getContentResolver().query(
                    Instances.CONTENT_URI.buildUpon()
                            .appendPath(Long.toString(now))
                            .appendPath(Long.toString(now + DEFAULT_LOOK_AHEAD_HOURS * HOUR_MILLIS))
                            .build(),
                    new String[]{
                            Instances.EVENT_ID,
                            Instances.BEGIN,
                            Instances.END,
                            Instances.CALENDAR_ID
                    },
                    event_condition,
                    null,
                    Instances.BEGIN
            );

            if (instances == null || !instances.moveToFirst()) return;

            StringBuilder event_ids = new StringBuilder();
            event_ids.append(instances.getString(0));
            long time_start = (instances.getLong(1) < now ? instances.getLong(2) : instances.getLong(1));
            long next_time = time_start;
            while (instances.moveToNext()) {
                time_start = (instances.getLong(1) < now ? instances.getLong(2) : instances.getLong(1));
                if (time_start > next_time) continue;
                event_ids.append(",").append(instances.getString(0));
            }

            Intent alarmIntent = new Intent(context, PhoneReceiver.class);
            alarmIntent.putExtra("type", "calendar");
            alarmIntent.putExtra("event_time", next_time);
            alarmIntent.putExtra("event_ids", event_ids.toString());

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            PendingIntent calPendingIntent = PendingIntent.getBroadcast(context, 7525, alarmIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, next_time, calPendingIntent);

            Log.w(APPTAG, "next calendar condition is in " + ((next_time - now) / 1000) + " seconds");

        } finally {
            if (events != null) events.close();
            if (instances != null) instances.close();
        }
    }

    public void matchCalendarConditions(@NotNull String event_ids, long now) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor events = null;
        Cursor when = null;
        Cursor instances = null;
        if (now == 0) now = Calendar.getInstance().getTimeInMillis();
        try {
            StringBuilder condition_append = new StringBuilder();
            condition_append.append(" AND " + Events._ID + " IN (")
                    .append(event_ids)
                    .append(")");

            StringBuilder condition = new StringBuilder();
            List<String> conditionArgs = new ArrayList<>();

            condition.append("(");
            when = db.rawQuery("SELECT w.when_id, title, available, desc, identifier, "
                    + "location FROM when_calendar" + V + " AS c," +
                    "when" + V + " AS w, tasks" + V + " AS t WHERE t.when_id = w.when_id AND " +
                    "w.when_type = 'calendar' AND w.when_item = c.rowid AND t.enabled = 1 AND " +
                    "w.enabled = 1 AND c.enabled = 1", emptyStringArray);
            while (when.moveToNext()) {
                if (events != null) events.close();
                String when_id = "w.when_id = " + when.getString(0);
                condition = new StringBuilder();
                conditionArgs.clear();
                condition.append("(");
                condition.append(Events.CALENDAR_ID);
                condition.append(" = ?)");
                final String calendar_id = when.getString(4);
                conditionArgs.add(calendar_id);
                final String titleWildcard = when.getString(1);
                final String descWildcard = when.getString(3);
                final String locWildcard = when.getString(5);
                final CalendarCondition.AvailabilityType availabilityType
                        = CalendarCondition.AvailabilityType.values()[when.getInt(2)];
                assert titleWildcard != null && descWildcard != null && locWildcard != null;
                if (!titleWildcard.equals("*")) {
                    final String titleCondition = titleWildcard.replace("%", "\\%").replace("*", "%");
                    condition.append(" AND (");
                    condition.append(Events.TITLE);
                    condition.append(" LIKE ?)");
                    conditionArgs.add(titleCondition);
                }
                if (!descWildcard.equals("*")) {
                    final String descCondition = descWildcard.replace("%", "\\%").replace("*", "%");
                    condition.append(" AND (");
                    condition.append(Events.DESCRIPTION);
                    condition.append(" LIKE ?)");
                    conditionArgs.add(descCondition);
                }
                if (!locWildcard.equals("*")) {
                    final String locCondition = locWildcard.replace("%", "\\%").replace("*", "%");
                    condition.append(" AND (");
                    condition.append(Events.EVENT_LOCATION);
                    condition.append(" LIKE ?)");
                    conditionArgs.add(locCondition);
                }
                if (availabilityType != CalendarCondition.AvailabilityType.ANY) {
                    condition.append(" AND (");
                    condition.append(Events.AVAILABILITY);
                    condition.append(" = ");
                    if (availabilityType == CalendarCondition.AvailabilityType.AVAILABLE)
                        condition.append(Events.AVAILABILITY_FREE);
                    else if (availabilityType == CalendarCondition.AvailabilityType.UNAVAILABLE)
                        condition.append(Events.AVAILABILITY_BUSY);
                    condition.append(")");
                }
                condition.append(condition_append.toString());
                events = context.getContentResolver().query(
                        Events.CONTENT_URI,
                        new String[]{Events._ID, Events.DTSTART, Events.DTEND},
                        condition.toString(),
                        conditionArgs.toArray(new String[conditionArgs.size()]),
                        null
                );
                if (events == null || !events.moveToFirst()) continue;

                boolean is_start = false;
                boolean is_end = false;
                do {
                    try {
                        final String event_id = events.getString(0);
                        //noinspection ConstantConditions
                        instances = context.getContentResolver().query(
                                Instances.CONTENT_URI.buildUpon()
                                        .appendPath(Long.toString(now))
                                        .appendPath(Long.toString(now + DEFAULT_LOOK_AHEAD_HOURS * HOUR_MILLIS))
                                        .build(),
                                new String[]{
                                        Instances.EVENT_ID,
                                        Instances.BEGIN,
                                        Instances.END,
                                        Instances.CALENDAR_ID
                                },
                                Instances.EVENT_ID + " = " + event_id + " AND (" + Instances.BEGIN
                                        + " = " + now + " OR " + Instances.END + " = " + now + ")",
                                null,
                                Instances.BEGIN
                        );
                        if (instances == null || !instances.moveToFirst()) continue;
                        if (now == (instances.getLong(1))) is_start = true;
                        if (now == (instances.getLong(2))) is_end = true;
                    } finally {
                        if (instances != null) {
                            instances.close();
                            instances = null;
                        }
                    }
                } while (events.moveToNext() && (!is_start || !is_end));
                if (is_end) {
                    runMatchingConditions("calendar", when_id, emptyStringArray, StartType.END);
                }
                if (is_start) {
                    runMatchingConditions("calendar", when_id, emptyStringArray, StartType.START_WITH_END);
                }
            }
        } finally {
            if (events != null) events.close();
            if (when != null) when.close();
            if (instances != null) instances.close();
        }
    }

    public void setTaskSelected(int task_id, boolean selected) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues values = new ContentValues();
        values.put("selected", selected);
        db.update("tasks" + V, values, "task_id = ?", new String[]{Integer.toString(task_id)});
    }

    public void setTaskExpanded(int task_id, boolean expanded) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues values = new ContentValues();
        values.put("expanded", expanded);
        db.update("tasks" + V, values, "task_id = ?", new String[]{Integer.toString(task_id)});
    }

    public boolean setTaskSelectAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues vals = new ContentValues();
        vals.put("selected", true);
        db.update("tasks" + V, vals, "undo = 0", null);
        return lastAffectedRow(db) > 0;
    }

    public void setTaskUnselectAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues vals = new ContentValues();
        vals.put("selected", false);
        db.update("tasks" + V, vals, null, null);
    }

    public int getTaskSelectionCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        SQLiteStatement s = db.compileStatement("SELECT COUNT(1) FROM tasks" + V + " WHERE selected = 1");
        assert s != null;
        return (int) s.simpleQueryForLong();
    }

    public Cursor getTasksCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        return db.query("tasks" + V, new String[]{"task_id", "enabled", "selected",
                        "description", "when_id", "what_id", "idx", "expanded"},
                "undo = 0", emptyStringArray, null, null,
                "idx ASC, task_id ASC"
        );
    }

    public Cursor getEntityCursor(int what_id, int when_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        return db.rawQuery(
                "SELECT 0 AS kind, 0 AS entity_id, 0 AS eid, 0 AS entity_type, "
                        + "0 AS enabled, '' AS description, 0 AS idx, 0 AS flag UNION "
                        + "SELECT 1, what_id, rowid, what_type, enabled, description, "
                        + "idx, flag FROM what" + V + " WHERE what_id = ? AND start = 1 AND undo = 0 UNION "
                        + "SELECT 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL UNION "
                        + "SELECT 3, what_id, rowid, what_type, enabled, description, "
                        + "idx, flag FROM what" + V + " WHERE what_id = ? AND start = 0 AND undo = 0 UNION "
                        + "SELECT 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL UNION "
                        + "SELECT 5, when_id, rowid, when_type, enabled, description, "
                        + "idx, flag FROM when" + V + " WHERE when_id = ? AND undo = 0 UNION "
                        + "SELECT 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL ORDER BY kind, idx",
                new String[]{"" + what_id, "" + what_id, "" + when_id}
        );
    }

    public void deleteSelectedTasks() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor c = db.rawQuery("UPDATE tasks" + V + " SET undo = enabled + 1, enabled = 0, selected = 0"
                + " WHERE selected = 1", emptyStringArray);
        if (c == null) return;
        c.moveToFirst();
        c.close();
        c = db.rawQuery("SELECT last_id FROM last_tasks" + V + " WHERE task_id IN "
                + "(SELECT task_id FROM tasks" + V + " WHERE undo > 0)", emptyStringArray);
        if (c == null || !c.moveToFirst()) return;
        StringBuilder last_ids = new StringBuilder();
        last_ids.append(c.getInt(0));
        while (c.moveToNext()) {
            last_ids.append(",").append(c.getInt(0));
        }
        c.close();
        c = db.rawQuery("UPDATE last" + V + " SET count = count - 1 WHERE count > 0 AND "
                + "last_id IN (" + last_ids + ")", emptyStringArray);
        c.moveToFirst();
        c.close();
        c = db.rawQuery("DELETE FROM last_tasks" + V + " WHERE last_id IN (" + last_ids + ")",
                emptyStringArray);
        c.moveToFirst();
        c.close();
    }

    public void deleteTask(int task_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor c = db.rawQuery("UPDATE tasks" + V + " SET undo = enabled + 1, enabled = 0, selected = 0"
                + " WHERE task_id = ?", new String[]{"" + task_id});
        c.moveToFirst();
        c.close();
        clearLastTasks(task_id);
    }

    public void deleteEntity(String entity, final long primary, final int task_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor c = null;

//        if (entity == "what") {
        //TODO: run "end" task now
//        }

        try {
            db.beginTransaction();

            c = db.rawQuery("UPDATE " + entity + V + " SET undo = enabled, enabled = 0 WHERE rowid = ?",
                    new String[]{"" + primary});
            c.moveToFirst();
            c.close();
            if (task_id > 0) {
                regenerateTaskDescription(task_id);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            if (c != null) c.close();
        }
    }

    public void undoCommit() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor c = null;
        try {
            db.beginTransaction();
            c = db.rawQuery("DELETE FROM when" + V + " WHERE when_id IN (SELECT when_id FROM tasks" + V + " WHERE undo > 0) OR undo > 0", emptyStringArray);
            c.moveToFirst();
            c.close();
            c = db.rawQuery("DELETE FROM what" + V + " WHERE what_id IN (SELECT what_id FROM tasks" + V + " WHERE undo > 0) OR undo > 0", emptyStringArray);
            c.moveToFirst();
            c.close();
            c = db.rawQuery("DELETE FROM tasks" + V + " WHERE undo > 0", emptyStringArray);
            c.moveToFirst();
            db.setTransactionSuccessful();
        } finally {
            if (c != null) c.close();
            db.endTransaction();
        }
    }

    public void undoRollback() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor c = null;
        try {
            db.beginTransaction();
            c = db.query("tasks" + V, new String[]{"task_id", "when_id", "what_id", "undo"}, "undo > 0",
                    emptyStringArray, null, null, null);
            while (c.moveToNext()) {
                int task_id = c.getInt(0);
                boolean enabled = c.getInt(3) == 2;
                if (enabled) this.enableTask(task_id, true, true);
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("undo", 0);
            db.update("tasks" + V, contentValues, null, null);
            c.close();
            c = db.rawQuery("SELECT task_id FROM tasks" + V + " AS t, when" + V + " AS w WHERE t.when_id = "
                    + "w.when_id AND w.undo > 0 UNION SELECT task_id FROM tasks" + V + " AS t, what" + V
                    + " AS w WHERE t.what_id = w.what_id AND w.undo > 0", emptyStringArray);
            List<Integer> ids = new ArrayList<>();
            while (c.moveToNext()) {
                ids.add(c.getInt(0));
            }
            c.close();

            c = db.rawQuery("UPDATE when" + V + " SET enabled = undo, undo = 0 WHERE undo > 0", emptyStringArray);
            c.moveToFirst();
            c.close();
            c = db.rawQuery("UPDATE what" + V + " SET enabled = undo, undo = 0 WHERE undo > 0", emptyStringArray);
            c.moveToFirst();
            c.close();
            c = null;

            for (Integer id : ids) {
                regenerateTaskDescription(id);
            }
            db.setTransactionSuccessful();
        } finally {
            if (c != null) c.close();
            db.endTransaction();
        }
    }

    public boolean isUndoPossible() {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor c = db.rawQuery("SELECT rowid FROM tasks" + V + " WHERE undo > 0 UNION "
                + "SELECT when_id FROM when" + V + " WHERE undo > 0 UNION "
                + "SELECT what_id FROM what" + V + " WHERE undo > 0", emptyStringArray);
        return c.moveToFirst() && c.getCount() > 0;
    }

    public void setRunningByItems(String running, @NotNull String[] ids) {
//        Log.d(APPTAG, "UPDATE RUNNING: "+TextUtils.join(",",ids)+" to " +running);
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues vals = new ContentValues();
        vals.put("running", running);
        db.update("when" + V, vals, "when_item IN (" + TextUtils.join(",", ids) + ")", emptyStringArray);
    }

    public void disableAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        db.rawQuery("UPDATE tasks" + V + " SET enabled = 0", emptyStringArray).moveToFirst();
    }

    public void resetRunningConditions() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        db.rawQuery("UPDATE when" + V + " SET running = 0, start_type = " + StartType.UNKNOWN.ordinal(),
                emptyStringArray).moveToFirst();
        db.rawQuery("UPDATE tasks" + V + " SET start_type = " + StartType.UNKNOWN.ordinal(),
                emptyStringArray).moveToFirst();
        db.rawQuery("UPDATE what" + V + " SET start_type = " + StartType.UNKNOWN.ordinal(),
                emptyStringArray).moveToFirst();
        db.delete("stats" + V, "", emptyStringArray);
    }

    public void scheduleActionsForLauncher(int task_id, boolean isStart) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            if (!swapAndCompareTaskStartType(task_id, isStart)) return;
            result = db.rawQuery("SELECT w.rowid FROM tasks" + V + " AS t, what" + V + " AS w "
                            + "WHERE t.what_id = w.what_id AND t.task_id = ? AND t.enabled = 1 "
                            + "AND w.enabled = 1",
                    new String[]{"" + task_id}
            );
            while (result.moveToNext()) {
                Intent runAction = new Intent(context, LauncherIntentService.class);
                runAction.putExtra("task_id", task_id);
                runAction.putExtra("primary", result.getLong(0));
                runAction.putExtra("is_start", isStart ? 1 : 0);
                synchronized (this) {
                    runActionCounter++;
                }
                context.startService(runAction);
            }
        } finally {
            if (result != null) result.close();
        }
    }

    private boolean swapAndCompareTaskStartType(int task_id, boolean isStart) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        int startType = isStart ? StartType.START_WITH_END.ordinal() : StartType.END.ordinal();
        contentValues.put("start_type", startType);
        db.update("tasks" + V, contentValues, "start_type != ? AND task_id = ?",
                new String[]{"" + startType, "" + task_id});
        return lastAffectedRow(db) > 0;
    }

    public void runMatchingConditions(final String type, final String when_query,
                                      final String[] when_parameters, @NotNull StartType startType) {
        if (startType == StartType.START_ONLY) {
            runMatchingConditions(type, when_query, when_parameters, StartType.END);
            runMatchingConditions(type, when_query, when_parameters, StartType.START_WITH_END);
            // good job everyone, drinks all around.
            return;
        }

        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;

        // yey. here we are again, rewriting this crap.
        // no hope for the human race

        final boolean isStart = startType != StartType.END;
        final String queryStartType = isStart ? QUERY_START_TYPE_START : QUERY_START_TYPE_END;

        Cursor result = null;
        try {
            final String rawQueryString = "SELECT w.rowid, t.task_id FROM tasks" + V + " AS t, when"
                    + V + " AS w, when_" + type + V + " AS z WHERE t.enabled = 1 AND w.enabled = 1 "
                    + "AND z.enabled = 1 AND t.when_id = w.when_id AND w.when_item = z.when_id AND "
                    + "w.when_type = '" + type + "' AND w.start_type NOT " + queryStartType + " AND "
                    + "(" + when_query + ")";

            result = db.rawQuery(rawQueryString, when_parameters);
            if (result == null) return; // no conditions matched, get out
            while (result.moveToNext()) {
                final long primary = result.getLong(0);
                final int task_id = result.getInt(1);
                synchronized (this) {
                    matchConditionCounter++;
                }
                // notify condition intent service
                Intent matchCondition = new Intent(context, ConditionIntentService.class);
                matchCondition.setAction(ConditionIntentService.ACTION_CONDITION_MATCH);
                matchCondition.putExtra(ConditionIntentService.EXTRA_TYPE, type);
                matchCondition.putExtra(ConditionIntentService.EXTRA_TASK_ID, task_id);
                matchCondition.putExtra(ConditionIntentService.EXTRA_WHEN_ROWID, primary);
                matchCondition.putExtra(ConditionIntentService.EXTRA_IS_START, isStart ? 1 : 0);
                context.startService(matchCondition);
            }

        } finally {
            if (result != null) result.close();
        }
    }

    public void scheduleMatchingTasks(@NotNull StartType startType) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;

        final boolean isStart = startType != StartType.END;

        StringBuilder rawQueryString = new StringBuilder();
        rawQueryString.append("SELECT DISTINCT t.task_id, t.description FROM tasks" + V + " AS t, "
                + "when" + V + " AS w WHERE t.enabled = 1 AND w.enabled = 1 AND t.when_id = w.when_id "
                + "AND t.start_type != ")
                .append(startType.ordinal());

        if (isStart) {
            rawQueryString.append(" AND t.task_id NOT IN (SELECT t.task_id FROM tasks" + V +
                    " AS t, when" + V + " AS w WHERE t.when_id = w.when_id AND t.enabled = 1 "
                    + "AND w.enabled = 1 AND w.start_type NOT ")
                    .append(QUERY_START_TYPE_START)
                    .append(")");
        } else {
            rawQueryString.append(" AND w.start_type NOT ")
                    .append(QUERY_START_TYPE_START);
        }

        Cursor result = null;
        try {
            List<Integer> task_ids = new ArrayList<>();

            result = db.rawQuery(rawQueryString.toString(), emptyStringArray);

            ContentValues contentValues = new ContentValues();
            contentValues.put("enabled", 0);
            while (result.moveToNext()) {
                final int task_id = result.getInt(0);
                final String description = result.getString(1);
                if (task_ids.add(task_id)) {
                    scheduleActionsForLauncher(task_id, isStart);
                    Log.w(APPTAG, "Run task " + task_id + " " + startType.toString());

                    final Calendar calendar = Calendar.getInstance();
                    final String date = DateFormat.getDateFormat(context).format(calendar.getTime());
                    final String time = DateFormat.getTimeFormat(context).format(calendar.getTime());

                    putToLog(context.getString(startType != StartType.END
                            ? R.string.log_line_conditions_met
                            : R.string.log_line_conditions_no_longer_met, date, time, description));

                    if (!isStart && hasTimeOnceCondition(task_id)) {
                        db.update("tasks" + V, contentValues, "task_id = ?", new String[]{"" + task_id});
                    }
                }
            }
        } finally {
            if (result != null) result.close();
        }
    }

    boolean hasTimeOnceCondition(int task_id) {
        return DatabaseUtils.longForQuery(getReadableDatabase(),
                "SELECT COUNT(1) FROM tasks" + V + " AS t, when" + V + " AS w, when_time" + V + " AS z "
                        + "WHERE t.enabled = 1 AND t.when_id = w.when_id AND w.enabled = 1 "
                        + "AND w.when_type = 'time' AND w.when_item = z.when_id AND z.enabled = 1 "
                        + "AND z.repeat = 0 AND t.task_id = ?", new String[]{"" + task_id}) > 0;
    }

    public void resetGooglePlayServiceRunning() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        db.rawQuery("UPDATE when" + V + " SET running = 0", emptyStringArray).moveToFirst();
        ContentValues values = new ContentValues();
        values.put("start_type", StartType.UNKNOWN.ordinal());
        db.update("what" + V, values, "what_type = 'location'", emptyStringArray);
    }

    @NotNull
    public List<Geofence> getGeofencesToAdd() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ArrayList<Geofence> ret = new ArrayList<>();
        Cursor result = null;
        try {
            result = db.rawQuery("SELECT DISTINCT l.rowid, l.lat, l.lng, l.rad "
                    + "FROM tasks" + V + " AS t, when" + V + " AS w, when_location" + V + " AS l "
                    + "WHERE t.enabled = 1 AND t.when_id = w.when_id AND w.enabled = 1 "
                    + "AND w.when_item = l.when_id AND w.when_type = 'location' AND l.enabled = 1 AND "
                    + "w.running = 0", emptyStringArray);
            List<String> toUpdate = new ArrayList<>();
            while (result.moveToNext()) {
                long primary = result.getLong(0);
                double lat = result.getDouble(1);
                double lng = result.getDouble(2);
                double rad = result.getDouble(3);
                Geofence geofence = new SimpleGeofence(Long.toString(primary), lat, lng, (float) rad,
                        Geofence.GEOFENCE_TRANSITION_ENTER
                                | Geofence.GEOFENCE_TRANSITION_EXIT
                ).toGeofence();
                ret.add(geofence);
                toUpdate.add(Long.toString(primary));
            }
            if (!toUpdate.isEmpty()) {
                String[] toUpdateArr = toUpdate.toArray(new String[toUpdate.size()]);
                setRunningByItems("3", toUpdateArr);
            }
            return ret;
        } finally {
            if (result != null) result.close();
        }
    }

    @NotNull
    public List<String> getGeofencesToRemove() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        List<String> ret = new ArrayList<>();
        Cursor result = null;
        try {
            result = db.rawQuery("SELECT DISTINCT l.rowid "
                    + "FROM tasks" + V + " AS t, when" + V + " AS w, when_location" + V + " AS l "
                    + "WHERE t.when_id = w.when_id AND w.when_item = l.when_id AND "
                    + "w.when_type = 'location' AND (t.enabled = 0 OR w.enabled = 0 OR l.enabled = 0) "
                    + "AND w.running IN (1,2,4)", emptyStringArray);
            List<String> toUpdate = new ArrayList<>();
            while (result.moveToNext()) {
                long primary = result.getLong(0);
                ret.add(Long.toString(primary));
                toUpdate.add(Long.toString(primary));
            }
            if (!toUpdate.isEmpty()) {
                String[] toUpdateArr = toUpdate.toArray(new String[toUpdate.size()]);
                setRunningByItems("5", toUpdateArr);
            }
            return ret;
        } finally {
            if (result != null) result.close();
        }
    }

    public void regenerateTaskDescription(int task_id) {
        String description = generateTaskDescription(task_id, 0, 0);
        if (description.isEmpty()) return;
        ContentValues cv = new ContentValues();
        cv.put("description", this.generateTaskDescription(task_id, 0, 0));
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        db.update("tasks" + V, cv, "task_id = ?", new String[]{"" + task_id});
    }

    @NotNull
    public String generateTaskDescription(int task_id, int when_id_i, int what_id_i) {
        SQLiteDatabase db = this.getReadableDatabase();
        assert db != null;
        Cursor cursor = null;
        String task_desc;

        try {
            cursor = db.query("tasks" + V, new String[]{"when_id", "what_id", "label"}, "task_id = ?",
                    new String[]{Integer.toString(task_id)}, null, null, null);
            if (!cursor.moveToFirst()) return "";
            String when_id = (when_id_i != 0 ? Integer.toString(when_id_i) : cursor.getString(0));
            String what_id = (what_id_i != 0 ? Integer.toString(what_id_i) : cursor.getString(1));
            String label = cursor.getString(2);
            if (label != null && !label.isEmpty()) {
                label = context.getString(R.string.task_desc_label_formatter, label);
            } else {
                label = "";
            }
            cursor.close();

            cursor = db.query("what" + V, new String[]{"taskdesc"}, "what_id = ? AND start = 1 AND undo = 0",
                    new String[]{what_id}, null, null, "idx");

            String start_action = "";
            while (cursor.moveToNext()) {
                if (cursor.isFirst()) {
                    start_action = cursor.getString(0);
                } else if (!cursor.isLast()) {
                    start_action = context.getString(R.string.task_desc_action_joiner, start_action, cursor.getString(0));
                } else {
                    start_action = context.getString(R.string.task_desc_action_last, start_action, cursor.getString(0));
                }
            }
            cursor.close();


            cursor = db.query("what" + V, new String[]{"taskdesc"}, "what_id = ? AND start = 0 AND undo = 0",
                    new String[]{what_id}, null, null, "idx");
            String end_action = "";
            while (cursor.moveToNext()) {
                if (cursor.isFirst()) {
                    end_action = cursor.getString(0);
                } else if (!cursor.isLast()) {
                    end_action = context.getString(R.string.task_desc_action_joiner, end_action, cursor.getString(0));
                } else {
                    end_action = context.getString(R.string.task_desc_action_last, end_action, cursor.getString(0));
                }
            }
            cursor.close();

            cursor = db.query("when" + V, new String[]{"taskdesc"}, "when_id = ? AND undo = 0",
                    new String[]{when_id}, null, null, "idx");

            String condition = "";
            while (cursor.moveToNext()) {
                if (cursor.isFirst()) {
                    condition = cursor.getString(0);
                } else if (!cursor.isLast()) {
                    condition = context.getString(R.string.task_desc_condition_joiner, condition, cursor.getString(0));
                } else {
                    condition = context.getString(R.string.task_desc_condition_last, condition, cursor.getString(0));
                }
            }

            if (end_action == null || end_action.isEmpty()) {
                task_desc = context.getString(R.string.task_desc_no_end, label, start_action, condition);
            } else {
                task_desc = context.getString(R.string.task_desc_with_end, label, start_action, condition, end_action);
            }

            task_desc = Character.toUpperCase(task_desc.charAt(0)) + task_desc.substring(1);

            return task_desc;

        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public void putLast(@NotNull LastType type, long value, int task_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor cursor = null;
        try {
            cursor = db.query("last_tasks" + V, new String[]{"1"},
                    "last_id = ? AND task_id = ?", new String[]{
                            Integer.toString(type.ordinal()),
                            Integer.toString(task_id)
                    }, null, null, null
            );
            if (cursor != null && cursor.moveToFirst()) {
                // task already added a restore value for this last_id
                return;
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            ContentValues values = new ContentValues();
            values.put("last_id", Integer.toString(type.ordinal()));
            values.put("task_id", Integer.toString(task_id));
            db.insertOrThrow("last_tasks" + V, null, values);
            values.clear();

            cursor = db.query("last" + V,
                    new String[]{"count", "value"}, "last_id = ?",
                    new String[]{Integer.toString(type.ordinal())}, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int count = cursor.getInt(0);
                if (count >= 1) {
                    values.put("count", count + 1);
                } else {
                    values.put("count", 1);
                    values.put("value", value);
                }
                db.update("last" + V, values, "last_id = ?", new String[]{"" + type.ordinal()});
            } else {
                values.put("last_id", type.ordinal());
                values.put("count", 1);
                values.put("value", value);
                db.insertOrThrow("last" + V, null, values);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public long getLast(@NotNull LastType type, int task_id) throws MiniTaskerException {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor res = null;
        try {
            db.delete("last_tasks" + V, "last_id = ? AND task_id = ?", new String[]{
                    Integer.toString(type.ordinal()),
                    Integer.toString(task_id)
            });
            if (0 == lastAffectedRow(db)) {
                MiniTaskerException e = new MiniTaskerException();
                e.reason = MiniTaskerException.Reason.NO_CORRESPONDING_PUT;
                throw e;
            }

            res = db.query("last" + V, new String[]{"value", "count"}, "last_id = ?",
                    new String[]{Integer.toString(type.ordinal())}, null, null, "count DESC");
            if (!res.moveToNext()) {
                MiniTaskerException e = new MiniTaskerException();
                e.reason = MiniTaskerException.Reason.SQL_NO_RESULT;
                throw e;
            }
            final long value = res.getLong(0);
            final int count = countLast(db, type, task_id);
            Log.d(APPTAG, "LastStack count: " + res.getLong(1) + "; smart count: " + count);
            ContentValues updateValues = new ContentValues();
            updateValues.put("count", count);
            db.update("last" + V, updateValues, "last_id = ?", new String[]{"" + type.ordinal()});
            if (count > 0) {
                MiniTaskerException e = new MiniTaskerException();
                e.reason = MiniTaskerException.Reason.NOT_LAST_LAST_ITEM;
                throw e;
            }
            return value;
        } finally {
            if (res != null) res.close();
        }
    }

//    public int countActiveLast(@NotNull SQLiteDatabase db) {
//        Cursor cursor = null;
//        try {
//            cursor = db.rawQuery("SELECT COUNT(*) FROM what" + V + " WHERE restore = 1 AND start_type = ?", new String[]{"" + StartType.START_WITH_END});
//        } finally {
//            if (cursor != null) cursor.close();
//        }
//        return 0;
//    }

    public void clearLastTasks(int task_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT w.what_id FROM tasks" + V + " AS t, what" + V + " AS w WHERE "
                    + "t.what_id = w.what_id AND t.enabled = 1 AND w.enabled = 1 AND w.start_type = "
                    + StartType.START_WITH_END.ordinal(), null);
            if (cursor == null || cursor.getCount() > 0) return;
            db.rawQuery("UPDATE last" + V + " SET count = count - 1 WHERE count > 0 AND last_id IN "
                            + "(SELECT last_id FROM last_tasks" + V + " WHERE task_id = ?) ",
                    new String[]{Integer.toString(task_id)}
            ).moveToFirst();
            db.delete("last_tasks" + V, "task_id = ?", new String[]{
                    Integer.toString(task_id)
            });
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public void clearLast() {
        SQLiteDatabase db = this.getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("count", 0);
        db.update("last" + V, contentValues, null, null);
    }

    public int countLast(@NotNull SQLiteDatabase db, @NotNull LastType lastType, int task_id) {
        Cursor cursor = null;
        int count = 0;
        try {
            StringBuilder query = new StringBuilder();
            query.append("SELECT COUNT(*) FROM tasks" + V + " AS t, what" + V + " AS w");
            switch (lastType) {
                case VOLUME_ALARM_VOLUME:
                case VOLUME_MEDIA_VOLUME:
                case VOLUME_NOTIFICATION:
                case VOLUME_RING_MODE:
                case VOLUME_RING_VOLUME:
                    query.append(", what_volumecontrol" + V);
                    query.append(" AS z WHERE w.what_item = z.what_id AND w.what_type = 'volumecontrol' AND (z.ignore & ");
                    switch (lastType) {
                        case VOLUME_MEDIA_VOLUME:
                            query.append("1");
                            break;
                        case VOLUME_ALARM_VOLUME:
                            query.append("2");
                            break;
                        case VOLUME_NOTIFICATION:
                            query.append("4");
                            break;
                        case VOLUME_RING_MODE:
                        case VOLUME_RING_VOLUME:
                            query.append("8");
                            break;
                    }
                    query.append(") == 0");
                    if (lastType == LastType.VOLUME_RING_MODE) {
                        cursor = db.rawQuery("SELECT COUNT(*) FROM tasks" + V + " AS t, what" + V
                                + " AS w, what_phone" + V
                                + " AS z WHERE w.what_item = z.what_id AND w.what_type = 'phone'"
                                + " AND z.target = ''  AND t.what_id = w.what_id AND w.restore = 1"
                                + " AND t.task_id != ? AND t.start_type = "
                                + StartType.START_WITH_END.ordinal(), new String[]{"" + task_id});
                        if (cursor != null && cursor.moveToFirst()) {
                            count += cursor.getInt(0);
                            cursor.close();
                            cursor = null;
                        }
                    }
                    break;
                case BRIGHTNESS:
                    query.append(" WHERE w.what_type = 'brightness'");
                    break;
                default:
                    query.append(", what_states" + V + " AS z WHERE w.what_item = z.what_id AND z.device = '");
                    switch (lastType) {
                        case AIRPLANE:
                            query.append("airplane");
                            break;
                        case BLUETOOTH:
                            query.append("bluetooth");
                            break;
                        case GPS:
                            query.append("gps");
                            break;
                        case MOBILE:
                            query.append("mobile");
                            break;
                        case NFC:
                            query.append("nfc");
                            break;
                        case ORIENTATION:
                            query.append("orientation");
                            break;
                        case SCANNING:
                            query.append("scanning");
                            break;
                        case SYNC:
                            query.append("sync");
                            break;
                        case WIFI_AP:
                            query.append("wifiap");
                            break;
                        case WIFI:
                            query.append("wifi");
                            break;
                        case USB_AP:
                            query.append("usbap");
                            break;
                    }
                    query.append("'");
                    break;
            }
            query.append(" AND t.what_id = w.what_id AND w.restore = 1 AND t.task_id != ? AND t.start_type = ");
            query.append(StartType.START_WITH_END.ordinal());
            cursor = db.rawQuery(query.toString(), new String[]{"" + task_id});
            if (!cursor.moveToFirst()) return 0;
            return count + cursor.getInt(0);
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public long getPhoneEntityByTask(int task_id, final String entity) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor r = null;
        try {
            r = db.rawQuery("SELECT w.rowid FROM tasks" + V + " AS t, " + entity + V + " AS w " +
                    "WHERE t.task_id = ? AND t." + entity + "_id = w." + entity + "_id AND w." + entity
                    + "_type = 'phone'", new String[]{Integer.toString(task_id)});
            if (r.moveToFirst())
                return r.getLong(0);
            return 0;
        } finally {
            if (r != null) r.close();
        }
    }

    public long getSettingsPrimary() {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query("when" + V, new String[]{"rowid"}, "when_type = 'settings'", emptyStringArray, null, null, null);
            if (result == null || !result.moveToFirst()) return 0;
            return result.getLong(0);
        } finally {
            if (result != null) result.close();
        }
    }

    public boolean importTask(@NotNull JSONArray actions, @NotNull JSONArray conditions, String description) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        try {
            db.beginTransaction();
            int what_id = 0;
            int when_id = 0;
            for (int i = 0; i < actions.length(); i++) {
                final JSONObject element = actions.getJSONObject(i);
                ContentValues contentValues = new ContentValues();
                final String type = element.getString("type");
                Iterator keys = element.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (!key.contains(JSON_TAG_ACTION)) continue;
                    contentValues.put(key.substring(JSON_TAG_ACTION.length()),
                            element.getString(key));
                }
                contentValues.put("enabled", 1);
                db.insert("what_" + type + V, null, contentValues);
                int what_item = (int) lastRowID(db);
                contentValues.clear();

                contentValues.put("what_id", what_id);
                contentValues.put("what_item", what_item);
                contentValues.put("what_type", type);
                contentValues.put("enabled", element.getInt("enabled"));
                contentValues.put("description", element.getString("description"));
                contentValues.put("taskdesc", element.getString("taskdesc"));
                contentValues.put("start", element.getInt("start"));
                contentValues.put("restore", element.getInt("restore"));

                db.insert("what" + V, null, contentValues);
                if (what_id == 0) {
                    what_id = (int) lastRowID(db);
                    contentValues.clear();
                    contentValues.put("what_id", what_id);
                    db.update("what" + V, contentValues, "rowid = ?", new String[]{"" + what_id});
                }
            }
            for (int i = 0; i < conditions.length(); i++) {
                final JSONObject element = conditions.getJSONObject(i);
                ContentValues contentValues = new ContentValues();
                final String type = element.getString("type");
                Iterator keys = element.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (!key.contains(JSON_TAG_CONDITION)) continue;
                    contentValues.put(key.substring(JSON_TAG_CONDITION.length()),
                            element.getString(key));
                }
                contentValues.put("enabled", 1);
                db.insert("when_" + type + V, null, contentValues);
                int when_item = (int) lastRowID(db);
                contentValues.clear();

                contentValues.put("when_id", when_id);
                contentValues.put("when_item", when_item);
                contentValues.put("when_type", type);
                contentValues.put("enabled", element.getInt("enabled"));
                contentValues.put("description", element.getString("description"));
                contentValues.put("taskdesc", element.getString("taskdesc"));
                db.insert("when" + V, null, contentValues);
                if (when_id == 0) {
                    when_id = (int) lastRowID(db);
                    contentValues.clear();
                    contentValues.put("when_id", when_id);
                    db.update("when" + V, contentValues, "rowid = ?", new String[]{"" + when_id});
                }
            }

            ContentValues contentValues = new ContentValues();

            contentValues.put("enabled", 0);
            contentValues.put("selected", 1);
            contentValues.put("description", description);
            contentValues.put("what_id", what_id);
            contentValues.put("when_id", when_id);
            contentValues.put("start_type", 0);
            db.insert("tasks" + V, null, contentValues);

            db.setTransactionSuccessful();
            return true;
        } catch (JSONException e) {
            return false;
        } finally {
            db.endTransaction();
        }
    }

    @Nullable
    public JSONArray exportTask(String task_ids) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        JSONArray result = new JSONArray();
        final String[] projection_tasks = new String[]{"when_id", "what_id", "description"};
        final String[] projection_actions = new String[]{"what_item", "what_type", "enabled",
                "description", "taskdesc", "start", "restore"};
        final String[] projection_conditions = new String[]{"when_item", "when_type", "enabled",
                "description", "taskdesc"};
        final String[] projection_wildcard = new String[]{"*"};
        Cursor selected_tasks = null;
        Cursor task_actions = null;
        Cursor task_action = null;
        Cursor task_conditions = null;
        Cursor task_condition = null;
        try {
            //noinspection ConstantConditions
            final int my_version = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionCode;
            String android_id = getPrivateSetting("ANDROID_ID", "");

            selected_tasks = db.query(
                    "tasks" + V,
                    projection_tasks,
                    "task_id IN (" + task_ids + ")",
                    emptyStringArray,
                    null,
                    null,
                    null);
            while (selected_tasks.moveToNext()) {
                final int when_id = selected_tasks.getInt(0);
                final int what_id = selected_tasks.getInt(1);
                final String description = selected_tasks.getString(2);
                JSONObject task = new JSONObject();
                task.put("version", my_version);
                task.put("description", description);
                task.put("author", android_id);
                if (task_actions != null) task_actions.close();
                task_actions = db.query(
                        "what" + V,
                        projection_actions,
                        "what_id = ?",
                        new String[]{"" + what_id},
                        null,
                        null,
                        null
                );
                JSONArray actions = new JSONArray();
                while (task_actions.moveToNext()) {
                    final int what_item = task_actions.getInt(0);
                    final String what_type = task_actions.getString(1);
                    final int enabled = task_actions.getInt(2);
                    final String action_description = task_actions.getString(3);
                    final String taskdesc = task_actions.getString(4);
                    final int start = task_actions.getInt(5);
                    final int restore = task_actions.getInt(6);
                    if (task_action != null) task_action.close();
                    task_action = db.query(
                            "what_" + what_type + V,
                            projection_wildcard,
                            "what_id = ?",
                            new String[]{"" + what_item},
                            null,
                            null,
                            null
                    );
                    JSONObject action = new JSONObject();
                    action.put("type", what_type);
                    action.put("enabled", enabled);
                    action.put("description", action_description);
                    action.put("taskdesc", taskdesc);
                    action.put("start", start);
                    action.put("restore", restore);
                    if (task_action.moveToFirst()) {
                        for (int j = 2; j < task_action.getColumnCount(); j++) {
                            final String name = JSON_TAG_ACTION + task_action.getColumnName(j);
                            switch (task_action.getType(j)) {
                                case Cursor.FIELD_TYPE_NULL:
                                    action.put(name, null);
                                    break;
                                case Cursor.FIELD_TYPE_BLOB:
                                    action.put(name, task_action.getBlob(j));
                                    break;
                                case Cursor.FIELD_TYPE_FLOAT:
                                    action.put(name, task_action.getFloat(j));
                                    break;
                                case Cursor.FIELD_TYPE_INTEGER:
                                    action.put(name, task_action.getInt(j));
                                    break;
                                case Cursor.FIELD_TYPE_STRING:
                                    action.put(name, task_action.getString(j));
                                    break;
                            }
                        }
                    }
                    actions.put(action);
                }
                task.put("actions", actions);
                if (task_conditions != null) task_conditions.close();
                task_conditions = db.query(
                        "when" + V,
                        projection_conditions,
                        "when_id = ?",
                        new String[]{"" + when_id},
                        null,
                        null,
                        null
                );
                JSONArray conditions = new JSONArray();
                while (task_conditions.moveToNext()) {
                    final int when_item = task_conditions.getInt(0);
                    final String when_type = task_conditions.getString(1);
                    final int enabled = task_conditions.getInt(2);
                    final String condition_description = task_conditions.getString(3);
                    final String taskdesc = task_conditions.getString(4);
                    if (task_condition != null) task_condition.close();
                    task_condition = db.query(
                            "when_" + when_type + V,
                            projection_wildcard,
                            "when_id = ?",
                            new String[]{"" + when_item},
                            null,
                            null,
                            null
                    );
                    JSONObject condition = new JSONObject();
                    condition.put("type", when_type);
                    condition.put("enabled", enabled);
                    condition.put("description", condition_description);
                    condition.put("taskdesc", taskdesc);
                    if (task_condition.moveToFirst()) {
                        for (int j = 2; j < task_condition.getColumnCount(); j++) {
                            final String name = JSON_TAG_CONDITION + task_condition.getColumnName(j);
                            switch (task_condition.getType(j)) {
                                case Cursor.FIELD_TYPE_NULL:
                                    condition.put(name, null);
                                    break;
                                case Cursor.FIELD_TYPE_BLOB:
                                    condition.put(name, task_condition.getBlob(j));
                                    break;
                                case Cursor.FIELD_TYPE_FLOAT:
                                    condition.put(name, task_condition.getFloat(j));
                                    break;
                                case Cursor.FIELD_TYPE_INTEGER:
                                    condition.put(name, task_condition.getInt(j));
                                    break;
                                case Cursor.FIELD_TYPE_STRING:
                                    condition.put(name, task_condition.getString(j));
                                    break;
                            }
                        }
                    }
                    conditions.put(condition);
                }
                task.put("conditions", conditions);
                result.put(task);
            }
            return result;
        } catch (@NotNull PackageManager.NameNotFoundException | JSONException e) {
            e.printStackTrace();
        } finally {
            if (selected_tasks != null) selected_tasks.close();
            if (task_actions != null) task_actions.close();
            if (task_action != null) task_action.close();
            if (task_conditions != null) task_conditions.close();
            if (task_condition != null) task_condition.close();
        }
        return null;
    }

    @Nullable
    public JSONObject exportAllDatabase(String complaint, String email, @Nullable String export) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        JSONObject result = new JSONObject();
        final String[] projection_wildcard = new String[]{"rowid, *"};
        Cursor table = null;
        try {
            //noinspection ConstantConditions
            final int my_version = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionCode;
            String android_id = getPrivateSetting("ANDROID_ID", "");
            result.put("version", my_version);
            result.put("android_id", android_id);
            result.put("sdk", Build.VERSION.SDK_INT);
            result.put("model", Build.MODEL);
            result.put("product", Build.PRODUCT);
            result.put("product_id", Build.ID);
            result.put("device", Build.DEVICE);
            result.put("hardware", Build.HARDWARE);
            result.put("manufacturer", Build.MANUFACTURER);
            result.put("complaint", complaint);
            result.put("email", email);

            if (export == null || export.equals("no")) return result;

            for (String table_name : mAllTables) {
                table = db.query(table_name, projection_wildcard, null, null, null, null, null);
                if (table == null) continue;
                JSONObject table_json = new JSONObject();
                table_json.put("name", table_name);
                if (table.moveToFirst()) {
                    table_json.put("columns", TextUtils.join(",", table.getColumnNames()));
                    JSONArray results = new JSONArray();
                    do {
                        JSONArray row = new JSONArray();
                        for (int i = 0; i < table.getColumnCount(); i++) {
                            switch (table.getType(i)) {
                                case Cursor.FIELD_TYPE_NULL:
                                    row.put(null);
                                    break;
                                case Cursor.FIELD_TYPE_BLOB:
                                    row.put(table.getBlob(i));
                                    break;
                                case Cursor.FIELD_TYPE_FLOAT:
                                    row.put(table.getFloat(i));
                                    break;
                                case Cursor.FIELD_TYPE_INTEGER:
                                    row.put(table.getInt(i));
                                    break;
                                case Cursor.FIELD_TYPE_STRING:
                                    row.put(table.getString(i));
                                    break;
                            }
                        }
                        results.put(row);
                    } while (table.moveToNext());
                    table_json.put("results", results);
                }
                result.put("table_" + table_name, table_json);
            }

            return result;
        } catch (@NotNull PackageManager.NameNotFoundException | JSONException ignored) {
        } finally {
            if (table != null) table.close();
        }
        return null;
    }

    public void deepCopyTask(int task_id) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        final String[] projection_wildcard = new String[]{"*"};
        try {
            db.beginTransaction();
            ContentValues taskValues = new ContentValues();
            int when_id;
            int what_id;
            {
                Cursor task = db.query("tasks" + V, projection_wildcard, "task_id = ?",
                        new String[]{"" + task_id}, null, null, null);
                if (task == null) return;
                if (!task.moveToFirst()) {
                    task.close();
                    return;
                }
                for (int i = 0; i < task.getColumnCount(); i++) {
                    putColumnIntoContentValue(task, i, taskValues);
                }
                taskValues.remove("task_id");
                when_id = task.getInt(task.getColumnIndexOrThrow("when_id"));
                what_id = task.getInt(task.getColumnIndexOrThrow("what_id"));
                String new_label = task.getString(task.getColumnIndexOrThrow("label"));
                int copy_number = task.getInt(task.getColumnIndexOrThrow("copy"));
                if (copy_number == 0) copy_number = task_id;
                taskValues.put("copy", copy_number);
                if (new_label == null) new_label = "";
                if (!new_label.startsWith("Copy ")) new_label = "Copy " + new_label;
                taskValues.put("label", new_label);
                task.close();
            }
            int new_when_id = 0;
            int new_what_id = 0;
            {
                Cursor what = db.query("what" + V, projection_wildcard, "what_id = ?",
                        new String[]{"" + what_id}, null, null, "idx");
                if (what == null) return;
                while (what.moveToNext()) {
                    ContentValues cv = new ContentValues();
                    for (int i = 0; i < what.getColumnCount(); i++) {
                        putColumnIntoContentValue(what, i, cv);
                    }
                    cv.put("what_id", new_what_id);

                    int what_item = what.getInt(what.getColumnIndexOrThrow("what_item"));
                    String what_type = what.getString(what.getColumnIndexOrThrow("what_type"));
                    Cursor what_item_cursor = db.query("what_" + what_type + V, projection_wildcard,
                            "what_id = ?", new String[]{"" + what_item}, null, null, null);
                    if (what_item_cursor == null) continue;
                    if (!what_item_cursor.moveToFirst()) {
                        what_item_cursor.close();
                        continue;
                    }
                    ContentValues valuesItem = new ContentValues();
                    for (int i = 0; i < what_item_cursor.getColumnCount(); i++) {
                        putColumnIntoContentValue(what_item_cursor, i, valuesItem);
                    }
                    what_item_cursor.close();
                    valuesItem.remove("what_id");

                    int new_what_item = (int) db.insert("what_" + what_type + V, null, valuesItem);
                    if (new_what_item == -1) continue;
                    cv.put("what_item", new_what_item);
                    int new_what_entry = (int) db.insert("what" + V, null, cv);
                    if (new_what_entry == -1) continue;
                    if (new_what_id == 0) {
                        new_what_id = new_what_entry;
                        cv.clear();
                        cv.put("what_id", new_what_id);
                        db.update("what" + V, cv, "rowid = ?", new String[]{"" + new_what_entry});
                    }
                }
                what.close();
            }
            {
                Cursor when = db.query("when" + V, projection_wildcard, "when_id = ?",
                        new String[]{"" + when_id}, null, null, "idx");
                if (when == null) return;
                while (when.moveToNext()) {
                    ContentValues cv = new ContentValues();
                    for (int i = 0; i < when.getColumnCount(); i++) {
                        putColumnIntoContentValue(when, i, cv);
                    }
                    cv.put("when_id", new_when_id);

                    int when_item = when.getInt(when.getColumnIndexOrThrow("when_item"));
                    String when_type = when.getString(when.getColumnIndexOrThrow("when_type"));
                    Cursor when_item_cursor = db.query("when_" + when_type + V, projection_wildcard,
                            "when_id = ?", new String[]{"" + when_item}, null, null, null);
                    if (when_item_cursor == null) continue;
                    if (!when_item_cursor.moveToFirst()) {
                        when_item_cursor.close();
                        continue;
                    }
                    ContentValues valuesItem = new ContentValues();
                    for (int i = 0; i < when_item_cursor.getColumnCount(); i++) {
                        putColumnIntoContentValue(when_item_cursor, i, valuesItem);
                    }
                    when_item_cursor.close();
                    valuesItem.remove("when_id");

                    int new_when_item = (int) db.insert("when_" + when_type + V, null, valuesItem);
                    if (new_when_item == -1) continue;
                    cv.put("when_item", new_when_item);
                    int new_when_entry = (int) db.insert("when" + V, null, cv);
                    if (new_when_entry == -1) continue;
                    if (new_when_id == 0) {
                        new_when_id = new_when_entry;
                        cv.clear();
                        cv.put("when_id", new_when_id);
                        db.update("when" + V, cv, "rowid = ?", new String[]{"" + new_when_entry});
                    }
                }
                when.close();
            }
            if (new_what_id == 0 || new_when_id == 0) return;
            taskValues.put("what_id", new_what_id);
            taskValues.put("when_id", new_when_id);
            taskValues.put("expanded", 1);
            taskValues.put("enabled", 0);
            int new_task_id = (int) db.insert("tasks" + V, null, taskValues);
            if (new_task_id == -1) return;
            String description = generateTaskDescription(new_task_id, 0, 0);
            taskValues.clear();
            taskValues.put("description", description);
            db.update("tasks" + V, taskValues, "task_id = ?", new String[]{"" + new_task_id});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void updateRunningActions(@NotNull ArrayList<String> actions, @NotNull StartType startType) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("start_type", startType.ordinal());
        final String in_actions = TextUtils.join(",", actions);
        db.update("what" + V, contentValues, "rowid IN (" + in_actions + ")", emptyStringArray);
    }

    public void setPrivateSetting(@Nullable String key, @Nullable String value) {
        if (key == null) return;
        if (value == null) value = "";
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", key);
        contentValues.put("value", value);
        db.update("settings" + V, contentValues, "key = ?", new String[]{key});
        if (lastAffectedRow(db) == 0) {
            db.insert("settings" + V, null, contentValues);
        }
    }

//    public void logStatsEvent(@NotNull StatsType kind) {
//        try {
//            SQLiteDatabase db = getWritableDatabase();
//            assert db != null;
//            Calendar cal = Calendar.getInstance();
//            long value = cal.getTimeInMillis();
//            ContentValues contentValues = new ContentValues();
//            contentValues.put("kind", kind.ordinal());
//            contentValues.put("value", value);
//            db.insert("stats"+V, null, contentValues);
//        } catch (SQLiteException ignore) {
//        }
//    }

//    public void logStatsEventValue(@NotNull StatsType kind, long value) {
//        try {
//            SQLiteDatabase db = getWritableDatabase();
//            assert db != null;
//            ContentValues contentValues = new ContentValues();
//            contentValues.put("kind", kind.ordinal());
//            contentValues.put("value", value);
//            db.insert("stats"+V, null, contentValues);
//        } catch (SQLException ignore) {
//        }
//    }

    @NotNull
    public String getPrivateSetting(String key, @NotNull String defaultValue) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor result = null;
        try {
            result = db.query("settings" + V, new String[]{"value"}, "key = ?",
                    new String[]{key}, null, null, null);
            if (!result.moveToFirst())
                return defaultValue;
            String res = result.getString(0);
            return res == null ? defaultValue : res;
        } finally {
            if (result != null) result.close();
        }
    }

    public boolean entityUpdateIndex(String entity_type, int entity_id, long row_id, boolean up, int childOldIdx, int childCount, int task_id) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        if ((up && childOldIdx == 0) || (!up && childOldIdx >= childCount)) return false;
        ContentValues cv = new ContentValues();
        int childNewIdx = childOldIdx + (up ? -1 : 1);
        cv.put("idx", childOldIdx);
        db.update(entity_type + V, cv, entity_type + "_id = ? AND idx = ?",
                new String[]{"" + entity_id, "" + childNewIdx});
        cv.clear();
        cv.put("idx", childNewIdx);
        db.update(entity_type + V, cv, "rowid = ?", new String[]{"" + row_id});
        cv.clear();
        cv.put("description", this.generateTaskDescription(task_id, 0, 0));
        db.update("tasks" + V, cv, "task_id = ?", new String[]{"" + task_id});
        return true;
    }

    @NotNull
    public String getTaskLabel(int task_id) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor c = db.query("tasks" + V, new String[]{"label"}, "task_id = ?", new String[]{"" + task_id}, null, null, null);
        if (!c.moveToFirst()) return "";
        String result = c.getString(0);
        c.close();
        if (result == null) return "";
        return result;
    }

    public void setTaskLabel(int task_id, String label) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("label", label);
        db.update("tasks" + V, contentValues, "task_id = ?", new String[]{"" + task_id});
        if (lastAffectedRow(db) == 0) return;
        String description = generateTaskDescription(task_id, 0, 0);
        contentValues.clear();
        contentValues.put("description", description);
        db.update("tasks" + V, contentValues, "task_id = ?", new String[]{"" + task_id});
    }

    @NotNull
    public String getSelectedIds() {
        StringBuilder res = new StringBuilder();
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor c = null;
        try {
            c = db.query("tasks" + V, new String[]{"task_id"}, "selected = 1", null, null, null, null);
            if (!c.moveToFirst()) return "";
            res.append(c.getString(0));
            while (c.moveToNext()) {
                res.append(",");
                res.append(c.getString(0));
            }
            return res.toString();
        } finally {
            if (c != null) c.close();
        }
    }

    @NotNull
    public Cursor getLogCursor() {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        return db.query(
                "log" + V,
                new String[]{
                        "rowid AS _id",
                        "content",
                },
                null, null, null, null, "timestamp ASC"
        );
    }

    public void putToLog(String content) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues values = new ContentValues();
        values.put("content", content);
        db.insert("log" + V, null, values);
    }

    public void clearLog() {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        db.delete("log" + V, "", emptyStringArray);
    }

    public void fixIdxActions(int entity_id, boolean start) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        Cursor c = null;
        try {
            db.beginTransaction();
            int idx = 0;
            ContentValues cv = new ContentValues();
            c = db.query(
                    "what" + V,
                    new String[]{"rowid"},
                    "what" + "_id = ? AND start = ?",
                    new String[]{"" + entity_id, start ? "1" : "0"},
                    null,
                    null,
                    "idx"
            );
            if (c == null) return;
            while (c.moveToNext()) {
                long rowid = c.getLong(0);
                cv.put("idx", idx);
                db.update("what" + V, cv, "rowid = ?", new String[]{"" + rowid});
                idx++;
            }
            db.setTransactionSuccessful();
        } finally {
            if (c != null) c.close();
            db.endTransaction();
        }
    }

    public void fixIdxConditions(int entity_id) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        Cursor c = null;
        try {
            db.beginTransaction();
            int idx = 0;
            ContentValues cv = new ContentValues();
            c = db.query(
                    "when" + V,
                    new String[]{"rowid"},
                    "when_id = ?",
                    new String[]{"" + entity_id},
                    null,
                    null,
                    "idx"
            );
            if (c == null) return;
            while (c.moveToNext()) {
                long rowid = c.getLong(0);
                cv.put("idx", idx);
                db.update("when" + V, cv, "rowid = ?", new String[]{"" + rowid});
                idx++;
            }
            db.setTransactionSuccessful();
        } finally {
            if (c != null) c.close();
            db.endTransaction();
        }
    }

    @NotNull
    public long[] getAllEntities(final String entity_type) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor c = null;
        try {
            c = db.query(entity_type + V, new String[]{"rowid"}, null, null, null, null, null);
            if (c == null || c.getCount() == 0) return new long[]{};
            long res[] = new long[c.getCount()];
            int i = 0;
            while (c.moveToNext()) {
                res[i] = c.getLong(0);
                i++;
            }
            return res;
        } finally {
            if (c != null) c.close();
        }
    }

    public void setLocationFlag(boolean clear) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("flag", clear ? 0 : 1);
        db.update("when" + V, contentValues, "when_type = ? AND enabled = 1", new String[]{"location"});
    }

    public void clearFlag(String entity_type, String entity_id_string) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("flag", 0);
        db.update(entity_type + V, contentValues, "rowid = ?", new String[]{entity_id_string});
    }

    @NotNull
    public String getDirectLink() {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        Cursor cursor = null;
        try {
            cursor = db.query("tasks" + V, new String[]{"task_id", "link_hash"}, "selected = 1",
                    null, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) return "";
            StringBuilder result = new StringBuilder();
            result.append("http://cond.im/run?tasks=");
            boolean not_first = false;
            do {
                String link_hash = cursor.getString(1);
                if (link_hash == null || link_hash.isEmpty()) {
                    link_hash = randomString(8);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("link_hash", link_hash);
                    final int task_id = cursor.getInt(0);
                    db.update("tasks" + V, contentValues,
                            "task_id = ?", new String[]{Integer.toString(task_id)});
                }
                if (not_first) result.append("+");
                result.append(link_hash);
                not_first = true;
            } while (cursor.moveToNext());
            return result.toString();
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public int getTaskIdByHash(@Nullable String hash) {
        if (hash == null || hash.isEmpty()) return 0;
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        Cursor cursor = null;
        try {
            cursor = db.query("tasks" + V, new String[]{"task_id"}, "link_hash = ?",
                    new String[]{hash}, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) return 0;
            return cursor.getInt(0);
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public int countFlags() {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        final String flag_nag = getPrivateSetting("FLAG_NAG_DISMISSED", "0");
        if (flag_nag.equals("1")) return 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put("expanded", 1);
        int count = db.update("tasks" + V, contentValues,
                "enabled = 1 AND (when_id IN (SELECT when_id FROM when" + V + " WHERE flag > 0) OR " +
                        "what_id IN (SELECT what_id FROM what" + V + " WHERE flag > 0))", null
        );
        if (count == 0) return 0;
        contentValues.put("expanded", 0);
        db.update("tasks" + V, contentValues,
                "enabled = 0 OR (when_id IN (SELECT when_id FROM when" + V + " WHERE flag = 0) AND " +
                        "what_id IN (SELECT what_id FROM what" + V + " WHERE flag = 0))", null
        );
        return count;
    }

    public long scheduleSmsDelivery(String target, String message, int parts) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        ContentValues contentValues = new ContentValues();
        contentValues.put("target", target);
        contentValues.put("message", message);
        contentValues.put("parts", parts);
        db.insert("sms_queue" + V, null, contentValues);
        return lastRowID(db);
    }

    public void reportSmsSentStatus(long queue_id, boolean is_sent) {
        SQLiteDatabase db = getWritableDatabase();
        assert db != null;
        Cursor cursor = null;
        try {
            cursor = db.query("sms_queue" + V,
                    new String[]{"target", "message", "parts", "sent", "failed", "attempt"},
                    "queue_id = ?", new String[]{"" + queue_id}, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) return;
            int sent = cursor.getInt(cursor.getColumnIndexOrThrow("sent"));
            int failed = cursor.getInt(cursor.getColumnIndexOrThrow("failed"));
            int parts = cursor.getInt(cursor.getColumnIndexOrThrow("parts"));
            int attempt = cursor.getInt(cursor.getColumnIndexOrThrow("attempt"));
            ContentValues values = new ContentValues();
            if (is_sent) sent++;
            else failed++;
            if (sent + failed == parts) {
                // decisions must be made now
                if (failed > 0 && attempt < 10) {
                    values.put("attempt", attempt + 1);
                    values.put("sent", 0);
                    values.put("failed", 0);
                    AlarmManager alarmManager = (AlarmManager) context
                            .getSystemService(Context.ALARM_SERVICE);
                    Intent alarmIntent = new Intent(context, PhoneReceiver.class);
                    alarmIntent.putExtra("type", "sms");
                    alarmIntent.putExtra("queue_id", queue_id);
                    alarmIntent.putExtra("target", cursor.getString(
                            cursor.getColumnIndexOrThrow("target")));
                    alarmIntent.putExtra("message", cursor.getString(
                            cursor.getColumnIndexOrThrow("message")));
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.SECOND, 30);
                    int id = (int) (1080 + (queue_id % 10));
                    PendingIntent timePendingIntent = PendingIntent.getBroadcast(context, id,
                            alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            timePendingIntent);
                    Log.w(APPTAG, "schedule sms resend in 30 seconds");
                } else {
                    // delete the queue
                    db.delete("sms_queue" + V, "queue_id = ?", new String[]{"" + queue_id});
                    return;
                }
            } else if (is_sent) {
                values.put("sent", sent);
            } else {
                values.put("failed", failed);
            }
            db.update("sms_queue" + V, values, "queue_id = ?", new String[]{"" + queue_id});
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public Cursor getPhoneConditionWhitelistCursor(int task_id) {
        SQLiteDatabase db = getReadableDatabase();
        assert db != null;
        return db.rawQuery("SELECT w.rowid AS _id, z.phone AS number FROM when_phone" + V + " AS z, "
                        + "when" + V + " AS w, tasks" + V + " AS t WHERE z.inverse = 1 AND z.when_id = w.when_item "
                        + "AND t.task_id = ? AND t.when_id = w.when_id AND w.undo = 0",
                new String[]{"" + task_id}
        );
    }

}


