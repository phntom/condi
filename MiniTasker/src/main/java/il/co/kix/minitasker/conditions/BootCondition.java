package il.co.kix.minitasker.conditions;

import android.content.ContentValues;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;

/**
 * Created by phantom on 07/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */

//TODO: fix running apps from sd

public class BootCondition extends ConditionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of();

    @Override
    protected void loadLocalValues(ContentValues myValues) throws NullPointerException {

    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.condition_boot_individual);
    }

    @NotNull
    @Override
    public String getName() {
        return "boot";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.condition_boot_description);
    }

    @Nullable
    @Override
    public Class<?> getRelatedActivity() {
        return null; //this is intentional
    }

    @Override
    public void init() {

    }

    @Override
    public boolean conditionApplies(boolean is_start) {
        return true;
    }
}
