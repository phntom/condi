package il.co.kix.minitasker;

/**
 * holds type values for start states
 *
 * Created by phantom on 06/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum StartType {
    UNKNOWN,
    START_ONLY,
    START_WITH_END,
    END,
}
