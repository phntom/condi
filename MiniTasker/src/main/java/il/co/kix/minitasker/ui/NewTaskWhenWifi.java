package il.co.kix.minitasker.ui;

import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.WifiCondition;

/**
 * wifi condition editor dialog
 * <p/>
 * Created by phantom on 07/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenWifi extends SavableConditionFragmentActivity {
    private TextView txtSsid;
    private TextView txtBssid;
    private TextView txtIp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WifiCondition condition = (WifiCondition) getCondition();
        if (inverse != null)
            inverse.setChecked(condition.isInverse());
        txtSsid = (TextView) findViewById(R.id.txtSSID);
        txtSsid.setText(condition.getSsid());
        txtBssid = (TextView) findViewById(R.id.txtBssid);
        txtBssid.setText(condition.getBssid());
        txtIp = (TextView) findViewById(R.id.txtIP);
        txtIp.setText(condition.getIp());
    }

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new WifiCondition();
    }

    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        if (inverse != null) {
            inverse.setVisible(true);
        }
        WifiCondition condition = (WifiCondition) getCondition();
        if (condition != null && inverse != null) {
            inverse.setChecked(condition.isInverse());
        }
        return result;
    }


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void performSaveCondition(ConditionBase condition1) {
        WifiCondition condition = (WifiCondition) condition1;
        condition.setInverse(inverse.isChecked());
        condition.setSsid(txtSsid.getText().toString());
        condition.setBssid(txtBssid.getText().toString());
        condition.setIp(txtIp.getText().toString());
    }

    @Override
    boolean isSaveEnabled() {
        return true;
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_when_wifi;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.wifi_condition_title;
    }
}
