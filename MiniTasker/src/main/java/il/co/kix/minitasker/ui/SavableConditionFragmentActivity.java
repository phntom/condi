package il.co.kix.minitasker.ui;

import android.os.Bundle;

import il.co.kix.minitasker.conditions.ConditionBase;

/**
 * base activity for conditions
 * <p/>
 * Created by phantom on 18/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
abstract public class SavableConditionFragmentActivity extends SavableFragmentActivity {

    private ConditionBase condition;

    abstract ConditionBase instantiateNewCondition();

    public ConditionBase getCondition() {
        return condition;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        condition = instantiateNewCondition();
        condition.setContext(this);
        if (entity_id > 0) {
            condition.setPrimary(entity_id);
        }
        if (last_entity_type != null && !last_entity_type.equals(condition.getName())) {
            condition = instantiateNewCondition();
            condition.setContext(this);
        }
        condition.setTaskId(task_id);
    }

    @Override
    public void performSave() {
        if (!isSaveEnabled()) return;
        performSaveCondition(condition);
        condition.save();
        if (isNotEditMode())
            condition.setTaskEnabled(true);
        entity_id = condition.getPrimary();
        last_entity_type = condition.getName();
        nextActivityMain(false);
    }

    protected abstract void performSaveCondition(ConditionBase condition);

}
