package il.co.kix.minitasker.actions;

import android.content.Context;
import android.location.LocationManager;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import il.co.kix.minitasker.ExecuteAsRootBase;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.GPSModeEnabler;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * GPS action
 * <p/>
 * Created by sphantom on 7/27/13.
 * <p/>
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class GPSAction extends StatesAction {

    private LocationManager locationManager;

    @Override
    public boolean save() {
        boolean result = super.save();
        if (!ExecuteAsRootBase.canRunRootCommands()) {
            setFlag(FlagsEnum.GPS_ROOT_FAILED);
        }
        return result;
    }

    @NotNull
    @Override
    public String getDeviceName() {
        return "gps";
    }

    @Override
    protected boolean perform_init() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            setFlag(FlagsEnum.GPS_LOCATION_SERVICES_MISSING);
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        List<String> providersList = locationManager.getProviders(true);
        return providersList.contains("gps") ? StateType.On : StateType.Off;
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.GPS;
    }

    @Override
    protected boolean perform_on() {
        return new GPSModeEnabler(context, true).perform();
    }

    @Override
    protected boolean perform_off() {
        return new GPSModeEnabler(context, false).perform();
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.GPS_ROOT_FAILED);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_gps_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_gps_individual_on);
            case Off:
                return context.getString(R.string.action_gps_individual_off);
            case Toggle:
                return context.getString(R.string.action_gps_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_gps_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_gps_description_on);
            case Off:
                return context.getString(R.string.action_gps_description_off);
            case Toggle:
                return context.getString(R.string.action_gps_description_toggle);
        }
        return "";
    }
}
