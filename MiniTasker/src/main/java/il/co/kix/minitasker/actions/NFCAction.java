package il.co.kix.minitasker.actions;

import android.nfc.NfcAdapter;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * NFC is connected action
 * <p/>
 * Created by sphantom on 7/28/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NFCAction extends StatesAction {
    private NfcAdapter mNfcAdapter;

    @NotNull
    @Override
    public String getDeviceName() {
        return "NFC";
    }

    @Override
    protected boolean perform_init() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(context);
        if (mNfcAdapter == null) {
            setFlag(FlagsEnum.NFC_MISSING);
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        return mNfcAdapter.isEnabled() ? StateType.On : StateType.Off;
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.NFC;
    }

    @Override
    protected boolean perform_on() {
        try {
            Class<?> NfcManagerClass = Class.forName(mNfcAdapter.getClass().getName());
            Method setNfcEnabled = NfcManagerClass.getDeclaredMethod("enable");
            setNfcEnabled.setAccessible(true);
            setNfcEnabled.invoke(mNfcAdapter);
            return true;
        } catch (ClassNotFoundException ignored) {
        } catch (InvocationTargetException ignored) {
        } catch (NoSuchMethodException ignored) {
        } catch (IllegalAccessException ignored) {
        }
        return false;
    }

    @Override
    protected boolean perform_off() {
        try {
            Class<?> NfcManagerClass = Class.forName(mNfcAdapter.getClass().getName());
            Method setNfcDisabled = NfcManagerClass.getDeclaredMethod("disable");
            setNfcDisabled.setAccessible(true);
            setNfcDisabled.invoke(mNfcAdapter);
            return true;
        } catch (ClassNotFoundException ignored) {
        } catch (InvocationTargetException ignored) {
        } catch (NoSuchMethodException ignored) {
        } catch (IllegalAccessException ignored) {
        }
        return false;
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.NFC_REFLECTION_FAILED);
    }


    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_nfc_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_nfc_individual_on);
            case Off:
                return context.getString(R.string.action_nfc_individual_off);
            case Toggle:
                return context.getString(R.string.action_nfc_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_nfc_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_nfc_description_on);
            case Off:
                return context.getString(R.string.action_nfc_description_off);
            case Toggle:
                return context.getString(R.string.action_nfc_description_toggle);
        }
        return "";
    }
}
