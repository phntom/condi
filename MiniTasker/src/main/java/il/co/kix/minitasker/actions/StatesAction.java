package il.co.kix.minitasker.actions;

import android.content.ContentValues;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.EntityBase;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.MiniTaskerException;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.StateType;
import il.co.kix.minitasker.ui.NewTaskSensorsOnOffToggleFragment;

/**
 * Created by phantom on 27/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
abstract public class StatesAction extends ActionBase {
    protected StateType state;

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "state", SqlDataType.TEXT,
            "device", SqlDataType.TEXT
    );

    //used by reflection
    @Nullable
    public static EntityBase AbstractFactoryHelper(@NotNull DatabaseHandler db, Integer item_id) {
        String device = db.getFieldByPrimary("what_states", item_id, "device");
        if (device == null) return null;
        switch (device) {
            case "airplane":
                return new AirplaneModeAction();
            case "bluetooth":
                return new BluetoothAction();
            case "gps":
                return new GPSAction();
            case "mobile":
                return new MobileDataAction();
            case "nfc":
                return new NFCAction();
            case "sync":
                return new SyncAction();
            case "wifi":
                return new WiFiAction();
            case "orientation":
                return new OrientationAction();
            case "scanning":
                return new ScanningAction();
            case "wifiap":
                return new WifiApAction();
        }
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        if (!isRestore()) return "";
        return context.getString(R.string.action_states_individual_restore);
    }

    @NotNull
    @Override
    public String getDescription() {
        if (!isRestore()) return "";
        return context.getString(R.string.action_states_description_restore);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        String sState = myValues.getAsString("state");
        switch (sState) {
            case "on":
                state = StateType.On;
                break;
            case "off":
                state = StateType.Off;
                break;
            case "toggle":
                state = StateType.Toggle;
                break;
        }
    }

    public StateType getState() {
        return state;
    }

    public void setState(@NotNull StateType state_) {
        state = state_;
        switch (state_) {
            case On:
                myValues.put("state", "on");
                break;
            case Off:
                myValues.put("state", "off");
                break;
            case Toggle:
                myValues.put("state", "toggle");
                break;
        }
        myValues.put("device", getDeviceName());
    }

    @NotNull
    @Override
    public String getName() {
        return "states";
    }

    @NotNull
    abstract public String getDeviceName();

    @Override
    public void init() {
        state = StateType.On;
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskSensorsOnOffToggleFragment.class;
    }

    @NotNull
    public String getRelatedFragmentTag() {
        return getDeviceName();
    }

    @Override
    public void perform(StartType is_start) {
        if (!perform_init()) return;
        StateType current_state = perform_get_current_state();
        StateType new_state = state;
        if (isRestore()) {
            try {
                if (new_state != StateType.Toggle) {
                    new_state = state == StateType.On ? StateType.Off : StateType.On;
                }
                new_state = db.getLast(perform_get_last_type(), task_id) == 1 ? StateType.On : StateType.Off;
            } catch (MiniTaskerException ignored) {
            }
        } else {
            db.putLast(perform_get_last_type(), current_state == StateType.On ? 1 : 0, task_id);
        }
        if (new_state == StateType.Toggle) {
            new_state = current_state == StateType.On ? StateType.Off : StateType.On;
        }
        boolean result = false;
        if (current_state == new_state) return;
        for (int retry = 1; retry <= 5; retry++) {
            switch (new_state) {
                case On:
                    result = perform_on();
                    break;
                case Off:
                    result = perform_off();
                    break;
            }
            try {
                if (perform_get_current_state() == new_state) break;
                Thread.sleep(1000);
                if (perform_get_current_state() == new_state) break;
                Log.e(APPTAG, "failed performing " + getDeviceName() + " to state " + new_state.name() + " attempt " + retry);
            } catch (InterruptedException e) {
                return;
            }
        }
        if (!result) {
            perform_fail();
        }
    }


    protected abstract boolean perform_init();

    @NotNull
    protected abstract StateType perform_get_current_state();

    @NotNull
    protected abstract LastType perform_get_last_type();

    protected abstract boolean perform_on();

    protected abstract boolean perform_off();

    protected abstract void perform_fail();
}
