package il.co.kix.minitasker.ui;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.PhoneCondition;

/**
 * new task for phone action - SMS forwarding
 * <p/>
 * Created by phantom on 25/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskSmsForwardActivity extends NewTaskCallBlockActivity {
    private EditText txtTarget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        txtTarget = (EditText) findViewById(R.id.txtTarget);
        assert txtTarget != null;
        ImageButton btnTarget = (ImageButton) findViewById(R.id.btnContactTarget);
        assert btnTarget != null;
        txtTarget.setText(phoneAction.getTargetNumber());

        btnTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPhoneTarget = txtTarget;
                Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });

        txtTarget.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setSaveEnabled();
            }
        });

        if (phoneCondition.getPhone().isEmpty() && !phoneCondition.isBlockPrivate()
                && !phoneCondition.isBlockUnrecognized()) {
            //default configuration
            EditText txtPhoneWildcard = (EditText) findViewById(R.id.txtPhoneWildcard);
            assert txtPhoneWildcard != null;
            txtPhoneWildcard.setText("*");
        }

        setSaveEnabled();
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_new_task_phone_forwarding;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.call_forward_title;
    }

    @Override
    protected void performSaveCondition(ConditionBase condition) {
        Editable edtTarget = txtTarget.getText();
        assert edtTarget != null;
        String targetNumber = edtTarget.toString();
        if (target_number[1] != null && targetNumber.equals(target_number[1]))
            phoneAction.setTargetContact(target_display[1]);
        phoneAction.setTargetNumber(targetNumber);
        super.performSaveCondition(condition);
        phoneCondition.setKind(PhoneCondition.KIND_FORWARD);
    }

    @Override
    boolean isSaveEnabled() {
        Editable editable = txtTarget.getText();
        assert editable != null;
        return super.isSaveEnabled() && !editable.toString().isEmpty();
    }
}
