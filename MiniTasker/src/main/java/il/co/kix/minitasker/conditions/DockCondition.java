package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.text.TextUtils;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;

/**
 * phone is docked condition
 * <p/>
 * Created by sphantom on 7/30/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class DockCondition extends ConditionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of();

    public static final String FIELD_DOCK = "FIELD_DOCK";


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(ContentValues myValues) {
//        intentionally blank
    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.condition_dock_individual);
    }

    @NotNull
    @Override
    public String getName() {
        return "dock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.condition_dock_description);
    }

    @Nullable
    @Override
    public Class<?> getRelatedActivity() {
        return null;
        // this is intentional
    }

    @Override
    public void init() {
        //Nothing to do here...
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        boolean docked = TextUtils.equals(db.getPrivateSetting(FIELD_DOCK, "0"), "1");
        return is_start ? docked : !docked;
    }
}
