package il.co.kix.minitasker;

import android.app.Application;
import android.support.v4.app.FragmentActivity;

import org.jetbrains.annotations.Nullable;

/**
 * application handler class
 *
 * Created by phantom on 06/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MiniTaskerApp extends Application {
    public void onCreate() {
        super.onCreate();
        //noinspection ConstantConditions
        if (SingletonDatabase.INSTANCE.db == null)
            SingletonDatabase.INSTANCE.db = new DatabaseHandler(getApplicationContext(), DatabaseHandler.DATABASE_NAME);

//        final Resources resources = getBaseContext().getResources();
//        Configuration newConfig = resources.getConfiguration();
//        DisplayMetrics metrics = resources.getDisplayMetrics();
//
//        final String language = SingletonDatabase.INSTANCE.db.getPrivateSetting("DISPLAY_LANGUAGE",
//                Locale.getDefault().getLanguage());
//        if (language == null) {
//            newConfig.locale = Locale.ENGLISH;
//        } else if (language.equals("iw")) {
//            newConfig.locale = new Locale("iw");
//        } else {
//            newConfig.locale = Locale.ENGLISH;
//        }
//        resources.updateConfiguration(newConfig, metrics);
    }

    @Nullable
    private FragmentActivity mCurrentActivity = null;
    @Nullable
    public FragmentActivity getCurrentActivity(){
        return mCurrentActivity;
    }
    public void setCurrentActivity(FragmentActivity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }
}
