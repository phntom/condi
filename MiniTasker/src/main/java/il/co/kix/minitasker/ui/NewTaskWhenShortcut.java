package il.co.kix.minitasker.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.ShortcutCondition;

/**
 * Created by phantom on 24/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenShortcut extends SavableConditionFragmentActivity {
    EditText txtTitle;
    @Nullable
    String appName;
    int appActionId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        txtTitle = (EditText) findViewById(R.id.txtTitle);
        ShortcutCondition condition = (ShortcutCondition) getCondition();
        txtTitle.setText(condition.getShortcutTitle());
        appName = condition.getAppName();
        appActionId = condition.getAppActionId();
    }

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new ShortcutCondition();
    }

    @Override
    protected void performSaveCondition(ConditionBase conditionBase) {
        ShortcutCondition condition = (ShortcutCondition) conditionBase;
        condition.setAppName(appName);
        String title = "";
        if (txtTitle != null && txtTitle.getText() != null && txtTitle.getText().toString() != null)
            title = txtTitle.getText().toString();
        condition.setShortcutTitle(title);
        condition.setAppActionId(appActionId);
        condition.save();

    }

    @Override
    boolean isSaveEnabled() {
        return txtTitle != null && txtTitle.getText() != null
                && !txtTitle.getText().toString().isEmpty()
                && appName != null && !appName.isEmpty()
                && appActionId != 0;
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_when_shortcut;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.condition_shortcut_title;
    }

    public void updateApp(String title, String app, int actionId) {
        if (txtTitle == null) return;
        txtTitle.setText(title);
        appName = app;
        if (appActionId != 0) {
            DatabaseHandler db = SingletonDatabase.INSTANCE.db;
            //TODO: remind me why this is here
            db.deleteEntity("what", appActionId, task_id);
            db.undoCommit();
        }
        appActionId = actionId;
        setSaveEnabled();
    }

    public void onClick_btnPickApp(@NotNull View ignored) {
        ignored.getHeight();
        new NewTaskAppsListFragment().show(getFragmentManager(), NewTaskAppsListFragment.SHORTCUT);
    }

    public void onClick_CreateSave(@NotNull View ignored) {
        ignored.getHeight();
        performSave();
    }


}
