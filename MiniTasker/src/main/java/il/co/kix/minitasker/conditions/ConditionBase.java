package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.EntityBase;
import il.co.kix.minitasker.EntityType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.utils.EntityUtils;

/**
 * condition base class for inheritance
 * <p/>
 * Created by phantom on 26/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class ConditionBase extends EntityBase {
    protected int when_id = 0;
    protected StartType startType = StartType.UNKNOWN;

    @Nullable
    static public ConditionBase ConditionBaseFactory(@NotNull Context context, long primary) {
        return (ConditionBase) EntityUtils.entityFactory(context, primary, "when",
                EntityUtils.conditions, "Condition");
    }

    public static boolean isDoubleRun(boolean is_start, StartType startType) {
        return (is_start && startType == StartType.START_WITH_END)
                || (!is_start && startType == StartType.END);
    }

    @Override
    public boolean save() {
        boolean result = super.save();
        ContentValues when_values = new ContentValues();
        when_values.put("when_id", when_id);
        when_values.put("when_item", item_id);
        when_values.put("when_type", getName());
        when_values.put("description", toString());
        when_values.put("taskdesc", getDescription());
        when_values.put("idx", 10000);
        when_values.put("start_type", startType.ordinal());
        primary = db.saveWhenWhat(primary, when_id, task_id, "when", when_values);
        db.fixIdxConditions(when_id);
        if (task_id > 0) {
            //noinspection ConstantConditions
            when_id = when_values.getAsInteger("when_id");
            ContentValues task_values = new ContentValues();
            task_values.put("when_id", when_id);
            task_values.put("description", db.generateTaskDescription(task_id, when_id, 0));
            task_values.put("start_type", StartType.UNKNOWN.ordinal());
            db.saveEntity(task_id, "tasks", task_values);
        }
        return result;
    }

    @NotNull
    public EntityType getType() {
        return EntityType.ENTITY_CONDITION;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "when_" + getName();
    }

    @Override
    public void load() {
        super.load();
        if (primary == 0) return;
        when_id = db.getIdByItem(primary, "when");
        task_id = db.getTaskByEntity(when_id, "when");
        String startTypeStringNumber = db.getFieldByPrimary("when", primary, "start_type");
        if (TextUtils.isEmpty(startTypeStringNumber)) {
            startType = StartType.UNKNOWN;
            return;
        }
        startType = StartType.values()[Integer.valueOf(startTypeStringNumber)];
    }

    @Override
    public boolean getEnabled() {
        return super.getEnabled() && db.isEnabled(task_id, when_id, "when");
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        ContentValues values = new ContentValues();
        values.put("enabled", enabled);
        db.saveEntity(primary, "when", values);
    }

    abstract public boolean conditionApplies(boolean is_start) throws InterruptedException;

    public void updateMatching(boolean is_start) throws InterruptedException {
        if (isDoubleRun(is_start, startType)) return;
        if (!db.debugMode && !conditionApplies(is_start)) return;
        startType = is_start ? StartType.START_WITH_END : StartType.END;
        Log.d(APPTAG, String.format("Condition %d is now " + (is_start ? "" : "not ") + "matching",
                primary));
        save();
    }
}
