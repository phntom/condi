package il.co.kix.minitasker;

/**
 * holds type values of actions
 *
 * Created by phantom on 05/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum LastType {
    BRIGHTNESS,
    BLUETOOTH,
    NFC,
    WIFI,
    SYNC,
    MOBILE,
    VOLUME_ALARM_VOLUME,
    VOLUME_MEDIA_VOLUME,
    VOLUME_RING_VOLUME,
    VOLUME_NOTIFICATION,
    VOLUME_RING_MODE,
    UNUSED1,
    AIRPLANE,
    GPS,
    ORIENTATION,
    SCANNING,
    WIFI_AP,
    USB_AP,
}
