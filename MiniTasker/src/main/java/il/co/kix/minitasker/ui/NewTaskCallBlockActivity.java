package il.co.kix.minitasker.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.PhoneAction;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.PhoneCondition;

/**
 * new task for call blocking in phone action
 * <p/>
 * Created by phantom on 25/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskCallBlockActivity extends SavableConditionFragmentActivity {
    static final int PICK_CONTACT_REQUEST = 1;  // The request code
    protected final String[] target_display = new String[2];
    protected final String[] target_number = new String[2];
    protected PhoneAction phoneAction;
    protected PhoneCondition phoneCondition;
    protected EditText txtPhoneTarget;
    protected View relList;
    @Nullable
    protected ListView lstWhitelist = null;
    protected SimpleCursorAdapter mAdapter;
    private EditText txtPhoneWildcard;
    private Switch swiPrivate;
    private Switch swiUnknown;
    private Switch swiInverse;
    private DatabaseHandler db;

    private long mSelectedId = 0;
    private int mSelected = AdapterView.INVALID_POSITION;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @NotNull Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            Uri pickedPhoneNumber = data.getData();
            assert pickedPhoneNumber != null;
            final String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.Contacts.DISPLAY_NAME,
            };
            Cursor c = getContentResolver().query(pickedPhoneNumber, projection, null, null, null);
            if (c == null || !c.moveToFirst()) return;
            String phoneNumber = c.getString(
                    c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
            if (phoneNumber == null) phoneNumber = "";
            phoneNumber = phoneNumber.replace(" ", "");
            phoneNumber = phoneNumber.replace("-", "");
            phoneNumber = phoneNumber.replace("(", "");
            phoneNumber = phoneNumber.replace(")", "");
            String displayName = c.getString(
                    c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
            txtPhoneTarget.setText(phoneNumber);
            int position = (txtPhoneTarget == txtPhoneWildcard ? 0 : 1);
            target_display[position] = displayName;
            target_number[position] = phoneNumber;
            setSaveEnabled();
            c.close();

            // we can save the display name as a hint for the description
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        txtPhoneWildcard = (EditText) findViewById(R.id.txtPhoneWildcard);
        assert txtPhoneWildcard != null;
        ImageButton btnContact = (ImageButton) findViewById(R.id.btnContact);
        assert btnContact != null;
        swiPrivate = (Switch) findViewById(R.id.swiPrivate);
        assert swiPrivate != null;
        swiUnknown = (Switch) findViewById(R.id.swiUnknown);
        assert swiUnknown != null;
        swiInverse = (Switch) findViewById(R.id.swiInverse);
        assert swiInverse != null;
        relList = findViewById(R.id.relList);

        swiPrivate.setChecked(phoneCondition.isBlockPrivate());
        swiUnknown.setChecked(phoneCondition.isBlockUnrecognized());

        swiInverse.setChecked(phoneCondition.isBlockInverse());

        if (relList != null) {
            boolean isChecked = swiInverse.isChecked();
            relList.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            if (!isChecked) {
                txtPhoneWildcard.setText(phoneCondition.getPhone());
                txtPhoneWildcard.selectAll();
            }
            swiPrivate.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            swiUnknown.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            swiInverse.setText(isChecked ? R.string.call_block_whitelist : R.string.call_block_blacklist);
            swiInverse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    relList.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    String txtWildcard = txtPhoneWildcard.getText() == null ? ""
                            : txtPhoneWildcard.getText().toString();
                    swiPrivate.setVisibility(isChecked ? View.GONE : View.VISIBLE);
                    swiUnknown.setVisibility(isChecked ? View.GONE : View.VISIBLE);
                    swiInverse.setText(isChecked ? R.string.call_block_whitelist : R.string.call_block_blacklist);
                    if (!isChecked && txtWildcard.isEmpty()) {
                        txtPhoneWildcard.setText(phoneCondition.getPhone());
                    } else if (isChecked && txtWildcard.equals(phoneCondition.getPhone())) {
                        txtPhoneWildcard.setText("");
                    }
                    setSaveEnabled();
                }
            });
        } else {
            txtPhoneWildcard.setText(phoneCondition.getPhone());
            swiInverse.setText(R.string.call_forward_inverse_selection);
        }

        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPhoneTarget = txtPhoneWildcard;
                Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });


        lstWhitelist = (ListView) findViewById(R.id.lstWhitelist);
        if (lstWhitelist != null) {
            db = SingletonDatabase.INSTANCE.db;
            Cursor cursor = db.getPhoneConditionWhitelistCursor(task_id);
            mAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
                    cursor, new String[]{"number"}, new int[]{android.R.id.text1}, 0);
            lstWhitelist.setAdapter(mAdapter);
            lstWhitelist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, @NotNull View view, int position, long id) {
                    lstWhitelist.setSelection(position);
                    view.setSelected(true);
                    mSelected = position;
                    mSelectedId = id;
                    Log.w(APPTAG, "long click call block pos " + position);
                    return true;
                }
            });
        }


        txtPhoneWildcard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setSaveEnabled();
            }
        });


        final View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSaveEnabled();
            }
        };
        swiPrivate.setOnClickListener(onClick);
        swiUnknown.setOnClickListener(onClick);

        setSaveEnabled();
    }

    @Override
    ConditionBase instantiateNewCondition() {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        long condition_primary = db.getPhoneEntityByTask(task_id, "when");
        long action_primary = db.getPhoneEntityByTask(task_id, "what");
        phoneAction = new PhoneAction();
        phoneAction.setContext(this);
        phoneAction.setPrimary(action_primary);
        phoneAction.setTaskId(task_id);
        phoneCondition = new PhoneCondition();
        phoneCondition.setContext(this);
        phoneCondition.setPrimary(condition_primary);
        return phoneCondition;
    }

    @Override
    protected void performSaveCondition(ConditionBase condition) {
        final Editable txtPhoneWildcardEditable = txtPhoneWildcard.getText();
        assert txtPhoneWildcardEditable != null;
        phoneAction.setStart(is_start);
        if (phoneAction.save() && phoneAction.isSilence()) {
            phoneAction.saveRestoreCopy();
        }
        String phoneNumber = txtPhoneWildcardEditable.toString();
        if (swiInverse.isChecked() && lstWhitelist != null) {
            if (lstWhitelist.getCount() == 0 && phoneNumber.length() > 0) {
                onClickAdd(findViewById(R.id.btnAdd));
            }
        } else {
            if (target_number[0] != null && target_number[0].equals(phoneNumber))
                phoneCondition.setContact(target_display[0]);
            phoneCondition.setPhone(phoneNumber);
            phoneCondition.setBlockPrivate(swiPrivate.isChecked());
            phoneCondition.setBlockUnrecognized(swiUnknown.isChecked());
            phoneCondition.setKind(PhoneCondition.KIND_BLOCK);
            phoneCondition.setBlockInverse(swiInverse.isChecked());
        }
    }

    @Override
    boolean isSaveEnabled() {
        Editable editable = txtPhoneWildcard.getText();
        assert editable != null;
        return (swiInverse.isChecked() && lstWhitelist != null ? lstWhitelist.getCount() > 0 : (
                !editable.toString().isEmpty()
                        || swiPrivate.isChecked()
                        || swiUnknown.isChecked()));
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_new_task_phone_call_blocking;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.phone_call_block;
    }

    public void onClickAdd(@NotNull View view) {
        final Editable txtPhoneWildcardEditable = txtPhoneWildcard.getText();
        assert txtPhoneWildcardEditable != null;
        String phoneNumber = txtPhoneWildcardEditable.toString();
        if (phoneNumber.length() == 0) return;
        if (mSelectedId != 0) {
            phoneCondition = new PhoneCondition();
            phoneCondition.setContext(this);
            phoneCondition.setTaskId(task_id);
        }
        if (target_number[0] != null && target_number[0].equals(phoneNumber))
            phoneCondition.setContact(target_display[0]);
        phoneCondition.setPhone(phoneNumber);
        phoneCondition.setBlockPrivate(swiPrivate.isChecked());
        phoneCondition.setBlockUnrecognized(swiUnknown.isChecked());
        phoneCondition.setKind(PhoneCondition.KIND_BLOCK);
        phoneCondition.setBlockInverse(swiInverse.isChecked());
        phoneCondition.save();
        mSelectedId = phoneCondition.getPrimary();
        txtPhoneWildcard.setText("");
        mAdapter.swapCursor(db.getPhoneConditionWhitelistCursor(task_id));

        //save but don't leave
        performSaveCondition(phoneCondition);
        phoneCondition.save();
        if (isNotEditMode())
            phoneCondition.setTaskEnabled(true);
    }

    public void onClickRemove(@NotNull View view) {
        if (mSelected == AdapterView.INVALID_POSITION) {
            Toast.makeText(this, R.string.call_block_non_selected, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.w(APPTAG, "remove id " + mSelectedId);
        db.deleteEntity("when", mSelectedId, task_id);
        mAdapter.swapCursor(db.getPhoneConditionWhitelistCursor(task_id));
        mSelected = AdapterView.INVALID_POSITION;
    }

    public void onClickEdit(@NotNull View view) {
        if (mSelected == AdapterView.INVALID_POSITION) {
            Toast.makeText(this, R.string.call_block_non_selected, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.w(APPTAG, "edit id " + mSelectedId);
        PhoneCondition selectedCondition = new PhoneCondition();
        selectedCondition.setContext(this);
        selectedCondition.setPrimary(mSelectedId);
        selectedCondition.setTaskId(task_id);
        txtPhoneWildcard.setText(selectedCondition.getPhone());
        db.deleteEntity("when", mSelectedId, task_id);
        mAdapter.swapCursor(db.getPhoneConditionWhitelistCursor(task_id));
        mAdapter.notifyDataSetChanged();
        mSelected = AdapterView.INVALID_POSITION;
    }
}