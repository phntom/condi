package il.co.kix.minitasker;

import org.jetbrains.annotations.Nullable;

/**
 * Created by phantom on 28/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum
        SingletonDatabase {
    INSTANCE;

    @Nullable
    public DatabaseHandler db;
}
