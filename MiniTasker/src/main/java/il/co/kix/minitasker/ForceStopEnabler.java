package il.co.kix.minitasker;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created by phantom on 16/12/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class ForceStopEnabler extends ExecuteAsRootBase {
    private final String packageName;

    public ForceStopEnabler(final String packageName, Context context) {
        super(context);
        this.packageName = packageName;
    }

    @NotNull
    @Override
    protected ArrayList<String> getCommandsToExecute() {
        return new ArrayList<String>() {{
            add(String.format("am force-stop %s", packageName));
        }};
    }
}
