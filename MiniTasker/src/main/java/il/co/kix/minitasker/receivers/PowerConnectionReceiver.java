package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.conditions.BatteryCondition;

/**
 * receives power connection broadcast
 * <p/>
 * Created by phantom on 05/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class PowerConnectionReceiver extends BackgroundBroadcastReceiver {
    public static final int BATTERY_PLUGGED_WIRELESS = 4;

    final static String[] emptyQueryParameters = new String[]{};
    final static String chargerConnectedQuery = "z.state IN (0, 1, 2, 4)";
    final static String chargerDisconnectedQuery = "z.state IN (3, 5, 6, 7, 8)";

    public void onReceiveBackground(@NotNull Context context, @NotNull Intent intent) {
        String action = intent.getAction();
        if (action == null) action = "";
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;

        if (action.equals(Intent.ACTION_POWER_DISCONNECTED)
                || action.equals(Intent.ACTION_POWER_CONNECTED)) {
            final boolean isConnected = action.equals(Intent.ACTION_POWER_CONNECTED);
            db.runMatchingConditions("charger",
                    isConnected ? chargerDisconnectedQuery : chargerConnectedQuery,
                    emptyQueryParameters, StartType.END);
            db.runMatchingConditions("charger",
                    isConnected ? chargerConnectedQuery : chargerDisconnectedQuery,
                    emptyQueryParameters, StartType.START_WITH_END);

        } else if (action.equals(Intent.ACTION_BATTERY_LOW)
                || action.equals(Intent.ACTION_BATTERY_OKAY)) {
            boolean is_low = action.equals(Intent.ACTION_BATTERY_LOW);
            Log.w("MiniTasker", "battery is " + (is_low ? "" : "not ") + "low");//
            db.setPrivateSetting(BatteryCondition.FIELD_BATTERY_LOW, is_low ? "1" : "0");
            db.runMatchingConditions("battery", "1=1", emptyQueryParameters,
                    (is_low ? StartType.START_WITH_END : StartType.END));
        }
    }
}