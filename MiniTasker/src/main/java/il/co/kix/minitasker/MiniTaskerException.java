package il.co.kix.minitasker;

import org.jetbrains.annotations.NotNull;

/**
 * holds exceptions for the application
 *
 * Created by phantom on 05/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MiniTaskerException extends Exception {
    enum Reason {
        NO_REASON,
        SQL_NO_RESULT,
        NOT_LAST_LAST_ITEM,
        NO_CORRESPONDING_PUT,
    }

    @NotNull
    Reason reason = Reason.NO_REASON;
}
