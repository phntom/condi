package il.co.kix.minitasker.actions;

import android.content.ContentValues;
import android.content.Context;
import android.media.AudioManager;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.MiniTaskerException;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskPhoneVolumeFragment;

/**
 * volume control action
 * <p/>
 * Created by sphantom on 8/3/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class VolumeControlAction extends ActionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.<String, SqlDataType>builder()
            .put("link_ringtone_notification", SqlDataType.BOOLEAN)
            .put("volume_media", SqlDataType.INTEGER)
            .put("volume_ringtone", SqlDataType.INTEGER)
            .put("volume_notification", SqlDataType.INTEGER)
            .put("volume_alarm", SqlDataType.INTEGER)
            .put("vibrate", SqlDataType.BOOLEAN)
            .put("ignore", SqlDataType.SET)
            .build();

    public final static int STREAM_MEDIA = 0;
    public final static int STREAM_ALARM = 1;
    public final static int STREAM_NOTIFICATION = 2;
    public final static int STREAM_RINGTONE = 3;

    private int volume_media;
    private int volume_ringtone;
    private int volume_notification;
    private int volume_alarm;
    private boolean link_ringtone_notification;
    private boolean vibrate;
    private AudioManager audioManager;
    private boolean[] active;

    @Override
    public void setContext(Context ctx) {
        super.setContext(ctx);
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        volume_media = normalized_volume(AudioManager.STREAM_MUSIC);
        volume_ringtone = normalized_volume(AudioManager.STREAM_RING);
        volume_notification = normalized_volume(AudioManager.STREAM_NOTIFICATION);
        volume_alarm = normalized_volume(AudioManager.STREAM_ALARM);
        link_ringtone_notification = true;
        vibrate = audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE;
        active = new boolean[5];
    }

    int normalized_volume(final int stream) {
        int current_level = audioManager.getStreamVolume(stream);
        int max_level = audioManager.getStreamMaxVolume(stream);
        return (int) (255d * ((double) current_level / (double) max_level));
    }

    int denormalize_volume(int level, int stream) {
        if (level < 0) return 0;
        return (int) ((double) level * (double) audioManager.getStreamMaxVolume(stream) / 255d);
    }

    public void init() {
        // init is in setContext
    }

    public int getVolume_media() {
        return volume_media;
    }

    public void setVolume_media(int volume_media) {
        this.volume_media = volume_media;
        myValues.put("volume_media", volume_media);
    }

    public int getVolume_ringtone() {
        return volume_ringtone;
    }

    public void setVolume_ringtone(int volume_ringtone) {
        this.volume_ringtone = volume_ringtone;
        myValues.put("volume_ringtone", volume_ringtone);
    }

    public int getVolume_notification() {
        return volume_notification;
    }

    public void setVolume_notification(int volume_notification) {
        this.volume_notification = volume_notification;
        myValues.put("volume_notification", volume_notification);
    }

    public int getVolume_alarm() {
        return volume_alarm;
    }

    public void setVolume_alarm(int volume_alarm) {
        this.volume_alarm = volume_alarm;
        myValues.put("volume_alarm", volume_alarm);
    }

    public boolean isLink_ringtone_notification() {
        return link_ringtone_notification;
    }

    public void setLink_ringtone_notification(boolean link_ringtone_notification) {
        this.link_ringtone_notification = link_ringtone_notification;
        myValues.put("link_ringtone_notification", link_ringtone_notification);
    }

    public boolean isVibrate() {
        return vibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.vibrate = vibrate;
        myValues.put("vibrate", (vibrate ? 1 : 0));
    }

    public boolean isActive(int position) {
        return active[position];
    }

    public void setActive(boolean active, int position) {
        this.active[position] = active;
        int b = 0;
        for (int i = 0; i < 4; i++) {
            if (!this.active[i]) b += 1 << i;
        }
        myValues.put("ignore", b);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        link_ringtone_notification = myValues.getAsInteger("link_ringtone_notification") == 1;
        volume_media = myValues.getAsInteger("volume_media");
        volume_ringtone = myValues.getAsInteger("volume_ringtone");
        volume_notification = myValues.getAsInteger("volume_notification");
        volume_alarm = myValues.getAsInteger("volume_alarm");
        vibrate = myValues.getAsInteger("vibrate") == 1;
        int b = myValues.getAsInteger("ignore");
        for (int i = 0; i < 4; i++) {
            active[i] = ((b & 1 << i) == 0);
        }
    }

    @Override
    public void perform(StartType is_start) {
        int level_alarm = denormalize_volume(volume_alarm, AudioManager.STREAM_ALARM);
        int level_music = denormalize_volume(volume_media, AudioManager.STREAM_MUSIC);
        int level_ringer = denormalize_volume(Math.abs(volume_ringtone), AudioManager.STREAM_RING);
        int level_notification = denormalize_volume(volume_notification, AudioManager.STREAM_NOTIFICATION);
        int mode_ringer = vibrate ? AudioManager.RINGER_MODE_VIBRATE :
                (level_ringer <= 0 ? AudioManager.RINGER_MODE_SILENT
                        : AudioManager.RINGER_MODE_NORMAL);

        if (is_start == StartType.START_WITH_END) {
            if (isActive(STREAM_ALARM))
                db.putLast(LastType.VOLUME_ALARM_VOLUME, audioManager.getStreamVolume(AudioManager.STREAM_ALARM), task_id);
            if (isActive(STREAM_MEDIA))
                db.putLast(LastType.VOLUME_MEDIA_VOLUME, audioManager.getStreamVolume(AudioManager.STREAM_MUSIC), task_id);
            if (isActive(STREAM_RINGTONE)) {
                db.putLast(LastType.VOLUME_RING_VOLUME, audioManager.getStreamVolume(AudioManager.STREAM_RING), task_id);
                db.putLast(LastType.VOLUME_RING_MODE, audioManager.getRingerMode(), task_id);
            }
            if (isActive(STREAM_NOTIFICATION))
                db.putLast(LastType.VOLUME_NOTIFICATION, audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION), task_id);
        } else if (isRestore()) {
            try {
                if (isActive(STREAM_ALARM))
                    level_alarm = (int) db.getLast(LastType.VOLUME_ALARM_VOLUME, task_id);
                if (isActive(STREAM_MEDIA))
                    level_music = (int) db.getLast(LastType.VOLUME_MEDIA_VOLUME, task_id);
                if (isActive(STREAM_NOTIFICATION))
                    level_notification = (int) db.getLast(LastType.VOLUME_NOTIFICATION, task_id);
                if (isActive(STREAM_RINGTONE)) {
                    level_ringer = (int) db.getLast(LastType.VOLUME_RING_VOLUME, task_id);
                    mode_ringer = (int) db.getLast(LastType.VOLUME_RING_MODE, task_id);
                }
            } catch (MiniTaskerException ignored) {
                return;
            }
        }

        if (isActive(STREAM_ALARM))
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, level_alarm, 0);
        if (isActive(STREAM_MEDIA))
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, level_music, 0);
        if (isActive(STREAM_NOTIFICATION))
            audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, level_notification, 0);
        if (isActive(STREAM_RINGTONE)) {
            if (mode_ringer == AudioManager.RINGER_MODE_NORMAL) {
                audioManager.setStreamVolume(AudioManager.STREAM_RING, level_ringer, 0);
            } else if (mode_ringer == AudioManager.RINGER_MODE_SILENT) {
                audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
            }
            audioManager.setRingerMode(mode_ringer);
        }
    }

    @NotNull
    @Override
    public String toString() {
        String[] c = new String[4];
        int i = 0;

        int volume_media_actual = Math.abs((int) (((float) volume_media / 255f) * 100));
        int volume_alarm_actual = Math.abs((int) (((float) volume_alarm / 255f) * 100));
        int volume_notification_actual = Math.abs((int) (((float) volume_notification / 255f) * 100));
        int volume_ring_actual = Math.abs((int) (((float) volume_ringtone / 255f) * 100));

        if (isActive(STREAM_MEDIA)) {
            if (isRestore()) {
                c[i] = context.getString(R.string.action_volume_control_description_media_restore);
            } else if (volume_media > 0) {
                c[i] = context.getString(R.string.action_volume_control_description_media_level, volume_media_actual);
            } else {
                c[i] = context.getString(R.string.action_volume_control_description_media_mute);
            }
            i++;
        }
        if (isActive(STREAM_ALARM)) {
            if (isRestore()) {
                c[i] = context.getString(R.string.action_volume_control_description_alarm_restore);
            } else if (volume_alarm > 0) {
                c[i] = context.getString(R.string.action_volume_control_description_alarm_level, volume_alarm_actual);
            } else {
                c[i] = context.getString(R.string.action_volume_control_description_alarm_mute);
            }
            i++;
        }
        if (link_ringtone_notification) {
            if (isActive(STREAM_RINGTONE)) {
                if (isRestore()) {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_notification_restore);
                } else if (vibrate) {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_notification_vibrate);
                } else if (volume_ringtone <= 0) {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_notification_mute);
                } else {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_notification_level, volume_ring_actual);
                }
                i++;
            }
        } else {
            if (isActive(STREAM_RINGTONE)) {
                if (isRestore()) {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_restore);
                } else if (vibrate) {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_vibrate);
                } else if (volume_ringtone <= 0) {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_mute);
                } else {
                    c[i] = context.getString(R.string.action_volume_control_description_ringtone_level, volume_ring_actual);
                }
                i++;
            }
            if (isActive(STREAM_NOTIFICATION)) {
                if (isRestore()) {
                    c[i] = context.getString(R.string.action_volume_control_description_notification_restore);
                } else if (volume_notification > 0) {
                    c[i] = context.getString(R.string.action_volume_control_description_notification_level, volume_notification_actual);
                } else {
                    c[i] = context.getString(R.string.action_volume_control_description_notification_mute);
                }
                i++;
            }
        }

        switch (i) {
            case 4:
                return context.getString(R.string.action_volume_control_description_container_4,
                        c[0], c[1], c[2], c[3]);
            case 3:
                return context.getString(R.string.action_volume_control_description_container_3,
                        c[0], c[1], c[2]);
            case 2:
                return context.getString(R.string.action_volume_control_description_container_2,
                        c[0], c[1]);
            case 1:
                return context.getString(R.string.action_volume_control_description_container_1,
                        c[0]);
            default:
                return "";
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "volumecontrol";
    }

    @NotNull
    @Override
    public String getDescription() {
        return toString();
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskPhoneVolumeFragment.class;
    }
}
