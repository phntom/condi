package il.co.kix.minitasker;

import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * adapter that displays applications with an icon
 * <p/>
 * Created by phantom on 26/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class AppsWithIconsAdapter extends BaseAdapter {
    @NotNull
    private final Context context;
    private final List<AppInfo> listAppInfo;
    @Nullable
    private final PackageManager packageManager;
    private final boolean hasRoot;

    public AppsWithIconsAdapter(@NotNull Context context, List<AppInfo> listAppInfo, boolean hasRoot) {
        super();
        this.context = context;
        this.listAppInfo = listAppInfo;
        packageManager = context.getPackageManager();
        this.hasRoot = hasRoot;
    }

    @Override
    public int getCount() {
        return listAppInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return listAppInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_with_icon_1, null);
            assert convertView != null;
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.icon);
        if (packageManager != null) {
            imageView.setImageDrawable(listAppInfo.get(position).getIcon(context, packageManager));
        }
        TextView textView = (TextView) convertView.findViewById(R.id.text1);
        textView.setText(listAppInfo.get(position).getPackageDisplayName());

        return convertView;
    }

    public boolean isHasRoot() {
        return hasRoot;
    }
}