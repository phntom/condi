package il.co.kix.minitasker.actions;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Map;

import il.co.kix.minitasker.ActivityUtils;
import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.receivers.BackgroundBroadcastReceiver;
import il.co.kix.minitasker.receivers.PhoneCallReceiver;
import il.co.kix.minitasker.receivers.SmsSentReceiver;
import il.co.kix.minitasker.ui.NewTaskPhoneSmsActivity;

/**
 * SMS sending action
 * <p/>
 * Created by phantom on 05/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class SmsAction extends ActionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "target", SqlDataType.TEXT,
            "target_name", SqlDataType.TEXT,
            "message", SqlDataType.TEXT
    );

    public static final String KEY_PREVIOUS_NUMBER = PhoneCallReceiver.KEY_PREVIOUS_NUMBER;
    @Nullable
    private String target;
    @Nullable
    private String target_name;
    @Nullable
    private String message;

    public static boolean sendSms(@NotNull Context mContext, String mTarget, String mMessage, long queue_id) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> parts = smsManager.divideMessage(mMessage);
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        if (queue_id == 0)
            queue_id = db.scheduleSmsDelivery(mTarget, mMessage, parts.size());
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
        PendingIntent sentPendingIntent = PendingIntent.getBroadcast(mContext, 3000 + (int) queue_id,
                new Intent(mContext, SmsSentReceiver.class)
                        .putExtra(SmsSentReceiver.EXTRA_QUEUE_ID, queue_id),
                PendingIntent.FLAG_CANCEL_CURRENT
        );
        for (String ignore : parts) {
            sentPendingIntents.add(sentPendingIntent);
        }

        try {
            smsManager.sendMultipartTextMessage(mTarget, null, parts, sentPendingIntents, null);
            ContentValues values = new ContentValues();
            values.put("address", mTarget);
            values.put("body", mMessage);
            mContext.getContentResolver().insert(Uri.parse("content://sms/sent"), values);
            return true;
        } catch (NullPointerException e) {
            Log.e(APPTAG, mContext.getString(R.string.action_sms_warn_failed));
        } catch (java.lang.SecurityException ignored) {
            Log.e(APPTAG, "android requires sms_read permission for writing messages");
            return true;
        }
        return false;
    }


    @Override
    public void perform(StartType is_start) {
        if (isRestore()) return;
        final String phoneNumber = db.getPrivateSetting(KEY_PREVIOUS_NUMBER, "");
        final String newMessage = (message != null ? message.replace("%%PHONE%%", phoneNumber) : "");
        final String newTarget = (target != null && target.equals("*69")) ? phoneNumber : "" + target;
        Context appContext = context.getApplicationContext();
        assert appContext != null;
        SharedPreferences mPrefs = appContext.getSharedPreferences
                (ActivityUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        if (sendSms(context, newTarget, newMessage, 0)) {
            editor.putString(BackgroundBroadcastReceiver.KEY_SHOW_TOAST,
                    context.getString(R.string.action_sms_toast_sending, target_name, newTarget));
        } else {
            editor.putString(BackgroundBroadcastReceiver.KEY_SHOW_TOAST,
                    context.getString(R.string.action_sms_toast_failed, target_name, newTarget));
            setFlag(FlagsEnum.SMS_SEND_FAILED);
        }
        editor.commit();
    }

    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        target = myValues.getAsString("target");
        target_name = myValues.getAsString("target_name");
        message = myValues.getAsString("message");
    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.action_sms_individual,
                ((target_name != null && target_name.isEmpty()) ? target : target_name), message);
    }

    @NotNull
    @Override
    public String getName() {
        return "sms";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.action_sms_description,
                ((target_name != null && target_name.isEmpty()) ? target : target_name), message);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskPhoneSmsActivity.class;
    }

    @Override
    public void init() {
        target = "";
        target_name = "";
        message = "";
    }

    @Nullable
    public String getTarget() {
        return target;
    }

    public void setTarget(@Nullable String target) {
        if (target == null) return;
        this.target = target;
        myValues.put("target", target);
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getTarget_name() {
//        return target_name;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setTarget_name(@Nullable String target_name) {
        if (target_name == null) return;
        this.target_name = target_name;
        myValues.put("target_name", target_name);
    }

    @Nullable
    public String getMessage() {
        return message;
    }

    public void setMessage(@Nullable String message) {
        if (message == null) return;
        this.message = message;
        myValues.put("message", message);
    }
}
