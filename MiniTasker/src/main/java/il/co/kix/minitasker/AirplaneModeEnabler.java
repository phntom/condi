package il.co.kix.minitasker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * airplane mode enabler
 *
 * Created by phantom on 30/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class AirplaneModeEnabler extends ExecuteAsRootBase {
    private final Context context;
    private final boolean enable;

    public AirplaneModeEnabler(Context context, boolean enable) {
        super(context);
        this.context = context;
        this.enable = enable;
    }

    boolean nonRootExecute() {
        return (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 ?
                nonRootExecuteICS() :
                nonRootExecuteJB() );
    }

    boolean nonRootExecuteICS() {
        //noinspection deprecation
        final String AIRPLANE_MODE_ON = android.provider.Settings.System.AIRPLANE_MODE_ON;
        Settings.System.putInt(
                context.getContentResolver(),
                AIRPLANE_MODE_ON, enable ? 0 : 1
        );
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", !enable);
        context.sendBroadcast(intent);
        return Settings.System.getInt(
                context.getContentResolver(),
                AIRPLANE_MODE_ON,
                0
        ) == (enable ? 1 : 0);
    }

    @SuppressLint("NewApi")
    boolean nonRootExecuteJB() {
        @SuppressLint("InlinedApi")
        final String AIRPLANE_MODE_ON = Settings.Global.AIRPLANE_MODE_ON;
        try {
            Settings.Global.putInt(
                    context.getContentResolver(),
                    AIRPLANE_MODE_ON, enable ? 0 : 1
            );
            Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            intent.putExtra("state", !enable);
            context.sendBroadcast(intent);
            return Settings.Global.getInt(
                    context.getContentResolver(),
                    AIRPLANE_MODE_ON,
                    0
            ) == (enable ? 1 : 0);
        } catch (SecurityException ignored) {
            return false;
        }
    }

    public boolean perform() {
        return nonRootExecute() || execute();
    }

    @NotNull
    @Override
    protected ArrayList<String> getCommandsToExecute() {
        return new ArrayList<String>() {{
            add("settings put global airplane_mode_on "+(enable?"1":"0"));
            add("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state "
                    +(enable ? "true" : "false"));
            add("CLASSPATH=/system/framework/settings.jar exec app_process /system/bin com.android.commands.settings.SettingsCmd put global airplane_mode_on "+(enable?"1":"0"));
        }};
    }
}
