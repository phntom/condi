package il.co.kix.minitasker.services;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;

import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.BackgroundProvider;

/**
 * ensures background provider is running
 * <p/>
 * Created by phantom on 25/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BackgroundService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getContentResolver().query(Uri.parse("content://" + BackgroundProvider.BACKGROUND_PROVIDER + "/on_boot/service"), null, null, null, null);
        return Service.START_STICKY;
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }
}
