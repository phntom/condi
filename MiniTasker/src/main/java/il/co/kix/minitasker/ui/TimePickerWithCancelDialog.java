package il.co.kix.minitasker.ui;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.R;

/**
 * extension for time picker dialog.
 * <p/>
 * Created by phantom on 05/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class TimePickerWithCancelDialog extends TimePickerDialog {

    private final OnTimeSetListener mCallback;
    private boolean stop = false;

    public TimePickerWithCancelDialog(@NotNull Context context, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
        super(context, callBack, hourOfDay, minute, is24HourView);
        mCallback = callBack;
        setButton(DialogInterface.BUTTON_NEGATIVE, context.getText(R.string.clear), this);
    }

    public TimePickerWithCancelDialog(@NotNull Context context, int theme, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
        super(context, theme, callBack, hourOfDay, minute, is24HourView);
        mCallback = callBack;
        setButton(DialogInterface.BUTTON_NEGATIVE, context.getText(R.string.clear), this);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == BUTTON_POSITIVE)
            super.onClick(dialog, BUTTON_POSITIVE);
        else {
            mCallback.onTimeSet(null, -1, -1);
            stop = true;
        }
    }

    @Override
    protected void onStop() {
        if (stop) return;
        super.onStop();
    }
}
