package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.MainActivity;
import il.co.kix.minitasker.ui.NewTaskWhenShortcut;

/**
 * Created by phantom on 24/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class ShortcutCondition extends ConditionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "appname", SqlDataType.TEXT,
            "appactionid", SqlDataType.INTEGER,
            "title", SqlDataType.TEXT
    );


    final static String NAME = "shortcut";
    @Nullable
    private String appName;
    private int appActionId;
    @Nullable
    private String shortcutTitle;
    private boolean changed;

    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        appName = myValues.getAsString("appname");
        //noinspection ConstantConditions
        appActionId = myValues.getAsInteger("appactionid");
        shortcutTitle = myValues.getAsString("title");
        changed = false;
    }

    @Override
    public String toString() {
        return context.getString(R.string.condition_shortcut_individual);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return context.getString(R.string.condition_shortcut_description);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenShortcut.class;
    }

    @Override
    public void init() {
        appName = "";
        appActionId = 0;
        shortcutTitle = "";
        changed = false;
    }

    @Nullable
    public String getAppName() {
        return appName;
    }

    public void setAppName(@Nullable String appName) {
        if (appName == null) return;
        this.appName = appName;
        myValues.put("appname", appName);
    }

    public int getAppActionId() {
        return appActionId;
    }

    public void setAppActionId(int appActionId) {
        this.appActionId = appActionId;
        myValues.put("appactionid", appActionId);
        changed = true;
    }

    @Nullable
    public String getShortcutTitle() {
        return shortcutTitle;
    }

    public void setShortcutTitle(@NotNull String shortcutTitle) {
        this.shortcutTitle = shortcutTitle;
        myValues.put("title", shortcutTitle);
    }

    @Override
    public boolean save() {
        boolean result = super.save();

        if (!changed) return false;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) return false;
        Intent shortcutIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        shortcutIntent.putExtra("task_id", task_id);
        shortcutIntent.putExtra("is_shortcut", 1);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, shortcutTitle);
        try {
            Drawable drawable = packageManager.getApplicationIcon(appName);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmapDrawable.getBitmap());
        } catch (PackageManager.NameNotFoundException ignore) {
            Context appContext = context.getApplicationContext();
            if (appContext != null) {
                addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                        Intent.ShortcutIconResource.fromContext(appContext, R.drawable.ic_launcher));
            }
        }

        addIntent
                .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        context.getApplicationContext().sendBroadcast(addIntent);
        changed = false;
        return result;
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        return true;
    }
}
