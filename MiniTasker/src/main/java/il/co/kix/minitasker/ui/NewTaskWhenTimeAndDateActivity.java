package il.co.kix.minitasker.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.TimeCondition;
//import android.support.v4.app.FragmentActivity;

/**
 * new task for time condition
 * <p/>
 * Created by phantom on 08/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */

public class NewTaskWhenTimeAndDateActivity extends SavableConditionFragmentActivity
        implements ListView.OnItemClickListener {
    static final String TAG_TIME_START = "start";
    static final String TAG_TIME_END = "end";
    int time_start = -1;
    int time_end = -1;
    private int days_of_the_week = 0;
    private NewTaskTimeAdapter newTaskTimeAdapter;
    private ListView listView;

    @Override
    boolean isSaveEnabled() {
        return time_start != -1;
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.list_activity;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.time_range_condition;
    }

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new TimeCondition();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TimeCondition condition = (TimeCondition) getCondition();
        time_start = condition.getTimeStart();
        time_end = condition.getTimeEnd();
        days_of_the_week = condition.getTimeRepeat();

        listView = (ListView) findViewById(R.id.list);
        View loading = findViewById(R.id.pbrLoading);
        newTaskTimeAdapter = new NewTaskTimeAdapter(this) {{
            if (time_end != -1) {
                final int hour = time_end / 60;
                final int min = time_end % 60;
                modifyText(POSITION_END_TIME,
                        getString(R.string.time_set_end_value, hour, min));
                modifyChecked(POSITION_END_TIME, time_end == -1);
            } else {
                modifyChecked(POSITION_END_TIME, false);
            }
            if (time_start != -1) {
                final int hour = time_start / 60;
                final int min = time_start % 60;
                modifyText(POSITION_START_TIME,
                        getString(R.string.time_set_start_value, hour, min));
//                        String.format(getString(R.string.time_set_start_value), hour, min));
            }
            for (int i = 0; i < 7; i++) {
                int b = 1 << i;
                modifyChecked(POSITION_REPEAT_SUNDAY + i, (days_of_the_week & b) != 0);
            }

        }};

        listView.setAdapter(newTaskTimeAdapter);
        listView.setOnItemClickListener(this);

        loading.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        newTaskTimeAdapter.setCheckedOnItemClickListener(this);

    }

    @Override
    protected void performSaveCondition(ConditionBase cdt) {
        TimeCondition condition = (TimeCondition) cdt;
        condition.setParams(time_start, time_end, days_of_the_week);
    }

    void setTime(@NotNull String tag, int time_value) {
        final int hour = time_value / 60;
        final int min = time_value % 60;
        if (tag.equals(TAG_TIME_START)) {
            time_start = time_value;
            newTaskTimeAdapter.modifyText(NewTaskTimeAdapter.POSITION_START_TIME,
                    String.format(getString(R.string.time_set_start_value), hour, min));
        } else if (tag.equals(TAG_TIME_END)) {
            time_end = time_value;
            newTaskTimeAdapter.modifyText(NewTaskTimeAdapter.POSITION_END_TIME,
                    (time_value == -1 ? getString(R.string.time_set_end_no_selection)
                            : String.format(getString(R.string.time_set_end_value), hour, min))
            );

        }
        newTaskTimeAdapter.notifyDataSetChanged();
        listView.refreshDrawableState();
        setSaveEnabled();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case NewTaskTimeAdapter.POSITION_START_TIME:
                new NewTaskWhenTimeAndDateTimePickerFragment()
                        .show(getFragmentManager(), TAG_TIME_START);
                break;
            case NewTaskTimeAdapter.POSITION_END_TIME:
                new NewTaskWhenTimeAndDateTimePickerFragment()
                        .show(getFragmentManager(), TAG_TIME_END);
                break;
            default:
                int b = 1 << (position - NewTaskTimeAdapter.POSITION_REPEAT_SUNDAY);
                boolean checked = (id == 1);
                if (checked && (days_of_the_week & b) != 0) return;
                if (!checked && (days_of_the_week & b) == 0) return;
                if (id == 0) {
                    b *= -1;
                }
                days_of_the_week += b;
                Log.w(MiniTaskerActivity.APPTAG, "check box handled here pos: " + position + " checked: " + id + " b:" + b);
        }
        setSaveEnabled();
    }
}
