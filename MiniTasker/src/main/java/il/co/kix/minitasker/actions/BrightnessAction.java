package il.co.kix.minitasker.actions;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.provider.Settings;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.MiniTaskerException;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskBatteryEmergencyBrightnessFragment;

/**
 * brightness action
 * <p/>
 * Created by sphantom on 8/2/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BrightnessAction extends ActionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "level", SqlDataType.INTEGER
    );

    // brightness = -1 in order to reset brightness to default level
    int level;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        level = myValues.getAsInteger("level");
    }

    @Override
    public void setContext(@Nullable Context ctx) {
        super.setContext(ctx);
        ContentResolver cr = context.getContentResolver();
        if (cr == null) return;
        try {
            level = Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS);
            if (Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS_MODE)
                    == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC)
                level *= -1;
        } catch (Settings.SettingNotFoundException ignored) {
        }
    }

    public int getBrightness() {
        return level;
    }

    public void setBrightness(int v) {
        level = v;
        myValues.put("level", level);
    }

    @Override
    public void perform(StartType is_start) {
        ContentResolver cr = context.getContentResolver();
        if (cr == null) return;
        int new_level = level;
        if (isRestore()) {
            try {
                new_level = (int) db.getLast(LastType.BRIGHTNESS, task_id);
            } catch (MiniTaskerException e) {
                return;
            }
        } else {
            try {
                int last_level = Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS);
                boolean automatic = Settings.System.getInt(cr,
                        Settings.System.SCREEN_BRIGHTNESS_MODE)
                        == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
                db.putLast(LastType.BRIGHTNESS, last_level * (automatic ? -1 : 1), task_id);
            } catch (Settings.SettingNotFoundException e) {
                return;
            }
        }
        if (new_level >= 0) {
            Settings.System.putInt(cr, Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            Settings.System.putInt(cr, Settings.System.SCREEN_BRIGHTNESS, new_level);
        } else {
//            new_level *= -1;
//            Settings.System.putInt(cr, Settings.System.SCREEN_BRIGHTNESS, new_level);
            Settings.System.putInt(cr, Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
        }
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_brightness_individual_restore);
        int percent = (int) ((double) level / 2.55);
        if (level < 0)
            return context.getString(R.string.action_brightness_individual_automatic);
        return context.getString(R.string.action_brightness_individual_level, percent);
    }

    @NotNull
    @Override
    public String getName() {
        return "brightness";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_brightness_description_restore);
        int percent = (int) ((double) level / 2.55);
        if (level < 0)
            return context.getString(R.string.action_brightness_description_automatic);
        return context.getString(R.string.action_brightness_description_level, percent);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskBatteryEmergencyBrightnessFragment.class;
    }

    @Override
    public void init() {
        level = -1;
    }

    @NotNull
    public String getRelatedFragmentTag() {
        return getName();
    }
}
