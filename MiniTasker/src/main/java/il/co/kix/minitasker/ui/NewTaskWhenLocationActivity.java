package il.co.kix.minitasker.ui;

import android.graphics.Color;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.LocationCondition;

/**
 * new task for location condition
 * <p/>
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenLocationActivity extends SavableConditionFragmentActivity
        implements GoogleMap.OnMapClickListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    final static double MIN_RADIUS = LocationCondition.MIN_RADIUS;
    private static final String URL_part_1 = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
    private static final String URL_part_2 = "&sensor=false&language=";
    @Nullable
    String mLocationDescription = "";
    @Nullable
    private GoogleMap mMap = null;
    private Circle mapCircle;
    //    private Menu menu;
    @Nullable
    private LocationClient mLocationClient = null;
    private AsyncTask<String, Void, String> mDescriptionTask;

//    private static final double EARTH_RADIUS = 6378100.0;

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_when_location;
    }

    @Override
    boolean isSaveEnabled() {
        return mapCircle != null;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.location_based_tasks;
    }

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new LocationCondition();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpMapIfNeeded();
        if (mLocationClient != null && !mLocationClient.isConnected()
                && !mLocationClient.isConnecting())
            mLocationClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @NotNull
    @Override
    public LocationCondition getCondition() {
        return (LocationCondition) super.getCondition();
    }

    private void setUpMapIfNeeded() {
        LocationCondition condition = getCondition();
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(this, this, this);
        }
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap == null) return;
            mMap.setMyLocationEnabled(true);
            mMap.setOnMapClickListener(this);
            if (condition.getRadius() > 0) {
                LatLng latLng = new LatLng(condition.getLatitude(), condition.getLongitude());
                CircleOptions circleOptions = new CircleOptions();
                circleOptions.center(latLng);
                circleOptions.radius(condition.getRadius());
                circleOptions.fillColor(Color.argb(200, 0, 51, 102));
                mapCircle = mMap.addCircle(circleOptions);
                setSaveEnabled();
                if (inverse != null)
                    inverse.setChecked(condition.isInverse());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        if (inverse != null) {
            inverse.setVisible(true);
        }
        LocationCondition condition = getCondition();
        if (inverse != null) {
            inverse.setChecked(condition.isInverse());
        }
        return result;
    }

    @Override
    protected void performSaveCondition(ConditionBase cdt) {
        LocationCondition condition = (LocationCondition) cdt;
        if (mapCircle == null) return;
        LatLng pos = mapCircle.getCenter();
        double rad = mapCircle.getRadius();
        condition.setRadius(rad);
        condition.setLatitude(pos.latitude);
        condition.setLongitude(pos.longitude);
        condition.setDescription(mLocationDescription);
        if (inverse != null) {
            condition.setInverse(inverse.isChecked());
        }
    }

    @Override
    public void onMapClick(@NotNull LatLng latLng) {
        if (mMap == null) return;
        float zoom = mMap.getMaxZoomLevel() - mMap.getCameraPosition().zoom;
        double radius = 256.0 * Math.pow(2, zoom) / 85.0;
        if (radius < MIN_RADIUS) radius = MIN_RADIUS;
        if (mapCircle == null) {
            CircleOptions circleOptions = new CircleOptions();
            circleOptions.center(latLng);
            circleOptions.radius(radius);
            circleOptions.fillColor(Color.argb(200, 0, 51, 102));
            mapCircle = mMap.addCircle(circleOptions);
            setSaveEnabled();
        } else {
            mapCircle.setCenter(latLng);
            mapCircle.setRadius(radius);
        }
        downloadLocationDescription(latLng);
    }

    public void downloadLocationDescription(@NotNull LatLng latLng) {
        if (mDescriptionTask != null && mDescriptionTask.getStatus() != AsyncTask.Status.FINISHED) {
            mDescriptionTask.cancel(true);
        }
        StringBuilder url = new StringBuilder();
        try {
            url.append(URL_part_1);
            url.append(URLEncoder.encode("" + latLng.latitude + "," + latLng.longitude, "UTF-8"));
            url.append(URL_part_2);
            url.append(Locale.getDefault().getLanguage());
        } catch (UnsupportedEncodingException ignored) {
            return;
        }

        mDescriptionTask = new AsyncTask<String, Void, String>() {
            @Nullable
            @Override
            protected String doInBackground(String... params) {
                String link = params[0];
                HttpGet request = new HttpGet(link);
                AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
                try {
                    final HttpParams httpParams = client.getParams();
                    HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
                    HttpConnectionParams.setSoTimeout(httpParams, 2000);

                    HttpResponse httpResponse = client.execute(request);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    InputStream contentResult = httpEntity.getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(contentResult, "UTF-8"), 8);
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                        stringBuilder.append("\n");
                    }
                    contentResult.close();
                    JSONObject json = new JSONObject(stringBuilder.toString());
                    JSONArray results = json.getJSONArray("results");
                    String address = null;
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject result = results.getJSONObject(i);
                        if (!result.has("formatted_address")) continue;
                        address = result.getString("formatted_address");
                        break;
                    }
                    return address;
                } catch (IOException ignored) {
                } catch (JSONException ignored) {
                } finally {
                    client.close();
                }
                return null;
            }

            @Override
            protected void onPostExecute(@Nullable String address) {
                if (this.isCancelled()) return;
                if (address == null) return;
                mLocationDescription = address;
                LocationCondition condition = getCondition();
                if (condition.getPrimary() != 0) {
                    condition.setDescription(mLocationDescription);
                    condition.save();
                    DatabaseHandler db = SingletonDatabase.INSTANCE.db;
                    db.regenerateTaskDescription(task_id);
                }
            }
        }.execute(url.toString());
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (mMap == null) return;
        Location location = mLocationClient != null ? mLocationClient.getLastLocation() : null;
        if (location == null) return;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mLocationClient != null) {
            mLocationClient.disconnect();
        }
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}