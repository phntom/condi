package il.co.kix.minitasker.actions;

import android.content.ContentValues;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskAppsListFragment;

/**
 * common base for launcher and background actions
 * <p/>
 * Created by phantom on 26/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
abstract public class PackageBaseAction extends ActionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "package", SqlDataType.TEXT,
            "display", SqlDataType.TEXT,
            "root", SqlDataType.BOOLEAN
    );

    @Nullable
    protected String packageName;
    @Nullable
    protected String packageDisplayName;
    boolean root;


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        packageName = myValues.getAsString("package");
        packageDisplayName = myValues.getAsString("display");
        root = myValues.getAsInteger("root") == 1;
    }


    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskAppsListFragment.class;
    }

    @Override
    public void init() {
        packageName = "";
        packageDisplayName = "";
        root = false;
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getPackageName() {
//        return packageName;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setPackageName(@NotNull String packageName) {
        this.packageName = packageName;
        myValues.put("package", packageName);
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getPackageDisplayName() {
//        return packageDisplayName;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setPackageDisplayName(@NotNull String packageDisplayName) {
        this.packageDisplayName = packageDisplayName;
        myValues.put("display", packageDisplayName);
    }

    public void setRoot(boolean root) {
        this.root = root;
        myValues.put("root", root ? 1 : 0);
    }
}
