package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.conditions.HdmiCondition;
import il.co.kix.minitasker.ui.MiniTaskerActivity;

public class HdmiReceiver extends BackgroundBroadcastReceiver {
    private static final String EXTRA_STATE = "state";

    @Override
    public void onReceiveBackground(Context context, @NotNull Intent intent) {
        if (!intent.hasExtra(EXTRA_STATE)) return;
        boolean state;
        if (intent.getExtras().get(EXTRA_STATE) instanceof Integer) {
            state = intent.getIntExtra(EXTRA_STATE, 0) == 1;
        } else {
            state = intent.getBooleanExtra(EXTRA_STATE, false);
        }
        Log.w(MiniTaskerActivity.APPTAG, "HDMI is " + (state ? "" : "dis") + "connected");
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        db.setPrivateSetting(HdmiCondition.FIELD_HDMI_PLUGGED, state ? "1" : "0");
        db.runMatchingConditions("hdmi", "1=1", new String[]{},
                (state ? StartType.START_WITH_END : StartType.END));
    }
}
