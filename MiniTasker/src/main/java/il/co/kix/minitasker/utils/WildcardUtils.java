package il.co.kix.minitasker.utils;

import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by phantom on 9/20/14.
 * special functions for the wildcard matching
 */
public class WildcardUtils {

    @NotNull
    public static String toRegex(@Nullable final String wildcard) {
        return commonToRegex(wildcard, "*", "?");
    }

    @NotNull
    public static String phoneVariantToRegex(@Nullable final String wildcard) {
        return commonToRegex(wildcard, "*", ".");
    }

    @NotNull
    private static String commonToRegex(@Nullable final String textToMatch,
                                        @NotNull final String charMatchAll,
                                        @NotNull final String charMatchSingle) {
        if (TextUtils.isEmpty(textToMatch)) return "";
        return String.valueOf(TextUtils.replace(textToMatch,
                new String[]{charMatchAll, charMatchSingle},
                new String[]{".*", "."}));
    }
}
