package il.co.kix.minitasker.actions;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.ForceStopEnabler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskAppsListFragment;

/**
 * terminate applications action
 * <p/>
 * Created by phantom on 26/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class TerminateAction extends PackageBaseAction {
    @NotNull
    @Override
    public String toString() {
        if (root) {
            return context.getString(R.string.action_terminate_individual_root, packageDisplayName);
        } else {
            return context.getString(R.string.action_terminate_individual, packageDisplayName);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "terminate";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (root) {
            return context.getString(R.string.action_terminate_description_root, packageDisplayName);
        } else {
            return context.getString(R.string.action_terminate_description, packageDisplayName);
        }
    }

    @NotNull
    @Override
    public String getRelatedFragmentTag() {
        return NewTaskAppsListFragment.TERMINATE;
    }

    @Override
    public void perform(StartType is_start) {
        if (isRestore()) return;
        //android.permission.KILL_BACKGROUND_PROCESSES
        boolean rootFailed = false;
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
            assert am != null;
            am.killBackgroundProcesses(packageName);
            if (root) {
                ForceStopEnabler forceStop = new ForceStopEnabler(packageName, context);
                rootFailed = !forceStop.execute();
            }
        } catch (ActivityNotFoundException ex) {
            setFlag(FlagsEnum.TERMINATE_FAILED_APP_NOT_FOUND);
            return;
        }
        if (rootFailed) {
            setFlag(FlagsEnum.TERMINATE_FAILED_NO_ROOT);
        }
    }
}
