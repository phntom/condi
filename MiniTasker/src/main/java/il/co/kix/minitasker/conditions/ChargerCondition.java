package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.os.BatteryManager;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskWhenHardwareChargerFragment;

/**
 * charger connection condition
 * <p/>
 * Created by sphantom on 8/2/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class ChargerCondition extends ConditionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "state", SqlDataType.INTEGER
    );

    static final String KEY_LAST_CHARGER_STATE = "LAST_CHARGER_STATE";
    public static final int BATTERY_PLUGGED_WIRELESS = 4;

    private int state;


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        state = myValues.getAsInteger("state");
    }

    @NotNull
    @Override
    public String toString() {
        switch (state) {
            case 0:
                return context.getString(R.string.condition_charger_individual_any);
            case 1:
                return context.getString(R.string.condition_charger_individual_usb);
            case 2:
                return context.getString(R.string.condition_charger_individual_ac);
            case 3:
                return context.getString(R.string.condition_charger_individual_inverse_any);
            case 4:
                return context.getString(R.string.condition_charger_individual_wireless);
            case 5:
                return context.getString(R.string.condition_charger_individual_inverse_any);
            case 6:
                return context.getString(R.string.condition_charger_individual_inverse_usb);
            case 7:
                return context.getString(R.string.condition_charger_individual_inverse_ac);
            case 8:
                return context.getString(R.string.condition_charger_individual_inverse_wireless);
        }
        return "";
    }

    @NotNull
    @Override
    public String getName() {
        return "charger";
    }

    @NotNull
    @Override
    public String getDescription() {
        switch (state) {
            case 0:
                return context.getString(R.string.condition_charger_description_any);
            case 1:
                return context.getString(R.string.condition_charger_description_usb);
            case 2:
                return context.getString(R.string.condition_charger_description_ac);
            case 3:
                return context.getString(R.string.condition_charger_description_inverse_any);
            case 4:
                return context.getString(R.string.condition_charger_description_wireless);
            case 5:
                return context.getString(R.string.condition_charger_description_inverse_any);
            case 6:
                return context.getString(R.string.condition_charger_description_inverse_usb);
            case 7:
                return context.getString(R.string.condition_charger_description_inverse_ac);
            case 8:
                return context.getString(R.string.condition_charger_description_inverse_wireless);
        }
        return "";

    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenHardwareChargerFragment.class;
    }

    @Override
    public void init() {
        state = 0;
    }

    @NotNull
    public String getRelatedFragmentTag() {
        return "Charger";
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        myValues.put("state", state);
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        Thread.sleep(1000);
        try {
            IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent targetIntent = context.registerReceiver(null, intentFilter);
            if (targetIntent == null) return false;
            int plugged = targetIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean is_ac = (plugged & BatteryManager.BATTERY_PLUGGED_AC) != 0;
            boolean is_usb = (plugged & BatteryManager.BATTERY_PLUGGED_USB) != 0;
            boolean is_wireless = (plugged & BATTERY_PLUGGED_WIRELESS) != 0;
            db.setPrivateSetting(KEY_LAST_CHARGER_STATE, "" + plugged);

            switch (state) {
                case 0:
                    return is_start ? (is_usb || is_ac || is_wireless)
                            : (!is_usb && !is_ac && !is_wireless);
                case 1:
                    return is_start ? is_usb : !is_usb;
                case 2:
                    return is_start ? is_ac : !is_ac;
                case 3:
                case 5:
                    return is_start ? (!is_usb && !is_ac && !is_wireless)
                            : (is_usb || is_ac || is_wireless);
                case 4:
                    return is_start ? is_wireless : !is_wireless;
                case 6:
                    return is_start ? !is_usb : is_usb;
                case 7:
                    return is_start ? !is_ac : is_ac;
                case 8:
                    return is_start ? !is_wireless : is_wireless;
            }
        } catch (ReceiverCallNotAllowedException ignored) {
            Log.e(APPTAG, "Error checking power cord status: ReceiverCallNotAllowedException");
        }
        return false;
    }
}
