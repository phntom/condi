package il.co.kix.minitasker.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.co.kix.minitasker.R;

/**
 * activity for expendable lists
 * <p/>
 * Created by phantom on 24/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class ExpandableListActivity extends MiniTaskerActivity implements ExpandableListView.OnChildClickListener {

    protected static final String ROOT_NAME = "ROOT_NAME";
    protected static final String CHILD_NAME = "CHILD_NAME";
    protected static final String CHILD_DESC = "CHILD_DESC";

    protected ExpandableListView lstMain;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstMain = (ExpandableListView) findViewById(R.id.lstMain);
        List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> listOfChildGroups = new ArrayList<List<Map<String, String>>>();
        final String[] groupArray = getResources().getStringArray(getGroupTitleArray());
        final int groupCount = groupArray.length;
        for (final String groupTitle : groupArray) {
            groupData.add(new HashMap<String, String>() {{
                put(ROOT_NAME, groupTitle);
            }});
        }
        final int groupLayout = R.layout.expandable_list_header;
        final String[] groupFrom = new String[]{ROOT_NAME};
        final int[] groupTo = new int[]{android.R.id.text1};
        for (int group = 0; group < groupCount; group++) {
            List<Map<String, String>> childGroup = new ArrayList<Map<String, String>>();
            final String[] childTitleArray = getResources().getStringArray(getChildTitleArray(group));
            final String[] childDescArray = getResources().getStringArray(getChildDescArray(group));
            for (int i = 0; i < childTitleArray.length; i++) {
                final String childTitle = childTitleArray[i];
                final String childDesc = childDescArray[i];
                childGroup.add(new HashMap<String, String>() {{
                    put(CHILD_NAME, childTitle);
                    put(CHILD_DESC, childDesc);
                }});
            }
            listOfChildGroups.add(childGroup);
        }
        final int childLayout = R.layout.expandable_list_item_2;
        final String[] childFrom = new String[]{CHILD_NAME, CHILD_DESC};
        final int[] childTo = new int[]{android.R.id.text1, android.R.id.text2};
        final SimpleExpandableListAdapter simpleExpandableListAdapter =
                new SimpleExpandableListAdapter(
                        this, //context
                        groupData,
                        groupLayout,
                        groupFrom,
                        groupTo,
                        listOfChildGroups,
                        childLayout,
                        childFrom,
                        childTo
                );
        lstMain.setAdapter(simpleExpandableListAdapter);
        for (int group = 0; group < groupCount; group++) {
            lstMain.expandGroup(group);
        }
//        lstMain.setOnItemClickListener(this);
//        lstMain.setOnItemLongClickListener(this);
        lstMain.setOnChildClickListener(this);
    }

    protected abstract int getGroupTitleArray();

    protected abstract int getChildTitleArray(int group);

    protected abstract int getChildDescArray(int group);

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_expandable_list;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//        Log.w(APPTAG, "onChildClick group: " + groupPosition+ " child: " + childPosition + " id:" + id);
        return onItemClick(groupPosition, childPosition);
    }

    protected abstract boolean onItemClick(int groupPosition, int childPosition);

//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.w(APPTAG, "onItemClick " + position + " id:" + id);
//    }
//
//    @Override
//    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.w(APPTAG, "onItemLongClick " + position + " id:" + id);
//        return false;
//    }
}
