package il.co.kix.minitasker;

/**
 * holds type values for activity detection
 *
 * Created by phantom on 08/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum DetectedActivityType {
    IN_VEHICLE,
    ON_BICYCLE,
    ON_FOOT,
    STILL,
    UNKNOWN,
    TILTING;

    public int toDatabase() {
        return 1 << (this.ordinal());
    }

}
