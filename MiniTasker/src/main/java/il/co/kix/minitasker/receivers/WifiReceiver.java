package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;

/**
 * handles wifi connectivity changes
 * <p/>
 * Created by phantom on 29/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class WifiReceiver extends BackgroundBroadcastReceiver {
    static final String FIELD_CONNECTED = "FIELD_CONNECTED";
    static final String FIELD_SSID = "FIELD_SSID";
    static final String FIELD_BSSID = "FIELD_BSSID";
    static final String FIELD_IP = "FIELD_IP";

    @Override
    public void onReceiveBackground(@NotNull Context context, Intent intent) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI
                && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
            WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo currentWifi = mainWifi.getConnectionInfo();
            // Wifi is connected
            String ssid = currentWifi.getSSID();
            if (ssid == null) ssid = "";
            String bssid = currentWifi.getBSSID();
            if (bssid == null) bssid = "";
            int ip_int = currentWifi.getIpAddress();
            String ip = (ip_int == 0 ? "" : String.format("%d.%d.%d.%d",
                    (ip_int & 0xff),
                    (ip_int >> 8 & 0xff),
                    (ip_int >> 16 & 0xff),
                    (ip_int >> 24 & 0xff)));
            if (ip == null) ip = "";

            db.setPrivateSetting(FIELD_SSID, ssid);
            db.setPrivateSetting(FIELD_BSSID, bssid);
            db.setPrivateSetting(FIELD_IP, ip);
            db.setPrivateSetting(FIELD_CONNECTED, "1");

            final String[] parameters = new String[]{ssid, bssid, ip};
            db.runMatchingConditions("wifi", "(((? NOT LIKE z.ssid OR ? NOT LIKE z.bssid OR ? NOT LIKE z.ip) AND z.inverse = 0) OR (? LIKE z.ssid AND ? LIKE z.bssid AND ? LIKE z.ip AND z.inverse = 1))", parameters, StartType.END);
            db.runMatchingConditions("wifi", "((? LIKE z.ssid AND ? LIKE z.bssid AND ? LIKE z.ip AND z.inverse = 0) OR ((? NOT LIKE z.ssid OR ? NOT LIKE z.bssid OR ? NOT LIKE z.ip) AND z.inverse = 1))", parameters, StartType.START_WITH_END);
        } else {
            final String[] parameters = new String[]{};
            db.runMatchingConditions("wifi", "(z.inverse = 0)", parameters, StartType.END);
            db.runMatchingConditions("wifi", "(z.inverse = 1)", parameters, StartType.START_WITH_END);
        }

    }

}
