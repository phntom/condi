package il.co.kix.minitasker;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.HeterogeneousExpandableList;
import android.widget.Switch;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * the new main list adapter
 * <p/>
 * Created by phantom on 28/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MainAdapter extends BaseExpandableListAdapter implements HeterogeneousExpandableList,
        View.OnLongClickListener, View.OnTouchListener {

    public static final int TAG_KEY_GROUP_POSITION = R.id.text1;
    public static final int TAG_KEY_CHILD_POSITION = R.id.edit_id;
    @NotNull
    private final DatabaseHandler db;
    private final Context context;
    @Nullable
    private Cursor tasks;
    @Nullable
    private Cursor[] children;
    private boolean dragging = false;

    public MainAdapter(Context context) {
        super();
        this.context = context;
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        reloadAllData();
    }

    @Override
    public int getGroupCount() {
        if (tasks == null) return 1;
        return tasks.getCount() + 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        Cursor cursor = (Cursor) getGroup(groupPosition);
        assert children != null;
        if (cursor == null || children[groupPosition] == null) return 0;
        return children[groupPosition].getCount();
    }

    @Nullable
    @Override
    public Object getGroup(int groupPosition) {
        if (tasks == null) return null;
        if (!tasks.moveToPosition(groupPosition)) return null;
        assert children != null;
        if (children[groupPosition] == null) {
            final int what_id = tasks.getInt(tasks.getColumnIndexOrThrow("what_id"));
            final int when_id = tasks.getInt(tasks.getColumnIndexOrThrow("when_id"));
            children[groupPosition] = db.getEntityCursor(what_id, when_id);
            if (null == children[groupPosition]) return null;
            if (!children[groupPosition].moveToFirst()) return null;
        }
        return tasks;
    }

    @Nullable
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (null == getGroup(groupPosition)) return null;
        assert children != null;
        Cursor cursor = children[groupPosition];
        if (childPosition < 0 || cursor.getCount() <= childPosition) return null;
        if (!cursor.moveToPosition(childPosition)) return null;
        return cursor;
    }

    @Override
    public long getGroupId(int groupPosition) {
        Cursor cursor = (Cursor) getGroup(groupPosition);
        if (cursor == null) return 0;
        return cursor.getInt(cursor.getColumnIndexOrThrow("task_id"));
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        Cursor cursor = (Cursor) getChild(groupPosition, childPosition);
        if (cursor == null) return 0;
        return cursor.getInt(cursor.getColumnIndexOrThrow("eid"));
    }

    public FlagsEnum getFlag(int groupPosition, int childPosition) {
        Cursor cursor = (Cursor) getChild(groupPosition, childPosition);
        if (cursor == null) return FlagsEnum.UNKNOWN;
        try {
            int flagPosition = cursor.getInt(cursor.getColumnIndexOrThrow("flag"));
            if (flagPosition < 1 || flagPosition >= FlagsEnum.values().length)
                return FlagsEnum.UNKNOWN;
            return FlagsEnum.values()[flagPosition];
        } catch (IllegalArgumentException ignored) {
            return FlagsEnum.UNKNOWN;
        }
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Nullable
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, @Nullable View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        int type = getGroupType(groupPosition);
        switch (type) {
            case 1:
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.main_list_group_add_task, null);
                }
                return convertView;
            case 0:
            default:
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.main_list_group_regular, null);
                    if (convertView == null) return null;
                    Switch switch1 = (Switch) convertView.findViewById(R.id.switch1);
                    switch1.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener) context);
                }
                Cursor groupElement = (Cursor) getGroup(groupPosition);
                if (groupElement == null) return null;
                TextView text1 = (TextView) convertView.findViewById(R.id.text1);
                if (text1 != null)
                    text1.setText(groupElement.getString(groupElement.getColumnIndexOrThrow("description")));
                Switch switch1 = (Switch) convertView.findViewById(R.id.switch1);
                if (switch1 != null) {
                    switch1.setTag(-2);
                    switch1.setChecked(groupElement.getInt(groupElement.getColumnIndexOrThrow("enabled")) == 1);
                    switch1.setTag(groupPosition);
                }
                boolean isSelected = groupElement.getInt(groupElement.getColumnIndexOrThrow("selected")) == 1;
//                if (isSelected != convertView.isSelected()) {
                convertView.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
                if (isSelected) {
                    convertView.setBackgroundResource(R.drawable.main_group_selector_selected);
                } else {
                    convertView.setBackgroundResource(R.drawable.main_group_selector);
                }
//                }
                convertView.setSelected(isSelected);
                return convertView;
        }
    }

    @Nullable
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, @Nullable View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        int type = getChildType(groupPosition, childPosition);
        Cursor childElement = (Cursor) getChild(groupPosition, childPosition);
        TextView textView = null;
        if (childElement == null) return null;
        if (convertView == null) {
            switch (type) {
                case 0:
                case 2:
                case 4:
                    convertView = inflater.inflate(R.layout.main_list_child_header, null);
                    textView = (TextView) (convertView != null
                            ? convertView.findViewById(android.R.id.text1) : null);
                    break;
                case 1:
                case 3:
                case 5:
                    convertView = inflater.inflate(R.layout.main_list_child_regular, null);
                    if (convertView != null) {
                        convertView.setOnDragListener((View.OnDragListener) context);
//                        convertView.setOnLongClickListener(this);
                        convertView.setOnTouchListener(this);
                    }
                    break;
                case 6:
                    convertView = inflater.inflate(R.layout.main_list_child_footer, null);
                    break;

            }
            if (convertView == null) return null;
            convertView.setTag((type == 6 ? (int) getGroupId(groupPosition) : type));
            switch (type) {
                case 0:
                    textView.setText(context.getString(R.string.main_activity_child_header_start_actions));
                    break;
                case 2:
                    textView.setText(context.getString(R.string.main_activity_child_header_end_actions));
                    break;
                case 4:
                    textView.setText(context.getString(R.string.main_activity_child_header_conditions));
                    break;
            }
        }
        convertView.setTag(TAG_KEY_GROUP_POSITION, groupPosition);
        convertView.setTag(TAG_KEY_CHILD_POSITION, childPosition);
        switch (type) {
            case 1:
            case 3:
            case 5:
                textView = (TextView) convertView.findViewById(R.id.text1);
                if (textView != null) {
                    String text = childElement.getString(childElement.getColumnIndexOrThrow("description"));
                    if (text == null) text = "";
                    textView.setText(text);
                }
                View flagView = convertView.findViewById(R.id.btnFlag);
                flagView.setVisibility(0 == childElement.getInt(childElement.getColumnIndexOrThrow("flag")) ? View.GONE : View.VISIBLE);
                break;
        }
        return convertView;

    }

    @Override
    public int getGroupType(int groupPosition) {
        if (tasks == null || groupPosition == tasks.getCount()) return 1;
        return 0;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        Cursor childElement = (Cursor) getChild(groupPosition, childPosition);
        if (childElement == null) return 0;
        return childElement.getInt(childElement.getColumnIndexOrThrow("kind"));
    }

    @Override
    public int getChildTypeCount() {
        return 7;
    }

    @Override
    public int getGroupTypeCount() {
        return 2;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    void reloadAllData() {
        super.notifyDataSetInvalidated();
        closeCursors();
        tasks = db.getTasksCursor();
        if (tasks == null) return;
        if (!tasks.moveToFirst()) return;
        children = new Cursor[tasks.getCount()];
    }

    public void closeCursors() {
        if (children != null) {
            for (Cursor cursor : children) {
                if (cursor != null) cursor.close();
            }
            children = null;
        }
        if (tasks != null) {
            tasks.close();
            tasks = null;
        }
    }

    public void closeCursor(int groupPosition) {
        if (children == null) return;
        if (groupPosition >= children.length) return;
        if (children[groupPosition] == null) return;
        children[groupPosition].close();
        children[groupPosition] = null;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        if (tasks == null) return;
        tasks.close();
        tasks = db.getTasksCursor();
    }

    @Override
    public void notifyDataSetInvalidated() {
        reloadAllData();
    }

    public boolean isGroupSelected(int groupPosition) {
        Cursor t = (Cursor) getGroup(groupPosition);
        return t != null && t.getInt(t.getColumnIndexOrThrow("selected")) == 1;
    }

    public void setGroupSelected(int groupPosition, boolean selected) {
        int task_id = (int) getGroupId(groupPosition);
        if (groupPosition >= getGroupCount())
            throw new IndexOutOfBoundsException();
        if (task_id == 0) return;
        db.setTaskSelected(task_id, selected);
        notifyDataSetChanged();
    }

    public boolean isGroupExpanded(int groupPosition) {
        Cursor t = (Cursor) getGroup(groupPosition);
        return t != null && t.getInt(t.getColumnIndexOrThrow("expanded")) == 1;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
        db.setTaskExpanded((int) getGroupId(groupPosition), true);
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
        db.setTaskExpanded((int) getGroupId(groupPosition), false);
    }

    @Override
    public boolean onLongClick(@Nullable View view) {
        if (view == null) return false;
        dragging = true;
        return true;
    }

    @Override
    public boolean onTouch(@NotNull View v, @NotNull MotionEvent event) {
//        Log.e("MiniTasker", event.toString());
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                dragging = true;
                return true;
//                break;
            case MotionEvent.ACTION_CANCEL:
                dragging = false;
                int coordinates[] = new int[2];
                v.getLocationOnScreen(coordinates);
                coordinates[1] += (v.getHeight() / 2);
                boolean up = (coordinates[1] > (int) event.getRawY());
                Integer type = (Integer) v.getTag();
                Integer groupPosition = (Integer) v.getTag(TAG_KEY_GROUP_POSITION);
                Integer childPosition = (Integer) v.getTag(TAG_KEY_CHILD_POSITION);
                if (type == null || groupPosition == null || childPosition == null) return true;
                String entity_type = (type == 5 ? "when" : "what");
                Cursor c = (Cursor) getChild(groupPosition, childPosition);
                if (c == null) return true;
                int entity_id = c.getInt(c.getColumnIndexOrThrow("entity_id"));
                long rowid = c.getInt(c.getColumnIndexOrThrow("eid"));
                int old_idx = c.getInt(c.getColumnIndexOrThrow("idx"));
                int task_id = (int) getGroupId(groupPosition);

                if (db.entityUpdateIndex(entity_type, entity_id, rowid, up, old_idx, c.getCount(), task_id)) {
                    closeCursor(groupPosition);
                    notifyDataSetChanged();
                }
                break;
            case MotionEvent.ACTION_UP:
                dragging = false;
                Log.d("MiniTasker", "dragging up");
                break;
        }
//        Log.d(MainActivity.APPTAG, "onTouch event: "+event.toString());
        return dragging;
    }

    public boolean isDragging() {
        return dragging;
    }
}
