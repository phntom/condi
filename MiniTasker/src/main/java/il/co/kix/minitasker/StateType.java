package il.co.kix.minitasker;

/**
 * holds type values for on/off/toggle state
 *
 * Created by phantom on 27/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum StateType {
    On,
    Off,
    Toggle
}
