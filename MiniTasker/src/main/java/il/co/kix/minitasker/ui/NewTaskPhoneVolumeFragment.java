package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.VolumeControlAction;

import static il.co.kix.minitasker.actions.VolumeControlAction.STREAM_ALARM;
import static il.co.kix.minitasker.actions.VolumeControlAction.STREAM_MEDIA;
import static il.co.kix.minitasker.actions.VolumeControlAction.STREAM_NOTIFICATION;
import static il.co.kix.minitasker.actions.VolumeControlAction.STREAM_RINGTONE;


/**
 * new task for volume control action
 *
 * Created by phantom on 15/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskPhoneVolumeFragment extends DialogFragment implements DialogInterface.OnClickListener {

    private static final int STREAM_RINGTONE_AND_NOTIFICATION = 4;
    private static final int SEEKBAR_MAX = 255;
    final int[] volume_levels = new int[5];

//    private CheckBox ringtone;
//    private CheckBox mediaCheckBox;
//    private CheckBox alarmCheckBox;
//    private CheckBox ringtoneAndNotificationCheckBox;
//    private CheckBox notificationCheckBox;
//    private CheckBox ringtoneCheckBox;
    final int[] states = new int[5];
    @Nullable
    VolumeControlAction action;
    boolean vibrate;

    AudioManager audioManager;
    private CheckBox chkMedia;
    private CheckBox chkLink;
    private RelativeLayout rlOn;
    private CheckBox chkRingtoneNotification;
    private RelativeLayout rlOff;
    private CheckBox chkRingtone;
    private CheckBox chkNotifications;
    private CheckBox chkAlarms;

    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        MiniTaskerActivity caller = (MiniTaskerActivity)getActivity();
        assert caller != null;

        audioManager = (AudioManager) caller.getSystemService(Context.AUDIO_SERVICE);

        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();

        builder.setTitle(R.string.phone_adjust_volume_control);

        action = new VolumeControlAction();
        action.setContext(caller);
        action.setPrimary(caller.entity_id);
        if (caller.last_entity_type != null
                && !caller.last_entity_type.equals(action.getName())) {
            action = new VolumeControlAction();
            action.setContext(caller);
        }
        action.setTaskId(caller.task_id);

        final View v = inflater.inflate(R.layout.popup_volume_control_main, null);
        assert v != null;

        chkMedia = (CheckBox) v.findViewById(R.id.chkMedia);
        ImageButton imgMedia = (ImageButton) v.findViewById(R.id.imgMedia);
        SeekBar skbMedia = (SeekBar) v.findViewById(R.id.skbMedia);
        chkLink = (CheckBox) v.findViewById(R.id.chkLink);
        rlOn = (RelativeLayout) v.findViewById(R.id.rlOn);
        chkRingtoneNotification = (CheckBox) v.findViewById(R.id.chkRingtoneNotification);
        ImageButton imgRingtoneNotification = (ImageButton) v.findViewById(R.id.imgRingtoneNotification);
        SeekBar skbRingtoneNotification = (SeekBar) v.findViewById(R.id.skbRingtoneNotification);
        rlOff = (RelativeLayout) v.findViewById(R.id.rlOff);
        chkRingtone = (CheckBox) v.findViewById(R.id.chkRingtone);
        ImageButton imgRingtone = (ImageButton) v.findViewById(R.id.imgRingtone);
        SeekBar skbRingtone = (SeekBar) v.findViewById(R.id.skbRingtone);
        chkNotifications = (CheckBox) v.findViewById(R.id.chkNotifications);
        ImageButton imgNotifications = (ImageButton) v.findViewById(R.id.imgNotifications);
        SeekBar skbNotifications = (SeekBar) v.findViewById(R.id.skbNotifications);
        chkAlarms = (CheckBox) v.findViewById(R.id.chkAlarms);
        ImageButton imgAlarms = (ImageButton) v.findViewById(R.id.imgAlarms);
        SeekBar skbAlarms = (SeekBar) v.findViewById(R.id.skbAlarms);

        chkLink.setChecked(action.isLink_ringtone_notification());
        rlOn.setVisibility(action.isLink_ringtone_notification() ? View.VISIBLE : View.GONE);
        rlOff.setVisibility(action.isLink_ringtone_notification() ? View.GONE : View.VISIBLE);

        vibrate = action.isVibrate();

        chkMedia.setChecked(action.isActive(STREAM_MEDIA));
        chkAlarms.setChecked(action.isActive(STREAM_ALARM));
        chkRingtoneNotification.setChecked(action.isActive(STREAM_RINGTONE)
                || action.isActive(STREAM_NOTIFICATION));
        chkRingtone.setChecked(action.isActive(STREAM_RINGTONE));
        chkNotifications.setChecked(action.isActive(STREAM_NOTIFICATION));

        volume_levels[STREAM_MEDIA] = action.getVolume_media();
        volume_levels[STREAM_ALARM] = action.getVolume_alarm();
        volume_levels[STREAM_RINGTONE] = action.getVolume_ringtone();
        volume_levels[STREAM_NOTIFICATION] = action.getVolume_notification();

        setListeners(chkMedia, skbMedia, imgMedia, STREAM_MEDIA);
        setListeners(chkAlarms, skbAlarms, imgAlarms, STREAM_ALARM);
        setListeners(chkRingtone, skbRingtone, imgRingtone, STREAM_RINGTONE);
        setListeners(chkNotifications, skbNotifications, imgNotifications, STREAM_NOTIFICATION);
        setListeners(chkRingtoneNotification, skbRingtoneNotification, imgRingtoneNotification,
                STREAM_RINGTONE);

        chkLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlOn.setVisibility(chkLink.isChecked() ? View.VISIBLE : View.GONE);
                rlOff.setVisibility(chkLink.isChecked() ? View.GONE : View.VISIBLE);
            }
        });

        builder.setView(v);
        builder.setPositiveButton(R.string.ok, this);

        return builder.create();
    }

    public void setListeners(CheckBox check_, @NotNull SeekBar seeker, ImageButton button_,
                             int currentVolumePosition_) {

        final CheckBox check = check_;
        final ImageButton button = button_;
        final int currentVolumePosition = currentVolumePosition_;
        final int state = (check == chkRingtoneNotification ? STREAM_RINGTONE_AND_NOTIFICATION
                : currentVolumePosition);

        seeker.setMax(SEEKBAR_MAX);
        seeker.setProgress(Math.abs(volume_levels[currentVolumePosition]));

        if (vibrate && (state == STREAM_RINGTONE_AND_NOTIFICATION || state == STREAM_RINGTONE)) {
            button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
            button.setImageResource(R.drawable.ic_action_ic_audio_ring_notif_vibrate);
            states[state] = 2;
        } else if (volume_levels[currentVolumePosition] <= 0) {
            button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
            button.setImageResource(R.drawable.ic_volume_muted);
            states[state] = 1;
        } else {
            button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
            if (currentVolumePosition == STREAM_RINGTONE
                    || currentVolumePosition == STREAM_RINGTONE_AND_NOTIFICATION) {
                button.setImageResource(R.drawable.ic_ring_volume);
            } else {
                button.setImageResource(R.drawable.ic_volume_on);
            }
            states[state] = 0;
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
                if (states[state] == 0) {
                    volume_levels[currentVolumePosition] =
                            -1 * Math.abs(volume_levels[currentVolumePosition]);
                    button.setImageResource(R.drawable.ic_volume_muted);
                    vibrate = false;
                    states[state] = 1;
                } else if (states[state] == 2
                        || (currentVolumePosition != STREAM_RINGTONE
                        && currentVolumePosition != STREAM_RINGTONE_AND_NOTIFICATION)) {
                    if (currentVolumePosition == STREAM_RINGTONE
                            || currentVolumePosition == STREAM_RINGTONE_AND_NOTIFICATION) {
                        button.setImageResource(R.drawable.ic_ring_volume);
                    } else {
                        button.setImageResource(R.drawable.ic_volume_on);
                    }
                    volume_levels[currentVolumePosition] =
                            Math.abs(volume_levels[currentVolumePosition]);
                    vibrate = false;
                    states[state] = 0;
                } else {
                    button.setImageResource(R.drawable.ic_action_ic_audio_ring_notif_vibrate);
                    volume_levels[currentVolumePosition] =
                            Math.abs(volume_levels[currentVolumePosition]);
                    vibrate = true;
                    states[state] = 2;
                }
                check.setChecked(true);
            }
        });


        seeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                volume_levels[currentVolumePosition] = i;
                if (i == 0 && states[state] != 1) {
                    states[state] = 1;
                    button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
                    button.setImageResource(R.drawable.ic_volume_muted);
                    vibrate = false;
                } else if (i == 1 && (state == STREAM_RINGTONE_AND_NOTIFICATION
                        || state == STREAM_RINGTONE) && states[state] != 2) {
                    states[state] = 2;
                    button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
                    button.setImageResource(R.drawable.ic_action_ic_audio_ring_notif_vibrate);
                    vibrate = true;
                } else if (i > 0 && states[state] != 0) {
                    states[state] = 0;
                    button.setBackgroundResource(android.R.drawable.screen_background_dark_transparent);
                    if (currentVolumePosition == STREAM_RINGTONE
                            || currentVolumePosition == STREAM_RINGTONE_AND_NOTIFICATION) {
                        button.setImageResource(R.drawable.ic_ring_volume);
                    } else {
                        button.setImageResource(R.drawable.ic_volume_on);
                    }
                    vibrate = false;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                check.setChecked(true);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Do nothing
            }
        });
    }


    @Override
    public void onClick(@NotNull DialogInterface dialogInterface, int i) {
        MiniTaskerActivity callingActivity = (MiniTaskerActivity)getActivity();
        if (callingActivity == null || action == null) return;

        action.setActive(chkMedia.isChecked(), STREAM_MEDIA);
        action.setVolume_media(volume_levels[STREAM_MEDIA]);
        action.setActive(chkAlarms.isChecked(), STREAM_ALARM);
        action.setVolume_alarm(volume_levels[STREAM_ALARM]);
        if (chkLink.isChecked()) {
            action.setLink_ringtone_notification(true);
            action.setActive(chkRingtoneNotification.isChecked(), STREAM_RINGTONE);
            action.setVolume_ringtone(volume_levels[STREAM_RINGTONE]);
            action.setActive(chkRingtoneNotification.isChecked(), STREAM_RINGTONE);
            action.setVolume_notification(volume_levels[STREAM_RINGTONE]);
            vibrate = states[STREAM_RINGTONE_AND_NOTIFICATION] == 2;
        } else {
            action.setLink_ringtone_notification(false);
            action.setActive(chkRingtone.isChecked(), STREAM_RINGTONE);
            action.setVolume_ringtone(volume_levels[STREAM_RINGTONE]);
            action.setActive(chkNotifications.isChecked(), STREAM_NOTIFICATION);
            action.setVolume_notification(volume_levels[STREAM_NOTIFICATION]);
            vibrate = states[STREAM_RINGTONE] == 2;
        }
        if (!action.isActive(STREAM_RINGTONE) && !action.isActive(STREAM_NOTIFICATION)
                && !action.isActive(STREAM_MEDIA) && !action.isActive(STREAM_ALARM)) {
            dialogInterface.dismiss();
            return;
        }
        action.setVibrate(vibrate);
        action.setStart(callingActivity.is_start);
        if (action.save()) {
            action.saveRestoreCopy();
        }
        callingActivity.entity_id = action.getPrimary();
        callingActivity.last_entity_type = action.getName();

        callingActivity.nextActivityWhen(true);
        dialogInterface.dismiss();
    }
}
