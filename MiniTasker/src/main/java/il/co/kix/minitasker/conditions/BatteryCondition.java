package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.text.TextUtils;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;

/**
 * battery is low condition
 * <p/>
 * Created by sphantom on 9/6/13.
 */
public class BatteryCondition extends ConditionBase {
    //TODO: check battery level here

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
    );

    public static final String FIELD_BATTERY_LOW = "FIELD_BATTERY_LOW";

    @Override
    protected void loadLocalValues(ContentValues myValues) throws NullPointerException {

    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.condition_battery_individual);
    }

    @NotNull
    @Override
    public String getName() {
        return "battery";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.condition_battery_description);
    }

    @Nullable
    @Override
    public Class<?> getRelatedActivity() {
        return null;
        // this is intentional
    }

    @Override
    public void init() {
        //Nothing to do here...
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        boolean batteryLow = TextUtils.equals(db.getPrivateSetting(FIELD_BATTERY_LOW, "0"), "1");
        return is_start ? batteryLow : !batteryLow;
    }
}
