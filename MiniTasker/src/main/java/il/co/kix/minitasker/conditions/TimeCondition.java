package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.text.TextUtils;
import android.util.Log;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskWhenTimeAndDateActivity;

/**
 * time condition
 * <p/>
 * Created by phantom on 27/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class TimeCondition extends ConditionBase {

    public enum Type {
        UNKNOWN,
        WEEKLY,
        HOURLY,
        PERIODICAL,
        ONCE
    }

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "starttime", SqlDataType.INTEGER,
            "endtime", SqlDataType.INTEGER,
            "repeat", SqlDataType.SET,
            "ranges", SqlDataType.TEXT,
            "type", SqlDataType.ENUM
    );

    public final static int minutesInAWeek = (int) TimeUnit.DAYS.toMinutes(7);

    int timeStart;
    int timeEnd;
    int timeRepeat;
    RangeSet<Integer> ranges;

    public void init() {
        timeStart = -1;
        timeEnd = -1;
        timeRepeat = 0;
        ranges = TreeRangeSet.create();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        timeStart = myValues.getAsInteger("starttime");
        timeEnd = myValues.getAsInteger("endtime");
        timeRepeat = myValues.getAsInteger("repeat");
        TextUtils.StringSplitter splitter = new TextUtils.SimpleStringSplitter(';');
        final String rangesValue = myValues.getAsString("ranges");
        if (TextUtils.isEmpty(rangesValue)) return;
        splitter.setString(rangesValue);
        for (String range : splitter) {
            if (TextUtils.isEmpty(range)) continue;
            String[] pair = range.split("-");
            if (pair.length != 2) continue;
            int lower = Integer.valueOf(pair[0]);
            int upper = Integer.valueOf(pair[1]);
            ranges.add(Range.closedOpen(lower, upper));
            if (upper >= minutesInAWeek) {
                ranges.add(Range.closedOpen(lower - minutesInAWeek, upper - minutesInAWeek));
            }
        }
    }

    public void setParams(int tStart, int tEnd, int tRepeat) {
        timeStart = tStart;
        timeEnd = tEnd;
        timeRepeat = tRepeat;
        myValues.put("starttime", timeStart);
        myValues.put("endtime", timeEnd);
        myValues.put("repeat", timeRepeat);
        ranges.clear();
        upgradeToRepeatList();
    }

    private String timeDescAux() {
        StringBuilder ret = new StringBuilder();
        int shour = timeStart / 60;
        int smin = timeStart % 60;
        int ehour = timeEnd / 60;
        int emin = timeEnd % 60;
        final boolean has_end = timeEnd >= 0;

        final String[] days = context.getResources().getStringArray(R.array.condition_time_days);

        if (timeRepeat == 127) {
            if (has_end)
                return context.getString(R.string.condition_time_description_daily_with_end,
                        shour, smin, ehour, emin);
            return context.getString(R.string.condition_time_description_daily,
                    shour, smin);
        }

        if (timeRepeat == 0) {
            if (has_end) return context.getString(R.string.condition_time_description_once_with_end,
                    shour, smin, ehour, emin);
            return context.getString(R.string.condition_time_description_once,
                    shour, smin);
        }

        int i = 1, j = 0;
        while (true) {
            while (j < 6 && (i & timeRepeat) == 0) {
                i *= 2;
                j++;
            }
            if ((i & timeRepeat) == 0) break;
            ret.append(" ").append(days[j]);
            i *= 2;
            j++;
            if (j == 7) break;
            if ((i & timeRepeat) == 0) continue;
            while (j < 7 && (i & timeRepeat) != 0) {
                i *= 2;
                j++;
            }
            ret.append("-").append(days[j - 1]);
        }

        if (has_end) return context.getString(R.string.condition_time_description_repeat_with_end,
                shour, smin, ehour, emin, ret.toString());
        return context.getString(R.string.condition_time_description_repeat,
                shour, smin, ret.toString());
    }

    public int getTimeStart() {
        return timeStart;
    }

    @Override
    public boolean save() {
        StringBuilder rangesValue = new StringBuilder();
        Set<Integer> quickLookupTimes = new HashSet<>();
        if (ranges.isEmpty()) upgradeToRepeatList();
        for (Range<Integer> integerRange : ranges.asRanges()) {
            if (rangesValue.length() > 0) rangesValue.append(";");
            rangesValue.append(integerRange.lowerEndpoint());
            rangesValue.append("-");
            rangesValue.append(integerRange.upperEndpoint());
            quickLookupTimes.add(Math.max(0, integerRange.lowerEndpoint()));
            quickLookupTimes.add(Math.min(minutesInAWeek - 1, integerRange.upperEndpoint()));
        }
        myValues.put("ranges", rangesValue.toString());

        boolean result = super.save();
        db.updateTimeConditionQuickLookup(primary, quickLookupTimes);
        db.scheduleNextTimeRangeWhen();
        checkIfRunning();
        return result;
    }

    public void upgradeToRepeatList() {
        final Calendar calendar = Calendar.getInstance();
        TimeCondition.upgradeToRepeatList(calendar, timeStart, timeEnd, timeRepeat, ranges);
    }

    public static void upgradeToRepeatList(Calendar calendar, int timeStart, int timeEnd,
                                           int timeRepeat,
                                           RangeSet<Integer> ranges) {
        //noinspection ResourceType
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1 - calendar.get(Calendar.DAY_OF_WEEK));
        int effectiveEndTime = timeStart < timeEnd ? timeEnd : timeEnd + 1440;
        long startOfWeek = calendar.getTimeInMillis();
        calendar.add(Calendar.MINUTE, timeStart);

        for (int day = 0; day < 7; day++) {
            int dayOfWeekToday = 1 << day;
            if (timeRepeat == 0 || (timeRepeat & dayOfWeekToday) != 0) {
                long millisFromStartOfWeek = calendar.getTimeInMillis() - startOfWeek;
                int rangeStart = (int) TimeUnit.MILLISECONDS.toMinutes(millisFromStartOfWeek);
                calendar.add(Calendar.MINUTE, effectiveEndTime - timeStart);
                millisFromStartOfWeek = calendar.getTimeInMillis() - startOfWeek;
                int rangeEnd = (int) TimeUnit.MILLISECONDS.toMinutes(millisFromStartOfWeek);
                Range<Integer> range = Range.closedOpen(rangeStart, rangeEnd);
                if (!ranges.encloses(range)) {
                    ranges.add(range);
                    if (rangeEnd >= minutesInAWeek) {
                        ranges.add(Range.closedOpen(rangeStart - minutesInAWeek,
                                rangeEnd - minutesInAWeek));
                    }
                }
                calendar.add(Calendar.MINUTE, timeStart - effectiveEndTime);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }

        }
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        if (ranges.isEmpty()) {
            upgradeToRepeatList();
        }
        return TimeCondition.conditionApplies(is_start, ranges, Calendar.getInstance());
    }

    public static boolean conditionApplies(boolean is_start,
                                           RangeSet<Integer> ranges,
                                           Calendar startOfWeek) throws InterruptedException {
        long nowMillis = startOfWeek.getTimeInMillis();
        //noinspection ResourceType
        startOfWeek.set(startOfWeek.get(Calendar.YEAR), startOfWeek.get(Calendar.MONTH),
                startOfWeek.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        startOfWeek.add(Calendar.DAY_OF_MONTH, 1 - startOfWeek.get(Calendar.DAY_OF_WEEK));
        long millisFromStartOfWeek = nowMillis - startOfWeek.getTimeInMillis();
        int minutesFromStartOfWeek = (int) TimeUnit.MILLISECONDS.toMinutes(millisFromStartOfWeek);

        boolean inRange = ranges.contains(minutesFromStartOfWeek);

        return is_start ? inRange : !inRange;
    }

    public int getTimeEnd() {
        return timeEnd;
    }

    public int getTimeRepeat() {
        return timeRepeat;
    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.condition_time_description_container_verbose, timeDescAux());
    }

    @NotNull
    @Override
    public String getName() {
        return "time";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.condition_time_description_container, timeDescAux());
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenTimeAndDateActivity.class;
    }

    @Override
    public void setTaskEnabled(boolean enabled) {
        super.setTaskEnabled(enabled);
        db.scheduleNextTimeRangeWhen();
        if (enabled) checkIfRunning();
    }

    void checkIfRunning() {
        Log.d(APPTAG, "TimeCondition checkIfRunning()");
        StartType targetStartType = startType == StartType.END ? StartType.START_WITH_END : StartType.END;
        try {
            if (!conditionApplies(targetStartType == StartType.START_WITH_END)) return;
            if (conditionApplies(targetStartType == StartType.END)) {
                Log.e(APPTAG, "TimeCondition checkIfRunning() time condition applies both ways start:" + timeStart + " end:" + timeEnd + " repeat:" + timeRepeat);
                return;
            }
            db.runMatchingConditions("time", "w.rowid = ?", new String[]{"" + primary}, targetStartType);
        } catch (InterruptedException ignored) {
        }
    }
}
