package il.co.kix.minitasker.actions;

import android.content.ContentResolver;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * auto sync actions
 * <p/>
 * Created by sphantom on 7/28/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class SyncAction extends StatesAction {
    @NotNull
    @Override
    public String getDeviceName() {
        return "sync";
    }

    @Override
    protected boolean perform_init() {
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        return ContentResolver.getMasterSyncAutomatically() ? StateType.On : StateType.Off;
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.SYNC;
    }

    @Override
    protected boolean perform_on() {
        ContentResolver.setMasterSyncAutomatically(true);
        return true;
    }

    @Override
    protected boolean perform_off() {
        ContentResolver.setMasterSyncAutomatically(false);
        return true;
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.SYNC_FAILED);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_sync_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_sync_individual_on);
            case Off:
                return context.getString(R.string.action_sync_individual_off);
            case Toggle:
                return context.getString(R.string.action_sync_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_sync_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_sync_description_on);
            case Off:
                return context.getString(R.string.action_sync_description_off);
            case Toggle:
                return context.getString(R.string.action_sync_description_toggle);
        }
        return "";
    }
}
