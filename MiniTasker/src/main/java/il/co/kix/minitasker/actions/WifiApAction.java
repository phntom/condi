package il.co.kix.minitasker.actions;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * Created by phantom on 2/25/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class WifiApAction extends StatesAction {

    private WifiManager mWifiManager;

    @NotNull
    @Override
    public String getDeviceName() {
        return "wifiap";
    }

    @Override
    protected boolean perform_init() {
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (mWifiManager == null) setFlag(FlagsEnum.WIFI_MISSING_SERVICE);
        return mWifiManager != null;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        final Class<?> cWifiManager = mWifiManager.getClass();
        try {
            Method mWifiAp = cWifiManager.getDeclaredMethod("isWifiApEnabled");
            mWifiAp.setAccessible(true);
            Boolean result = (Boolean) mWifiAp.invoke(mWifiManager);
            return (result != null && result ? StateType.On : StateType.Off);
        } catch (InvocationTargetException ignore) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (NoSuchMethodException e) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (IllegalAccessException e) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (Exception ignore) {
            setFlag(FlagsEnum.WIFI_AP_ACTION_FAILED);
        }
        return StateType.Off;
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.WIFI_AP;
    }

    protected boolean perform_action(boolean on) {
        try {
            final Class<?> cWifiManager = Class.forName(mWifiManager.getClass().getName());
            Method mWifiAp = cWifiManager.getDeclaredMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            mWifiAp.setAccessible(true);
            Method mWifiGetConfiguration = cWifiManager.getDeclaredMethod("getWifiApConfiguration");
            mWifiGetConfiguration.setAccessible(true);

            Boolean result;
            if (on) {
                WifiConfiguration mWifiConfig =
                        (WifiConfiguration) mWifiGetConfiguration.invoke(mWifiManager);
                if (mWifiConfig == null) return false;
                result = (Boolean) mWifiAp.invoke(mWifiManager, mWifiConfig, true);
            } else {
                result = (Boolean) mWifiAp.invoke(mWifiManager, null, false);
            }
            return result != null && result;
        } catch (InvocationTargetException ignore) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (NoSuchMethodException e) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (IllegalAccessException e) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (ClassNotFoundException e) {
            setFlag(FlagsEnum.WIFI_AP_REFLECTION_FAILED);
        } catch (Exception ignore) {
            setFlag(FlagsEnum.WIFI_AP_ACTION_FAILED);
        }
        return false;
    }

    @Override
    protected boolean perform_on() {
        return perform_action(true);
    }

    @Override
    protected boolean perform_off() {
        return perform_action(false);
    }

    @Override
    protected void perform_fail() {
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_wifi_ap_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_wifi_ap_individual_on);
            case Off:
                return context.getString(R.string.action_wifi_ap_individual_off);
            case Toggle:
                return context.getString(R.string.action_wifi_ap_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_wifi_ap_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_wifi_ap_description_on);
            case Off:
                return context.getString(R.string.action_wifi_ap_description_off);
            case Toggle:
                return context.getString(R.string.action_wifi_ap_description_toggle);
        }
        return "";
    }
}
