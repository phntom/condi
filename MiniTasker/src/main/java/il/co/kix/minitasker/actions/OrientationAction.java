package il.co.kix.minitasker.actions;

import android.provider.Settings;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * Created by phantom on 20/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class OrientationAction extends StatesAction {
    @NotNull
    @Override
    public String getDeviceName() {
        return "orientation";
    }

//    @Override
//    void perform(StartType is_start) {
//        try {
//            int current_orientation =
//                android.provider.Settings.System.getInt(context.getContentResolver(),
//                        Settings.System.ACCELEROMETER_ROTATION, 0);
//            if (isRestore()) {
//                try {
//                    current_orientation = (int) db.getLast(LastType.ORIENTATION);
//                } catch (MiniTaskerException e) {
//                    return;
//                }
//
//            } else {
//                db.putLast(LastType.ORIENTATION, current_orientation);
//                switch (state) {
//                    case On:
//                        current_orientation = 0;
//                        break;
//                    case Off:
//                        current_orientation = 1;
//                        break;
//                    case Toggle:
//                        current_orientation = (current_orientation == 1 ? 0 : 1);
//                        break;
//                }
//            }
//            android.provider.Settings.System.putInt(context.getContentResolver(),
//                    Settings.System.ACCELEROMETER_ROTATION, current_orientation);
//        } catch (Exception ignored) {
//            setFlag(FlagsEnum.ORIENTATION_FAILED);
//        }
//    }

    @Override
    protected boolean perform_init() {
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        int rotation = android.provider.Settings.System.getInt(context.getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0);
        switch (rotation) {
            case 0:
                return StateType.On;
            case 1:
            default:
                return StateType.Off;
        }
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.ORIENTATION;
    }

    @Override
    protected boolean perform_on() {
        return android.provider.Settings.System.putInt(context.getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0);
    }

    @Override
    protected boolean perform_off() {
        return android.provider.Settings.System.putInt(context.getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 1);
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.ORIENTATION_FAILED);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore())
            return context.getString(R.string.action_orientation_lock_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_orientation_lock_individual_on);
            case Off:
                return context.getString(R.string.action_orientation_lock_individual_off);
            case Toggle:
                return context.getString(R.string.action_orientation_lock_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore())
            return context.getString(R.string.action_orientation_lock_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_orientation_lock_description_on);
            case Off:
                return context.getString(R.string.action_orientation_lock_description_off);
            case Toggle:
                return context.getString(R.string.action_orientation_lock_description_toggle);
        }
        return "";
    }
}
