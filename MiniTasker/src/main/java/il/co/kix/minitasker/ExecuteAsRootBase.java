package il.co.kix.minitasker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * super user execution helper
 *
 * Created by phantom on 30/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class ExecuteAsRootBase
{
    private static final String APPTAG = "MiniTasker";

    private final Context mContext;


    protected ExecuteAsRootBase(Context context) {
        mContext = context;
    }

    public static boolean canRunRootCommands()
    {
        boolean retval;
        Process suProcess;

        try
        {
            suProcess = Runtime.getRuntime().exec("su");

            DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
            BufferedReader osRes = new BufferedReader(
                    new InputStreamReader(suProcess.getInputStream()));

            // Getting the id of the current user to check if this is root
            os.writeBytes("id\n");
            os.flush();

            String currUid = osRes.readLine();
            boolean exitSu;
            if (null == currUid)
            {
                retval = false;
                exitSu = false;
                Log.d(APPTAG, "Can't get root access or denied by user");
            }
            else if (currUid.contains("uid=0"))
            {
                retval = true;
                exitSu = true;
                Log.d(APPTAG, "Root access granted");
            }
            else
            {
                retval = false;
                exitSu = true;
                Log.d(APPTAG, "Root access rejected: " + currUid);
            }

            if (exitSu)
            {
                os.writeBytes("exit\n");
                os.flush();
            }
        }
        catch (Exception e)
        {
            // Can't get root !
            // Probably broken pipe exception on trying to write to output stream (os) after su failed, meaning that the device is not rooted

            retval = false;
            Log.d(APPTAG, "Root access rejected [" + e.getClass().getName() + "] : " + e.getMessage());
        }

        return retval;
    }


    public boolean execute()
    {
        boolean retval = canRunRootCommands();
        if (!retval) return false;
        try
        {
            //write alternative settings file
            String settings_content =
                    "# Script to start \"settings\" on the device\n" +
                    "#\n" +
                    "base=/system\n" +
                    "export CLASSPATH=$base/framework/settings.jar\n" +
                    "exec app_process $base/bin com.android.commands.settings.SettingsCmd \"$@\"";
            //noinspection deprecation
            @SuppressLint("WorldReadableFiles")
            FileOutputStream settings_file = mContext.openFileOutput("settings", Context.MODE_WORLD_READABLE);
            settings_file.write(settings_content.getBytes());
            settings_file.close();
            String path = mContext.getFileStreamPath("settings").getAbsolutePath();


            ArrayList<String> commands = getCommandsToExecute();
            if (commands.size() > 0)
            {

                Process suProcess = Runtime.getRuntime().exec("su");

                DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());

                os.writeBytes("chmod +x "+path+"\n");
                path = path.substring(0, path.length() - 9);
                os.writeBytes("export PATH=$PATH:"+path+"\n");
                os.flush();

                // Execute commands that require root access
                for (String currCommand : commands)
                {
                    os.writeBytes(currCommand + "\n");
                    os.flush();
                }

                os.writeBytes("exit\n");
                os.flush();
//                retval = true;

//                try
//                {
//                    int suProcessRetval = suProcess.waitFor();
//                    if (255 != suProcessRetval)
//                    {
//                        // Root access granted
//                        retval = true;
//                    }
//                    else
//                    {
//                        // Root access denied
//                        retval = false;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    Log.e(APPTAG, "Error executing root action", ex);
//                }
            }
        }
        catch (IOException ex)
        {
            Log.w(APPTAG, "Can't get root access", ex);
        }
        catch (SecurityException ex)
        {
            Log.w(APPTAG, "Can't get root access", ex);
        }
        catch (Exception ex)
        {
            Log.w(APPTAG, "Error executing internal operation", ex);
        }

        return true;
    }
    @NotNull
    protected abstract ArrayList<String> getCommandsToExecute();
}
