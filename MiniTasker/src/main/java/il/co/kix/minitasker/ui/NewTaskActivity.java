package il.co.kix.minitasker.ui;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;

/**
 * activity for creating new tasks
 *
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskActivity extends ExpandableListActivity {
    @Override
    int resourceActivityTitle() {
        if (isNotEditMode())
            return R.string.new_task_title;
        else
            return R.string.new_task_action_title;
    }

    @Override
    protected int getGroupTitleArray() {
        return R.array.new_task_group_titles;
    }

    @Override
    protected int getChildTitleArray(int group) {
        switch (group) {
            case 0:
                return R.array.new_task_child_3_titles;
            case 1:
                return R.array.new_task_child_2_titles;
            case 2:
                return R.array.new_task_child_1_titles;
            default:
                return 0;
        }
    }

    @Override
    protected int getChildDescArray(int group) {
        switch (group) {
            case 0:
                return R.array.new_task_child_3_descriptions;
            case 1:
                return R.array.new_task_child_2_descriptions;
            case 2:
                return R.array.new_task_child_1_descriptions;
            default:
                return 0;
        }
    }

    @Override
    protected boolean onItemClick(int groupPosition, int childPosition) {
        if (task_id == 0) {
            DatabaseHandler db = SingletonDatabase.INSTANCE.db;
            task_id = db.startNewTask();
        }
        switch (groupPosition) {
            case 0:
                switch (childPosition) {
                    case 0:
                        nextActivity(NewTaskAppsMusicActivity.class, false);
                        break;
                    case 1:
                        new NewTaskAppsListFragment()
                                .show(getFragmentManager(), NewTaskAppsListFragment.LAUNCH);
                        break;
                    case 2:
                        new NewTaskNavigationFragment()
                                .show(getFragmentManager(), APPTAG);
                        break;
                }
                break;
            case 1:
                switch (childPosition) {
                    case 0:
                        new NewTaskPhoneVolumeFragment()
                                .show(getFragmentManager(), APPTAG);
                        break;
                    case 1:
                        nextActivity(NewTaskCallBlockActivity.class, false);
                        break;
                    case 2:
                        nextActivity(NewTaskSmsForwardActivity.class, false);
                        break;
                    case 3:
                        new NewTaskPhoneNotificationFragment()
                                .show(getFragmentManager(), APPTAG);
                        break;
                    case 4:
                        nextActivity(NewTaskPhoneSmsActivity.class, false);
                        break;
                }
                break;
            case 2:
                switch (childPosition) {
                    case 0:
                        nextActivity(NewTaskBatterySensorsActivity.class, false);
                        break;
                    case 1:
                        new NewTaskBatteryEmergencyBrightnessFragment()
                                .show(getFragmentManager(), APPTAG);
                        break;
                    case 2:
                        new NewTaskAppsListFragment()
                                .show(getFragmentManager(), NewTaskAppsListFragment.TERMINATE);
                        break;
                }
                break;

        }
        return true;
    }
}
