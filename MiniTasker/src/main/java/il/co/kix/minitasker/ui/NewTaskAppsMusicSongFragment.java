package il.co.kix.minitasker.ui;

import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;

/**
 * new task for music action - play a song
 * <p/>
 * Created by phantom on 28/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskAppsMusicSongFragment extends NewTaskAppsMusicPlaylistFragment {
    @Nullable
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                caller,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{
                        MediaStore.MediaColumns._ID,
                        MediaStore.MediaColumns.TITLE
                },
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (listView == null) return;
        SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) listView.getAdapter();
        if (oldAdapter != null) {
            Cursor oldCursor = oldAdapter.swapCursor(data);
            if (oldCursor != null)
                oldCursor.close();
        } else {
            SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                    caller,
                    R.layout.list_item_1,
                    data,
                    new String[]{MediaStore.MediaColumns.TITLE},
                    new int[]{android.R.id.text1},
                    0
            );
            listView.setAdapter(cursorAdapter);
        }
        loadingView.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (listView == null) return;
        SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) listView.getAdapter();
        if (oldAdapter != null) {
            Cursor oldCursor = oldAdapter.swapCursor(null);
            if (oldCursor != null)
                oldCursor.close();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final long songId = listView.getAdapter().getItemId(position);
        TextView itemView = (TextView) view;
        //noinspection ConstantConditions
        action.setSongName(itemView.getText().toString());
        //noinspection ConstantConditions
        Log.w(MiniTaskerActivity.APPTAG, "Song " + itemView.getText().toString() + " id is " + songId);
        action.setSongId(songId);
        if (action.save()) {
            action.saveRestoreCopy();
        }
        assert caller != null;
        caller.entity_id = action.getPrimary();
        caller.nextActivityWhen(false);
        mDialog.dismiss();
    }

    @Override
    protected int getDialogTitle() {
        return R.string.new_task_choose_a_song;
    }
}
