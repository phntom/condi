package il.co.kix.minitasker;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * data structure for application list fragments
 * <p/>
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class AppInfo implements Comparable {
    private String packageName = "";
    private String packageDisplayName = "";
    // --Commented out by Inspection (28/09/13 23:45):private Drawable icon = null;
    @Nullable
    private ApplicationInfo applicationInfo = null;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageDisplayName() {
        return packageDisplayName;
    }

    public void setPackageDisplayName(String packageDisplayName) {
        this.packageDisplayName = packageDisplayName;
    }

    @Nullable
    public Drawable getIcon(@NotNull Context context, @NotNull PackageManager packageManager) {
        if (applicationInfo == null) {
            return context.getResources().getDrawable(android.R.drawable.sym_def_app_icon);
        }
        return applicationInfo.loadIcon(packageManager);
    }

    public void setApplicationInfo(@Nullable ApplicationInfo applicationInfo) {
        if (applicationInfo == null) return;
        this.applicationInfo = applicationInfo;
    }

    @Override
    public int compareTo(@Nullable Object another) {
        if (another == null) return 0;
        AppInfo a = (AppInfo) another;
        return packageDisplayName.compareTo(a.getPackageDisplayName());
    }
}
