package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;

/**
 * Created by phantom on 10/11/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class TaskLabelDialog extends DialogFragment
        implements DialogInterface.OnClickListener {
    int task_id = 0;
    private EditText txtLabel;
    private DatabaseHandler db;

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MiniTaskerActivity caller = (MiniTaskerActivity) getActivity();
        if (caller == null) return null;
        Integer tag = Integer.valueOf(getTag());
        if (tag == null) return null;
        task_id = tag;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        builder.setTitle(R.string.task_label_title);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.task_label_positive, this);
        final View view = caller.getLayoutInflater().inflate(R.layout.task_label, null);
        if (view == null) return null;
        txtLabel = (EditText) view.findViewById(R.id.txtLabel);
        db = SingletonDatabase.INSTANCE.db;
        String label = db.getTaskLabel(task_id);
        if (!label.isEmpty()) {
            txtLabel.setText(label);
            txtLabel.setSelection(0, label.length());
        }
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (txtLabel == null) return;
        Editable editable = txtLabel.getText();
        if (editable == null) return;
        String label = editable.toString();
        if (label == null) return;
        db.setTaskLabel(task_id, label);
        MainActivity caller = (MainActivity) getActivity();
        if (caller != null) {
            caller.refreshAdapter();
        }
    }
}
