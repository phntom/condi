package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskWhenHardwareHeadsetFragment;

/**
 * headset connection condition
 * <p/>
 * Created by sphantom on 7/30/13.
 * <p/>
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class HeadsetCondition extends ConditionBase {
    private static final String KEY_LAST_HEADSET_STATE = "LAST_HEADSET_STATE";
    private static final String KEY_LAST_HEADSET_NAME = "LAST_HEADSET_NAME";
    private static final String KEY_LAST_HEADSET_MIC = "LAST_HEADSET_MIC";
    protected int state;

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "state", SqlDataType.INTEGER
    );

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        myValues.put("state", state);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        state = myValues.getAsInteger("state");
    }

    @NotNull
    @Override
    public String toString() {
        switch (state) {
            case 0:
                return context.getString(R.string.condition_headset_individual_any);
            case 1:
                return context.getString(R.string.condition_headset_individual_with_mic);
            case 2:
                return context.getString(R.string.condition_headset_individual_no_mic);
            case 3:
                return context.getString(R.string.condition_headset_individual_inverse_any);
            case 4:
                return context.getString(R.string.condition_headset_individual_inverse_with_mic);
            case 5:
                return context.getString(R.string.condition_headset_individual_inverse_no_mic);
        }
        return "";
    }

    @NotNull
    @Override
    public String getName() {
        return "headset";
    }

    @NotNull
    @Override
    public String getDescription() {
        switch (state) {
            case 0:
                return context.getString(R.string.condition_headset_description_any);
            case 1:
                return context.getString(R.string.condition_headset_description_with_mic);
            case 2:
                return context.getString(R.string.condition_headset_description_no_mic);
            case 3:
                return context.getString(R.string.condition_headset_description_inverse_any);
            case 4:
                return context.getString(R.string.condition_headset_description_inverse_with_mic);
            case 5:
                return context.getString(R.string.condition_headset_description_inverse_no_mic);
        }
        return "";
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenHardwareHeadsetFragment.class;
    }

    @Override
    public void init() {
        state = 0;
    }

    @NotNull
    public String getRelatedFragmentTag() {
        return "Headset";
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        Thread.sleep(1000);
        try {
            IntentFilter intentFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
            Intent targetIntent = context.registerReceiver(null, intentFilter);
            if (targetIntent == null) return false;
            final boolean headsetPlugged = targetIntent.getIntExtra("state", 0) == 1;
            final int microphonePresent = targetIntent.getIntExtra("microphone", -1);
            final String headsetType = "" + targetIntent.getStringExtra("name");

            db.setPrivateSetting(KEY_LAST_HEADSET_STATE, headsetPlugged ? "1" : "0");
            db.setPrivateSetting(KEY_LAST_HEADSET_NAME, headsetType);
            db.setPrivateSetting(KEY_LAST_HEADSET_MIC, "" + microphonePresent);

            switch (state) {
                case 0:
                    return is_start ? headsetPlugged
                            : !headsetPlugged;
                case 1:
                    return is_start ? headsetPlugged && (Math.abs(microphonePresent) == 1)
                            : !headsetPlugged;
                case 2:
                    return is_start ? headsetPlugged && (microphonePresent < 1)
                            : !headsetPlugged;
                case 3:
                    return is_start ? !headsetPlugged
                            : headsetPlugged;
                case 4:
                    return is_start ? !headsetPlugged
                            : headsetPlugged && (Math.abs(microphonePresent) == 1);
                case 5:
                    return is_start ? !headsetPlugged
                            : headsetPlugged && (microphonePresent < 1);

            }
        } catch (ReceiverCallNotAllowedException ignored) {
            Log.e(APPTAG, "Error checking power cord status: ReceiverCallNotAllowedException");
        }
        return false;
    }
}
