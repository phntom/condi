package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.MediaAction;

/**
 * new task for music action - play a playlist
 * <p/>
 * Created by phantom on 28/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskAppsMusicPlaylistFragment extends DialogFragment
        implements LoaderManager.LoaderCallbacks<Cursor>,
        DialogInterface.OnClickListener,
        AdapterView.OnItemClickListener {

    protected Dialog mDialog;
    protected View loadingView;
    protected ListView listView;
    @Nullable
    MiniTaskerActivity caller;
    @Nullable
    MediaAction action;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();
        final View v = inflater.inflate(R.layout.list_activity, null);
        assert v != null;
        loadingView = v.findViewById(R.id.pbrLoading);
        listView = (ListView) v.findViewById(R.id.list);
        builder.setView(v);
        builder.setTitle(getDialogTitle());
        action = new MediaAction();
        action.setContext(caller);
        action.setPrimary(caller.entity_id);
        action.setTaskId(caller.task_id);
        builder.setNegativeButton(R.string.cancel, this);
        listView.setOnItemClickListener(this);

        LoaderManager loaderManager = getLoaderManager();
        assert loaderManager != null;
        Loader loader = getLoaderManager().getLoader(0);
        if (loader != null && loader.isReset()) {
            getLoaderManager().restartLoader(0, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        }

        mDialog = builder.create();
        return mDialog;
    }

    protected int getDialogTitle() {
        return R.string.new_task_choose_a_playlist;
    }

    @Nullable
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                caller,
                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[]{
                        MediaStore.Audio.Playlists._ID,
                        MediaStore.Audio.Playlists.NAME
                },
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (listView == null) return;
        SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) listView.getAdapter();
        if (oldAdapter != null) {
            Cursor oldCursor = oldAdapter.swapCursor(data);
            if (oldCursor != null)
                oldCursor.close();
        } else {
            SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                    caller,
                    R.layout.list_item_1,
                    data,
                    new String[]{MediaStore.Audio.Playlists.NAME},
                    new int[]{android.R.id.text1},
                    0
            );
            listView.setAdapter(cursorAdapter);
        }
        loadingView.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (listView == null) return;
        SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) listView.getAdapter();
        if (oldAdapter != null) {
            Cursor oldCursor = oldAdapter.swapCursor(null);
            if (oldCursor != null)
                oldCursor.close();
        }
        listView.setAdapter(null);
        listView.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(@NotNull DialogInterface dialog, int which) {
        dialog.dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final long playlistId = listView.getAdapter().getItemId(position);
        TextView itemView = (TextView) view;
        //noinspection ConstantConditions
        action.setPlaylist(itemView.getText().toString());
        //noinspection ConstantConditions
        Log.w(MiniTaskerActivity.APPTAG, "Playlist " + itemView.getText().toString() + " id is " + playlistId);
        action.setPlaylistId(playlistId);
        if (action.save()) {
            action.saveRestoreCopy();
        }
        assert caller != null;
        caller.entity_id = action.getPrimary();
        caller.nextActivityWhen(false);
        mDialog.dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (listView == null) return;
        SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) listView.getAdapter();
        if (oldAdapter != null) {
            Cursor oldCursor = oldAdapter.swapCursor(null);
            if (oldCursor != null)
                oldCursor.close();
        }
    }
}

