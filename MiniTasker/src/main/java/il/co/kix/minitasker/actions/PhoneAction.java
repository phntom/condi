package il.co.kix.minitasker.actions;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.ActivityUtils;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.MiniTaskerException;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.receivers.BackgroundBroadcastReceiver;
import il.co.kix.minitasker.ui.NewTaskCallBlockActivity;
import il.co.kix.minitasker.ui.NewTaskSmsForwardActivity;

/**
 * phone calls receive action
 * <p/>
 * Created by phantom on 24/08/13.
 * this action is for phone blocking (call rejecting)
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class PhoneAction extends ActionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "target", SqlDataType.TEXT,
            "contact", SqlDataType.TEXT
    );

    public static final String KEY_PREVIOUS_MESSAGE =
            "il.co.kix.minitasker.KEY_PREVIOUS_MESSAGE";
    public static final String KEY_PREVIOUS_ORIGIN =
            "il.co.kix.minitasker.KEY_PREVIOUS_ORIGIN";
    @Nullable
    private String targetNumber;
    private String targetContact;

    @Override
    public void perform(StartType is_start) {
        if (targetNumber == null) return;
        if (targetNumber.isEmpty()) {
            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            if (isRestore()) {
                try {
                    Log.w(APPTAG, context.getString(R.string.action_phone_warn_restore));
                    int phone_ringer_mode = (int) db.getLast(LastType.VOLUME_RING_MODE, task_id);
                    audioManager.setRingerMode(phone_ringer_mode);
                } catch (MiniTaskerException ignored) {
                }
            } else {
                Log.w(APPTAG, context.getString(R.string.action_phone_warn_silencing));
                db.putLast(LastType.VOLUME_RING_MODE, audioManager.getRingerMode(), task_id);
                audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            }

        } else if (!isRestore()) {
            final String message = db.getPrivateSetting(KEY_PREVIOUS_MESSAGE, "");
            final String origin = db.getPrivateSetting(KEY_PREVIOUS_ORIGIN, "");
            final String new_message = context.getString(R.string.action_phone_new_message_template,
                    origin, message);
            Log.w(APPTAG, context.getString(R.string.action_phone_warn_forwarding));

            if (!SmsAction.sendSms(context, targetNumber, new_message, 0)) {
                setFlag(FlagsEnum.SMS_SEND_FAILED);
            }
            Context appContext = context.getApplicationContext();
            assert appContext != null;
            SharedPreferences mPrefs = appContext.getSharedPreferences
                    (ActivityUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(BackgroundBroadcastReceiver.KEY_SHOW_TOAST,
                    context.getString(R.string.action_phone_toast_sending_message, targetNumber));
            editor.commit();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        targetNumber = myValues.getAsString("target");
    }

    @Nullable
    @Override
    public String toString() {
        if (targetNumber == null || targetNumber.isEmpty()) {
            if (isRestore()) {
                return context.getString(R.string.action_phone_individual_restore);
            } else {
                return context.getString(R.string.action_phone_individual_silence);
            }
        } else {
            return context.getString(R.string.action_phone_individual_forward, (targetContact.isEmpty() ? targetNumber : targetContact));
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "phone";
    }

    @Nullable
    @Override
    public String getDescription() {
        if (targetNumber == null || targetNumber.isEmpty()) {
            if (isRestore()) {
                return context.getString(R.string.action_phone_description_restore);
            } else {
                return context.getString(R.string.action_phone_description_silence);
            }
        } else {
            return context.getString(R.string.action_phone_description_forward,
                    (targetContact.isEmpty() ? targetNumber : targetContact));
        }
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        if (targetNumber == null || targetNumber.isEmpty())
            return NewTaskCallBlockActivity.class;
        else
            return NewTaskSmsForwardActivity.class;
    }

    @Nullable
    public String getTargetNumber() {
        return targetNumber;
    }

    public void setTargetNumber(@NotNull String targetNumber) {
        this.targetNumber = targetNumber;
        myValues.put("target", targetNumber);
    }

    public void setTargetContact(String targetContact) {
        this.targetContact = targetContact;
        myValues.put("contact", targetContact);
    }

    @Override
    public void init() {
        targetNumber = "";
        targetContact = "";
    }

    @Override
    public boolean save() {
        if (!myValues.containsKey("target"))
            myValues.put("target", "");
        return super.save();
    }

    public boolean isSilence() {
        return targetNumber == null || targetNumber.isEmpty();
    }
}
