package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;

/**
 * online market task importing dialog
 * <p/>
 * Created by phantom on 31/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MarketImportDialog extends DialogFragment implements DialogInterface.OnClickListener {
    @Nullable
    private MarketActivity caller;

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        caller = (MarketActivity) getActivity();
        assert caller != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);

        builder.setTitle(R.string.market_import_title);
        builder.setMessage(R.string.market_import_message);
        builder.setPositiveButton(R.string.market_import_import, this);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(@NotNull DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    @Override
    public void onClick(@NotNull DialogInterface dialog, int which) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        if (caller == null || caller.element_actions == null
                || caller.element_conditions == null) return;
        boolean success = db.importTask(caller.element_actions, caller.element_conditions,
                caller.element_description);
        dialog.dismiss();
        if (!success) {
            //caller.nextActivity(MainActivity.class, true);
//        } else {
            new AlertDialog.Builder(caller)
                    .setTitle(R.string.market_import_title)
                    .setMessage(R.string.market_import_problem)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(@NotNull DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
        }
    }
}
