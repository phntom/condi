package il.co.kix.minitasker;


/**
 * activity recognition utility
 *
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public final class ActivityUtils {

    // Used to track what type of request is in process
//    public enum REQUEST_TYPE {ADD, REMOVE}

    public static final String APPTAG = "MiniTasker";

    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    // Intent actions and extras for sending information from the IntentService to the Activity
//    public static final String ACTION_CONNECTION_ERROR =
//            "il.co.kix.minitasker.activityrecognition.ACTION_CONNECTION_ERROR";

    public static final String ACTION_REFRESH_STATUS_LIST =
                    "il.co.kix.minitasker.activityrecognition.ACTION_REFRESH_STATUS_LIST";

    public static final String CATEGORY_LOCATION_SERVICES =
            "il.co.kix.minitasker.activityrecognition.CATEGORY_LOCATION_SERVICES";

//    public static final String EXTRA_CONNECTION_ERROR_CODE =
//            "il.co.kix.minitasker.activityrecognition.EXTRA_CONNECTION_ERROR_CODE";
//
//    public static final String EXTRA_CONNECTION_ERROR_MESSAGE =
//            "il.co.kix.minitasker.activityrecognition.EXTRA_CONNECTION_ERROR_MESSAGE";

    // Constants used to establish the activity update interval
    public static final int MILLISECONDS_PER_SECOND = 1000;

    public static final int DETECTION_INTERVAL_SECONDS = 20;

    public static final int DETECTION_INTERVAL_MILLISECONDS =
            MILLISECONDS_PER_SECOND * DETECTION_INTERVAL_SECONDS;

    // Shared Preferences repository name
    public static final String SHARED_PREFERENCES =
            "il.co.kix.minitasker.activityrecognition.SHARED_PREFERENCES";

    // Key in the repository for the previous activity
    public static final String KEY_PREVIOUS_ACTIVITY_TYPE =
            "il.co.kix.minitasker.activityrecognition.KEY_PREVIOUS_ACTIVITY_TYPE";

    // Constants for constructing the log file name
//    public static final String LOG_FILE_NAME_PREFIX = "activityrecognition";
//    public static final String LOG_FILE_NAME_SUFFIX = ".log";

    // Keys in the repository for storing the log file info
//    public static final String KEY_LOG_FILE_NUMBER =
//            "il.co.kix.minitasker.activityrecognition.KEY_LOG_FILE_NUMBER";
//    public static final String KEY_LOG_FILE_NAME =
//            "il.co.kix.minitasker.activityrecognition.KEY_LOG_FILE_NAME";


}
