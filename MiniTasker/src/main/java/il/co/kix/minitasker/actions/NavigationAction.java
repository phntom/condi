package il.co.kix.minitasker.actions;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskNavigationFragment;

/**
 * navigation action
 * <p/>
 * Created by phantom on 25/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NavigationAction extends ActionBase {
    @Nullable
    protected String address;

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "address", SqlDataType.TEXT
    );

    boolean perform_waze() {
        try {
            Uri uri = Uri.parse("waze://");
            Uri uri2 = uri.buildUpon().appendQueryParameter("q", address).build();
            Intent intent = new Intent(Intent.ACTION_VIEW, uri2);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException ex) {
//            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
//            startActivity(intent);
            Log.w(APPTAG, "Waze not found");
            return false;
        }
    }

    boolean perform_googlemaps() {
        try {
            String url = "google.navigation:q=" + Uri.encode(address);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException ex) {
            Log.w(APPTAG, "Google Navigation not found");
            return false;
        }
    }

    boolean perform_googlemaps2() {
        try {
            Uri uri = Uri.parse("http://maps.google.com/maps");
            Uri uri2 = uri.buildUpon().appendQueryParameter("q", address).build();
            Intent intent = new Intent(Intent.ACTION_VIEW, uri2);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException ex) {
            Log.w(APPTAG, "Well this is f*ed up, google maps http failed");
            return false;
        }
    }

    @Override
    public void perform(StartType is_start) {
        if (isRestore()) return;
        if (perform_waze()) return;
        if (perform_googlemaps()) return;
        perform_googlemaps2();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        address = myValues.getAsString("address");
    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.action_navigation_individual, address);
    }

    @NotNull
    @Override
    public String getName() {
        return "navigation";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.action_navigation_description, address);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskNavigationFragment.class;
    }

    @Override
    public void init() {
        address = "";
    }

    @Nullable
    public String getAddress() {
        return address;
    }

    public void setAddress(@Nullable String address) {
        if (address == null) return;
        this.address = address;
        myValues.put("address", address);
    }
}
