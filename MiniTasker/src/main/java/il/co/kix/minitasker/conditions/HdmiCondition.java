package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.text.TextUtils;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;

/**
 * Created by phantom on 15/12/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class HdmiCondition extends ConditionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of();

    public static final String FIELD_HDMI_PLUGGED = "FIELD_HDMI_PLUGGED";

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(ContentValues myValues) throws NullPointerException {
        //
    }

    @Nullable
    @Override
    public String toString() {
        return context.getString(R.string.condition_hdmi_individual);
    }

    @NotNull
    @Override
    public String getName() {
        return "hdmi";
    }

    @Nullable
    @Override
    public String getDescription() {
        return context.getString(R.string.condition_hdmi_description);
    }

    @Nullable
    @Override
    public Class<?> getRelatedActivity() {
        return null;
    }

    @Override
    public void init() {
        //
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        boolean plugged = TextUtils.equals(db.getPrivateSetting(FIELD_HDMI_PLUGGED, "0"), "1");
        return is_start ? plugged : !plugged;
    }
}
