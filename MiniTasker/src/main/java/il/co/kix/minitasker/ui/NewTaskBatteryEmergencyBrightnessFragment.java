package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.ToggleButton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.BrightnessAction;

/**
 * new task for battery emergency action
 * <p/>
 * Created by michal on 08/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskBatteryEmergencyBrightnessFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @Nullable
    BrightnessAction action;
    @Nullable
    MiniTaskerActivity caller = null;
    private ToggleButton t;
    private SeekBar s;

    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();

        builder.setMessage(R.string.action_brightness_message);
        builder.setTitle(R.string.action_brightness_title);

        View v = inflater.inflate(R.layout.popup_new_task_battery_emergency_brightness, null);
        assert v != null;

        s = (SeekBar) v.findViewById(R.id.seekBar);
        t = (ToggleButton) v.findViewById(R.id.toggleButton);

        action = new BrightnessAction();
        action.setContext(caller);
        action.setPrimary(caller.entity_id);
        if (caller.last_entity_type != null
                && !caller.last_entity_type.equals(action.getName())) {
            action = new BrightnessAction();
            action.setContext(caller);
        }
        action.setTaskId(caller.task_id);
        int level = action.getBrightness();
        s.setMax(255);
        t.setChecked(level < 0);
        s.setProgress(Math.abs(level));
        s.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                t.setChecked(false);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        builder.setView(v);

        builder.setPositiveButton(R.string.ok, this);

        return builder.create();
    }

    @Override
    public void onClick(@NotNull DialogInterface dialogInterface, int i) {
        int value = s.getProgress();
        if (t.isChecked()) value *= -1;
        if (caller == null || action == null) return;
        action.setBrightness(value);
        action.setStart(caller.is_start);
        if (action.save()) {
            action.saveRestoreCopy();
        }
        caller.entity_id = action.getPrimary();
        caller.last_entity_type = action.getName();
        caller.nextActivityWhen(true);
        dialogInterface.dismiss();
    }
}