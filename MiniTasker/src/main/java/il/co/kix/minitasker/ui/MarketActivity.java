package il.co.kix.minitasker.ui;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ExpandableListAdapter;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;

/**
 * the online task market activity
 * <p/>
 * Created by phantom on 31/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MarketActivity extends ExpandableListActivity
        implements LoaderManager.LoaderCallbacks<SimpleExpandableListAdapter> {
//    private static final int GROUP_TOP = 0;
//    private static final int GROUP_RECENT = 1;

    //    private static final String URL = "https://web.nix.co.il/minitasker/market-XX.txt";
    private static final String DYNAMIC_URL = "https://kix.co.il/minitasker/market.php";

//    private static final String TAG_TOP = "top";
//    private static final String TAG_RECENT = "recent";
//    private static final String TAG_IMPORTER = "importer";

    //    private static final String TAG_VERSION = "version";
    private static final String TAG_DESCRIPTION = "description";
    // --Commented out by Inspection (28/09/13 23:45):private static final String TAG_AUTHOR = "author";
    // --Commented out by Inspection (28/09/13 23:45):private static final String TAG_RATING = "rating";
//    private static final String TAG_TIME = "time";
    private static final String TAG_ACTIONS = "actions";
    private static final String TAG_CONDITIONS = "conditions";
    private static final String TAG_CONTENTS = "contents";
    private static final String ACTION_VIEW = "android.intent.action.VIEW";
    @Nullable
    JSONArray element_actions = null;
    @Nullable
    JSONArray element_conditions = null;
    @Nullable
    String element_description = null;
    //    private JSONObject elements_object;
    @NotNull
    private List<JSONArray> elements_array = new ArrayList<JSONArray>();
    @NotNull
    private List<String> elements_titles = new ArrayList<String>();
    private boolean is_landing = true;
    private boolean success = false;
    private boolean loading;
    @Nullable
    private String importer_id;
    private int myVersion;
    private LoaderManager loaderManager;
    private DatabaseHandler db;
    private DefaultHttpClient httpClient;


    @Override
    protected int getGroupTitleArray() {
        return R.array.market_loading;
    }

    @Override
    protected int getChildTitleArray(int group) {
        return R.array.market_empty;
    }

    @Override
    protected int getChildDescArray(int group) {
        return R.array.market_empty;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.market_title;
    }

    @Override
    protected boolean onItemClick(int groupPosition, int childPosition) {
        if (loading) return false;
        if (!success) return false;
        try {
            JSONArray target = elements_array.get(groupPosition);
            JSONObject element = target.getJSONObject(childPosition);
            element_actions = element.getJSONArray(TAG_ACTIONS);
            element_conditions = element.getJSONArray(TAG_CONDITIONS);
            element_description = element.getString(TAG_DESCRIPTION);
            new MarketImportDialog().show(getFragmentManager(), APPTAG);
            return true;
        } catch (JSONException ignored) {
            success = false;
            return false;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        try {
            //noinspection ConstantConditions
            myVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        db = SingletonDatabase.INSTANCE.db;
        httpClient = new DefaultHttpClient();
        importer_id = null;
        if (intent != null) {
            final String action = intent.getAction();
            if (action != null && action.equals(ACTION_VIEW)) {
                Uri uri = intent.getData();
                assert uri != null;
                final String lastPathSegment = uri.getLastPathSegment();
                if (lastPathSegment != null) {
                    if (lastPathSegment.equals("importer.php") || lastPathSegment.equals("s")) {
                        importer_id = uri.getQueryParameter("id");
                    } else if (lastPathSegment.equals("reset.php")) {
                        db.disableAll();
                        db.resetRunningConditions();
                        db.clearLast();
                        Toast.makeText(this, "Running state and restore last values are now reset!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        is_landing = importer_id == null;
        loaderManager = getLoaderManager();
        assert loaderManager != null;
        Loader loader = getLoaderManager().getLoader(0);
        if (loader != null && loader.isReset()) {
            getLoaderManager().restartLoader(0, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        }

    }

//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//        loaderManager.initLoader(0, null, this).forceLoad();
//    }

    @NotNull
    private String downloadJson() {
        // Making HTTP request
        try {
            // defaultHttpClient
            HttpPost http = new HttpPost(DYNAMIC_URL);
            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
            String android_id = db.getPrivateSetting("ANDROID_ID", "");
            parameters.add(new BasicNameValuePair("android_id", android_id));
            parameters.add(new BasicNameValuePair("version", "" + myVersion));
            parameters.add(new BasicNameValuePair("locale", Locale.getDefault().getLanguage()));
            if (!is_landing) {
                parameters.add(new BasicNameValuePair("importer_id", importer_id));
            }
            http.setEntity(new UrlEncodedFormEntity(parameters));

            final HttpParams httpParams = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
            HttpConnectionParams.setSoTimeout(httpParams, 2000);

            HttpResponse httpResponse = httpClient.execute(http);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream contentResult = httpEntity.getContent();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(contentResult, "UTF-8"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
            contentResult.close();
            return stringBuilder.toString();

        } catch (UnsupportedEncodingException ignored) {
        } catch (ClientProtocolException ignored) {
        } catch (IOException ignored) {
        }
        return "";
    }

    @NotNull
    private List<Map<String, String>> parseJsonArray(@NotNull JSONArray array) throws JSONException {
        List<Map<String, String>> child = new ArrayList<Map<String, String>>();
        for (int i = 0; i < array.length(); i++) {
//            StringBuilder stringBuilder = new StringBuilder();
            final JSONObject element = array.getJSONObject(i);
//            final int version = element.getInt(TAG_VERSION);
            final String child_name = element.getString(TAG_CONTENTS);
//            final String description = element.getString(TAG_DESCRIPTION);
//            final long timestamp = element.getLong(TAG_TIME);
            element.getJSONArray(TAG_ACTIONS);
            element.getJSONArray(TAG_CONDITIONS);
//            final SimpleDateFormat dateFormat = new SimpleDateFormat();
//            if (version != myVersion)
//                stringBuilder.append(getString(R.string.market_incompatible));
//            stringBuilder.append(description);
//            stringBuilder.append(getString(R.string.market_date_uploaded));
//            stringBuilder.append(dateFormat.format(timestamp));
//            final String child_name = stringBuilder.toString();
            child.add(new HashMap<String, String>() {{
                put(CHILD_NAME, child_name);
            }});
        }
        return child;
    }

    @NotNull
    private List<List<Map<String, String>>> parseJson(String input) {
        List<List<Map<String, String>>> result = new ArrayList<List<Map<String, String>>>();
        try {
            JSONObject json = new JSONObject(input);
//            elements_object = json;
            Iterator<?> keys = json.keys();
            elements_array.clear();
            elements_titles.clear();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (json.get(key) instanceof JSONArray) {
                    elements_array.add(json.getJSONArray(key));
                    elements_titles.add(key);
                    result.add(parseJsonArray(json.getJSONArray(key)));
                }
            }
            success = true;
        } catch (JSONException e) {
            result.clear();
            result.add(new ArrayList<Map<String, String>>() {{
                add(new HashMap<String, String>() {{
                    put(CHILD_NAME, getString(R.string.market_failedparsing));
                }});
            }});
        }
        return result;
    }

    @NotNull
    @Override
    public Loader<SimpleExpandableListAdapter> onCreateLoader(int id, Bundle args) {
//        final boolean is_landing_pass = is_landing;
        return new AsyncTaskLoader<SimpleExpandableListAdapter>(this) {
            @NotNull
            @Override
            public SimpleExpandableListAdapter loadInBackground() {
                List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
                List<List<Map<String, String>>> listOfChildGroups;
                success = false;
                loading = true;
                listOfChildGroups = parseJson(downloadJson());
                final String[] groupArray = elements_titles.toArray(new String[elements_titles.size()]);
                if (listOfChildGroups.size() == groupArray.length) {
                    for (final String groupTitle : groupArray) {
                        groupData.add(new HashMap<String, String>() {{
                            put(ROOT_NAME, groupTitle);
                        }});
                    }
                } else {
                    groupData.add(new HashMap<String, String>() {{
                        put(ROOT_NAME, getString(R.string.market_error));
                    }});
                }
                return new SimpleExpandableListAdapter(
                        MarketActivity.this,
                        groupData,
                        R.layout.expandable_list_header,
                        new String[]{ROOT_NAME},
                        new int[]{android.R.id.text1},
                        listOfChildGroups,
                        R.layout.list_item_1,
                        new String[]{CHILD_NAME},
                        new int[]{android.R.id.text1}
                );
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<SimpleExpandableListAdapter> loader, @NotNull SimpleExpandableListAdapter data) {
        loading = false;
        lstMain.setAdapter(data);
        for (int group = 0; group < data.getGroupCount(); group++) {
            lstMain.expandGroup(group);
        }
    }

    @Override
    public void onLoaderReset(Loader<SimpleExpandableListAdapter> loader) {
        loading = false;
        lstMain.setAdapter((ExpandableListAdapter) null);
    }
}
