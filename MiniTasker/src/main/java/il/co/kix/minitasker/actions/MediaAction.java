package il.co.kix.minitasker.actions;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.KeyEvent;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Map;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.MediaActionType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskAppsMusicActivity;

/**
 * media action
 * <p/>
 * Created by sphantom on 7/27/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MediaAction extends ActionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "type", SqlDataType.ENUM,
            "playlist_id", SqlDataType.INTEGER,
            "playlist", SqlDataType.TEXT,
            "song_id", SqlDataType.INTEGER,
            "song_name", SqlDataType.TEXT
    );


    private MediaActionType type;
    @Nullable
    private String playlist_name;

// --Commented out by Inspection START (28/09/13 23:45):
//    public long getPlaylistId() {
//        return playlist_id;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getPlaylistName() {
//        return playlist_name;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

// --Commented out by Inspection START (28/09/13 23:45):
//    public MediaActionType getMediaType() {
//        return type;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    private long playlist_id;
    @Nullable
    private String song_name;
    private long song_id;

    @NotNull
    @Override
    public String toString() {
        return getDescription();
    }

    @NotNull
    @Override
    public String getName() {
        return "media";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore())
            return context.getString(R.string.action_media_description_pause);
        switch (type) {
            case Play:
                return context.getString(R.string.action_media_description_play);
            case Pause:
                return context.getString(R.string.action_media_description_pause);
            case Toggle:
                return context.getString(R.string.action_media_description_toggle);
            case Playlist:
                return context.getString(R.string.action_media_description_playlist, playlist_name);
            case Song:
                return context.getString(R.string.action_media_description_song, song_name);
        }
        return "";
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskAppsMusicActivity.class;
    }

    @Override
    public void init() {
        type = MediaActionType.Pause;
        playlist_name = "";
        playlist_id = 0;
        song_id = 0;
        song_name = "";
    }

    @NotNull
    public String getRelatedFragmentTag() {
        return "";
    }

    @Override
    public void perform(StartType is_start) {
        int action = 0;
        if (isRestore())
            type = MediaActionType.Pause;
        switch (type) {
            case Play:
                action = KeyEvent.KEYCODE_MEDIA_PLAY;
                break;
            case Pause:
                action = KeyEvent.KEYCODE_MEDIA_PAUSE;
                break;
            case Toggle:
                action = KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE;
                break;
            case Playlist: {
                Uri uri = Uri.withAppendedPath(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                        "" + playlist_id);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    Intent intent = Intent.createChooser(new Intent()
//                            .setAction(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_APP_MUSIC),
//                            "Select default media player");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(uri, "audio/*");
                context.startActivity(intent);
                return;
            }
            case Song: {
                try {
                    if (playlist_name != null && !playlist_name.isEmpty()) {
                        File musicFile2Play = new File(playlist_name);
                        //this is to remain backwards compatible
                        if (!musicFile2Play.exists()) {
                            setFlag(FlagsEnum.MEDIA_MUSIC_FILE_DOES_NOT_EXISTS);
                            return;
                        }
                        Intent i2 = new Intent();
                        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i2.setAction(Intent.ACTION_VIEW);
                        i2.setDataAndType(Uri.fromFile(musicFile2Play), "audio/mp3");
                        context.startActivity(i2);
                        return;
                    }
                } catch (Exception ignored) {

                }
                Uri uri = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        "" + song_id);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(uri, "audio/*");
                context.startActivity(intent);
                return;
            }
        }
        //        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        try {
            Intent i = new Intent("com.android.music.musicservicecommand");
            switch (action) {
                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    i.putExtra("command", "play");
                    break;
                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    i.putExtra("command", "pause");
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    i.putExtra("command", "togglepause");
                    break;
                default:
                    return;
            }
            context.sendOrderedBroadcast(i, null);
            long event_time = SystemClock.uptimeMillis();
            Intent downIntent = new Intent(Intent.ACTION_MEDIA_BUTTON, null);
            KeyEvent downEvent = new KeyEvent(event_time, event_time, KeyEvent.ACTION_DOWN, action, 0);
            downIntent.putExtra(Intent.EXTRA_KEY_EVENT, downEvent);
            context.sendOrderedBroadcast(downIntent, null);
            Intent upIntent = new Intent(Intent.ACTION_MEDIA_BUTTON, null);
            KeyEvent upEvent = new KeyEvent(event_time, event_time, KeyEvent.ACTION_UP, action, 0);
            upIntent.putExtra(Intent.EXTRA_KEY_EVENT, upEvent);
            context.sendOrderedBroadcast(upIntent, null);
        } catch (Exception ignored) {
        }
    }


    public void setPlaylist(String playlist) {
        playlist_name = playlist;
        setType(MediaActionType.Playlist);
        myValues.put("playlist", playlist_name);
    }

    public void setType(@NotNull MediaActionType t) {
        type = t;
        myValues.put("type", t.ordinal());
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        int ordinal = myValues.getAsInteger("type");
        type = MediaActionType.values()[ordinal];
        playlist_id = myValues.getAsLong("playlist_id");
        playlist_name = myValues.getAsString("playlist");
        song_id = myValues.getAsLong("song_id");
        song_name = myValues.getAsString("song_name");
    }


    public void setPlaylistId(long playlistId) {
        this.playlist_id = playlistId;
        myValues.put("playlist_id", playlistId);
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public long getSongId() {
//        return song_id;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setSongId(long song_id) {
        this.song_id = song_id;
        setType(MediaActionType.Song);
        myValues.put("song_id", song_id);

    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getSongName() {
//        return song_name;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setSongName(String song_name) {
        this.song_name = song_name;
        myValues.put("song_name", song_name);
    }

    @Override
    public boolean save() {
        if (isRestore()) type = MediaActionType.Pause;
        return super.save();
    }
}
