package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;

/**
 * Created by phantom on 16/12/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class FlagFragment extends DialogFragment {
    @Nullable
    String entity_type = null;
    @Nullable
    String entity_id = null;

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //noinspection ConstantConditions
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.flag_activity_title);

        int message = R.string.flag_all_good;

        String tag = getTag() == null ? "" : getTag();
        String[] words = tag.split(" ");
        FlagsEnum flag = FlagsEnum.UNKNOWN;
        if (words.length == 3) {
            try {
                entity_type = words[0];
                entity_id = words[1];
                flag = FlagsEnum.valueOf(words[2]);
            } catch (IllegalArgumentException ignored) {
            }
        }
        switch (flag) {
            case ALL_GOOD:
                message = R.string.flag_all_good;
                break;
            case AIRPLANE_ROOT_FAILED:
                message = R.string.flag_airplane_root_failed;
                break;
            case BLUETOOTH_ADAPTER_MISSING:
                message = R.string.flag_bluetooth_adapter_missing;
                break;
            case BLUETOOTH_IMMEDIATE_ERROR:
                message = R.string.flag_bluetooth_immediate_error;
                break;
            case GPS_LOCATION_SERVICES_MISSING:
                message = R.string.flag_gps_location_services_missing;
                break;
            case GPS_ROOT_FAILED:
                message = R.string.flag_gps_root_failed;
                break;
            case MOBILE_DATA_FAILED_ILLEGAL_ACCESS_EXCEPTION:
                message = R.string.flag_mobile_data_failed_illegal_access_exception;
                break;
            case MOBILE_DATA_FAILED_INVOCATION_TARGET_EXCEPTION:
                message = R.string.flag_mobile_data_failed_invocation_target_exception;
                break;
            case MOBILE_DATA_FAILED_REFLECTION:
                message = R.string.flag_mobile_data_failed_reflection;
                break;
            case NFC_MISSING:
                message = R.string.flag_nfc_missing;
                break;
            case NFC_REFLECTION_FAILED:
                message = R.string.flag_nfc_reflection_failed;
                break;
            case SYNC_FAILED:
                message = R.string.flag_sync_failed;
                break;
            case WIFI_MISSING_SERVICE:
                message = R.string.flag_wifi_missing_service;
                break;
            case WIFI_FAILED:
                message = R.string.flag_wifi_failed;
                break;
            case WIFI_SECURITY_BUG:
                message = R.string.flag_wifi_security_bug;
                break;
            case ORIENTATION_FAILED:
                message = R.string.flag_orientation_failed;
                break;
            case LAUNCH_FAILED_APP_NOT_FOUND:
                message = R.string.flag_launch_failed_app_not_found;
                break;
            case LAUNCH_FAILED_MISC:
                message = R.string.flag_launch_failed_misc;
                break;
            case TERMINATE_FAILED_APP_NOT_FOUND:
                message = R.string.flag_terminate_failed_app_not_found;
                break;
            case TERMINATE_FAILED_NO_ROOT:
                message = R.string.flag_terminate_failed_no_root;
                break;
            case MEDIA_MUSIC_FILE_DOES_NOT_EXISTS:
                message = R.string.flag_media_music_file_does_not_exists;
                break;
            case SMS_SEND_FAILED:
                message = R.string.flag_sms_send_failed;
                break;
        }

        builder.setMessage(message);

        builder.setPositiveButton(R.string.flag_dismiss, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(@NotNull DialogInterface dialog, int which) {
                if (entity_type != null && entity_id != null) {
                    DatabaseHandler db = SingletonDatabase.INSTANCE.db;
                    db.clearFlag(entity_type, entity_id);
                    NotificationManager mNotificationManager =
                            (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.cancel(2);
                    db.setPrivateSetting("FLAG_NAG_DISMISSED", "0");
                }
                MainActivity callingActivity = (MainActivity) getActivity();
                if (callingActivity != null) {
                    callingActivity.refreshAdapter();
                }
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(@NotNull DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder.show();
    }
}
