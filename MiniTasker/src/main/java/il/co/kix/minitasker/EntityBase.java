package il.co.kix.minitasker;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.ui.MiniTaskerActivity;
import il.co.kix.minitasker.utils.EntityUtils;

/**
 * entity base class for inheritance
 * <p/>
 * Created by phantom on 26/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class EntityBase {
    public static final String APPTAG = "cond.im";
    @NotNull
    private final String tableName;
    @NotNull
    protected Context context;
    @NotNull
    protected ContentValues myValues;
    @NotNull
    protected DatabaseHandler db;
    protected long primary = 0;
    protected int task_id = 0;
    protected int item_id = 0;

    public EntityBase() {
        tableName = getTableName();
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        initFields();
        init();
    }

    public void setContext(@Nullable Context ctx) {
        if (ctx == null)
            throw new NullPointerException();
        context = ctx;
    }

    public boolean save() {
        Log.d(APPTAG, "save() " + getName());
        if (!myValues.containsKey("enabled"))
            myValues.put("enabled", 1);
        item_id = (int) db.saveEntity(item_id, tableName, myValues);
        if (task_id == 0) {
            Log.wtf(APPTAG, "FATAL SQL Error!");
        }
        return primary == 0;
    }

    protected void load() {
        //noinspection ConstantConditions
        if (context == null) {
            Log.wtf(APPTAG, "Fatal: " + getName() + " uninitialized context");
            throw new NullPointerException();
        }
        if (item_id == 0) return;
        //noinspection ConstantConditions
        myValues = db.loadEntity(item_id, tableName);
        if (myValues == null) {
            Log.e(APPTAG, "Fatal: " + getName() + " db.loadEntity returned null");
            initFields();
            return;
        }
        loadFieldsFromValues();
        try {
            loadLocalValues(myValues);
        } catch (NullPointerException e) {
            Log.wtf(MiniTaskerActivity.APPTAG, "failed loadLocalValues " + e.getCause());
        }
    }

    protected abstract void loadLocalValues(ContentValues myValues) throws NullPointerException;

    public long getPrimary() {
        return primary;
    }

    public void setPrimary(long prim) {
        primary = prim;
        switch (getType()) {
            case ENTITY_ACTION:
                item_id = db.getItemByEntity(primary, "what");
                break;
            case ENTITY_CONDITION:
                item_id = db.getItemByEntity(primary, "when");
                break;
        }

        if (item_id > 0) {
            load();
        }
    }

    @Nullable
    abstract public String toString();

    @NotNull
    abstract public String getName();

    @Nullable
    abstract public String getDescription();

    @NotNull
    abstract protected String getTableName();

    @NotNull
    public abstract EntityType getType();

    @Nullable
    abstract public Class<?> getRelatedActivity();

    abstract public void init();

    public void initFields() {
        myValues = new ContentValues();
        final Map<String, SqlDataType> fields = EntityUtils.getFields(getType(), getName());
        if (fields == null) return;
        for (String fieldName : fields.keySet()) {
            SqlDataType fieldType = fields.get(fieldName);
            switch (fieldType) {
                case BLOB:
                case TEXT:
                    myValues.put(fieldName, "");
                    break;
                case INTEGER:
                case INTEGER_KEY:
                case PRIMARY:
                case ENUM:
                case SET:
                    myValues.put(fieldName, 0);
                    break;
                case BOOLEAN:
                    myValues.put(fieldName, false);
                    break;
                case REAL:
                    myValues.put(fieldName, 0.0);
                    break;
                case NULL:
                    myValues.putNull(fieldName);
                    break;
            }
        }
    }

    public void loadFieldsFromValues() {
        final Map<String, SqlDataType> fields = EntityUtils.getFields(getType(), getName());
        if (fields == null) return;
        for (String key : fields.keySet()) {
            if (!myValues.containsKey(key)) {
                Log.e(APPTAG, "Fatal: " + getName() + " SQL doesn't have field " + key);
                myValues.put(key, 0);
            }
        }
    }

    @NotNull
    public String getRelatedFragmentTag() {
        return "";
    }

    public void setTaskEnabled(boolean enabled) {
        if (task_id == 0) save();
        if (task_id == 0) throw new AssertionError();
        db.enableTask(task_id, enabled, true);
    }

    @SuppressWarnings("ConstantConditions")
    public boolean getEnabled() {
        return myValues != null && myValues.containsKey("enabled") && myValues.getAsInteger("enabled") == 1;
    }

    public void setEnabled(boolean enabled) {
        myValues.put("enabled", (enabled ? 1 : 0));
        save();
    }

    public int getTaskId() {
        return task_id;
    }

    public void setTaskId(int tId) {
        task_id = tId;
    }
}
