package il.co.kix.minitasker.receivers;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;

import static android.bluetooth.BluetoothAdapter.EXTRA_CONNECTION_STATE;
import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;
import static android.bluetooth.BluetoothAdapter.STATE_CONNECTING;
import static android.bluetooth.BluetoothAdapter.STATE_DISCONNECTED;
import static android.bluetooth.BluetoothAdapter.STATE_DISCONNECTING;

/**
 * Created by phantom on 15/03/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BluetoothReceiver extends BackgroundBroadcastReceiver {
    @Override
    public void onReceiveBackground(Context context, @NotNull Intent intent) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        int me_curr_state = intent.getIntExtra(EXTRA_CONNECTION_STATE, STATE_DISCONNECTED);
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        String mac = (device != null && device.getAddress() != null ? device.getAddress() : "");

        switch (me_curr_state) {
            case STATE_CONNECTING:
            case STATE_DISCONNECTING:
            case STATE_DISCONNECTED: {
                final String[] parameters = new String[]{};
                db.runMatchingConditions("bluetooth", "(z.inverse = 0)", parameters, StartType.END);
                db.runMatchingConditions("bluetooth", "(z.inverse = 1)", parameters, StartType.START_WITH_END);
            }
            case STATE_CONNECTED: {
                final String[] parameters = new String[]{mac};
                db.runMatchingConditions("bluetooth", "((? NOT LIKE z.mac AND z.inverse = 0) OR (? LIKE z.mac AND z.inverse = 1))", parameters, StartType.END);
                db.runMatchingConditions("bluetooth", "((? LIKE z.mac AND z.inverse = 0) OR (? NOT LIKE z.mac AND z.inverse = 1))", parameters, StartType.START_WITH_END);

            }
        }

        Log.d("cond.im", "Bluetooth: \n" +
                        "Connection state: " + me_curr_state + "\n" +
                        //"Previous state: " + me_prev_state + "\n" +
                        "Device: " + (device != null ? device.getAddress() : "") + "\n"
        );
    }
}
