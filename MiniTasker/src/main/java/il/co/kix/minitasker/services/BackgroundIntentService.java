package il.co.kix.minitasker.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.ActivityUtils;
import il.co.kix.minitasker.receivers.BackgroundBroadcastReceiver;

/**
 * Created by phantom on 07/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
abstract public class BackgroundIntentService extends IntentService {
    static final String KEY_SHOW_TOAST = BackgroundBroadcastReceiver.KEY_SHOW_TOAST;


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BackgroundIntentService(String name) {
        super(name);
    }

    protected void onHandleIntent(Intent intent) {
        final Context appContext = getApplicationContext();
        final Context thisContext = this;
        assert appContext != null;
        new AsyncTask<Intent, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Intent... params) {
                Intent intent_ = params[0];
                onBackgroundHandleIntent(intent_);
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                SharedPreferences mPrefs = appContext.getSharedPreferences(
                        ActivityUtils.SHARED_PREFERENCES,
                        Context.MODE_PRIVATE);
                String showToast = mPrefs.getString(KEY_SHOW_TOAST, "");
                if (showToast != null && !showToast.isEmpty()) {
                    Toast.makeText(thisContext, showToast, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.remove(KEY_SHOW_TOAST);
                    editor.commit();
                }
            }
        }.execute(intent);

    }

    protected abstract void onBackgroundHandleIntent(Intent intent);
}
