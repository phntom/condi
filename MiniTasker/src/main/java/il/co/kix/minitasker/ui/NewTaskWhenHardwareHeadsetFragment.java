package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.HeadsetCondition;

/**
 * new task for headset condition
 * <p/>
 * Created by sphantom on 6/22/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenHardwareHeadsetFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @Nullable
    HeadsetCondition condition;
    Spinner spinner;
    private Switch swiInverse;


    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MiniTaskerActivity caller = (MiniTaskerActivity) getActivity();
        assert caller != null;

        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();

        builder.setTitle(R.string.activity_headset_title);
        builder.setMessage(R.string.activity_headset_message);

        condition = new HeadsetCondition();
        condition.setContext(caller);
        condition.setPrimary(caller.entity_id);
        if (caller.last_entity_type != null
                && !caller.last_entity_type.equals(condition.getName())) {
            condition = new HeadsetCondition();
            condition.setContext(caller);
        }
        condition.setTaskId(caller.task_id);

        View v = inflater.inflate(R.layout.popup_when_hardware_headset, null);
        assert v != null;
        String[] values = getResources().getStringArray(R.array.activity_headset_dropdown);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(caller, android.R.layout.simple_spinner_item, values);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(LTRadapter);
        int pos = condition.getState();
        swiInverse = (Switch) v.findViewById(R.id.swiInverse);
        swiInverse.setChecked(pos < 3);
        if (pos >= 3) pos -= 3;
        spinner.setSelection(pos);

        builder.setPositiveButton(R.string.ok, this);

        builder.setView(v);
        return builder.create();
    }

    @Override
    public void onClick(@NotNull DialogInterface dialogInterface, int i) {
        MiniTaskerActivity callingActivity = (MiniTaskerActivity) getActivity();
        if (callingActivity == null || condition == null) return;
        condition.setTaskId(callingActivity.task_id);
        int state = spinner.getSelectedItemPosition();
        if (!swiInverse.isChecked()) state += 3;
        condition.setState(state);
        condition.save();
        callingActivity.entity_id = condition.getPrimary();
        callingActivity.last_entity_type = condition.getName();
        if (callingActivity.isNotEditMode())
            condition.setTaskEnabled(true);
        callingActivity.nextActivityMain(true);
        dialogInterface.dismiss();
    }
}