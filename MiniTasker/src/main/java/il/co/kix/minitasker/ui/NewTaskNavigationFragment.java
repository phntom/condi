package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.NavigationAction;

/**
 * new task for navigation action
 * <p/>
 * Created by phantom on 25/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskNavigationFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @Nullable
    private NavigationAction navigationAction;
    @Nullable
    private MiniTaskerActivity caller;
    private EditText txtAddress;

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        navigationAction = new NavigationAction();
        navigationAction.setContext(caller);
        navigationAction.setPrimary(caller.entity_id);
        if (caller.last_entity_type != null
                && !caller.last_entity_type.equals(navigationAction.getName())) {
            navigationAction = new NavigationAction();
            navigationAction.setContext(caller);
        }
        navigationAction.setTaskId(caller.task_id);
        final AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        final LayoutInflater inflater = caller.getLayoutInflater();
        builder.setTitle(R.string.popup_navigation_action_title);
        builder.setMessage(R.string.popup_navigation_action_message);
        final View v = inflater.inflate(R.layout.popup_action_navigate_edittext, null);
        assert v != null;
        txtAddress = (EditText) v.findViewById(R.id.txtAddress);
        assert txtAddress != null;
        txtAddress.setText(navigationAction.getAddress());
        txtAddress.selectAll();
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, this);
        return builder.create();

    }

    @Override
    public void onClick(@NotNull DialogInterface dialog, int which) {
        assert txtAddress != null;
        Editable txtAddressText = txtAddress.getText();
        assert txtAddressText != null;
        if (txtAddressText.toString().isEmpty()) {
            dialog.dismiss();
            return;
        }
        assert navigationAction != null;
        navigationAction.setAddress(txtAddressText.toString());
        assert caller != null;
        navigationAction.setStart(caller.is_start);
        navigationAction.save();
        assert caller != null;
        caller.entity_id = navigationAction.getPrimary();
        caller.last_entity_type = navigationAction.getName();
        caller.nextActivityWhen(true);
    }
}
