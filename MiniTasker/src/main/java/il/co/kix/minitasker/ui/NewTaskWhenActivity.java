package il.co.kix.minitasker.ui;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.BatteryCondition;
import il.co.kix.minitasker.conditions.BootCondition;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.DockCondition;
import il.co.kix.minitasker.conditions.HdmiCondition;

/**
 * new condition
 * <p/>
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenActivity extends ExpandableListActivity {

    @Override
    int resourceActivityTitle() {
        return R.string.when_title;
    }

    @Override
    protected int getGroupTitleArray() {
        return R.array.when_group_titles;
    }

    @Override
    protected int getChildTitleArray(int group) {
        switch (group) {
            case 0:
            default:
                return R.array.when_child_1_titles;
            case 1:
                return R.array.when_child_2_titles;
            case 2:
                return R.array.when_child_3_titles;
        }
    }

    @Override
    protected int getChildDescArray(int group) {
        switch (group) {
            case 0:
            default:
                return R.array.when_child_1_descriptions;
            case 1:
                return R.array.when_child_2_descriptions;
            case 2:
                return R.array.when_child_3_descriptions;
        }
    }

    @Override
    protected boolean onItemClick(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case 0:
                switch (childPosition) {
                    case 0:
                        nextActivity(NewTaskWhenLocationActivity.class, false);
                        break;
                }
                break;
            case 1:
                switch (childPosition) {
                    case 0:
                        nextActivity(NewTaskWhenTimeAndDateActivity.class, false);
                        break;
                    case 1:
                        nextActivity(NewTaskWhenTimeCalendarActivity.class, false);
                        break;
                    case 2:
                        nextActivity(NewTaskWhenShortcut.class, false);
                        break;
                }
                break;
            case 2:
                switch (childPosition) {
                    case 0:
                        new NewTaskWhenMovingStateFragment()
                                .show(getFragmentManager(), APPTAG);
                        break;
                    case 1:
                        new NewTaskWhenHardwareChargerFragment()
                                .show(getFragmentManager(), "Charger");
                        break;
                    case 2:
                        new NewTaskWhenHardwareHeadsetFragment()
                                .show(getFragmentManager(), "Headset");
                        break;
                    case 3:
                        nextActivity(NewTaskWhenWifi.class, false);
                        break;
                    case 4:
                        nextActivity(NewTaskWhenBluetooth.class, false);
                        break;
                    case 5: {
                        ConditionBase condition = new DockCondition();
                        condition.setContext(this);
                        condition.setPrimary(entity_id);
                        condition.setTaskId(task_id);
                        condition.save();
                        condition.setTaskEnabled(true);
                        entity_id = condition.getPrimary();
                        nextActivityMain(false);
                    }
                    break;
                    case 6: {
                        ConditionBase condition = new HdmiCondition();
                        condition.setContext(this);
                        condition.setPrimary(entity_id);
                        condition.setTaskId(task_id);
                        condition.save();
                        condition.setTaskEnabled(true);
                        entity_id = condition.getPrimary();
                        nextActivityMain(false);
                    }
                    break;
                    case 7: {
                        BootCondition condition = new BootCondition();
                        condition.setContext(this);
                        condition.setPrimary(entity_id);
                        condition.setTaskId(task_id);
                        condition.save();
                        condition.setTaskEnabled(true);
                        entity_id = condition.getPrimary();
                        nextActivityMain(false);
                    }
                    break;
                    case 8: {
                        ConditionBase condition = new BatteryCondition();
                        condition.setContext(this);
                        condition.setPrimary(entity_id);
                        condition.setTaskId(task_id);
                        condition.save();
                        condition.setTaskEnabled(true);
                        entity_id = condition.getPrimary();
                        nextActivityMain(false);
                    }
                    break;
                    case 9:
                        break;
                }
                break;
        }
        return true;
    }
}