package il.co.kix.minitasker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.ui.MainActivity;

/**
 * Created by phantom on 20/12/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BugFragment extends DialogFragment {
    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        assert activity != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();

        builder.setTitle(R.string.bug_report_title);

        final View view = inflater.inflate(R.layout.bug_report, null);

        assert view != null;

        builder.setView(view)
                .setPositiveButton(R.string.bug_button_report, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NotNull DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), R.string.bug_report_thank_you, Toast.LENGTH_LONG).show();
                        //noinspection ConstantConditions
                        String complaint = ((TextView) view.findViewById(R.id.txtProblem)).getText().toString();
                        //noinspection ConstantConditions
                        String email = ((TextView) view.findViewById(R.id.txtEmail)).getText().toString();
                        String export = ((CheckBox) view.findViewById(R.id.chkUploadDb)).isChecked() ? "yes" : "no";
                        ((MainActivity) getActivity()).reportBug(complaint, email, export);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NotNull DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.show();
    }
}