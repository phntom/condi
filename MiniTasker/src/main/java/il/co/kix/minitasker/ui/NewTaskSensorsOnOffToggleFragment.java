package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;
import il.co.kix.minitasker.actions.AirplaneModeAction;
import il.co.kix.minitasker.actions.BluetoothAction;
import il.co.kix.minitasker.actions.GPSAction;
import il.co.kix.minitasker.actions.MobileDataAction;
import il.co.kix.minitasker.actions.NFCAction;
import il.co.kix.minitasker.actions.OrientationAction;
import il.co.kix.minitasker.actions.ScanningAction;
import il.co.kix.minitasker.actions.StatesAction;
import il.co.kix.minitasker.actions.SyncAction;
import il.co.kix.minitasker.actions.WiFiAction;
import il.co.kix.minitasker.actions.WifiApAction;

/**
 * new task for sensors activities
 * <p/>
 * Created by sphantom on 7/27/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskSensorsOnOffToggleFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @Nullable
    StatesAction action;
    Spinner spinner;

    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        MiniTaskerActivity caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();
        long primary = caller.entity_id;
        if (caller.last_entity_type != null && !caller.last_entity_type.equals("states")) {
            primary = 0;
        }

        final String tag = (getTag() == null ? "" : getTag());

        if (tag.equals("gps")) {
            builder.setMessage(R.string.what_state_should_the_gps_be_on);
            builder.setTitle(R.string.gps_title);
            action = new GPSAction();
        } else if (tag.equals("bluetooth")) {
            builder.setMessage(R.string.what_state_should_the_bt_be_on);
            builder.setTitle(R.string.bt_title);
            action = new BluetoothAction();
        } else if (tag.equals("nfc")) {
            builder.setMessage(R.string.what_state_should_the_nfc_be_on);
            builder.setTitle(R.string.nfc_title);
            action = new NFCAction();
        } else if (tag.equals("wifi")) {
            builder.setMessage(R.string.what_state_should_the_wifi_be_on);
            builder.setTitle(R.string.wifi_title);
            action = new WiFiAction();
        } else if (tag.equals("mobile")) {
            builder.setMessage(R.string.what_state_should_the_3g_be_on);
            builder.setTitle(R.string.data_title);
            action = new MobileDataAction();
        } else if (tag.equals("sync")) {
            builder.setMessage(R.string.what_state_should_the_data_sync_be_on);
            builder.setTitle(R.string.sync_title);
            action = new SyncAction();
        } else if (tag.equals("airplane")) {
            builder.setMessage(R.string.what_state_should_the_airplane_mode_be_on);
            builder.setTitle(R.string.airplane_title);
            action = new AirplaneModeAction();
        } else if (tag.equals("orientation")) {
            builder.setMessage(R.string.what_state_should_orientation_lock);
            builder.setTitle(R.string.orientation_title);
            action = new OrientationAction();
        } else if (tag.equals("scanning")) {
            builder.setMessage(R.string.what_state_should_always_scanning);
            builder.setTitle(R.string.always_scanning_title);
            action = new ScanningAction();
        } else if (tag.equals("wifiap")) {
            builder.setMessage(R.string.what_state_should_wifi_ap);
            builder.setTitle(R.string.wifi_ap_title);
            action = new WifiApAction();
//        } else if (tag.equals("usbap")) {
//            builder.setMessage(R.string.what_state_should_usb_ap);
//            builder.setTitle(R.string.usb_ap_title);
//            action = new WifiApAction(); //TODO: change this
        } else {
            Log.wtf("MiniTasker", "you missed tag " + tag);
            return builder.create();
        }
        action.setContext(caller);
        action.setPrimary(primary);
        action.setTaskId(caller.task_id);

        View v = inflater.inflate(R.layout.popup_on_off_toggle, null);
        assert v != null;
        String[] values = getResources().getStringArray(R.array.sensor_state);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(caller, android.R.layout.simple_spinner_item, values);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(LTRadapter);
        spinner.setSelection(action.getState().ordinal());

        builder.setPositiveButton(R.string.ok, this);

        builder.setView(v);
        return builder.create();
    }

    @Override
    public void onClick(@NotNull DialogInterface dialogInterface, int i) {
        MiniTaskerActivity callingActivity = (MiniTaskerActivity) getActivity();
        assert callingActivity != null;
        assert action != null;

        switch (spinner.getSelectedItemPosition()) {
            case 0:
                action.setState(StateType.On);
                break;
            case 1:
                action.setState(StateType.Off);
                break;
            case 2:
                action.setState(StateType.Toggle);
                break;
        }

        assert action != null;
        action.setStart(callingActivity.is_start);
        boolean is_new = false;
        if (action.save()) {
            action.saveRestoreCopy();
            is_new = true;
        }
        callingActivity.entity_id = action.getPrimary();
        callingActivity.last_entity_type = action.getName();
        callingActivity.nextActivityWhen(!is_new);
        dialogInterface.dismiss();
    }
}
