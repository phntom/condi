package il.co.kix.minitasker.ui;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.CompoundButton;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import il.co.kix.minitasker.BackgroundProvider;
import il.co.kix.minitasker.BugFragment;
import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.EntityBase;
import il.co.kix.minitasker.FeebackFragment;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.MainAdapter;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.ActionBase;
import il.co.kix.minitasker.conditions.ConditionBase;

/**
 * main activity
 * <p/>
 * Created by phantom on 03/04/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */

public class MainActivity extends MiniTaskerActivity
        implements CompoundButton.OnCheckedChangeListener, View.OnDragListener,
        View.OnTouchListener, LoaderManager.LoaderCallbacks<MainAdapter> {
    private static final String SHARE_URL = "https://kix.co.il/minitasker/share.php";
    private static final String BUG_URL = "https://kix.co.il/minitasker/bugreport.php";
    private static final String FIELD_NAG_FEEDBACK_1 = "NAG_FEEDBACK_1";
    protected ExpandableListView list;
    DatabaseHandler db;
    boolean isLoading = false;
    @Nullable
    private MainActionMode mActionMode = null;
    @Nullable
    private ActionMode mLastActionMode;
    @Nullable
    private MainAdapter mainAdapter;
    private boolean mActionModeStarted;
    private View mUndoView;
    private LoaderManager loaderManager;
    private View pbrLoading;
    @Nullable
    private String share_ids = null;
    @Nullable
    private String[] bug_report = null;
    private boolean copy_direct_link = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(false);
        }

        db = SingletonDatabase.INSTANCE.db;

        mActionMode = new MainActionMode();

        getContentResolver().query(Uri.parse("content://" + BackgroundProvider.BACKGROUND_PROVIDER + "/on_boot/check"), null, null, null, null);

        final Intent intent = getIntent();

        if (intent != null && intent.getIntExtra("is_shortcut", 0) == 1 && task_id != 0) {
//            final StartType start_type = StartType.START_ONLY;
//            Intent intent2 = new Intent() {{
//                putExtra("task_id", task_id);
//                putExtra("start_type", start_type.ordinal());
//                putExtra("simulated", 1);
//            }};
//            SimulatedRunReceiver simulated = new SimulatedRunReceiver();
//            simulated.onReceive(this, intent2);
            db.scheduleActionsForLauncher(task_id, true);
            this.finish();
            return;
        }
        if (intent != null) {
            final String action = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_VIEW)) {
                Uri uri = intent.getData();
                if (uri != null) {
                    final String lastPathSegment = uri.getLastPathSegment();
                    if (lastPathSegment != null && lastPathSegment.equals("run")) {
                        TextUtils.StringSplitter splitter = new TextUtils.SimpleStringSplitter('+');
                        splitter.setString("" + uri.getQueryParameter("tasks"));
                        for (String task_hash : splitter) {
                            final int task_id_run = db.getTaskIdByHash(task_hash);
                            db.scheduleActionsForLauncher(task_id_run, true);
                        }
                        this.finish();
                    }
                }
            }
        }
        list = (ExpandableListView) findViewById(R.id.main_view);
        mUndoView = findViewById(R.id.undo);
        pbrLoading = findViewById(R.id.pbrLoading);

        loaderManager = getLoaderManager();
    }

    public void refreshAdapter() {
        if (mainAdapter != null) {
            mainAdapter.notifyDataSetChanged();
        }
    }

    void forceReloadAdapter() {
        if (loaderManager == null) return;
//        boolean need_force = loaderManager.getLoader(0) == null;
        isLoading = true;
        Loader loader = loaderManager.getLoader(0);
        if (loader != null && loader.isReset()) {
            getLoaderManager().restartLoader(0, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        }

    }

//    void invalidateAdapter() {
//        if (mainAdapter == null) return;
//        mainAdapter.notifyDataSetInvalidated();
//        mUndoView.setVisibility(db.isUndoPossible() ? View.VISIBLE : View.GONE);
//        for (int i = 0; i < mainAdapter.getGroupCount(); i++) {
//            if (mainAdapter.isGroupExpanded(i)) list.expandGroup(i);
//            else list.collapseGroup(i);
//        }
//    }

    @NotNull
    @Override
    public Loader<MainAdapter> onCreateLoader(int id, Bundle args) {
        AsyncTaskLoader<MainAdapter> loader = new AsyncTaskLoader<MainAdapter>(this) {
            @NotNull
            @Override
            public MainAdapter loadInBackground() {
//                db.autoCleanup();
                if (db.upgradeNeeded()) {
                    db.autoCleanup();
                    {
                        long[] entities = db.getAllEntities("what");
                        for (long entity_id : entities) {
                            ActionBase entity = ActionBase.ActionBaseFactory(MainActivity.this, entity_id);
                            if (entity == null) continue;
                            if (entity.getTaskId() == 0) {
                                Log.d(APPTAG, "upgrade: bad task_id for action " + entity_id);
                                continue;
                            }
                            entity.save();
                        }
                    }
                    {
                        long[] entities = db.getAllEntities("when");
                        for (long entity_id : entities) {
                            ConditionBase entity = ConditionBase.ConditionBaseFactory(MainActivity.this, entity_id);
                            if (entity == null) continue;
                            if (entity.getTaskId() == 0) {
                                Log.d(APPTAG, "upgrade: bad task_id for condition " + entity_id);
                                continue;
                            }
                            entity.save();
                        }
                    }
                    {
                        long[] entities = db.getAllEntities("tasks");
                        for (long task_id : entities) {
                            db.regenerateTaskDescription((int) task_id);
                        }
                    }
                }
                if (share_ids != null) {
                    try {
                        final JSONArray json = db.exportTask(share_ids);
                        final String content = json != null ? json.toString() : "";
                        final String response = uploadShareRequest(content);
                        if (response == null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this,
                                            getString(R.string.share_error_upload),
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            JSONObject json_response = new JSONObject(response);
                            if (!json_response.getString("result").equals("ok")) {
                                final String error_message = json_response.getString("error");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this, error_message,
                                                Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final String link = json_response.getString("link");
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, link);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                            }
                        }
                    } catch (JSONException ignored) {
                    }
                    share_ids = null;
                }
                if (bug_report != null && bug_report.length == 3) {
                    try {
                        final JSONObject json = db.exportAllDatabase(bug_report[0], bug_report[1], bug_report[2]);
                        bug_report = null;
                        final String content = json != null ? json.toString() : "";
                        final String response = uploadBugReport(content);
                        if (response == null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this,
                                            getString(R.string.report_error_upload),
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            JSONObject json_response = new JSONObject(response);
                            if (!json_response.getString("result").equals("ok")) {
                                final String error_message = json_response.getString("error");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this, error_message,
                                                Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    } catch (JSONException ignored) {
                    }
                }
                if (copy_direct_link) {
                    copy_direct_link = false;
                    String direct_link = db.getDirectLink();
                    if (!direct_link.isEmpty()) {
                        final int count = db.getTaskSelectionCount();
                        final ClipData clip = ClipData.newPlainText("Link for Condi tasks", direct_link);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ClipboardManager clipboard = (ClipboardManager)
                                        getSystemService(CLIPBOARD_SERVICE);
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(MainActivity.this,
                                        getResources().getQuantityText(
                                                R.plurals.notification_copy_link_copied, count),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        });
                    }
                }
                return new MainAdapter(MainActivity.this);
            }

            @Override
            protected void onStartLoading() {
//                Log.d(APPTAG, "onStartLoading");
                super.onStartLoading();
                pbrLoading.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
                mUndoView.setVisibility(View.GONE);
            }
        };
        loader.forceLoad();
        return loader;
    }

    @Nullable
    public String uploadShareRequest(final String request) {
        try {
            final String android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(SHARE_URL);
            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("android_id", android_id));
            parameters.add(new BasicNameValuePair("tasks", request));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));

            final HttpParams httpParams = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
            HttpConnectionParams.setSoTimeout(httpParams, 2000);

            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream contentResult = httpEntity.getContent();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(contentResult, "UTF-8"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
            contentResult.close();
            return stringBuilder.toString();
        } catch (IOException ignored) {
        }
        return null;
    }

    @Nullable
    public String uploadBugReport(final String request) {
        try {
            final String android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(BUG_URL);
            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("android_id", android_id));
            parameters.add(new BasicNameValuePair("report", request));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));

            final HttpParams httpParams = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
            HttpConnectionParams.setSoTimeout(httpParams, 2000);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream contentResult = httpEntity.getContent();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(contentResult, "UTF-8"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
            contentResult.close();
            return stringBuilder.toString();
        } catch (IOException ignored) {
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<MainAdapter> loader, MainAdapter data) {
        if (!isLoading) return;
        isLoading = false;

        if (mainAdapter != null && mainAdapter != data) {
            mainAdapter.closeCursors();
        }
        mainAdapter = data;
        list.setAdapter(mainAdapter);
        list.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        list.setMultiChoiceModeListener(mActionMode);
        list.setOnTouchListener(this);
        list.setOnGroupClickListener(mActionMode);

        if (mainAdapter != null) {
            for (int i = 0; i < mainAdapter.getGroupCount(); i++) {
                if (mainAdapter.isGroupExpanded(i)) list.expandGroup(i);
            }
        }

        final int count = db.getTaskSelectionCount();
        if (count != 0 && !mActionModeStarted) {
            startActionMode(mActionMode);
        }

        mUndoView.setVisibility(db.isUndoPossible() ? View.VISIBLE : View.GONE);
        list.setVisibility(View.VISIBLE);
        pbrLoading.setVisibility(View.GONE);

        loaderManager.destroyLoader(0);
    }

    @Override
    public void onLoaderReset(Loader<MainAdapter> loader) {
        list.setAdapter((ExpandableListAdapter) null);
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_main;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.minitasker_title;
    }

    @Override
    protected void onResume() {
        super.onResume();
        int resCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resCode != ConnectionResult.SUCCESS) {
            long lastError = Long.valueOf(db.getPrivateSetting("GOOGLE_PLAY_SERVICE_TIME", "0"));
            long now = Calendar.getInstance().getTimeInMillis();
            if (now > lastError + 1000 * 60 * 5) {
                GooglePlayServicesUtil.getErrorDialog(resCode, this, 1).show();
            }
            db.setPrivateSetting("GOOGLE_PLAY_SERVICE_TIME", "" + Calendar.getInstance().getTimeInMillis());
        }
        switch (resCode) {
            case ConnectionResult.SERVICE_MISSING:
                db.setPrivateSetting("GOOGLE_PLAY_SERVICE_ERROR", "SERVICE_MISSING");
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                db.setPrivateSetting("GOOGLE_PLAY_SERVICE_ERROR", "SERVICE_VERSION_UPDATE_REQUIRED");
                break;
            case ConnectionResult.SERVICE_DISABLED:
                db.setPrivateSetting("GOOGLE_PLAY_SERVICE_ERROR", "SERVICE_DISABLED");
                break;
            case ConnectionResult.SERVICE_INVALID:
                db.setPrivateSetting("GOOGLE_PLAY_SERVICE_ERROR", "SERVICE_INVALID");
                break;
            case ConnectionResult.DATE_INVALID:
                db.setPrivateSetting("GOOGLE_PLAY_SERVICE_ERROR", "DATE_INVALID");
                break;
            case ConnectionResult.SUCCESS:
                db.setPrivateSetting("GOOGLE_PLAY_SERVICE_ERROR", "");
        }

//        if (mLastLocale != null) {
//            if (!Locale.getDefault().getDisplayLanguage().equals(mLastLocale)) {
//                Loader loader = loaderManager.getLoader(0);
//                if (loader != null)
//                    loader.reset();
//            }
//            mLastLocale = Locale.getDefault().getDisplayLanguage();
//        }

        forceReloadAdapter();

        long now = Calendar.getInstance().getTimeInMillis();
        String mNagFeedback = db.getPrivateSetting(FIELD_NAG_FEEDBACK_1, "");
        if (now > 1414800000000L && mNagFeedback.equals("")) {
            new FeebackFragment().show(getFragmentManager(), "");
            db.setPrivateSetting(FIELD_NAG_FEEDBACK_1, "1");
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mainAdapter != null) mainAdapter.closeCursors();
        mainAdapter = null;
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        Log.d("MiniTasker", "onNewIntent Main");
//        if (intent.getBooleanExtra("complete", false)) {
//            DatabaseHandler db = new DatabaseHandler(this);
//            db.scheduleNextTimeRangeWhen(this);
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        Intent target;
        switch (item.getItemId()) {
            case R.id.action_addtask:
                target = new Intent(this, NewTaskActivity.class);
                break;
            case R.id.action_select_all:
                if (db.setTaskSelectAll()) {
                    if (mainAdapter != null) {
                        mainAdapter.notifyDataSetChanged();
                    }
                    startActionMode(mActionMode);
                }
                return super.onOptionsItemSelected(item);
            case R.id.action_settings:
                target = new Intent(this, SettingsSavableActivity.class);
                break;
            case R.id.action_about:
                new AboutFragment().show(getFragmentManager(), "");
                return true;
            case R.id.action_feedback:
                new FeebackFragment().show(getFragmentManager(), "");
                return true;
            case R.id.action_bug_report:
                new BugFragment().show(getFragmentManager(), "");
                return true;
            case R.id.action_market:
                target = new Intent(this, MarketActivity.class);
                break;
            case R.id.action_log:
                target = new Intent(this, LogActivity.class);
                break;
            case R.id.action_reset:
                db.disableAll();
                db.resetRunningConditions();
                db.clearLast();
                onResume();
                return super.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }
        startActivity(target);
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onGroupExpand(int groupPosition) {
//        final int count = mainAdapter.getGroupCount();
//        for (int i = 0; i < count; i++) {
//            if (i == groupPosition) continue;
//            list.collapseGroup(i);
//        }
//    }


    @Override
    public void onCheckedChanged(@NotNull CompoundButton buttonView, boolean isChecked) {
        // group switch is modified
        int groupPosition = (buttonView.getTag() == null ? -1 : (Integer) buttonView.getTag());
        if (groupPosition == -2) return; // to prevent disabling disabled events on init
        if (groupPosition == -1) {
            Log.e(APPTAG, "failed setting enabled status on task, invalid groupPosition");
            return;
        }
        int task_id = (mainAdapter != null ? (int) mainAdapter.getGroupId(groupPosition) : 0);
        if (task_id == 0) {
            Log.e(APPTAG, "failed setting enabled status on task, invalid task_id");
            return;
        }
        db.enableTask(task_id, isChecked, true);
        Log.d(APPTAG, (isChecked ? "enabled" : "disabled") + " task " + task_id);
        mainAdapter.notifyDataSetChanged();
    }

    public void onClickAddGroup(@NotNull View ignore) {
        ignore.getHeight();
        startActivity(new Intent(this, NewTaskActivity.class));
    }

    private int getTaskIdForFooter(@NotNull View view) {
        if (view.getParent() == null) return 0;
        Integer task_id = (Integer) ((View) (view.getParent())).getTag();
        if (task_id == null) return 0;
        return task_id;
    }

    public void onClickLabelChild(@NotNull View view) {
        int task_id = getTaskIdForFooter(view);
        if (task_id == 0) return;
        new TaskLabelDialog().show(getFragmentManager(), "" + task_id);
    }

    public void onClickRemoveChild(@NotNull View view) {
        int task_id = getTaskIdForFooter(view);
        if (task_id == 0) return;
        db.deleteTask(task_id);
        forceReloadAdapter();
    }

    public void onClickShareChild(@NotNull View view) {
        int task_id = getTaskIdForFooter(view);
        if (task_id == 0) return;
//        new MarketShareDialog().show(getFragmentManager(), "" + task_id);
        shareTasks("" + task_id);
    }

    public void onClickDuplicateChild(@NotNull View view) {
        if (mainAdapter == null) return;
        int task_id = getTaskIdForFooter(view);
        if (task_id == 0) return;
        db.deepCopyTask(task_id);
        forceReloadAdapter();
    }

    public void onClickFlagChild(@NotNull View view) {
        if (mainAdapter == null) return;
        View parent = (view.getParent() == null ? null : (View) view.getParent());
        if (parent == null) return;
        Integer groupPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_GROUP_POSITION);
        Integer childPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_CHILD_POSITION);
        Integer childType = (Integer) parent.getTag();
        if (groupPosition == null || childPosition == null || childType == null) return;
        FlagsEnum flag = mainAdapter.getFlag(groupPosition, childPosition);
        if (flag == FlagsEnum.UNKNOWN) return;
        final String entity_type = childType == 5 ? "when" : "what";
        final String entity_id = " " + mainAdapter.getChildId(groupPosition, childPosition) + " ";
        new FlagFragment().show(getFragmentManager(), entity_type + entity_id + flag.toString());
    }

    public void onClickEditChild(@NotNull View view) {
        View parent = (view.getParent() == null ? null : (View) view.getParent());
        if (parent == null) return;
        Integer groupPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_GROUP_POSITION);
        Integer childPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_CHILD_POSITION);
        Integer childType = (Integer) parent.getTag();
        if (groupPosition == null || childPosition == null || childType == null) return;
        entity_id = mainAdapter != null ? mainAdapter.getChildId(groupPosition, childPosition) : 0;
        task_id = mainAdapter != null ? (int) mainAdapter.getGroupId(groupPosition) : 0;
        if (entity_id == 0 || task_id == 0) return;
        EntityBase entity;
        switch (childType) {
            case 1:
                // start action
            case 3:
                // end action
                entity = ActionBase.ActionBaseFactory(this, entity_id);
                if (entity != null && ((ActionBase) entity).isRestore()) {
                    new NoEditRestoreFragment().show(getFragmentManager(), "");
                    return;
                }
                is_start = childType == 1;
                break;
            case 5:
                // condition
                entity = ConditionBase.ConditionBaseFactory(this, entity_id);
                break;
            default:
                return;
        }
        if (entity == null) {
            Log.e(APPTAG, "Bad ActionBaseFactory for entity " + entity_id);
            return;
        }
        Class<?> targetActivity = entity.getRelatedActivity();
        if (targetActivity == null) {
            Log.e(APPTAG, "Bad getRelatedActivity for entity " + entity_id);
            return;
        }
        if (Activity.class.isAssignableFrom(targetActivity)) {
            Intent target = new Intent(this, targetActivity);
            target.putExtra("task_id", task_id);
            target.putExtra("entity_id", entity_id);
            target.putExtra("edit", 0);
            target.putExtra("is_start", is_start);
            startActivity(target);
        } else if (DialogFragment.class.isAssignableFrom(targetActivity)) {
            try {
                edit_section = 0;
                String tag = entity.getRelatedFragmentTag();
                DialogFragment f = (DialogFragment) targetActivity.newInstance();
                f.show(getFragmentManager(), tag);
            } catch (InstantiationException | IllegalAccessException ignored) {
            }
        }
    }

    public void onClickDeleteChild(@NotNull View view) {
        View parent = (view.getParent() == null ? null : (View) view.getParent());
        if (parent == null) return;
        Integer groupPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_GROUP_POSITION);
        Integer childPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_CHILD_POSITION);
        Integer childType = (Integer) parent.getTag();
        if (groupPosition == null || childPosition == null || childType == null) return;
        long entity_id = mainAdapter != null ? mainAdapter.getChildId(groupPosition, childPosition) : 0;
        int task_id = mainAdapter != null ? (int) mainAdapter.getGroupId(groupPosition) : 0;
        if (entity_id == 0) return;
        mainAdapter.closeCursor(groupPosition);
        db.deleteEntity(childType == 5 ? "when" : "what", entity_id, task_id);
        forceReloadAdapter();
    }

    public void onClickAddChild(@NotNull View view) {
        View parent = (view.getParent() == null ? null : (View) view.getParent());
        if (parent == null) return;
        Integer groupPosition = (Integer) parent.getTag(MainAdapter.TAG_KEY_GROUP_POSITION);
        Integer childType = (Integer) parent.getTag();
        if (childType == null || groupPosition == null) return;
        int task_id = mainAdapter != null ? (int) mainAdapter.getGroupId(groupPosition) : 0;
        if (task_id == 0) return;
        Log.d(APPTAG, "addChild group: " + groupPosition + " task_id: " + task_id);
        Intent target;
        switch (childType) {
            case 0:
                // start
                target = new Intent(this, NewTaskActivity.class);
                target.putExtra("is_start", true);
                break;
            case 2:
                // end
                target = new Intent(this, NewTaskActivity.class);
                target.putExtra("is_start", false);
                break;
            case 4:
                // condition
                target = new Intent(this, NewTaskWhenActivity.class);
                break;
            default:
                return;
        }
        target.putExtra("task_id", task_id);
        target.putExtra("entity_id", 0L);
        target.putExtra("edit", 0);
        target.putExtra("edit_new", true);
        startActivity(target);
    }

    public void onClickUndoRollback(@NotNull View ignore) {
        ignore.getHeight();
        db.undoRollback();
        forceReloadAdapter();
    }

    public void onClickUndoCommit(@NotNull View ignore) {
        ignore.getHeight();
        db.undoCommit();
        forceReloadAdapter();
    }

    @Override
    public boolean onDrag(View v, @NotNull DragEvent event) {
        final int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                Log.d(APPTAG, "drag started");
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                Log.d(APPTAG, "drag entered");
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                Log.d(APPTAG, "drag exited");
                break;
            case DragEvent.ACTION_DRAG_LOCATION:
                Log.d(APPTAG, "drag location");
                break;
            case DragEvent.ACTION_DROP:
                Log.d(APPTAG, "drag drop");
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                Log.d(APPTAG, "drag ended");
                break;
        }
        return true;
    }

    @Override
    public boolean onTouch(View v, @NotNull MotionEvent event) {
        return event.getAction() == MotionEvent.ACTION_MOVE && mainAdapter != null && mainAdapter.isDragging();
    }

    public void shareTasks(String task_ids) {
        share_ids = task_ids;
        forceReloadAdapter();
    }

    public void reportBug(String complaint, String email, String export) {
        bug_report = new String[]{complaint, email, export};
        forceReloadAdapter();
    }

    private final class MainActionMode implements ActionMode.Callback,
            AbsListView.MultiChoiceModeListener, ExpandableListView.OnGroupClickListener {
        final DatabaseHandler db;

        public MainActionMode() {
            db = MainActivity.this.db;
        }

        private void updateTitleMenu() {
            final int count = db.getTaskSelectionCount();
            if (mLastActionMode == null) return;
            if (count == 0) {
                mLastActionMode.finish();
                return;
            }
            mLastActionMode.setTitle(Integer.toString(count));
        }

        @Override
        public boolean onActionItemClicked(@NotNull ActionMode mode, @NotNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_deletetask:
                    db.deleteSelectedTasks();
//                    if (mLastActionMode != null) {
//                        mLastActionMode.finish();
//                    }
                    forceReloadAdapter();
                    break;
                case R.id.action_sharetask:
//                    new MarketShareDialog().show(getFragmentManager(), db.getSelectedIds());
                    MainActivity.this.shareTasks(db.getSelectedIds());
//                    if (mLastActionMode != null) {
//                        mLastActionMode.finish();
//                    }
//                    return true;
                case R.id.action_select_all:
                    db.setTaskSelectAll();
                    if (mainAdapter != null) {
                        mainAdapter.notifyDataSetChanged();
                    }
                    break;
                case R.id.action_copy_link:
                    copy_direct_link = true;
                    forceReloadAdapter();
                    return true;
                case R.id.action_settings:
                    startActivity(new Intent(MainActivity.this, SettingsSavableActivity.class));
                    return true;
                default:
            }
            mode.finish();

            return true;
        }


        @Override
        public boolean onCreateActionMode(@NotNull ActionMode mode, @NotNull Menu menu) {
            mActionModeStarted = true;
            mLastActionMode = mode;
            getMenuInflater().inflate(R.menu.mainactionedit, menu);
            updateTitleMenu();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionModeStarted = false;
            mActionMode = new MainActionMode();
            list.setMultiChoiceModeListener(mActionMode);
            db.setTaskUnselectAll();
            if (mainAdapter != null) {
                mainAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            if (mainAdapter == null) return;
            final long packedPosition = list.getExpandableListPosition(position);
            final int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
            mainAdapter.setGroupSelected(groupPosition, checked);
            updateTitleMenu();
        }

        @Override
        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            if (!mActionModeStarted || mainAdapter == null) return false;
            boolean checked = !mainAdapter.isGroupSelected(groupPosition);
            mainAdapter.setGroupSelected(groupPosition, checked);
            updateTitleMenu();
            return true;
        }
    }

}
