package il.co.kix.minitasker;

/**
 * Created by phantom on 01/11/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum SqlDataType {
    NULL,
    INTEGER,
    BOOLEAN,
    ENUM,
    SET,
    REAL,
    TEXT,
    BLOB,
    PRIMARY,
    INTEGER_KEY
}
