package il.co.kix.minitasker.actions;

import android.content.Context;
import android.net.ConnectivityManager;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * mobile data connection action
 * <p/>
 * Created by sphantom on 7/28/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MobileDataAction extends StatesAction {

    private Object iConnectivityManager;
    private Method setMobileDataEnabledMethod;
    private Method getMobileDataEnabledMethod;

    @NotNull
    @Override
    public String getDeviceName() {
        return "mobile";
    }

    @Override
    protected boolean perform_init() {
        try {
            final ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final Class conmanClass = Class.forName(connectivityManager.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);

            iConnectivityManager = iConnectivityManagerField.get(connectivityManager);
            final Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            getMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("getMobileDataEnabled");
            getMobileDataEnabledMethod.setAccessible(true);
            return true;
        } catch (ClassNotFoundException ignored) {
        } catch (NoSuchMethodException ignored) {
        } catch (IllegalAccessException ignored) {
        } catch (NoSuchFieldException ignored) {
        }
        setFlag(FlagsEnum.MOBILE_DATA_FAILED_REFLECTION);
        return false;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        try {
            Boolean result = (Boolean) getMobileDataEnabledMethod.invoke(iConnectivityManager);
            return result != null && result ? StateType.On : StateType.Off;
        } catch (IllegalAccessException ignored) {
            setFlag(FlagsEnum.MOBILE_DATA_FAILED_ILLEGAL_ACCESS_EXCEPTION);
        } catch (InvocationTargetException ignored) {
            setFlag(FlagsEnum.MOBILE_DATA_FAILED_INVOCATION_TARGET_EXCEPTION);
        }
        return StateType.Off;
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.MOBILE;
    }

    @Override
    protected boolean perform_on() {
        try {
            setMobileDataEnabledMethod.invoke(iConnectivityManager, true);
        } catch (IllegalAccessException ignored) {
            setFlag(FlagsEnum.MOBILE_DATA_FAILED_ILLEGAL_ACCESS_EXCEPTION);
        } catch (InvocationTargetException ignored) {
            setFlag(FlagsEnum.MOBILE_DATA_FAILED_INVOCATION_TARGET_EXCEPTION);
        }
        return true;
    }

    @Override
    protected boolean perform_off() {
        try {
            setMobileDataEnabledMethod.invoke(iConnectivityManager, false);
        } catch (IllegalAccessException ignored) {
            setFlag(FlagsEnum.MOBILE_DATA_FAILED_ILLEGAL_ACCESS_EXCEPTION);
        } catch (InvocationTargetException ignored) {
            setFlag(FlagsEnum.MOBILE_DATA_FAILED_INVOCATION_TARGET_EXCEPTION);
        }
        return true;
    }

    @Override
    protected void perform_fail() {
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_mobile_data_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_mobile_data_individual_on);
            case Off:
                return context.getString(R.string.action_mobile_data_individual_off);
            case Toggle:
                return context.getString(R.string.action_mobile_data_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_mobile_data_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_mobile_data_description_on);
            case Off:
                return context.getString(R.string.action_mobile_data_description_off);
            case Toggle:
                return context.getString(R.string.action_mobile_data_description_toggle);
        }
        return "";
    }
}
