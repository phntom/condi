package il.co.kix.minitasker.actions;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.MainActivity;
import il.co.kix.minitasker.ui.NewTaskPhoneNotificationFragment;

/**
 * notification message action
 * <p/>
 * Created by phantom on 27/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MessageAction extends ActionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "message", SqlDataType.TEXT
    );

    @Nullable
    String message;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        message = myValues.getAsString("message");
    }

    @Nullable
    public String getMsg() {
        return message;
    }

    public void setMsg(@NotNull String msg) {
        message = msg;
        myValues.put("message", message);
    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.action_message_individual, message);
    }

    @NotNull
    @Override
    public String getName() {
        return "message";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.action_message_description, message);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskPhoneNotificationFragment.class;
    }

    @Override
    public void init() {
        message = "";
    }

    @Override
    public void perform(StartType is_start) {
        if (isRestore()) return;
        Intent targetIntent = new Intent(context, MainActivity.class);
        targetIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                targetIntent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_minitasker)
                        .setContentTitle(context.getString(R.string.minitasker_title))
                        .setContentText(message);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }
}