package il.co.kix.minitasker.actions;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.ui.NewTaskAppsListFragment;

/**
 * application launcher action
 * <p/>
 * Created by phantom on 26/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class LaunchAction extends PackageBaseAction {
    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.action_launch_individual, packageDisplayName);
    }

    @NotNull
    @Override
    public String getName() {
        return "launch";
    }

    @NotNull
    @Override
    public String getDescription() {
        return context.getString(R.string.action_launch_description, packageDisplayName);
    }

    @NotNull
    @Override
    public String getRelatedFragmentTag() {
        return NewTaskAppsListFragment.LAUNCH;
    }

    @Override
    public void perform(StartType is_start) {
        if (isRestore()) return;
        //noinspection ConstantConditions
        try {
            final PackageManager manager = context.getPackageManager();
            if (manager != null) {
                Intent LaunchIntent = manager.getLaunchIntentForPackage(packageName);
                context.startActivity(LaunchIntent);
                return;
            }
        } catch (ActivityNotFoundException ignored) {
            setFlag(FlagsEnum.LAUNCH_FAILED_APP_NOT_FOUND);
        } catch (Exception ignored) {
        }
        setFlag(FlagsEnum.LAUNCH_FAILED_MISC);
    }
}

