package il.co.kix.minitasker.ui;

import android.view.Menu;
import android.view.MenuItem;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;

/**
 * common base for phone sms activity and savable condition fragment activity
 * <p/>
 * Created by phantom on 18/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class SavableFragmentActivity extends MiniTaskerActivity {

    @Nullable
    protected MenuItem save = null;
    @Nullable
    protected MenuItem inverse = null;

    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save, menu);
        save = menu.findItem(R.id.action_saveitem);
        inverse = menu.findItem(R.id.action_inverse);
        setSaveEnabled();
        return true;
    }

    abstract boolean isSaveEnabled();

    void setSaveEnabled() {
        if (save != null)
            save.setEnabled(isSaveEnabled());
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_saveitem:
                performSave();
                break;
            case R.id.action_inverse:
                if (inverse != null)
                    inverse.setChecked(!inverse.isChecked());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected abstract void performSave();

}
