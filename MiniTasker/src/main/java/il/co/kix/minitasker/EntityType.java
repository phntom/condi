package il.co.kix.minitasker;

/**
 * holds type values for entities
 *
 * Created by phantom on 27/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum EntityType {
    ENTITY_BASE,
    ENTITY_CONDITION,
    ENTITY_ACTION,
    ENTITY_TASK
}
