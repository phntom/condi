package il.co.kix.minitasker;

import android.content.Context;
import android.location.LocationManager;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * GPS mode enabler
 *
 * Created by sphantom on 9/4/13.
 */
public class GPSModeEnabler  extends ExecuteAsRootBase {
    private final Context context;
    private final boolean enable;

    public GPSModeEnabler(Context context, boolean enable) {
        super(context);
        this.context = context;
        this.enable = enable;
    }

    public boolean perform() {
        return execute();
    }


    @NotNull
    @Override
    protected ArrayList<String> getCommandsToExecute() {
        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providersList = locationManager.getProviders(true);
        if (enable && !providersList.contains("gps")) {
            providersList.add("gps");
        }
        if (!enable && providersList.contains("gps")) {
            providersList.remove("gps");
        }
        final String providers = TextUtils.join(",",providersList);
        return new ArrayList<String>() {{
            add(String.format("settings put secure location_providers_allowed %s", providers));
            add(String.format("CLASSPATH=/system/framework/settings.jar exec app_process /system/bin com.android.commands.settings.SettingsCmd put secure location_providers_allowed %s", providers));
        }};
    }
}
