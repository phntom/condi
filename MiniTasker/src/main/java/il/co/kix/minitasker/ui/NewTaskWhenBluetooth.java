package il.co.kix.minitasker.ui;

import android.app.LoaderManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.AsyncTaskLoader;
import android.content.Loader;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.BluetoothCondition;
import il.co.kix.minitasker.conditions.ConditionBase;

/**
 * Created by phantom on 4/6/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenBluetooth extends SavableConditionFragmentActivity
        implements LoaderManager.LoaderCallbacks<ListAdapter>, AdapterView.OnItemClickListener {
    private String lastMac = null;
    private String lastFriendly = null;
    private ListView lstBluetooth;
    private View pbrLoading;
    private List<Pair<String, String>> lstDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BluetoothCondition condition = (BluetoothCondition) getCondition();
        lastMac = condition.getMac();
        lastFriendly = condition.getFriendly();
        if (TextUtils.isEmpty(lastFriendly)) {
            lastFriendly = "Any bluetooth device"; //TODO: translate
        }
        if (TextUtils.isEmpty(lastMac)) {
            lastMac = "%";
        }
        lstBluetooth = (ListView) findViewById(R.id.list);
        lstBluetooth.setOnItemClickListener(this);
        pbrLoading = findViewById(R.id.pbrLoading);

        LoaderManager loaderManager = getLoaderManager();
        assert loaderManager != null;
        Loader loader = getLoaderManager().getLoader(0);
        if (loader != null && loader.isReset()) {
            getLoaderManager().restartLoader(0, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        }

    }

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new BluetoothCondition();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void performSaveCondition(ConditionBase condition1) {
        BluetoothCondition condition = (BluetoothCondition) condition1;
        condition.setMac(lastMac);
        condition.setFriendly(lastFriendly);
    }

    @Override
    boolean isSaveEnabled() {
        return true;
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.list_activity;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.condition_bluetooth_title;
    }

    @NotNull
    @Override
    public Loader<ListAdapter> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<ListAdapter>(this) {
            @NotNull
            @Override
            public SimpleAdapter loadInBackground() {
                lstDevices = new ArrayList<>();
                ArrayList<Map<String, String>> data = new ArrayList<>();
                final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                data.add(ImmutableMap.of("friendly", "Any bluetooth device"));
                lstDevices.add(Pair.create("%", "Any bluetooth device"));

                if (btAdapter != null) {
                    final Set<BluetoothDevice> bondedDevices = btAdapter.getBondedDevices();
                    for (final BluetoothDevice bondedDevice : bondedDevices) {
                        data.add(ImmutableMap.of("friendly", bondedDevice.getName()));
                        lstDevices.add(Pair.create(bondedDevice.getAddress(), bondedDevice.getName()));
                    }
                }

                return new SimpleAdapter(
                        getContext(),
                        data,
                        android.R.layout.simple_selectable_list_item,
                        new String[]{"friendly"},
                        new int[]{android.R.id.text1}
                );
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<ListAdapter> loader, ListAdapter data) {
        lstBluetooth.setAdapter(data);
        lstBluetooth.setVisibility(View.VISIBLE);
        pbrLoading.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<ListAdapter> loader) {
        lstBluetooth.setAdapter(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        lastMac = lstDevices.get(position).first;
        lastFriendly = lstDevices.get(position).second;
    }
}
