package il.co.kix.minitasker.ui;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import org.jetbrains.annotations.Nullable;

import java.util.Calendar;

/**
 * new task for time and date activity
 * <p/>
 * Created by phantom on 08/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */

public class NewTaskWhenTimeAndDateTimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    static final String TAG_TIME_START = NewTaskWhenTimeAndDateActivity.TAG_TIME_START;
    static final String TAG_TIME_END = NewTaskWhenTimeAndDateActivity.TAG_TIME_END;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        NewTaskWhenTimeAndDateActivity source = (NewTaskWhenTimeAndDateActivity) getActivity();
        assert source != null;
        final String tag = (getTag() == null ? "" : getTag());

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog time_picker;
        if (tag.equals(TAG_TIME_END) && source.time_end != -1) {
            hour = source.time_end / 60;
            minute = source.time_end % 60;
        } else if (source.time_start != -1) {
            hour = source.time_start / 60;
            minute = source.time_start % 60;
        }

        if (tag.equals(TAG_TIME_START)) {
            time_picker = new TimePickerDialog(source, this, hour, minute,
                    DateFormat.is24HourFormat(source));
        } else {
            time_picker = new TimePickerWithCancelDialog(source, this, hour, minute,
                    DateFormat.is24HourFormat(source));
        }

        // Create a new instance of TimePickerDialog and return it
        return time_picker;
    }

    public void onTimeSet(@Nullable TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        NewTaskWhenTimeAndDateActivity source = (NewTaskWhenTimeAndDateActivity) getActivity();
        if (source == null) return;
        String tag = getTag();
        int time = (view == null ? -1 : hourOfDay * 60 + minute);
        if (tag != null) {
            source.setTime(tag, time);
        }
    }
}
