package il.co.kix.minitasker.receivers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.actions.PhoneAction;
import il.co.kix.minitasker.conditions.PhoneCondition;

/**
 * receives SMS broadcast
 * <p/>
 * Created by phantom on 29/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class SmsReceiver extends BackgroundBroadcastReceiver {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
//    private static final String APPTAG = "MiniTasker";

    public void onReceiveBackground(@NotNull Context context, @NotNull Intent intent) {
        //noinspection ConstantConditions
        if (intent.getAction().equals(SMS_RECEIVED)) {
            Bundle bundle = intent.getExtras();
            if (bundle == null) return;
            StringBuilder messageBuilder = new StringBuilder();
            Object[] pdus = (Object[]) bundle.get("pdus");
            final SmsMessage[] messages = new SmsMessage[pdus.length];
            for (int i = 0; i < pdus.length; i++) {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                messageBuilder.append(messages[i].getMessageBody());
            }
            if (messages.length > -1) {
                String message = messageBuilder.toString();
                String number = messages[0].getOriginatingAddress();

                boolean private_number = number == null || number.isEmpty() || !number.matches("\\+?\\d+");
                boolean recognized_number = false;
                String display_name = null;
                if (!private_number) {
                    //check if known number
                    // requires permission READ_CONTACTS
                    final Uri baseUri = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
                    assert baseUri != null;
                    Uri uri = Uri.withAppendedPath(baseUri, Uri.encode(number));
                    ContentResolver resolver = context.getContentResolver();
                    assert uri != null;
                    Cursor c = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
                    if (c != null && c.moveToFirst()) {
                        recognized_number = true;
                        display_name = c.getString(c.getColumnIndexOrThrow(ContactsContract.PhoneLookup.DISPLAY_NAME));
                    }
                    if (c != null) {
                        c.close();
                    }
                } else {
                    number = "private";
                }
                DatabaseHandler db = SingletonDatabase.INSTANCE.db;
                assert db != null;

                db.setPrivateSetting(PhoneAction.KEY_PREVIOUS_MESSAGE, message);
                db.setPrivateSetting(PhoneAction.KEY_PREVIOUS_ORIGIN,
                        (display_name == null ? number : display_name));

                final String[] query_parameters = new String[]{};

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("z.kind = '");
                stringBuilder.append(PhoneCondition.KIND_FORWARD);
                stringBuilder.append("' AND (");

                if (private_number) {
                    stringBuilder.append("z.private = 1");
                } else {
                    stringBuilder.append("('");
                    stringBuilder.append(number);
                    stringBuilder.append("' LIKE z.phone AND z.inverse = 0) OR ('");
                    stringBuilder.append(number);
                    stringBuilder.append("' NOT LIKE z.phone AND z.inverse = 1)");
                }
                if (!recognized_number) {
                    stringBuilder.append(" OR z.unrecognized = 1");
                }
                stringBuilder.append(")");

                db.runMatchingConditions("phone", stringBuilder.toString(), query_parameters, StartType.START_ONLY);

                db.setPrivateSetting(PhoneAction.KEY_PREVIOUS_MESSAGE, "");
                db.setPrivateSetting(PhoneAction.KEY_PREVIOUS_ORIGIN, "");

            }
        }
    }
}
