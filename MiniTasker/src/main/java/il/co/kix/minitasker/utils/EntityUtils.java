package il.co.kix.minitasker.utils;

import android.content.Context;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.EntityBase;
import il.co.kix.minitasker.EntityType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.actions.BrightnessAction;
import il.co.kix.minitasker.actions.LaunchAction;
import il.co.kix.minitasker.actions.MediaAction;
import il.co.kix.minitasker.actions.MessageAction;
import il.co.kix.minitasker.actions.NavigationAction;
import il.co.kix.minitasker.actions.PhoneAction;
import il.co.kix.minitasker.actions.SmsAction;
import il.co.kix.minitasker.actions.StatesAction;
import il.co.kix.minitasker.actions.TerminateAction;
import il.co.kix.minitasker.actions.VolumeControlAction;
import il.co.kix.minitasker.conditions.ActivityCondition;
import il.co.kix.minitasker.conditions.BatteryCondition;
import il.co.kix.minitasker.conditions.BluetoothCondition;
import il.co.kix.minitasker.conditions.BootCondition;
import il.co.kix.minitasker.conditions.CalendarCondition;
import il.co.kix.minitasker.conditions.ChargerCondition;
import il.co.kix.minitasker.conditions.DockCondition;
import il.co.kix.minitasker.conditions.HdmiCondition;
import il.co.kix.minitasker.conditions.HeadsetCondition;
import il.co.kix.minitasker.conditions.LocationCondition;
import il.co.kix.minitasker.conditions.PhoneCondition;
import il.co.kix.minitasker.conditions.SettingsCondition;
import il.co.kix.minitasker.conditions.ShortcutCondition;
import il.co.kix.minitasker.conditions.TimeCondition;
import il.co.kix.minitasker.conditions.WifiCondition;

import static il.co.kix.minitasker.EntityBase.APPTAG;

/**
 * Created by phantom on 01/11/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public final class EntityUtils {
    @NotNull
    public static final Map<String, Class<? extends EntityBase>> conditions
            = ImmutableMap.<String, Class<? extends EntityBase>>builder()
            .put("activity", ActivityCondition.class)
            .put("battery", BatteryCondition.class)
            .put("boot", BootCondition.class)
            .put("calendar", CalendarCondition.class)
            .put("charger", ChargerCondition.class)
            .put("dock", DockCondition.class)
            .put("headset", HeadsetCondition.class)
            .put("location", LocationCondition.class)
            .put("phone", PhoneCondition.class)
            .put("settings", SettingsCondition.class)
            .put("shortcut", ShortcutCondition.class)
            .put("time", TimeCondition.class)
            .put("wifi", WifiCondition.class)
            .put("hdmi", HdmiCondition.class)
            .put("bluetooth", BluetoothCondition.class)
            .build();

    @NotNull
    public static final Map<String, Class<? extends EntityBase>> actions
            = ImmutableMap.<String, Class<? extends EntityBase>>builder()
            .put("brightness", BrightnessAction.class)
            .put("launch", LaunchAction.class)
            .put("media", MediaAction.class)
            .put("message", MessageAction.class)
            .put("navigation", NavigationAction.class)
            .put("phone", PhoneAction.class)
            .put("sms", SmsAction.class)
            .put("states", StatesAction.class)
            .put("terminate", TerminateAction.class)
            .put("volumecontrol", VolumeControlAction.class)
            .build();


    @NotNull
    public static final Map<Class<? extends EntityBase>, Map<String, SqlDataType>> fields
            = ImmutableMap.<Class<? extends EntityBase>, Map<String, SqlDataType>>builder()
            .put(ActivityCondition.class, ActivityCondition.fields)
            .put(BatteryCondition.class, BatteryCondition.fields)
            .put(BootCondition.class, BootCondition.fields)
            .put(CalendarCondition.class, CalendarCondition.fields)
            .put(ChargerCondition.class, ChargerCondition.fields)
            .put(DockCondition.class, DockCondition.fields)
            .put(HeadsetCondition.class, HeadsetCondition.fields)
            .put(LocationCondition.class, LocationCondition.fields)
            .put(PhoneCondition.class, PhoneCondition.fields)
            .put(SettingsCondition.class, SettingsCondition.fields)
            .put(ShortcutCondition.class, ShortcutCondition.fields)
            .put(TimeCondition.class, TimeCondition.fields)
            .put(WifiCondition.class, WifiCondition.fields)
            .put(HdmiCondition.class, HdmiCondition.fields)
            .put(BluetoothCondition.class, BluetoothCondition.fields)
            .put(BrightnessAction.class, BrightnessAction.fields)
            .put(LaunchAction.class, LaunchAction.fields)
            .put(MediaAction.class, MediaAction.fields)
            .put(MessageAction.class, MessageAction.fields)
            .put(NavigationAction.class, NavigationAction.fields)
            .put(PhoneAction.class, PhoneAction.fields)
            .put(SmsAction.class, SmsAction.fields)
            .put(StatesAction.class, StatesAction.fields)
            .put(TerminateAction.class, TerminateAction.fields)
            .put(VolumeControlAction.class, VolumeControlAction.fields)
            .build();


    @Nullable
    public static Map<String, SqlDataType> getFields(EntityType type, String name) {
        if (type == EntityType.ENTITY_ACTION) {
            return fields.get(actions.get(name));
        }
        if (type == EntityType.ENTITY_CONDITION) {
            return fields.get(conditions.get(name));
        }
        return null;
    }

    @Nullable
    public static EntityBase entityFactory(@NotNull Context context, long primary,
                                           final String entityType,
                                           final Map<String, Class<? extends EntityBase>> entities,
                                           final String entityCategory) {
        assert SingletonDatabase.INSTANCE.db != null;
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        String type = db.getTypeById(primary, entityType);
        if (type == null) return null;
        if (!entities.containsKey(type)) {
            Log.e(APPTAG, context.getString(R.string.action_base_failed_instantiate,
                    type + entityCategory));
            return null;
        }
        EntityBase result;
        try {
            if (entityType.equals("what") && type.equals("states")) {
                int item_id = db.getItemByEntity(primary, entityType);
                result = StatesAction.AbstractFactoryHelper(db, item_id);
            } else {
                result = entities.get(type).newInstance();
            }
            if (result == null) return null;
            result.setContext(context);
            result.setPrimary(primary);
            return result;
        } catch (@NotNull InstantiationException e) {
            Log.e(APPTAG, "entityFactory " + entityCategory + " InstantiationException primary: "
                    + primary + " type: " + type);
        } catch (@NotNull IllegalAccessException e) {
            Log.e(APPTAG, "entityFactory " + entityCategory + " IllegalAccessException primary: "
                    + primary + " type: " + type);
        }

        return null;
    }
}
