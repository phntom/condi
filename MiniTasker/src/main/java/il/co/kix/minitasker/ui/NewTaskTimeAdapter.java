package il.co.kix.minitasker.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;

/**
 * time adapter for new task when time and date activity
 * <p/>
 * Created by phantom on 05/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskTimeAdapter extends BaseAdapter
        implements CompoundButton.OnCheckedChangeListener {
    static final int itemCount = 11;
    static final int POSITION_START_TIME = 1;
    static final int POSITION_END_TIME = 2;
    static final int POSITION_REPEAT_SUNDAY = 4;
    @NotNull
    private final TextView[] views;
    @NotNull
    private final String[] texts;
    @NotNull
    private final boolean[] checked;
    private final Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public NewTaskTimeAdapter(Context context) {
        super();
        this.context = context;
        views = new TextView[itemCount];
        texts = new String[itemCount];
        checked = new boolean[itemCount];
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    public int getItemViewLayer(int position) {
        switch (position) {
            case 0:
            case 3:
                return R.layout.list_subtitle;
            case POSITION_START_TIME:
            case POSITION_END_TIME:
                return R.layout.list_item_1;
            default:
                return R.layout.list_item_checkbox_small;
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
            case 3:
                return 0;
            case POSITION_START_TIME:
            case POSITION_END_TIME:
                return 1;
            default:
                return 2;
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        switch (position) {
            case 0:
            case 3:
                return false;
            default:
                return true;
        }
    }


    @Override
    public int getCount() {
        return itemCount;
    }

    @Override
    public Object getItem(int position) {
        return views[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = inflater.inflate(getItemViewLayer(position), null);
            assert convertView != null;
            switch (getItemViewType(position)) {
                case 2:
                    CheckBox checkBox = (CheckBox) convertView;
                    checkBox.setOnCheckedChangeListener(this);
                    break;
            }
        }
        TextView textView = (TextView) convertView;
        views[position] = textView;
        textView.setFocusable(!isEnabled(position));
        textView.setTag(position);
        setText(textView, position);
        if (getItemViewType(position) == 2) {
            CheckBox checkBox = (CheckBox) convertView;
            checkBox.setChecked(checked[position]);
            checkBox.setTag(position);
        }
        return convertView;
    }

    public void setCheckedOnItemClickListener(AdapterView.OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public void setText(@NotNull TextView convertView, int position) {
        if (texts[position] != null) {
            convertView.setText(texts[position]);
            return;
        }
        switch (position) {
            case 0:
                convertView.setText(R.string.time_title_1);
                break;
            case 1:
                convertView.setText(R.string.time_set_start_no_selection);
                break;
            case 2:
                convertView.setText(R.string.time_set_end_no_selection);
                break;
            case 3:
                convertView.setText(R.string.time_title_2);
                break;
            case 4:
                convertView.setText(R.string.time_repeat_sun);
                break;
            case 5:
                convertView.setText(R.string.time_repeat_mon);
                break;
            case 6:
                convertView.setText(R.string.time_repeat_tue);
                break;
            case 7:
                convertView.setText(R.string.time_repeat_wed);
                break;
            case 8:
                convertView.setText(R.string.time_repeat_thu);
                break;
            case 9:
                convertView.setText(R.string.time_repeat_fri);
                break;
            case 10:
                convertView.setText(R.string.time_repeat_sat);
                break;
        }
    }

    public void modifyText(int position, String text) {
        TextView textView = views[position];
        texts[position] = text;
        Integer item_position = (textView != null ? (Integer) textView.getTag() : null);
        if (textView == null || item_position == null || item_position != position) return;
        textView.setText(text);
    }

    public void modifyChecked(int position, boolean is_checked) {
        CheckBox checkBox = (CheckBox) views[position];
        checked[position] = is_checked;
        if (checkBox == null) return;
        checkBox.setChecked(is_checked);
    }

    @Override
    public void onCheckedChanged(@NotNull CompoundButton buttonView, boolean isChecked) {
        Integer position = (Integer) buttonView.getTag();
        if (position == null) return;
        checked[position] = isChecked;
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(null, buttonView, position, (isChecked ? 1 : 0));
        }

    }
}
