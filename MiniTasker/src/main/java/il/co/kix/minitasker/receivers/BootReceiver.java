package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.BackgroundProvider;
import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;

/**
 * on boot broadcast receiver
 * <p/>
 * Created by phantom on 06/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BootReceiver extends BackgroundBroadcastReceiver {
    static private final String[] empty = new String[]{};
    static private final String boot = "boot";
    static private final String oneEqOne = "1=1";

    @SuppressWarnings("ConstantConditions")
    public void onReceiveBackground(@NotNull Context context, Intent intent) {
        Log.w(DatabaseHandler.APPTAG, "onBoot");

        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;

//        db.autoCleanup();
        db.clearLast();
        db.resetRunningConditions();

        final Uri baseUri = Uri.parse("content://" + BackgroundProvider.BACKGROUND_PROVIDER);
        context.getContentResolver().query(Uri.withAppendedPath(baseUri, "/on_boot/self"),
                null, null, null, null);

        db.runMatchingConditions(boot, oneEqOne, empty, StartType.START_ONLY);
    }
}
