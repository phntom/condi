package il.co.kix.minitasker.actions;

import android.os.Build;
import android.provider.Settings;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * Created by phantom on 22/02/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 * WIFI_SCAN_ALWAYS_AVAILABLE
 */
public class ScanningAction extends StatesAction {
    public static final String WIFI_SCAN_ALWAYS_AVAILABLE =
            "wifi_scan_always_enabled";

    @NotNull
    @Override
    public String getDeviceName() {
        return "scanning";
    }

    @Override
    protected boolean perform_init() {
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        int scanning_enabled = Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 ? 0 :
                Settings.Global.getInt(context.getContentResolver(), WIFI_SCAN_ALWAYS_AVAILABLE, 0);
        switch (scanning_enabled) {
            case 1:
                return StateType.On;
            case 0:
            default:
                return StateType.Off;
        }
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.SCANNING;
    }

    @Override
    protected boolean perform_on() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            return false;
        Settings.Global.putInt(context.getContentResolver(), WIFI_SCAN_ALWAYS_AVAILABLE, 1);
        return true;
    }

    @Override
    protected boolean perform_off() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            return false;
        Settings.Global.putInt(context.getContentResolver(), WIFI_SCAN_ALWAYS_AVAILABLE, 0);
        return true;
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.SCANNING_NOT_SUPPORTED);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_scanning_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_scanning_individual_on);
            case Off:
                return context.getString(R.string.action_scanning_individual_off);
            case Toggle:
                return context.getString(R.string.action_scanning_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_scanning_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_scanning_description_on);
            case Off:
                return context.getString(R.string.action_scanning_description_off);
            case Toggle:
                return context.getString(R.string.action_scanning_description_toggle);
        }
        return "";
    }
}
