package il.co.kix.minitasker.receivers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.conditions.PhoneCondition;

/**
 * phone calls broadcast receiver
 * <p/>
 * Created by phantom on 29/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class PhoneCallReceiver extends BackgroundBroadcastReceiver {
    // --Commented out by Inspection (28/09/13 23:45):public static final String APPTAG = "MiniTasker";
    public static final String KEY_PREVIOUS_NUMBER =
            "il.co.kix.minitasker.KEY_PREVIOUS_NUMBER";


    public void onReceiveBackground(@NotNull Context context, @NotNull Intent intent) {
//        Log.w("MiniTasker", "PhoneCallReceiver "+intent.getAction() + " state: "
//                + intent.getStringExtra(TelephonyManager.EXTRA_STATE));
        //noinspection ConstantConditions
        String action = intent.getAction();
        if (action == null) return;
        if (!action.equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)) return;
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (state == null) return;

        String number;
        StartType start;
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            db.setPrivateSetting(KEY_PREVIOUS_NUMBER, number);
            start = StartType.START_WITH_END;
        } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            number = db.getPrivateSetting(KEY_PREVIOUS_NUMBER, "");
            start = StartType.END;
        } else {
            return;
        }
//        Log.w(APPTAG, "Incoming call from "+number+" "+(start == StartType.END ? "end" : "start"));
        boolean private_number = number == null || number.isEmpty() || !number.matches("\\+?\\d+");
        boolean recognized_number = false;
        if (!private_number) {
            //check if known number
            // requires permission READ_CONTACTS
            final Uri baseUri = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
            assert baseUri != null;
            Uri uri = Uri.withAppendedPath(baseUri, Uri.encode(number));
            ContentResolver resolver = context.getContentResolver();
            assert uri != null;
            Cursor c = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
            if (c != null && c.moveToFirst()) recognized_number = true;
            if (c != null) c.close();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("z.kind = '");
        stringBuilder.append(PhoneCondition.KIND_BLOCK);
        stringBuilder.append("' AND (");

        if (private_number) {
            stringBuilder.append("z.private = 1");
        } else {
            stringBuilder.append("('");
            stringBuilder.append(number);
            stringBuilder.append("' LIKE z.phone AND z.inverse = 0) OR ('");
            stringBuilder.append(number);
            stringBuilder.append("' NOT LIKE z.phone AND z.inverse = 1)");
        }
        if (!recognized_number) {
            stringBuilder.append(" OR z.unrecognized = 1");
        }
        stringBuilder.append(")");

        final String[] query_parameters = new String[]{};
        db.runMatchingConditions("phone", stringBuilder.toString(), query_parameters, start);
    }
}
