package il.co.kix.minitasker.conditions;

import android.content.ContentValues;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.SettingsSavableActivity;

/**
 * settings activity data container
 * <p/>
 * Created by phantom on 29/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class SettingsCondition extends ConditionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "statistics", SqlDataType.BOOLEAN,
            "activity_timeout", SqlDataType.INTEGER
    );

    private boolean statistics;

    @Override
    public void setPrimary(long prim) {
        super.setPrimary(db.getSettingsPrimary());
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        statistics = myValues.getAsInteger("statistics") == 1;
    }

    @NotNull
    @Override
    public String toString() {
        return getName();
    }

    @NotNull
    @Override
    public String getName() {
        return "settings";
    }

    @NotNull
    @Override
    public String getDescription() {
        return getName();
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return SettingsSavableActivity.class;
    }

    @Override
    public void init() {
        statistics = false;
    }

    public void setActivity_timeout(int activity_timeout) {
        myValues.put("activity_timeout", activity_timeout);
    }

    public boolean isStatistics() {
        return statistics;
    }

    public void setStatistics(boolean statistics) {
        this.statistics = statistics;
        myValues.put("statistics", (statistics ? 1 : 0));
    }

    @Override
    public void setTaskId(int tId) {
        task_id = -1;
        //do not set a task id, this is not a real condition
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        return false;
    }
}
