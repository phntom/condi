package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;

/**
 * headset state broadcast receiver
 * <p/>
 * Created by phantom on 06/08/13.
 * <p/>
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class HeadsetStateReceiver extends BackgroundBroadcastReceiver {
    public void onReceiveBackground(Context context, @NotNull Intent intent) {
        boolean is_plugged = intent.getIntExtra("state", 0) == 1;
//        String type = intent.getStringExtra("name");
        boolean has_mic = intent.getIntExtra("microphone", 0) == 1;

        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        final String when_query_start = "z.state " +
                (is_plugged ? "IN (0, " + (has_mic ? "1" : "2") + ")"
                        : "IN (3, " + (has_mic ? "4" : "5") + ")");
        final String when_query_end = "z.state " +
                (is_plugged ? "IN (3, " + (has_mic ? "4" : "5") + ")"
                        : "IN (0, " + (has_mic ? "1" : "2") + ")");
        final String[] when_parameters = new String[]{};
        db.runMatchingConditions("headset", when_query_end, when_parameters, StartType.END);
        db.runMatchingConditions("headset", when_query_start, when_parameters, StartType.START_WITH_END);
    }
}
