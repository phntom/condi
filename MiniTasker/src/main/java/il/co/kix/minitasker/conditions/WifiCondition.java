package il.co.kix.minitasker.conditions;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Calendar;
import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.receivers.PhoneReceiver;
import il.co.kix.minitasker.ui.NewTaskWhenWifi;
import il.co.kix.minitasker.utils.WildcardUtils;

/**
 * Wifi status change condition
 * <p/>
 * Created by phantom on 07/10/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class WifiCondition extends ConditionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "ssid", SqlDataType.TEXT,
            "bssid", SqlDataType.TEXT,
            "ip", SqlDataType.TEXT,
            "inverse", SqlDataType.BOOLEAN
    );

    private static final String WILDCARD = "%";

    private boolean inverse;
    @Nullable
    private String ssid;
    @Nullable
    private String bssid;
    @Nullable
    private String ip;

    static final String FIELD_SSID = "FIELD_SSID";
    static final String FIELD_BSSID = "FIELD_BSSID";
    static final String FIELD_IP = "FIELD_IP";
    static final String FIELD_CONNECTED = "FIELD_CONNECTED";


    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        ssid = myValues.getAsString("ssid");
        bssid = myValues.getAsString("bssid");
        ip = myValues.getAsString("ip");
        //noinspection ConstantConditions
        inverse = myValues.getAsInteger("inverse") == 1;
    }

    @NotNull
    public String getDescriptionAux(boolean verbose) {
        String[] container = new String[3];
        int count = 0;
        if (ssid != null && !ssid.isEmpty()) {
            boolean ssid_suffix = ssid.charAt(0) == '%';
            boolean ssid_prefix = ssid.charAt(ssid.length() - 1) == '%';
            if (ssid.equals(WILDCARD)) {
                if (verbose) {
                    container[count] = context.getString(R.string.condition_wifi_description_ssid_any);
                } else {
                    count--;
                }
            } else if (ssid_prefix && ssid_suffix) {
                String contains = ssid.substring(1, ssid.length() - 2);
                container[count] = context.getString(R.string.condition_wifi_description_ssid_contains, contains);
            } else if (ssid_prefix) {
                String prefix = ssid.substring(1);
                container[count] = context.getString(R.string.condition_wifi_description_ssid_prefix, prefix);
            } else if (ssid_suffix) {
                String suffix = ssid.substring(0, ssid.length() - 1);
                container[count] = context.getString(R.string.condition_wifi_description_ssid_suffix, suffix);
            } else {
                String ssid_matching = ssid.replace("%", "*");
                container[count] = context.getString(R.string.condition_wifi_description_ssid_matching, ssid_matching);
            }
            count++;
        }
        if (bssid != null && !bssid.isEmpty()) {
            boolean bssid_suffix = bssid.charAt(0) == '%';
            boolean bssid_prefix = bssid.charAt(bssid.length() - 1) == '%';
            if (bssid.equals(WILDCARD)) {
                if (verbose) {
                    container[count] = context.getString(R.string.condition_wifi_description_bssid_any);
                } else {
                    count--;
                }
            } else if (bssid_prefix && bssid_suffix) {
                String contains = bssid.substring(1, bssid.length() - 2);
                container[count] = context.getString(R.string.condition_wifi_description_bssid_contains, contains);
            } else if (bssid_prefix) {
                String prefix = bssid.substring(1);
                container[count] = context.getString(R.string.condition_wifi_description_bssid_prefix, prefix);
            } else if (bssid_suffix) {
                String suffix = bssid.substring(0, bssid.length() - 1);
                container[count] = context.getString(R.string.condition_wifi_description_bssid_suffix, suffix);
            } else {
                String bssid_matching = bssid.replace("%", "*");
                container[count] = context.getString(R.string.condition_wifi_description_bssid_matching, bssid_matching);
            }
            count++;
        }
        if (ip != null && !ip.isEmpty()) {
            boolean ip_suffix = ip.charAt(0) == '%';
            boolean ip_prefix = ip.charAt(ip.length() - 1) == '%';
            if (ip.equals(WILDCARD)) {
                if (verbose) {
                    container[count] = context.getString(R.string.condition_wifi_description_ip_any);
                } else {
                    count--;
                }
            } else if (ip_prefix && ip_suffix) {
                String contains = ip.substring(1, ip.length() - 2);
                container[count] = context.getString(R.string.condition_wifi_description_ip_contains, contains);
            } else if (ip_prefix) {
                String prefix = ip.substring(1);
                container[count] = context.getString(R.string.condition_wifi_description_ip_prefix, prefix);
            } else if (ip_suffix) {
                String suffix = ip.substring(0, ip.length() - 1);
                container[count] = context.getString(R.string.condition_wifi_description_ip_suffix, suffix);
            } else {
                String ip_matching = ip.replace("%", "*");
                container[count] = context.getString(R.string.condition_wifi_description_ip_matching, ip_matching);
            }
            count++;
        }

        if (count == 0) {
            if (inverse)
                return context.getString(R.string.condition_wifi_description_inverse_matches_anything);
            return context.getString(R.string.condition_wifi_description_matches_anything);
        }
        String rest;
        switch (count) {
            case 3:
                rest = context.getString(R.string.condition_wifi_description_container_3,
                        container[0], container[1], container[2]);
                break;
            case 2:
                rest = context.getString(R.string.condition_wifi_description_container_2,
                        container[0], container[1]);
                break;
            case 1:
                rest = context.getString(R.string.condition_wifi_description_container_1,
                        container[0]);
                break;
            default:
                return "";
        }
        if (inverse)
            return context.getString(R.string.condition_wifi_description_inverse_container, rest);
        return context.getString(R.string.condition_wifi_description_container, rest);
    }

    @NotNull
    @Override
    public String toString() {
        return getDescriptionAux(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "wifi";
    }

    @NotNull
    @Override
    public String getDescription() {
        return getDescriptionAux(false);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenWifi.class;
    }

    @Override
    public void init() {
        ssid = "%";
        bssid = "%";
        ip = "%";
        inverse = false;
    }

    public boolean isInverse() {
        return inverse;
    }

    public void setInverse(boolean inverse) {
        this.inverse = inverse;
        myValues.put("inverse", (this.inverse ? 1 : 0));
    }

    @NotNull
    public String getSsid() {
        return toRegularWildcard(ssid);
    }

    public void setSsid(@NotNull String ssid) {
        if (ssid.isEmpty())
            ssid = "*";
        this.ssid = cleanupWildcard(ssid);
        myValues.put("ssid", this.ssid);
    }

    @NotNull
    public String getBssid() {
        return toRegularWildcard(bssid);
    }

    public void setBssid(@NotNull String bssid) {
        if (bssid.isEmpty())
            bssid = "*";
        this.bssid = cleanupWildcard(bssid);
        myValues.put("bssid", this.bssid);
    }

    @NotNull
    public String getIp() {
        return toRegularWildcard(ip);
    }

    public void setIp(@NotNull String ip) {
        if (ip.isEmpty())
            ip = "*";
        this.ip = cleanupWildcard(ip);
        myValues.put("ip", this.ip);
    }

    private String cleanupWildcard(String wildcard) {
        String ret = wildcard;
        while (ret.contains("**")) {
            ret = ret.replace("**", "@@@@DOUBLESTAR@@@@");
        }
        ret = ret.replace("*", "%");
        while (ret.contains("@@@@DOUBLESTAR@@@@")) {
            ret = ret.replace("@@@@DOUBLESTAR@@@@", "*");
        }
        return ret;
    }

    @NotNull
    private String toRegularWildcard(@Nullable String sqlWildcard) {
        if (sqlWildcard == null) sqlWildcard = "";
        return sqlWildcard.replace("%", "*");
    }

    @Override
    public boolean save() {
        Intent alarmIntent = new Intent(context, PhoneReceiver.class);
        alarmIntent.putExtra("type", "wifi");

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent wifiPendingIntent = PendingIntent.getBroadcast(context, 6670, alarmIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        long next_time = Calendar.getInstance().getTimeInMillis() + 1000;
        alarmManager.set(AlarmManager.RTC_WAKEUP, next_time, wifiPendingIntent);

        return super.save();
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        Thread.sleep(1000);

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()
                && networkInfo.getType() == ConnectivityManager.TYPE_WIFI
                && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
            WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo currentWifi = mainWifi.getConnectionInfo();
            // Wifi is connected
            String currentSsid = "" + currentWifi.getSSID();
            String currentBssid = "" + currentWifi.getBSSID();
            int ip_int = currentWifi.getIpAddress();
            String currentIp = (ip_int == 0 ? "" : String.format("%d.%d.%d.%d",
                    (ip_int & 0xff),
                    (ip_int >> 8 & 0xff),
                    (ip_int >> 16 & 0xff),
                    (ip_int >> 24 & 0xff)));

            db.setPrivateSetting(FIELD_SSID, currentSsid);
            db.setPrivateSetting(FIELD_BSSID, currentBssid);
            db.setPrivateSetting(FIELD_IP, currentIp);
            db.setPrivateSetting(FIELD_CONNECTED, "1");

            Log.d(APPTAG, "Wifi connected " + networkInfo.toString());

            return matchWifi(is_start, currentSsid, currentBssid, currentIp);

        } else {
            db.setPrivateSetting(FIELD_CONNECTED, "0");
            final String lastSsid = db.getPrivateSetting(FIELD_SSID, "");
            final String lastBssid = db.getPrivateSetting(FIELD_BSSID, "");
            final String lastIp = db.getPrivateSetting(FIELD_IP, "");
            Log.d(APPTAG, "Wifi is disconnected: "
                    + (networkInfo != null ? networkInfo.toString() : ""));

            return matchWifi(!is_start, lastSsid, lastBssid, lastIp);
        }
    }

    private boolean matchWifi(final boolean is_start, @NotNull final String matchSsid,
                              @NotNull final String matchBssid, @NotNull final String matchIp) {
        final boolean matchesSsid = matchSsid.matches(WildcardUtils.toRegex(ssid));
        final boolean matchesBssid = matchBssid.matches(WildcardUtils.toRegex(bssid));
        final boolean matchesIp = matchIp.matches(WildcardUtils.toRegex(ip));
        final boolean matchAll = matchesSsid && matchesBssid && matchesIp;
        final boolean doesNotMatchAll = !matchesSsid || !matchesBssid || !matchesIp;

        return (((!is_start && !inverse) || (is_start && inverse)) && doesNotMatchAll)
                || (((!is_start && inverse) || (is_start && !inverse)) && matchAll);
    }


}
