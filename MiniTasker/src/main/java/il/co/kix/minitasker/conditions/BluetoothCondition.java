package il.co.kix.minitasker.conditions;

import android.content.ContentValues;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskWhenBluetooth;

/**
 * Created by phantom on 4/6/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BluetoothCondition extends ConditionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "mac", SqlDataType.TEXT,
            "name", SqlDataType.TEXT
    );

    private String mac;
    private String friendly;

    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        mac = myValues.getAsString("mac");
        friendly = myValues.getAsString("name");
        if (mac == null) init();
        if (friendly == null) friendly = "";
    }

    @Nullable
    @Override
    public String toString() {
        return context.getString(R.string.condition_bluetooth_individual,
                friendly.isEmpty() ? mac : friendly);
    }

    @NotNull
    @Override
    public String getName() {
        return "bluetooth";
    }

    @Nullable
    @Override
    public String getDescription() {
        return context.getString(R.string.condition_bluetooth_description,
                friendly.isEmpty() ? mac : friendly);
    }

    @Nullable
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenBluetooth.class;
    }

    @Override
    public void init() {
        mac = "";
        friendly = "";
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        myValues.put("mac", mac);
        this.mac = mac;
    }

    public String getFriendly() {
        return friendly;
    }

    public void setFriendly(String friendly) {
        myValues.put("name", friendly);
        this.friendly = friendly;
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        return true;
    }
}
