package il.co.kix.minitasker.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.actions.ActionBase;
import il.co.kix.minitasker.receivers.PhoneReceiver;

/**
 * Created by phantom on 2/16/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class LauncherIntentService extends IntentService {
    private static final String APPTAG = DatabaseHandler.APPTAG;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public LauncherIntentService() {
        super("LauncherIntentService");
    }

    @Override
    protected void onHandleIntent(@NotNull Intent intent) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        try {
            int task_id = intent.getIntExtra("task_id", 0);
            long primary = intent.getLongExtra("primary", 0L);
            boolean is_start = intent.getIntExtra("is_start", 0) == 1;
            if (task_id == 0 || primary == 0) return;
            ActionBase action = ActionBase.ActionBaseFactory(this, primary);
            if (action == null) return;
            if (action.isStart() != is_start) return;
            Log.w(APPTAG, "Run action " + action.toString());

            StartType startType = is_start ? StartType.START_WITH_END : StartType.END;
            action.perform(startType);
            ArrayList<String> rowIDs = new ArrayList<>();
            rowIDs.add("" + primary);
            db.updateRunningActions(rowIDs, startType);
            if (!is_start) {
                db.clearLastTasks(task_id);
            }

            Context context = this;
            //noinspection ConstantConditions
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent alarmIntent = new Intent(context, PhoneReceiver.class);
            alarmIntent.putExtra("type", "toast");
            Calendar calendar = Calendar.getInstance();
            PendingIntent timePendingIntent = PendingIntent.getBroadcast(context, 7016,
                    alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(),
                    timePendingIntent);
        } finally {
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (db) {
                db.runActionCounter--;
            }
        }
        SystemClock.sleep(100);
    }
}
