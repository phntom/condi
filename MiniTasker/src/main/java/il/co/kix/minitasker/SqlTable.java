package il.co.kix.minitasker;

import android.database.DatabaseUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phantom on 15/11/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class SqlTable {
    private final String table_name;
    private final int version_created;
    @NotNull
    private List<String> column_names = new ArrayList<String>();
    @NotNull
    private List<SqlDataType> column_types = new ArrayList<SqlDataType>();
    @NotNull
    private List<Object> column_defaults = new ArrayList<Object>();
    @NotNull
    private List<Integer> column_version = new ArrayList<Integer>();

    SqlTable(String table_name, int version_created) {
        this.table_name = table_name;
        this.version_created = version_created;
    }

    @NotNull
    SqlTable addColumn(String column_name, SqlDataType column_type, Object column_default, int version_added) {
        column_names.add(column_name);
        column_types.add(column_type);
        column_defaults.add(column_default);
        column_version.add(version_added);
        return this;
    }

    @NotNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE `");
        builder.append(table_name);
        builder.append("` (\n");
        boolean not_first = false;
        for (int i = 0; i < column_names.size(); i++) {
            if (not_first) builder.append(",\n");
            else not_first = true;
            builder.append("  `");
            builder.append(column_names.get(i));
            builder.append("` ");
            switch (column_types.get(i)) {
                case NULL:
                    builder.append("NULL");
                    continue;
                case INTEGER:
                case BOOLEAN:
                case ENUM:
                case SET:
                    builder.append("INTEGER");
                    break;
                case REAL:
                    builder.append("REAL");
                    break;
                case TEXT:
                    builder.append("TEXT");
                    break;
                case BLOB:
                    builder.append("BLOB");
                    break;
                case PRIMARY:
                    builder.append("INTEGER PRIMARY KEY");
                    continue;
                case INTEGER_KEY:
                    builder.append("INTEGER KEY");
                    continue;
            }
            if (column_defaults.get(i) == null)
                continue;
            builder.append(" NOT NULL DEFAULT ");
            DatabaseUtils.appendValueToSql(builder, column_defaults.get(i));
        }
        builder.append("\n);\n");
        return builder.toString();
    }

    @NotNull
    public String[] toUpgradeString(int oldVersion) {
        List<String> result = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < column_names.size(); i++) {
            if (column_version.get(i) <= oldVersion)
                continue;

            builder.append("ALTER TABLE `");
            builder.append(table_name);
            builder.append("` ADD COLUMN `");
            builder.append(column_names.get(i));
            builder.append("` ");

            switch (column_types.get(i)) {
                case NULL:
                    builder.append("NULL");
                    continue;
                case INTEGER:
                case BOOLEAN:
                case ENUM:
                case SET:
                    builder.append("INTEGER");
                    break;
                case REAL:
                    builder.append("REAL");
                    break;
                case TEXT:
                    builder.append("TEXT");
                    break;
                case BLOB:
                    builder.append("BLOB");
                    break;
                case PRIMARY:
                    builder.append("INTEGER PRIMARY KEY");
                    continue;
                case INTEGER_KEY:
                    builder.append("INTEGER KEY");
                    continue;
            }
            if (column_defaults.get(i) != null) {
                builder.append(" NOT NULL DEFAULT ");
                DatabaseUtils.appendValueToSql(builder, column_defaults.get(i));
            }
            builder.append(";\n");
            result.add(builder.toString());
            builder = new StringBuilder();
        }
        return result.toArray(new String[result.size()]);
    }

    public int getVersion_created() {
        return version_created;
    }

}
