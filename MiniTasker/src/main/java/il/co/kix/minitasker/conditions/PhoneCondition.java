package il.co.kix.minitasker.conditions;

import android.content.ContentValues;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskCallBlockActivity;
import il.co.kix.minitasker.ui.NewTaskSmsForwardActivity;

/**
 * calls forwarding or blocking condition
 * <p/>
 * Created by phantom on 24/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class PhoneCondition extends ConditionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.<String, SqlDataType>builder()
            .put("phone", SqlDataType.TEXT)
            .put("private", SqlDataType.BOOLEAN)
            .put("unrecognized", SqlDataType.BOOLEAN)
            .put("kind", SqlDataType.TEXT)
            .put("contact", SqlDataType.TEXT)
            .put("inverse", SqlDataType.BOOLEAN)
            .put("contents", SqlDataType.TEXT)
            .build();

    public static final String KIND_BLOCK = "block";
    public static final String KIND_FORWARD = "forward";
    private String phone;
    private boolean blockPrivate;
    private boolean blockUnrecognized;
    private String kind;
    private boolean blockInverse;
    @Nullable
    private String contact;
//    private String contents;

    public String getPhone() {
        return phone;
    }

    public void setPhone(@NotNull String phone) {
        this.phone = phone;
        final String phoneSql = phone.replace("*", "%").replace(".", "?");
        myValues.put("phone", phoneSql);
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getKind() {
//        return kind;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setKind(@NotNull String kind) {
        this.kind = kind;
        myValues.put("kind", kind);
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getContact() {
//        return contact;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setContact(@NotNull String contact) {
        this.contact = contact;
        myValues.put("contact", contact);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        phone = myValues.getAsString("phone").replace("%", "*").replace("?", ".");
        blockPrivate = myValues.getAsInteger("private") == 1;
        blockUnrecognized = myValues.getAsInteger("unrecognized") == 1;
        blockInverse = myValues.getAsInteger("inverse") == 1;
        kind = myValues.getAsString("kind");
        contact = myValues.getAsString("contact");
    }

    @NotNull
    @Override
    public String toString() {
        return getDescription();
    }

    @NotNull
    @Override
    public String getName() {
        return "phone";
    }

    @NotNull
    @Override
    public String getDescription() {
        String[] container = new String[3];
        int count = 0;
        if (!phone.isEmpty()) {
            if (phone.equals("*")) {
                if (blockInverse) {
                    container[count] = context.getString(R.string.condition_phone_description_inverse_phone_any);
                } else {
                    container[count] = context.getString(R.string.condition_phone_description_phone_any);
                }
            } else {
                final String matches = (contact == null || contact.isEmpty() ? phone : contact);
                if (blockInverse) {
                    container[count] = context.getString(R.string.condition_phone_description_number_inverse_matches, matches);
                } else {
                    container[count] = context.getString(R.string.condition_phone_description_number_matches, matches);
                }
            }
            count++;
        }
        if (blockUnrecognized) {
            container[count] = context.getString(R.string.condition_phone_description_unrecognized);
            count++;
        }
        if (blockPrivate) {
            container[count] = context.getString(R.string.condition_phone_description_private);
            count++;
        }
        switch (count) {
            case 3:
                return context.getString(R.string.condition_phone_description_container_3,
                        container[0], container[1], container[2]);
            case 2:
                return context.getString(R.string.condition_phone_description_container_2,
                        container[0], container[1]);
            case 1:
                return context.getString(R.string.condition_phone_description_container_1,
                        container[0]);
        }
        return "";
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        if (kind == null || kind.equals(KIND_BLOCK))
            return NewTaskCallBlockActivity.class;
        else if (kind.equals(KIND_FORWARD))
            return NewTaskSmsForwardActivity.class;
        else
            return NewTaskCallBlockActivity.class;
    }

    @Override
    public void init() {
        phone = "";
        contact = "";
        blockUnrecognized = false;
        blockPrivate = false;
        blockInverse = false;
        kind = KIND_BLOCK;
//        contents = "%";
    }

    public boolean isBlockPrivate() {
        return blockPrivate;
    }

    public void setBlockPrivate(boolean blockPrivate) {
        this.blockPrivate = blockPrivate;
        myValues.put("private", (blockPrivate ? 1 : 0));
    }

    public boolean isBlockUnrecognized() {
        return blockUnrecognized;
    }

    public void setBlockUnrecognized(boolean blockUnrecognized) {
        this.blockUnrecognized = blockUnrecognized;
        myValues.put("unrecognized", (blockUnrecognized ? 1 : 0));
    }

    public boolean isBlockInverse() {
        return blockInverse;
    }

    public void setBlockInverse(boolean blockInverse) {
        this.blockInverse = blockInverse;
        myValues.put("inverse", blockInverse ? 1 : 0);
    }

//    public void setContents(String contents) {
//        myValues.put("contents", contents);
//    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        return true;
    }
}
