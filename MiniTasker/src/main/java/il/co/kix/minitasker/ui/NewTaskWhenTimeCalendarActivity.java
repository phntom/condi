package il.co.kix.minitasker.ui;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.conditions.CalendarCondition;
import il.co.kix.minitasker.conditions.ConditionBase;

/**
 * new task for calendar condition
 * <p/>
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskWhenTimeCalendarActivity extends SavableConditionFragmentActivity {

    final List<Long> calendarIds = new ArrayList<Long>();
    final List<String> calendarNames = new ArrayList<String>();
    private EditText txtDescriptionWildcard;
    private EditText txtTitleWildcard;
    private EditText txtLocationWildcard;
    private Spinner spnrAvaliability;
    private Spinner spnrCalendar;

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new CalendarCondition();
    }

    @Override
    boolean isSaveEnabled() {
        return spnrAvaliability.getSelectedItemPosition() != AdapterView.INVALID_POSITION
                && spnrCalendar.getSelectedItemPosition() != AdapterView.INVALID_POSITION
                && spnrCalendar.getSelectedItemPosition() < calendarIds.size();
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_when_time_calender;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.calender_based_tasks;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalendarCondition condition = (CalendarCondition) getCondition();

        txtDescriptionWildcard = (EditText) findViewById(R.id.txtDescriptionWildcard);
        txtTitleWildcard = (EditText) findViewById(R.id.txtTitleWildcard);
        txtLocationWildcard = (EditText) findViewById(R.id.txtLocationWildcard);
        spnrAvaliability = (Spinner) findViewById(R.id.spnrAvailability);
        spnrCalendar = (Spinner) findViewById(R.id.spnrCalendar);

        String[] valuesAvailability = getResources().getStringArray(R.array.calender_availability);
        ArrayAdapter<String> availabilityAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, valuesAvailability);
        availabilityAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spnrAvaliability.setAdapter(availabilityAdapter);

        loadCalendars();
        ArrayAdapter<String> calendarAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, calendarNames);
        spnrCalendar.setAdapter(calendarAdapter);

        txtDescriptionWildcard.setText(condition.getDescriptionWildcard());
        txtTitleWildcard.setText(condition.getTitleWildcard());
        txtLocationWildcard.setText(condition.getLocationWildcard());
        spnrAvaliability.setSelection(condition.getAvailabilityType().ordinal());
        long calendarId = condition.getCalendarIdentifier();
        int calendarIdIndex = calendarIds.indexOf(calendarId);
        if (calendarIdIndex >= 0)
            spnrCalendar.setSelection(calendarIdIndex);

        setSaveEnabled();

    }

    protected void loadCalendars() {
        calendarIds.clear();
        calendarNames.clear();
        String[] projection =
                new String[]{
                        CalendarContract.Calendars._ID,
                        CalendarContract.Calendars.CALENDAR_DISPLAY_NAME
                };
        Cursor calCursor =
                getContentResolver().
                        query(CalendarContract.Calendars.CONTENT_URI,
                                projection,
                                CalendarContract.Calendars.VISIBLE + " = 1",
                                null,
                                CalendarContract.Calendars._ID + " ASC");
        if (calCursor == null) return;
        if (calCursor.moveToFirst()) {
            do {
                long id = calCursor.getLong(0);
                String displayName = calCursor.getString(1);
                calendarIds.add(id);
                calendarNames.add(displayName);
            } while (calCursor.moveToNext());
        }
        calCursor.close();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void performSaveCondition(ConditionBase cdt) {
        CalendarCondition condition = (CalendarCondition) cdt;
        condition.setDescriptionWildcard(txtDescriptionWildcard.getText().toString());
        condition.setTitleWildcard(txtTitleWildcard.getText().toString());
        condition.setLocationWildcard(txtLocationWildcard.getText().toString());
        int availabilityIndex = spnrAvaliability.getSelectedItemPosition();
        CalendarCondition.AvailabilityType availabilityType =
                CalendarCondition.AvailabilityType.values()[availabilityIndex];
        condition.setAvailabilityType(availabilityType);
        int calendarIndex = spnrCalendar.getSelectedItemPosition();
        if (calendarIndex < 0 || calendarIndex >= calendarIds.size())
            return;
        condition.setCalendarIdentifier(calendarIds.get(calendarIndex));
        condition.setCalendarName(calendarNames.get(calendarIndex));
    }

}