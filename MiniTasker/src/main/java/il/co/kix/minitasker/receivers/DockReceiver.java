package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.conditions.DockCondition;

/**
 * phone is docked broadcast receiver
 * <p/>
 * Created by sphantom on 9/4/13.
 */
public class DockReceiver extends BackgroundBroadcastReceiver {
    final static String[] empty = new String[]{};
    static private final String oneEqOne = "1=1";

    public void onReceiveBackground(Context context, @NotNull Intent intent) {
        final String action = intent.getAction();
        if (action == null || !action.equals(Intent.ACTION_DOCK_EVENT)) return;
        int state = intent.getIntExtra(Intent.EXTRA_DOCK_STATE, Intent.EXTRA_DOCK_STATE_UNDOCKED);
        boolean docked = !(state == Intent.EXTRA_DOCK_STATE_UNDOCKED);
        Log.w(DatabaseHandler.APPTAG, "the phone is " + (docked ? "" : "un") + "docked");
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;

        db.setPrivateSetting(DockCondition.FIELD_DOCK, docked ? "1" : "0");

        db.runMatchingConditions("dock", oneEqOne, empty,
                (docked ? StartType.START_WITH_END : StartType.END));
    }
}
