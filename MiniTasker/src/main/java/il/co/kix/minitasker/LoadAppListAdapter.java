package il.co.kix.minitasker;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * background loader for application list
 *
 * Created by phantom on 26/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class LoadAppListAdapter extends AsyncTaskLoader<AppsWithIconsAdapter> {
    @NotNull
    private final Context context;
    private final boolean isTerminate;

    public LoadAppListAdapter(@NotNull Context context, boolean isTerminate) {
        super(context);
        this.context = context;
        this.isTerminate = isTerminate;
    }

    @NotNull
    @Override
    public AppsWithIconsAdapter loadInBackground() {
//        Log.w("MiniTasker", "loadInBackground loadInBackground loadInBackground");
//        Context context = getContext();
        PackageManager packageManager = context.getPackageManager();
        assert packageManager != null;

        List<ApplicationInfo> data = packageManager.getInstalledApplications(0);
        List<AppInfo> appInfoList = new ArrayList<AppInfo>();
        for (ApplicationInfo applicationInfo : data) {
            if (applicationInfo == null) continue;
            try {
                final String appName = String.valueOf(applicationInfo.loadLabel(packageManager));
                if (appName == null || applicationInfo.packageName.equals("android"))
                    continue;
                final String packageName = applicationInfo.packageName;
                final File mApkFile = new File(applicationInfo.sourceDir);
                final ApplicationInfo applicationInfo1 = (mApkFile.exists() ? applicationInfo : null);
                AppInfo appInfo = new AppInfo() {{
                    setPackageDisplayName(appName);
                    setPackageName(packageName);
                    setApplicationInfo(applicationInfo1);
                }};
                appInfoList.add(appInfo);
            } catch (Resources.NotFoundException ignored) {
            }
        }
        Collections.sort(appInfoList);

        boolean hasRoot = false;
        if (isTerminate) {
            hasRoot = ForceStopEnabler.canRunRootCommands();
        }

        return new AppsWithIconsAdapter (
                context,
                appInfoList,
                hasRoot
        );
    }
}
