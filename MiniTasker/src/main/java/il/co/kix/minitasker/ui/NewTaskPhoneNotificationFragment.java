package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.MessageAction;

/**
 * new task for message action
 * <p/>
 * Created by sphantom on 7/31/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskPhoneNotificationFragment extends DialogFragment implements DialogInterface.OnClickListener {
    @Nullable
    MessageAction action;
    EditText txtNotification;

    @Nullable
    MiniTaskerActivity caller;

    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        action = new MessageAction();
        action.setContext(caller);
        action.setPrimary(caller.entity_id);
        if (caller.last_entity_type != null
                && !caller.last_entity_type.equals(action.getName())) {
            action = new MessageAction();
            action.setContext(caller);
        }
        action.setTaskId(caller.task_id);
        final AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        final LayoutInflater inflater = caller.getLayoutInflater();
        builder.setTitle(R.string.popup_notification_tasks);
        builder.setMessage(R.string.enter_your_msg);
        final View v = inflater.inflate(R.layout.activity_new_task_phone_notifications_add, null);
        assert v != null;
        txtNotification = (EditText) v.findViewById(R.id.txtNotification);
        assert txtNotification != null;
        txtNotification.setText(action.getMsg());
        txtNotification.selectAll();
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, this);

        return builder.create();
    }

    @Override
    public void onClick(@NotNull DialogInterface dialog, int which) {
        assert txtNotification != null;
        Editable txtNotificationText = txtNotification.getText();
        assert txtNotificationText != null;
        if (txtNotificationText.toString().isEmpty()) {
            dialog.dismiss();
            return;
        }
        assert action != null;
        action.setMsg(txtNotificationText.toString());
        assert caller != null;
        action.setStart(caller.is_start);
        action.save();
        caller.entity_id = action.getPrimary();
        caller.last_entity_type = action.getName();
        caller.nextActivityWhen(true);
    }
}