package il.co.kix.minitasker.ui;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import org.jetbrains.annotations.Nullable;

import java.util.List;

import il.co.kix.minitasker.MediaActionType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.ActionBase;
import il.co.kix.minitasker.actions.MediaAction;

/**
 * new task for media activity
 * <p/>
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskAppsMusicActivity extends ExpandableListActivity {

    protected ActionBase action;

    @Override
    int resourceActivityTitle() {
        return R.string.Music_tasks;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        action = new MediaAction();
        action.setContext(this);
        action.setPrimary(entity_id);
        action.setTaskId(task_id);
    }

    @Override
    protected int getGroupTitleArray() {
        return R.array.media_action_groups;
    }

    @Override
    protected int getChildTitleArray(int group) {
        switch (group) {
            case 0:
            default:
                return R.array.media_action_child_1_titles;
            case 1:
                return R.array.media_action_child_2_titles;
        }
    }

    @Override
    protected int getChildDescArray(int group) {
        switch (group) {
            case 0:
            default:
                return R.array.media_action_child_1_descriptions;
            case 1:
                return R.array.media_action_child_2_descriptions;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w(APPTAG, "request code: " + requestCode + " resultCode: " + resultCode);
        if (data == null) return;
        Uri uri = data.getData();
        if (uri == null) return;
        MediaAction action1 = (MediaAction) action;
        Log.w(APPTAG, "result: " + uri.toString());
        switch (requestCode) {
            case 1:
                action1.setType(MediaActionType.Playlist);
                action1.setPlaylist(uri.toString());
                break;
            case 2:
                List<String> segments = uri.getPathSegments();
                if (segments == null || segments.get(1) == null || segments.get(2) == null
                        || segments.get(3) == null || !segments.get(1).equals("audio")
                        || !segments.get(2).equals("media")) return;
                final String[] proj = {MediaStore.Audio.Media._ID,
                        MediaStore.Images.Media.DATA,
                        MediaStore.Audio.Media.TITLE
                };
                ContentResolver resolver = getContentResolver();
                Cursor c = resolver.query(uri, proj, null, null, null);
                if (c == null) return;
                if (!c.moveToFirst()) return;
                action1.setPlaylist(c.getString(c.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)));
                action1.setSongName(c.getString(c.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)));
                action1.setSongId(c.getLong(c.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)));
                action1.setType(MediaActionType.Song);
                c.close();
                action1.setStart(is_start);
                if (action1.save()) {
                    action1.saveRestoreCopy();
                }
                entity_id = action.getPrimary();
                nextActivityWhen(false);
                break;
        }
    }

    @Override
    protected boolean onItemClick(int groupPosition, int childPosition) {
        MediaAction mediaAction = (MediaAction) action;
        switch (groupPosition) {
            case 0:
                switch (childPosition) {
                    case 0:
//                        if (entity_id != 0) {
                        mediaAction.setType(MediaActionType.Play);
//                        } else {
//                            new NewTaskAppsListFragment()
//                                    .show(getFragmentManager(), NewTaskAppsListFragment.MUSIC);
//                            return true;
//                        }
                        break;
                    case 1:
                        mediaAction.setType(MediaActionType.Pause);
                        break;
                    case 2:
                        mediaAction.setType(MediaActionType.Toggle);
                        break;
                }
                mediaAction.setStart(is_start);
                if (mediaAction.save()) {
                    mediaAction.saveRestoreCopy();
                }
                entity_id = mediaAction.getPrimary();
                nextActivityWhen(false);
                break;
            case 1:
                switch (childPosition) {
                    case 0:
//                        try {
//                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                            intent.setType(MediaStore.Audio.Playlists.CONTENT_TYPE);
//                            startActivityForResult(intent, 1);
//                        } catch (ActivityNotFoundException e) {
                        new NewTaskAppsMusicPlaylistFragment().show(getFragmentManager(), APPTAG);
//                        }
                        break;
                    case 1:
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, 2);
                        } catch (ActivityNotFoundException e) {
                            new NewTaskAppsMusicSongFragment().show(getFragmentManager(), APPTAG);
                        }
                        break;
                }
                break;
        }
        return true;
    }
}