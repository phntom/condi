package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.net.Uri;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.BackgroundProvider;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskWhenLocationActivity;

/**
 * location condition
 * <p/>
 * Created by phantom on 18/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class LocationCondition extends ConditionBase {
    public final static double MIN_RADIUS = 75d;

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "lat", SqlDataType.REAL,
            "lng", SqlDataType.REAL,
            "rad", SqlDataType.REAL,
            "inverse", SqlDataType.BOOLEAN,
            "description", SqlDataType.TEXT
    );

    int save_reentrancy = 0;
    private double latitude;
    private double longitude;
    private double radius;
    private boolean inverse;
    @Nullable
    private String description;

    @NotNull
    @Override
    public String toString() {
        if (description == null || description.isEmpty())
            description = latitude + "," + longitude + "x" + radius;
        if (inverse)
            return context.getString(R.string.condition_location_individual_inverse, description, (int) radius);
        else
            return context.getString(R.string.condition_location_individual_positive, description, (int) radius);
    }

    @NotNull
    @Override
    public String getName() {
        return "location";
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
        myValues.put("lat", latitude);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
        myValues.put("lng", longitude);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        if (this.radius < MIN_RADIUS) this.radius = MIN_RADIUS;
        myValues.put("rad", radius);
    }

    public boolean isInverse() {
        return inverse;
    }

    public void setInverse(boolean inverse) {
        this.inverse = inverse;
        myValues.put("inverse", inverse ? 1 : 0);
    }

    @Override
    public boolean save() {
        save_reentrancy++;
        boolean is_edit = (primary != 0 && save_reentrancy == 1);
        boolean prev_enabled = true;
        if (is_edit) {
            if (myValues.containsKey("enabled"))
                //noinspection ConstantConditions
                prev_enabled = myValues.getAsInteger("enabled") == 1;
            setEnabled(false);
        }
        boolean result = super.save();
        if (is_edit) {
            setEnabled(prev_enabled);
        }
        save_reentrancy--;
        return result;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) {
        latitude = myValues.getAsDouble("lat");
        longitude = myValues.getAsDouble("lng");
        radius = myValues.getAsDouble("rad");
        if (radius > 0d && radius < MIN_RADIUS) radius = MIN_RADIUS;
        inverse = myValues.getAsInteger("inverse") == 1;
        description = myValues.getAsString("description");
    }

    @NotNull
    @Override
    public String getDescription() {
        if (description == null || description.isEmpty())
            description = String.format("%.6f%n,%.6f%n", latitude, longitude);
        if (inverse)
            return context.getString(R.string.condition_location_description_inverse,
                    description, (int) radius);
        else
            return context.getString(R.string.condition_location_description_positive,
                    description, (int) radius);
    }

    public void setDescription(@Nullable String description) {
        if (description == null) description = "";
        this.description = description;
        myValues.put("description", description);
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenLocationActivity.class;
    }

    @Override
    public void init() {
        latitude = 0d;
        longitude = 0d;
        radius = 0d;
        inverse = false;
        description = "";
    }

    @Override
    public void setEnabled(boolean enabled) {
        boolean prevEnableState = getEnabled();
        super.setEnabled(enabled);
        boolean currEnableState = getEnabled();
        if (prevEnableState == currEnableState) return;
        if (currEnableState) conditionRegister();
        else conditionUnregister();
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        //TODO: probably rewrite this
        return true;
    }

    @Override
    public void setTaskEnabled(boolean enabled) {
        boolean prevEnableState = getEnabled();
        super.setTaskEnabled(enabled);
        boolean currEnableState = getEnabled();
        if (prevEnableState == currEnableState) return;
        if (currEnableState) conditionRegister();
        else conditionUnregister();
    }

    void conditionRegister() {
        context.getContentResolver().query(Uri.parse("content://" + BackgroundProvider.BACKGROUND_PROVIDER + "/on_boot/run"), null, null, null, null);
    }

    void conditionUnregister() {
        context.getContentResolver().query(Uri.parse("content://" + BackgroundProvider.BACKGROUND_PROVIDER + "/on_boot/run"), null, null, null, null);

    }
}
