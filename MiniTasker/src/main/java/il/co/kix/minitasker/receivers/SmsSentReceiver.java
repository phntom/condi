package il.co.kix.minitasker.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;

public class SmsSentReceiver extends BroadcastReceiver {
    public static final String EXTRA_QUEUE_ID = "il.co.kix.minitasker.extra.queue_id";

    @Override
    public void onReceive(Context context, @NotNull Intent intent) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        long queue_id = intent.getLongExtra(EXTRA_QUEUE_ID, 0);
        switch (getResultCode()) {
            case Activity.RESULT_OK:
                Log.w(DatabaseHandler.APPTAG, "SMS sent successfully");
                db.reportSmsSentStatus(queue_id, true);
                return;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Log.e(DatabaseHandler.APPTAG, "SMS send failed - generic failure");
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                Log.e(DatabaseHandler.APPTAG, "SMS send failed - no service");
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Log.e(DatabaseHandler.APPTAG, "SMS send failed - null pdu");
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Log.e(DatabaseHandler.APPTAG, "SMS send failed - radio off");
                break;
        }
        db.reportSmsSentStatus(queue_id, false);
    }
}
