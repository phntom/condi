package il.co.kix.minitasker.receivers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.BackgroundProvider;
import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.actions.SmsAction;

import static il.co.kix.minitasker.ui.MiniTaskerActivity.APPTAG;

/**
 * receives time broadcast
 * <p/>
 * Created by phantom on 22/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class PhoneReceiver extends BackgroundBroadcastReceiver {

    @Override
    public void onReceiveBackground(@NotNull Context context, @NotNull Intent intent) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        try {

            if (!intent.hasExtra("type")) {
                Log.w(APPTAG, "invalid type of intent; aborting");
                return;
            }
            String type = intent.getStringExtra("type");
            if (type == null) type = "";
            switch (type) {
                case "time": {
                    if (!intent.hasExtra("condition_ids")) {
                        Log.w(APPTAG, "invalid condition_ids; aborting");
                        return;
                    }
                    final String condition_ids = intent.getStringExtra("condition_ids");
                    if (TextUtils.isEmpty(condition_ids)) {
                        Log.w(APPTAG, "empty condition_ids; aborting");
                        return;
                    }

                    final String query = "w.rowid IN (?) AND w.start_type != ?";
                    db.runMatchingConditions("time", query, new String[]{condition_ids,
                            "" + StartType.END.ordinal()}, StartType.END);
                    db.runMatchingConditions("time", query, new String[]{condition_ids,
                            "" + StartType.START_WITH_END.ordinal()}, StartType.START_WITH_END);

                    break;
                }
                case "calendar":
                    if (!intent.hasExtra("event_time") || !intent.hasExtra("event_time")) {
                        Log.w(APPTAG, "invalid when_ids; aborting");
                        return;
                    }
                    final long next_time = intent.getLongExtra("event_time", 0);
                    final String event_ids = intent.getStringExtra("event_ids");
                    if (event_ids == null || !event_ids.matches("\\d+(,\\d+)*")) {
                        Log.w(APPTAG, "event_ids don't match the required pattern");
                        return;
                    }
                    db.matchCalendarConditions(event_ids, next_time);
                    db.setPrivateSetting("LAST_CALENDAR_TIME", "" + next_time);

                    break;
                case "check":
                    context.getContentResolver().query(Uri.parse("content://" +
                            BackgroundProvider.BACKGROUND_PROVIDER + "/on_boot/time"), null, null, null, null);
                    break;
                case "activity": {
                    if (!intent.hasExtra("activity_type")) return;
                    int activity_type = intent.getIntExtra("activity_type", 0);
                    final String when_query = "NOT z.activity & ?";
                    final String[] when_parameters = new String[]{"" + activity_type};
                    db.runMatchingConditions("activity", when_query, when_parameters, StartType.END);
                    break;
                }
                case "sms":
                    long queue_id = intent.getLongExtra("queue_id", 0);
                    if (queue_id < 1) return;
                    final String target = intent.getStringExtra("target");
                    final String message = intent.getStringExtra("message");
                    if (target == null || message == null) return;
                    SmsAction.sendSms(context, target, message, queue_id);
                    break;
            }
        } finally {
            //noinspection ConstantConditions
            if (db != null && !db.debugMode) {
                db.scheduleNextTimeRangeWhen();
                db.scheduleNextCalendarEvent();
            }
        }
    }

}
