package il.co.kix.minitasker.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.ActivityUtils;
import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.ui.MainActivity;

/**
 * receiver that handles background broadcasts
 * <p/>
 * Created by phantom on 25/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class BackgroundBroadcastReceiver extends BroadcastReceiver {
    public static final String KEY_SHOW_TOAST = "KEY_SHOW_TOAST";

    @Override
    public void onReceive(@NotNull Context context, Intent intent) {
        PendingResult result = goAsync();
        final Context mContext = context;
        final Context appContext = context.getApplicationContext();
        assert appContext != null;
        new AsyncTask<Object, Void, PendingResult>() {
            @NotNull
            @Override
            protected PendingResult doInBackground(Object... params) {
                PendingResult result_ = (PendingResult) params[0];
                Context context_ = (Context) params[1];
                Intent intent_ = (Intent) params[2];
                onReceiveBackground(context_, intent_);
                return result_;
            }

            @Override
            protected void onPostExecute(@Nullable PendingResult pendingResult) {
                SharedPreferences mPrefs = appContext.getSharedPreferences(
                        ActivityUtils.SHARED_PREFERENCES,
                        Context.MODE_PRIVATE);
                String showToast = "" + mPrefs.getString(KEY_SHOW_TOAST, "");
                DatabaseHandler db = SingletonDatabase.INSTANCE.db;
                if (db == null) return;
                final int count_flags = db.countFlags();
                if (!showToast.isEmpty()) {
                    Toast.makeText(mContext, showToast, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.remove(KEY_SHOW_TOAST);
                    editor.apply();
                }
                if (count_flags > 0) {
                    db.setPrivateSetting("FLAG_NAG_DISMISSED", "1");
                    Intent targetIntent = new Intent(appContext, MainActivity.class);
                    targetIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent contentIntent = PendingIntent.getActivity(appContext, 0,
                            targetIntent, 0);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(appContext)
                                    .setSmallIcon(R.drawable.ic_minitasker)
                                    .setContentTitle(appContext.getString(R.string.minitasker_title))
                                    .setContentText(appContext.getResources().getQuantityText(
                                            R.plurals.flag_notification_problem, count_flags));
                    mBuilder.setContentIntent(contentIntent);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND);
                    mBuilder.setAutoCancel(false);
                    NotificationManager mNotificationManager =
                            (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(2, mBuilder.build());
                }
                if (pendingResult != null) pendingResult.finish();
            }
        }.execute(result, context, intent);
    }

    abstract public void onReceiveBackground(Context context, Intent intent);
}
