package il.co.kix.minitasker.services;/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import il.co.kix.minitasker.ActivityUtils;
import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.DetectedActivityType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.receivers.PhoneReceiver;

/**
 * Service that receives ActivityRecognition updates. It receives updates
 * in the background, even if the main Activity is not visible.
 */
public class ActivityRecognitionIntentService extends BackgroundIntentService {

    public static final String KEY_ACTIVITY_DELAY = "KEY_ACTIVITY_DELAY";
    // Formats the timestamp in the log
    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSZ";
    @Nullable
    private PendingIntent activityEndPendingIntent = null;


    // A date formatter
    private SimpleDateFormat mDateFormat;

    public ActivityRecognitionIntentService() {
        // Set the label for the service's background thread
        super("il.co.kix.minitasker.services.ActivityRecognitionIntentService");
    }

    /**
     * Called when a new activity detection update is available.
     */
    @Override
    protected void onBackgroundHandleIntent(Intent intent) {

        // Get a handle to the repository
        //noinspection ConstantConditions
        SharedPreferences mPrefs = getApplicationContext().getSharedPreferences(
                ActivityUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // Get a date formatter, and catch errors in the returned timestamp
        try {
            mDateFormat = (SimpleDateFormat) DateFormat.getDateTimeInstance();
        } catch (Exception e) {
            Log.e(ActivityUtils.APPTAG, getString(R.string.date_format_error));
        }

        // Format the timestamp according to the pattern, then localize the pattern
        mDateFormat.applyPattern(DATE_FORMAT_PATTERN);
        mDateFormat.applyLocalizedPattern(mDateFormat.toLocalizedPattern());

        // If the intent contains an update
        if (ActivityRecognitionResult.hasResult(intent)) {

//            test1();

            // Get the update
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

            // Log the update
//            logActivityRecognitionResult(result);

            // Get the most probable activity from the list of activities in the update
            DetectedActivity mostProbableActivity = result.getMostProbableActivity();

//            Log.w("MiniTasker", "Activity Detected: "
//                    + getNameFromType(mostProbableActivity.getType())
//                    + " confidence " + mostProbableActivity.getConfidence());

            // Get the confidence percentage for the most probable activity
            int confidence = mostProbableActivity.getConfidence();

            // Get the type of activity
            int activityType = mostProbableActivity.getType();

            if (activityType == DetectedActivity.TILTING)
                activityType = DetectedActivity.UNKNOWN;

            if (confidence < 66 || activityType == DetectedActivity.UNKNOWN)
                return;

            int previousType = mPrefs.getInt(ActivityUtils.KEY_PREVIOUS_ACTIVITY_TYPE,
                    DetectedActivity.UNKNOWN);

            if (previousType != activityType) {
                Editor editor = mPrefs.edit();
                editor.putInt(ActivityUtils.KEY_PREVIOUS_ACTIVITY_TYPE, activityType);
                editor.commit();

                DetectedActivityType new_type = convertToEnumType(activityType);
                DetectedActivityType old_type = convertToEnumType(previousType);

                onBackgroundHandle(new_type, old_type);
            }
        }
    }

    void onBackgroundHandle(@NotNull DetectedActivityType new_type, @NotNull DetectedActivityType old_type) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        if (old_type != DetectedActivityType.UNKNOWN) {
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            if (activityEndPendingIntent != null) {
                alarmManager.cancel(activityEndPendingIntent);
            }
            Intent intent = new Intent(this, PhoneReceiver.class);
            intent.putExtra("type", "activity");
            intent.putExtra("activity_type", new_type.toDatabase());
            activityEndPendingIntent =
                    PendingIntent.getBroadcast(this, 2024, intent,
                            PendingIntent.FLAG_CANCEL_CURRENT);

            int delay = 0;
            try {
                delay = Integer.decode(db.getPrivateSetting(KEY_ACTIVITY_DELAY, "30"));
            } catch (NumberFormatException ignored) {
            }
            long next_time = Calendar.getInstance().getTimeInMillis() + delay * 1000;
//            db.logStatsEventValue(StatsType.SCHEDULED, next_time);
            alarmManager.set(AlarmManager.RTC_WAKEUP, next_time, activityEndPendingIntent);

//            final String when_query = "z.activity & ? AND NOT z.activity & ?";
//            final String[] when_parameters =
//                    new String[] { Integer.toString(old_type.toDatabase()),
//                            Integer.toString(new_type.toDatabase()) };
//            db.runMatchingConditions("activity", when_query, when_parameters,
//                    StartType.END);
        }
        {
            db.setPrivateSetting("ACTIVITY_RECOGNITION_NEW", new_type.name());
            db.setPrivateSetting("ACTIVITY_RECOGNITION_OLD", old_type.name());
        }
        {
            final String when_query = "z.activity & ? AND NOT z.activity & ?";
            final String[] when_parameters =
                    new String[]{Integer.toString(new_type.toDatabase()),
                            Integer.toString(old_type.toDatabase())};
            db.runMatchingConditions("activity", when_query, when_parameters,
                    StartType.START_WITH_END);
        }
    }


//    /**
//     * Map detected activity types to strings
//     *
//     * @param activityType The detected activity type
//     * @return A user-readable name for the type
//     */
//    private String getNameFromType(int activityType) {
//        switch(activityType) {
//            case DetectedActivity.IN_VEHICLE:
//                return "in_vehicle";
//            case DetectedActivity.ON_BICYCLE:
//                return "on_bicycle";
//            case DetectedActivity.ON_FOOT:
//                return "on_foot";
//            case DetectedActivity.STILL:
//                return "still";
//            case DetectedActivity.UNKNOWN:
//                return "unknown";
//            case DetectedActivity.TILTING:
//                return "tilting";
//        }
//        return "unknown";
//    }

    @NotNull
    private DetectedActivityType convertToEnumType(int activityType) {
        switch (activityType) {
            case DetectedActivity.IN_VEHICLE:
                return DetectedActivityType.IN_VEHICLE;
            case DetectedActivity.ON_BICYCLE:
                return DetectedActivityType.ON_BICYCLE;
            case DetectedActivity.ON_FOOT:
                return DetectedActivityType.ON_FOOT;
            case DetectedActivity.STILL:
                return DetectedActivityType.STILL;
            case DetectedActivity.UNKNOWN:
                return DetectedActivityType.UNKNOWN;
            case DetectedActivity.TILTING:
                return DetectedActivityType.TILTING;
        }
        return DetectedActivityType.UNKNOWN;
    }

}
