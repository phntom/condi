package il.co.kix.minitasker.ui;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.MiniTaskerApp;

/**
 * base class for all activities
 * <p/>
 * Created by phantom on 02/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
abstract public class MiniTaskerActivity extends FragmentActivity {
    public static final String APPTAG = "MiniTasker";
    @Nullable
    protected MiniTaskerApp mMyApp;
    protected int task_id;
    protected long entity_id;
    protected int edit_section;
    @Nullable
    protected ActionBar actionBar;
    protected boolean is_start;
    @Nullable
    String last_entity_type = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMyApp = (MiniTaskerApp) this.getApplicationContext();

        setContentView(layoutResourceToInflate());

        actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(resourceActivityTitle()));
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        task_id = getIntent().getIntExtra("task_id", 0);
        entity_id = getIntent().getLongExtra("entity_id", 0L);
        edit_section = getIntent().getIntExtra("edit", -1);
        is_start = getIntent().getBooleanExtra("is_start", true);
    }

    abstract int layoutResourceToInflate();

    abstract int resourceActivityTitle();

    protected void nextActivity(Class<? extends MiniTaskerActivity> targetClass, boolean clearTop) {
        Intent target = new Intent(this, targetClass);
        if (clearTop) {
            target.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        target.putExtra("task_id", task_id);
        target.putExtra("edit", edit_section);
        target.putExtra("entity_id", 0L);
        target.putExtra("is_start", is_start);
        startActivity(target);
    }

    protected void nextActivityWhen(boolean is_fragment) {
        Class<? extends MiniTaskerActivity> targetClass;
        boolean clearTop = false;
        if (edit_section != -1) {
            targetClass = MainActivity.class;
            if (is_fragment) {
                if (this.getClass() == MainActivity.class) {
                    ((MainActivity) this).forceReloadAdapter();
                    return;
                }
            }
            clearTop = true;
        } else {
            targetClass = NewTaskWhenActivity.class;
        }
        nextActivity(targetClass, clearTop);
    }

    protected void nextActivityMain(boolean is_fragment) {
        Class<? extends MiniTaskerActivity> targetClass = MainActivity.class;
        if (edit_section != -1) {
            if (is_fragment) {
                if (this.getClass() == MainActivity.class) {
                    ((MainActivity) this).forceReloadAdapter();
                    return;
                }
            }
        }
        nextActivity(targetClass, true);
    }

    protected boolean isNotEditMode() {
        return edit_section == -1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMyApp != null) {
            mMyApp.setCurrentActivity(this);
        }
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        FragmentActivity currActivity = mMyApp != null ? mMyApp.getCurrentActivity() : null;
        if (currActivity != null && currActivity.equals(this))
            mMyApp.setCurrentActivity(null);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent target = new Intent(this, MainActivity.class);
                target.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(target);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

