package il.co.kix.minitasker;

/**
 * holds type values for media actions
 *
 * Created by sphantom on 7/27/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum MediaActionType {
    Play,
    Pause,
    Toggle,
    Playlist,
    Song
}
