package il.co.kix.minitasker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Calendar;
import java.util.List;

import il.co.kix.minitasker.receivers.HeadsetStateReceiver;
import il.co.kix.minitasker.receivers.PhoneReceiver;
import il.co.kix.minitasker.services.BackgroundService;
import il.co.kix.minitasker.ui.MainActivity;

/**
 * registers for private android broadcasts
 * <p/>
 * Created by phantom on 28/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BackgroundProvider extends ContentProvider {

    public static final String BACKGROUND_PROVIDER = "il.co.kix.minitasker.backgroundprovider";
    @NotNull
    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "on_boot/check", 1);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "on_boot/self", 2);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "on_boot/time", 3);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "on_boot/run", 4);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "headset/register", 5);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "headset/unregister", 6);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "location/register", 7);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "location/unregister", 8);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "activity/register", 9);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "activity/unregister", 10);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "on_boot/service", 11);
        sUriMatcher.addURI(BACKGROUND_PROVIDER, "ensure_conditions", 12);
    }

    private static final String APPTAG = MainActivity.APPTAG;
    //    private int delay = 5;
//    private int decay = 3;
    private static final int MAX_DELAY = 60 * 5;
    int locationsRegistered = 0;
    @Nullable
    private HeadsetStateReceiver headsetReceiver = null;
    //    private boolean on_boot_ran = false;
    private boolean on_boot_service = false;
    @Nullable
    private PendingIntent bootPendingIntent = null;
    @Nullable
    private IntentFilter mBroadcastFilter = null;
    @Nullable
    private GeofenceRemover mGeofenceRemover = null;
    @Nullable
    private GeofenceRequester mGeofenceRequester = null;
    private boolean registered_activity_detection = false;
    private DetectionRequester mDetectionRequester;
    private DetectionRemover mDetectionRemover;
    @NotNull
    private DatabaseHandler db;

    @Override
    public boolean onCreate() {
        Context context = getContext();
        if (context != null && SingletonDatabase.INSTANCE.db == null) {
            SingletonDatabase.INSTANCE.db = new DatabaseHandler(context.getApplicationContext(),
                    DatabaseHandler.DATABASE_NAME);
        }
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        try {
            BluetoothAdapter.getDefaultAdapter(); // must initialize in a ui thread
        } catch (Exception ignore) {
        }

        return false;
    }

    void onBoot() {
        Context context = getContext();
        if (context == null) return;
        Log.d(APPTAG, "periodical calendar, headset, location and activity check");

        if (db.hasConditionTypeEnabled("headset")) {
            headsetRegister(context);
        }

        if (db.hasConditionTypeEnabled("location")) {
            locationRegister(context);
        } else {
            locationUnregister(context);
        }

        if (db.hasConditionTypeEnabled("activity")) {
            activityRegister(context);
        } else {
            activityUnregister();
        }
    }

    @Nullable
    @Override
    public Cursor query(@NotNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Context context = getContext();
        assert context != null;
//        StatsType launchKind = StatsType.UNKNOWN;

        switch (sUriMatcher.match(uri)) {
            case 11: //on_boot/service
                on_boot_service = true;
            case 1: //on_boot/check
//                if (on_boot_ran) {
//                    db.scheduleNextTimeRangeWhen();
//                    db.scheduleNextCalendarEvent();
//                    return null;
//                }
//                launchKind = StatsType.MAIN_LAUNCH;
            case 2: //on_boot/self
//                if (launchKind == StatsType.UNKNOWN)
//                    launchKind = StatsType.BOOT_LAUNCH;
            case 3: //on_boot/time
//                if (launchKind == StatsType.UNKNOWN)
//                    launchKind = StatsType.TIME_LAUNCH;
                autoWakeUp();
                if (!on_boot_service) {
                    Intent intent = new Intent(context, BackgroundService.class);
                    context.startService(intent);
                    db.scheduleNextTimeRangeWhen();
                    db.scheduleNextCalendarEvent();
                }
            case 4: //on_boot/run
                onBoot();
                break;
            case 5:
                //headset/register
                headsetRegister(context);
                break;
            case 6:
                //headset/unregister
                headsetUnregister(context);
                break;
            case 7:
                //location/register
                locationRegister(context);
                break;
            case 8:
                //location/unregister
                activityUnregister();
                break;
            case 9:
                //activity/register
                activityRegister(context);
                break;
            case 10:
                //activity/unregister
                break;
            default:
                Log.w(APPTAG, "Unknown URI for BackgroundProvider " + uri.toString());
                return null;
        }
//        Log.w(APPTAG, "Request "+uri.toString()+" processed");
        return null;
    }

    private void locationRegister(@NotNull Context context) {
//        Log.d(APPTAG, "locationRegister");
        if (checkGoogle(context)) return;
        if (mBroadcastFilter == null) {
            mBroadcastFilter = new IntentFilter(ActivityUtils.ACTION_REFRESH_STATUS_LIST);
            mBroadcastFilter.addCategory(ActivityUtils.CATEGORY_LOCATION_SERVICES);
        }
        List<String> geofencesRemove = db.getGeofencesToRemove();
        if (!geofencesRemove.isEmpty()) {
            locationsRegistered -= geofencesRemove.size();
            Log.w(APPTAG, "locationRegister remove " + TextUtils.join(",", geofencesRemove));
            if (mGeofenceRemover == null)
                mGeofenceRemover = new GeofenceRemover(context);
            mGeofenceRemover.removeGeofencesById(geofencesRemove);
        }
        if (mGeofenceRequester == null || mGeofenceRequester.mFailed) {
            mGeofenceRequester = new GeofenceRequester(context);
            db.resetGooglePlayServiceRunning();
        }
        List<Geofence> geofences = db.getGeofencesToAdd();
        if (!geofences.isEmpty()) {
            locationsRegistered += geofences.size();
            Log.w(APPTAG, "locationRegister add " + TextUtils.join(",", geofences));
            mGeofenceRequester.addGeofences(geofences);
        } else if (mGeofenceRequester != null) {
            if (!db.getPrivateSetting("ACTIVE_PROBE_ENABLED", "0").equals("1")) return;
            long now = Calendar.getInstance().getTimeInMillis();
//            Log.w(APPTAG, "locationRegister periodical "
//                    + (now - mGeofenceRequester.mLastDetectedTime) + " "
//                    + (now - mGeofenceRequester.mLastRequestedTime) );
            if (now - mGeofenceRequester.mLastDetectedTime > 14 * 60 * 1000
                    || now - mGeofenceRequester.mLastRequestedTime > 5 * 60 * 1000) {
                mGeofenceRequester.activeProbe();
            }
        }
    }

    private void locationUnregister(@NotNull Context context) {
//        Log.d(APPTAG, "locationUnregister");
        if (checkGoogle(context)) return;
        if (mBroadcastFilter == null) {
            mBroadcastFilter = new IntentFilter(ActivityUtils.ACTION_REFRESH_STATUS_LIST);
            mBroadcastFilter.addCategory(ActivityUtils.CATEGORY_LOCATION_SERVICES);
        }
        List<String> geofencesRemove = db.getGeofencesToRemove();
        if (!geofencesRemove.isEmpty()) {
            Log.w(APPTAG, "locationUnregister remove " + TextUtils.join(",", geofencesRemove));
            if (mGeofenceRemover == null)
                mGeofenceRemover = new GeofenceRemover(context);
            mGeofenceRemover.removeGeofencesById(geofencesRemove);
        }
    }

    private void activityRegister(@NotNull Context context) {
        if (checkGoogle(context)) return;
        if (mDetectionRequester == null || mDetectionRequester.mFailed) {
            mDetectionRequester = new DetectionRequester(context);
            mDetectionRemover = new DetectionRemover(context);
        }
        registered_activity_detection = true;
        mDetectionRequester.requestUpdates();
    }

    private void activityUnregister() {
        if (mDetectionRemover == null || mDetectionRequester == null
                || !registered_activity_detection) {
            return;
        }
        registered_activity_detection = false;
        mDetectionRemover.removeUpdates(mDetectionRequester.getRequestPendingIntent());
    }

    private void autoWakeUp() {
        Context context = getContext();
//        if (on_boot_ran) {
//            if (delay < MAX_DELAY) {
//                delay += 1<<decay;
//                decay++;
//            }
//            if (delay > MAX_DELAY) {
//                delay = MAX_DELAY;
//            }
//        } else {
//              on_boot_ran = true;
//        }
        assert context != null;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (bootPendingIntent != null) {
            alarmManager.cancel(bootPendingIntent);
        }
        Intent alarmIntent = new Intent(context, PhoneReceiver.class);
        alarmIntent.putExtra("type", "check");
        bootPendingIntent = PendingIntent.getBroadcast(context, 7837, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        long next_time = Calendar.getInstance().getTimeInMillis() + MAX_DELAY * 1000;
//        db.logStatsEventValue(StatsType.SCHEDULED, next_time);
        alarmManager.set(AlarmManager.RTC_WAKEUP, next_time, bootPendingIntent);
//        Log.w(APPTAG, "Auto wake up scheduled in " + delay + " seconds");

    }

    private boolean checkGoogle(@NotNull Context context) {
        int resCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        switch (resCode) {
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.DATE_INVALID:
                return true;
        }
        return false;
    }


    private void headsetRegister(@NotNull Context context) {
        if (headsetReceiver != null) return;
        IntentFilter receiverFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        headsetReceiver = new HeadsetStateReceiver();
        context.registerReceiver(headsetReceiver, receiverFilter);
    }

    private void headsetUnregister(@NotNull Context context) {
        if (headsetReceiver == null) return;
        context.unregisterReceiver(headsetReceiver);
        headsetReceiver = null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
