package il.co.kix.minitasker.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.SmsAction;

/**
 * new task for SMS action
 * <p/>
 * Created by phantom on 05/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskPhoneSmsActivity extends SavableFragmentActivity {
    static final int PICK_CONTACT_REQUEST = 1;  // The request code
    private SmsAction action;
    private EditText txtMessage;
    private EditText txtTarget;
    @Nullable
    private String strContactName = "";
    @Nullable
    private String strContact = "";

    @Override
    boolean isSaveEnabled() {
        //noinspection ConstantConditions
        return !txtTarget.getText().toString().isEmpty()
                && !txtMessage.getText().toString().isEmpty();
    }

    @Override
    protected void performSave() {
        action.setTarget_name(strContactName);
        //noinspection ConstantConditions
        action.setTarget(txtTarget.getText().toString());
        //noinspection ConstantConditions
        action.setMessage(txtMessage.getText().toString());
        action.setStart(is_start);
        action.save();
        entity_id = action.getPrimary();
        last_entity_type = action.getName();
        nextActivityWhen(false);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        action = new SmsAction();
        action.setContext(this);
        action.setPrimary(entity_id);
        if (last_entity_type != null && !last_entity_type.equals(action.getName())) {
            action = new SmsAction();
            action.setContext(this);
        }
        action.setTaskId(task_id);

        txtMessage = (EditText) findViewById(R.id.txtMessage);
        assert txtMessage != null;
        txtMessage.setText(action.getMessage());
        txtMessage.selectAll();
        txtTarget = (EditText) findViewById(R.id.txtTarget);
        assert txtTarget != null;
        txtTarget.setText(action.getTarget());
        txtTarget.selectAll();
        ImageButton btnContact = (ImageButton) findViewById(R.id.btnContactTarget);
        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContactIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI);
                pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });

        txtTarget.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(@NotNull Editable s) {
                if (!s.toString().equals(strContact))
                    strContactName = "";
                setSaveEnabled();
            }
        });

        txtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //empty
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //empty
            }

            @Override
            public void afterTextChanged(Editable s) {
                setSaveEnabled();
            }
        });

        setSaveEnabled();

    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_new_task_sms;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.sms_title;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @NotNull Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            Uri pickedPhoneNumber = data.getData();
            assert pickedPhoneNumber != null;
            final String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.Contacts.DISPLAY_NAME,
            };
            Cursor c = getContentResolver().query(pickedPhoneNumber, projection, null, null, null);
            if (c == null || !c.moveToFirst()) return;
            String phoneNumber = c.getString(c.getColumnIndexOrThrow(
                    ContactsContract.CommonDataKinds.Phone.NUMBER));
            assert phoneNumber != null;
            phoneNumber = phoneNumber.replace(" ", "");
            phoneNumber = phoneNumber.replace("-", "");
            phoneNumber = phoneNumber.replace("(", "");
            phoneNumber = phoneNumber.replace(")", "");
            txtTarget.setText(phoneNumber);

            strContact = phoneNumber;
            strContactName = c.getString(c.getColumnIndexOrThrow(
                    ContactsContract.Contacts.DISPLAY_NAME));

            setSaveEnabled();

            c.close();
        }
    }

}
