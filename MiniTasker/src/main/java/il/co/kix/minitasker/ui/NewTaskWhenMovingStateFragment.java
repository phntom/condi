package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.DetectedActivityType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.conditions.ActivityCondition;
import il.co.kix.minitasker.services.ActivityRecognitionIntentService;

/**
 * new task for activity condition
 * <p/>
 * Created by michal on 20/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */

public class NewTaskWhenMovingStateFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @Nullable
    ActivityCondition condition;
    private CheckBox chkWalking;
    private CheckBox chkBicycle;
    private CheckBox chkDriving;
    private CheckBox chkStill;
    private EditText txtDelay;

    @NotNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MiniTaskerActivity caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();

        builder.setTitle(getString(R.string.set_moving_state));

        View v = inflater.inflate(R.layout.popup_when_moving_state, null);
        assert v != null;

        chkStill = (CheckBox) v.findViewById(R.id.standing);
        chkWalking = (CheckBox) v.findViewById(R.id.walking);
        chkBicycle = (CheckBox) v.findViewById(R.id.bicycle);
        chkDriving = (CheckBox) v.findViewById(R.id.driving);
        txtDelay = (EditText) v.findViewById(R.id.txtDelay);

        condition = new ActivityCondition();
        condition.setContext(caller);
        condition.setPrimary(caller.entity_id);
        if (caller.last_entity_type != null
                && !caller.last_entity_type.equals(condition.getName())) {
            condition = new ActivityCondition();
            condition.setContext(caller);
        }
        condition.setTaskId(caller.task_id);
        chkStill.setChecked(condition.still);
        chkWalking.setChecked(condition.on_foot);
        chkBicycle.setChecked(condition.on_bicycle);
        chkDriving.setChecked(condition.in_vehicle);
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        final String delay = db.getPrivateSetting(
                ActivityRecognitionIntentService.KEY_ACTIVITY_DELAY, "30");
        txtDelay.setText(delay);

        builder.setPositiveButton(R.string.ok, this);
        builder.setView(v);
        return builder.create();
    }

    @Override
    public void onClick(@NotNull DialogInterface dialogInterface, int i) {
        MiniTaskerActivity callingActivity = (MiniTaskerActivity) getActivity();
        if (callingActivity == null || condition == null) return;
        if (!chkBicycle.isChecked() && !chkDriving.isChecked() && !chkWalking.isChecked()
                && !chkStill.isChecked()) return;

        ArrayList<DetectedActivityType> types = new ArrayList<DetectedActivityType>();
        if (chkStill.isChecked()) types.add(DetectedActivityType.STILL);
        if (chkWalking.isChecked()) types.add(DetectedActivityType.ON_FOOT);
        if (chkDriving.isChecked()) types.add(DetectedActivityType.IN_VEHICLE);
        if (chkBicycle.isChecked()) types.add(DetectedActivityType.ON_BICYCLE);
        if (types.isEmpty()) types.add(DetectedActivityType.STILL);

        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        final Editable edtDelay = txtDelay.getText();
        String delay = edtDelay != null ? edtDelay.toString() : null;
        if (delay != null)
            db.setPrivateSetting(ActivityRecognitionIntentService.KEY_ACTIVITY_DELAY, delay);

        condition.setValues(types);
        condition.save();
        callingActivity.entity_id = condition.getPrimary();
        callingActivity.last_entity_type = condition.getName();

        if (callingActivity.isNotEditMode())
            condition.setTaskEnabled(true);
        callingActivity.nextActivityMain(true);

        dialogInterface.dismiss();
    }

}