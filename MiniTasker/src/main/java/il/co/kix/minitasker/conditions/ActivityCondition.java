package il.co.kix.minitasker.conditions;

import android.content.ContentValues;
import android.net.Uri;
import android.util.Log;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

import il.co.kix.minitasker.BackgroundProvider;
import il.co.kix.minitasker.DetectedActivityType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskWhenMovingStateFragment;

/**
 * condition for activity recognition
 * <p/>
 * Created by phantom on 08/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class ActivityCondition extends ConditionBase {

    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "activity", SqlDataType.SET
    );

    public boolean in_vehicle;
    public boolean on_bicycle;
    public boolean on_foot;
    public boolean still;
//    private boolean unknown;
//    private boolean tilting;

    @Override
    public boolean save() {
        boolean result = super.save();
        context.getContentResolver().query(Uri.parse(BackgroundProvider.BACKGROUND_PROVIDER
                + "/activity/register"), null, null, null, null);
        return result;
    }

    @Override
    public boolean conditionApplies(boolean is_start) {
        DetectedActivityType newType = DetectedActivityType.valueOf(
                db.getPrivateSetting("ACTIVITY_RECOGNITION_NEW",
                        DetectedActivityType.UNKNOWN.name()));
        switch (newType) {
            case IN_VEHICLE:
                return is_start ? in_vehicle : !in_vehicle;
            case ON_BICYCLE:
                return is_start ? on_bicycle : !on_bicycle;
            case ON_FOOT:
                return is_start ? on_foot : !on_foot;
            case STILL:
                return is_start ? still : !still;
        }

        return false;
    }

    @NotNull
    @Override
    public String toString() {
        return context.getString(R.string.condition_activity_individual_container, getDescription());
    }

    @NotNull
    @Override
    public String getName() {
        return "activity";
    }

    @NotNull
    @Override
    public String getDescription() {
        String[] elements = new String[4];
        int count = 0;
        if (in_vehicle) {
            elements[count] = context.getString(R.string.condition_activity_element_vehicle);
            count++;
        }
        if (on_bicycle) {
            elements[count] = context.getString(R.string.condition_activity_element_bicycle);
            count++;
        }
        if (on_foot) {
            elements[count] = context.getString(R.string.condition_activity_element_foot);
            count++;
        }
        if (still) {
            elements[count] = context.getString(R.string.condition_activity_element_still);
            count++;
        }
        switch (count) {
            case 4:
                return context.getString(R.string.condition_activity_container_4, elements[0],
                        elements[1], elements[2], elements[3]);
            case 3:
                return context.getString(R.string.condition_activity_container_3, elements[0],
                        elements[1], elements[2]);
            case 2:
                return context.getString(R.string.condition_activity_container_2, elements[0],
                        elements[1]);
            case 1:
                return context.getString(R.string.condition_activity_container_1, elements[0]);
        }
        Log.e(APPTAG, "ActivityCondition getDescription bad value");
        return "";
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenMovingStateFragment.class;
    }

    @Override
    public void init() {
        in_vehicle = false;
        on_bicycle = false;
        on_foot = false;
        still = false;
//        unknown = false;
//        tilting = false;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        int exp = myValues.getAsInteger("activity");
        in_vehicle = ((exp & 1) != 0);
        on_bicycle = ((exp & 2) != 0);
        on_foot = ((exp & 4) != 0);
        still = ((exp & 8) != 0);
//        unknown = ((exp & 16) != 0);
//        tilting = ((exp & 32) != 0);
    }

    public void setValues(@NotNull List<DetectedActivityType> types) {
        int sum = 0;
        in_vehicle = false;
        on_bicycle = false;
        on_foot = false;
        still = false;
//        unknown = false;
//        tilting = false;
        for (DetectedActivityType dat : types) {
            switch (dat) {
//                case TILTING:
//                    tilting = true;
//                    break;
//                case UNKNOWN:
//                    unknown = true;
//                    break;
                case STILL:
                    still = true;
                    break;
                case ON_FOOT:
                    on_foot = true;
                    break;
                case IN_VEHICLE:
                    in_vehicle = true;
                    break;
                case ON_BICYCLE:
                    on_bicycle = true;
                    break;
            }
            sum += dat.toDatabase();
        }
        myValues.put("activity", sum);
    }
}
