package il.co.kix.minitasker.actions;

import android.content.Context;
import android.net.wifi.WifiManager;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * WiFi connection action
 * <p/>
 * Created by sphantom on 7/27/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class WiFiAction extends StatesAction {
    private WifiManager w;

    @NotNull
    @Override
    public String getDeviceName() {
        return "wifi";
    }

    @Override
    protected boolean perform_init() {
        w = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (w == null) {
            setFlag(FlagsEnum.WIFI_MISSING_SERVICE);
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        switch (w.getWifiState()) {
            case WifiManager.WIFI_STATE_ENABLED:
            case WifiManager.WIFI_STATE_ENABLING:
                return StateType.On;
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_UNKNOWN:
            default:
                return StateType.Off;
        }
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.WIFI;
    }

    @Override
    protected boolean perform_on() {
        try {
            return w.setWifiEnabled(true);
        } catch (java.lang.SecurityException ignored) {
            setFlag(FlagsEnum.WIFI_SECURITY_BUG);
            return true;
        }
    }

    @Override
    protected boolean perform_off() {
        try {
            return w.setWifiEnabled(false);
        } catch (java.lang.SecurityException ignored) {
            setFlag(FlagsEnum.WIFI_SECURITY_BUG);
            return true;
        }
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.WIFI_FAILED);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_wifi_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_wifi_individual_on);
            case Off:
                return context.getString(R.string.action_wifi_individual_off);
            case Toggle:
                return context.getString(R.string.action_wifi_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_wifi_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_wifi_description_on);
            case Off:
                return context.getString(R.string.action_wifi_description_off);
            case Toggle:
                return context.getString(R.string.action_wifi_description_toggle);
        }
        return "";
    }
}
