package il.co.kix.minitasker.actions;

import android.bluetooth.BluetoothAdapter;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.StateType;

/**
 * bluetooth action
 * <p/>
 * Created by sphantom on 7/27/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class BluetoothAction extends StatesAction {
    private BluetoothAdapter b;

    @NotNull
    @Override
    public String getDeviceName() {
        return "bluetooth";
    }

    @Override
    protected boolean perform_init() {
        b = BluetoothAdapter.getDefaultAdapter();
        if (b == null) {
            setFlag(FlagsEnum.BLUETOOTH_ADAPTER_MISSING);
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    protected StateType perform_get_current_state() {
        int current_state = b.getState();
        switch (current_state) {
            case BluetoothAdapter.STATE_ON:
            case BluetoothAdapter.STATE_TURNING_ON:
                return StateType.On;
            case BluetoothAdapter.STATE_OFF:
            case BluetoothAdapter.STATE_TURNING_OFF:
                return StateType.Off;
            default:
                return StateType.Off;
        }
    }

    @NotNull
    @Override
    protected LastType perform_get_last_type() {
        return LastType.BLUETOOTH;
    }

    @Override
    protected boolean perform_on() {
        return b.enable();
    }

    @Override
    protected boolean perform_off() {
        return b.disable();
    }

    @Override
    protected void perform_fail() {
        setFlag(FlagsEnum.BLUETOOTH_IMMEDIATE_ERROR);
    }

    @NotNull
    @Override
    public String toString() {
        if (isRestore()) return context.getString(R.string.action_bluetooth_individual_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_bluetooth_individual_on);
            case Off:
                return context.getString(R.string.action_bluetooth_individual_off);
            case Toggle:
                return context.getString(R.string.action_bluetooth_individual_toggle);
        }
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        if (isRestore()) return context.getString(R.string.action_bluetooth_description_restore);
        switch (state) {
            case On:
                return context.getString(R.string.action_bluetooth_description_on);
            case Off:
                return context.getString(R.string.action_bluetooth_description_off);
            case Toggle:
                return context.getString(R.string.action_bluetooth_description_toggle);
        }
        return "";
    }

}
