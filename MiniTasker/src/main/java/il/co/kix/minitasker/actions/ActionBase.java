package il.co.kix.minitasker.actions;

import android.content.ContentValues;
import android.content.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.EntityBase;
import il.co.kix.minitasker.EntityType;
import il.co.kix.minitasker.FlagsEnum;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.utils.EntityUtils;


/**
 * action base class for inheritance
 * <p/>
 * Created by phantom on 27/07/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public abstract class ActionBase extends EntityBase {
    protected int what_id = 0;
    private boolean start = true;
    private boolean restore = false;

    @Nullable
    static public ActionBase ActionBaseFactory(@NotNull Context context, long primary) {
        return (ActionBase) EntityUtils.entityFactory(context, primary, "what", EntityUtils.actions,
                "Action");
    }

    @Override
    public boolean save() {
        boolean result = super.save();
        ContentValues what_values = new ContentValues();
        what_values.put("what_id", what_id);
        what_values.put("what_item", item_id);
        what_values.put("what_type", getName());
        what_values.put("description", toString());
        what_values.put("taskdesc", getDescription());
        what_values.put("start", start ? 1 : 0);
        what_values.put("restore", restore ? 1 : 0);
        what_values.put("idx", 10000);
//        what_valuess.put("enabled", true);
        primary = db.saveWhenWhat(primary, what_id, task_id, "what", what_values);
        db.fixIdxActions(what_id, start);
        //noinspection ConstantConditions
        what_id = what_values.getAsInteger("what_id");
        ContentValues task_values = new ContentValues();
        task_values.put("what_id", what_id);
        task_values.put("description", db.generateTaskDescription(task_id, 0, what_id));
        db.saveEntity(task_id, "tasks", task_values);
        return result;
    }

    @Override
    public void load() {
        super.load();
        int[] values = db.getActionValues(primary);
        what_id = values[0];
        start = values[1] == 1;
        restore = values[2] == 1;
        task_id = db.getTaskByEntity(what_id, "what");
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public int getWhatId() {
//        return what_id;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

// --Commented out by Inspection START (28/09/13 23:45):
//    public void setWhatId(int wId) {
//        what_id = wId;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    @NotNull
    public EntityType getType() {
        return EntityType.ENTITY_ACTION;
    }

    public abstract void perform(StartType is_start);


    @NotNull
    @Override
    protected String getTableName() {
        return "what_" + getName();
    }

    @Override
    public boolean getEnabled() {
        return super.getEnabled() && db.isEnabled(task_id, what_id, "what");
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        ContentValues values = new ContentValues();
        values.put("enabled", enabled);
        db.saveEntity(primary, "what", values);
    }

    public boolean isRestore() {
        return restore;
    }

    public void setRestore(boolean restore) {
        this.restore = restore;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }

    public void setFlag(@NotNull FlagsEnum flag) {
        ContentValues values = new ContentValues();
        values.put("flag", flag.ordinal());
        db.saveEntity(primary, "what", values);
    }

    public void saveRestoreCopy() {
        if (restore) return;
        setRestore(true);
        ContentValues what_values = new ContentValues();
        what_values.put("what_id", what_id);
        what_values.put("what_item", item_id);
        what_values.put("what_type", getName());
        what_values.put("description", toString());
        what_values.put("taskdesc", getDescription());
        what_values.put("start", start ? 0 : 1);
        what_values.put("restore", 1);
        what_values.put("idx", 10000);
        what_values.put("enabled", 1);
        db.saveWhenWhat(0, what_id, task_id, "what", what_values);
        db.fixIdxActions(what_id, !start);
        setRestore(false);
    }

//    public void shallowCopyStop() {
//        if (restore) return;
//        item_id = 0;
//        start = !start;
//        save();
//    }
}
