package il.co.kix.minitasker.ui;

import il.co.kix.minitasker.R;

/**
 * new task for sensors activities
 * <p/>
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskBatterySensorsActivity extends ExpandableListActivity {

    @Override
    int resourceActivityTitle() {
        return R.string.new_task_connectivity_and_sensors;
    }

    @Override
    protected int getGroupTitleArray() {
        return R.array.new_task_connectivity_groups;
    }

    @Override
    protected int getChildTitleArray(int group) {
        switch (group) {
            case 0:
            default:
                return R.array.new_task_connectivity_1_titles;
            case 1:
                return R.array.new_task_connectivity_2_titles;
        }
    }

    @Override
    protected int getChildDescArray(int group) {
        switch (group) {
            case 0:
            default:
                return R.array.new_task_connectivity_1_descriptions;
            case 1:
                return R.array.new_task_connectivity_2_descriptions;
        }
    }

    @Override
    protected boolean onItemClick(int groupPosition, int childPosition) {
        String tag = null;
        switch (groupPosition) {
            case 0:
                switch (childPosition) {
                    case 0:
                        tag = "wifi";
                        break;
                    case 1:
                        tag = "sync";
                        break;
                    case 2:
                        tag = "mobile";
                        break;
                    case 3:
                        tag = "airplane";
                        break;
                    case 4:
                        tag = "wifiap";
                        break;
                    case 5:
                        tag = "usbap";
                        break;
                }
                break;
            case 1:
                switch (childPosition) {
                    case 0:
                        tag = "gps";
                        break;
                    case 1:
                        tag = "bluetooth";
                        break;
                    case 2:
                        tag = "nfc";
                        break;
                    case 3:
                        tag = "orientation";
                        break;
                    case 4:
                        tag = "scanning";
                        break;
                }
                break;
        }
        if (tag == null) tag = "";
        new NewTaskSensorsOnOffToggleFragment().show(getFragmentManager(), tag);
        return true;
    }
}