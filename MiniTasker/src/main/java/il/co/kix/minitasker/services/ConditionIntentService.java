package il.co.kix.minitasker.services;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StartType;
import il.co.kix.minitasker.conditions.ConditionBase;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * This class will manage condition requests in a centralized and sequential way
 */
public class ConditionIntentService extends IntentService {
    public static final String ACTION_CONDITION_MATCH =
            "il.co.kix.minitasker.services.action.CONDITION_MATCH";

    public static final String EXTRA_TYPE = "il.co.kix.minitasker.services.extra.CONDITION_TYPE";
    public static final String EXTRA_TASK_ID = "il.co.kix.minitasker.services.extra.TASK_ID";
    public static final String EXTRA_WHEN_ROWID = "il.co.kix.minitasker.services.extra.WHEN_ROWID";
    public static final String EXTRA_IS_START = "il.co.kix.minitasker.services.extra.IS_START";

    private static final String APPTAG = DatabaseHandler.APPTAG;

    public ConditionIntentService() {
        super("ConditionIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        try {
            if (null == intent || !ACTION_CONDITION_MATCH.equals(intent.getAction())) {
                return;
            }

            final String condition_type = intent.getStringExtra(EXTRA_TYPE);
            final int task_id = intent.getIntExtra(EXTRA_TASK_ID, 0);
            final long when_rowid = intent.getLongExtra(EXTRA_WHEN_ROWID, 0);
            final boolean is_start = intent.getIntExtra(EXTRA_IS_START, 0) == 1;
            if (condition_type == null || condition_type.equals("") || task_id == 0 || when_rowid == 0) {
                Log.e(APPTAG, String.format("ConditionIntentService received a bad intent task_id: %d, "
                        + "when_rowid: %d condition_type: %s", task_id, when_rowid, condition_type));
                return;
            }

            if (TextUtils.isEmpty(condition_type)) return;
            ConditionBase condition = ConditionBase.ConditionBaseFactory(this, when_rowid);
            if (condition == null) {
                Log.e(APPTAG, String.format("ConditionIntentService Failed loading condition %d",
                        when_rowid));
                return;
            }
            if (condition.getTaskId() != task_id) {
                Log.e(APPTAG, String.format("ConditionIntentService Invalid task_id %d for condition "
                                + "%d detected. Corrected to %d", condition.getTaskId(), when_rowid,
                        task_id));
                condition.setTaskId(task_id);
                condition.save();
            }

            try {
                condition.updateMatching(is_start);
            } catch (InterruptedException ignored) {
                return;
            }

            db.scheduleMatchingTasks(is_start ? StartType.START_WITH_END : StartType.END);
        } finally {
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (db) {
                db.matchConditionCounter--;
            }
        }
    }

}
