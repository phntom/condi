package il.co.kix.minitasker;

/**
 * holds type values for statistics
 *
 * Created by phantom on 25/09/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public enum StatsType {
    UNKNOWN,
    BOOT_LAUNCH,
    TIME_LAUNCH,
    MAIN_LAUNCH,
    MAIN_DESTROY,
    USER_USAGE,
    SCHEDULED,
}
