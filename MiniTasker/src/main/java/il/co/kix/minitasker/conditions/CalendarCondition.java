package il.co.kix.minitasker.conditions;

import android.content.ContentValues;

import com.google.common.collect.ImmutableMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SqlDataType;
import il.co.kix.minitasker.ui.NewTaskWhenTimeCalendarActivity;

/**
 * calendar events condition
 * <p/>
 * Created by phantom on 18/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class CalendarCondition extends ConditionBase {
    public static Map<String, SqlDataType> fields = ImmutableMap.of(
            "identifier", SqlDataType.INTEGER,
            "location", SqlDataType.TEXT,
            "title", SqlDataType.TEXT,
            "desc", SqlDataType.TEXT,
            "cal_name", SqlDataType.TEXT
    );

    private static final String WILDCARD = "*";


    private AvailabilityType availabilityType;
    private long calendarIdentifier;
    @Nullable
    private String calendarName;
    @Nullable
    private String locationWildcard;
    @Nullable
    private String titleWildcard;
    @Nullable
    private String descriptionWildcard;

    @Override
    public boolean save() {
        boolean result = super.save();
        db.scheduleNextCalendarEvent();
        return result;
    }

    @Override
    public boolean conditionApplies(boolean is_start) throws InterruptedException {
        //TODO: get an event from calendar that start<now and end>now
        return true;
    }

// --Commented out by Inspection START (28/09/13 23:45):
//    public String getCalendarName() {
//        return calendarName;
//    }
// --Commented out by Inspection STOP (28/09/13 23:45)

    public void setCalendarName(@Nullable String calendarName) {
        if (calendarName == null) return;
        this.calendarName = calendarName;
        myValues.put("cal_name", calendarName);
    }

    @NotNull
    @Override
    public String toString() {
        return getDescriptionAux(true);
    }

    @NotNull
    @Override
    public String getDescription() {
        return getDescriptionAux(false);
    }

    @NotNull
    public String getDescriptionAux(boolean verbose) {
        String[] container = new String[4];
        int count = 0;
        if (titleWildcard != null && !titleWildcard.isEmpty()) {
            boolean title_suffix = titleWildcard.charAt(0) == '*';
            boolean title_prefix = titleWildcard.charAt(titleWildcard.length() - 1) == '*';
            if (titleWildcard.equals(WILDCARD)) {
                if (verbose) {
                    container[count] = context.getString(R.string.condition_calendar_description_title_any);
                } else {
                    count--;
                }
            } else if (title_prefix && title_suffix) {
                String contains = titleWildcard.substring(1, titleWildcard.length() - 1);
                container[count] = context.getString(R.string.condition_calendar_description_title_contains, contains);
            } else if (title_prefix) {
                String prefix = titleWildcard.substring(1);
                container[count] = context.getString(R.string.condition_calendar_description_title_prefix, prefix);
            } else if (title_suffix) {
                String suffix = titleWildcard.substring(0, titleWildcard.length() - 1);
                container[count] = context.getString(R.string.condition_calendar_description_title_suffix, suffix);
            } else {
                container[count] = context.getString(R.string.condition_calendar_description_title_matching, titleWildcard);
            }
            count++;
        }
        if (locationWildcard != null && !locationWildcard.isEmpty()) {
            boolean location_suffix = locationWildcard.charAt(0) == '*';
            boolean location_prefix = locationWildcard.charAt(locationWildcard.length() - 1) == '*';
            if (locationWildcard.equals(WILDCARD)) {
                if (verbose) {
                    container[count] = context.getString(R.string.condition_calendar_description_location_any);
                } else {
                    count--;
                }
            } else if (location_prefix && location_suffix) {
                String contains = locationWildcard.substring(1, locationWildcard.length() - 1);
                container[count] = context.getString(R.string.condition_calendar_description_location_contains, contains);
            } else if (location_prefix) {
                String prefix = locationWildcard.substring(1);
                container[count] = context.getString(R.string.condition_calendar_description_location_prefix, prefix);
            } else if (location_suffix) {
                String suffix = locationWildcard.substring(0, locationWildcard.length() - 1);
                container[count] = context.getString(R.string.condition_calendar_description_location_suffix, suffix);
            } else {
                container[count] = context.getString(R.string.condition_calendar_description_location_matching, locationWildcard);
            }
            count++;
        }
        if (descriptionWildcard != null && !descriptionWildcard.isEmpty()) {
            boolean description_suffix = descriptionWildcard.charAt(0) == '*';
            boolean description_prefix = descriptionWildcard.charAt(descriptionWildcard.length() - 1) == '*';
            if (descriptionWildcard.equals(WILDCARD)) {
                if (verbose) {
                    container[count] = context.getString(R.string.condition_calendar_description_description_any);
                } else {
                    count--;
                }
            } else if (description_prefix && description_suffix) {
                String contains = descriptionWildcard.substring(1, descriptionWildcard.length() - 1);
                container[count] = context.getString(R.string.condition_calendar_description_description_contains, contains);
            } else if (description_prefix) {
                String prefix = descriptionWildcard.substring(1);
                container[count] = context.getString(R.string.condition_calendar_description_description_prefix, prefix);
            } else if (description_suffix) {
                String suffix = descriptionWildcard.substring(0, descriptionWildcard.length() - 1);
                container[count] = context.getString(R.string.condition_calendar_description_description_suffix, suffix);
            } else {
                container[count] = context.getString(R.string.condition_calendar_description_description_matching, descriptionWildcard);
            }
            count++;
        }
        if (availabilityType == AvailabilityType.ANY && verbose) {
            container[count] = context.getString(R.string.condition_calendar_availability_any);
            count++;
        } else if (availabilityType == AvailabilityType.AVAILABLE) {
            container[count] = context.getString(R.string.condition_calendar_availability_available);
            count++;
        } else if (availabilityType == AvailabilityType.UNAVAILABLE) {
            container[count] = context.getString(R.string.condition_calendar_availability_busy);
            count++;
        }

        if (count == 0)
            return context.getString(R.string.condition_calendar_description_matches_anything, calendarName);
        String rest;
        switch (count) {
            case 4:
                rest = context.getString(R.string.condition_calendar_description_container_4,
                        container[0], container[1], container[2], container[3]);
                break;
            case 3:
                rest = context.getString(R.string.condition_calendar_description_container_3,
                        container[0], container[1], container[2]);
                break;
            case 2:
                rest = context.getString(R.string.condition_calendar_description_container_2,
                        container[0], container[1]);
                break;
            case 1:
                rest = context.getString(R.string.condition_calendar_description_container_1,
                        container[0]);
                break;
            default:
                return "";
        }
        return context.getString(R.string.condition_calendar_description_container, calendarName, rest);
    }

    @NotNull
    @Override
    public String getName() {
        return "calendar";
    }

    @NotNull
    @Override
    public Class<?> getRelatedActivity() {
        return NewTaskWhenTimeCalendarActivity.class;
    }

    @Override
    public void init() {
        calendarIdentifier = 0L;
        availabilityType = AvailabilityType.ANY;
        locationWildcard = "*";
        titleWildcard = "*";
        descriptionWildcard = "*";
        calendarName = "";
    }

    public AvailabilityType getAvailabilityType() {
        return availabilityType;
    }

    public void setAvailabilityType(@NotNull AvailabilityType availabilityType) {
        this.availabilityType = availabilityType;
        myValues.put("available", availabilityType.ordinal());
    }

    public long getCalendarIdentifier() {
        return calendarIdentifier;
    }

    public void setCalendarIdentifier(long calendarIdentifier) {
        this.calendarIdentifier = calendarIdentifier;
        myValues.put("identifier", calendarIdentifier);
    }

    @Nullable
    public String getLocationWildcard() {
        return locationWildcard;
    }

    public void setLocationWildcard(@NotNull String locationWildcard) {
        if (locationWildcard.isEmpty())
            locationWildcard = "*";
        locationWildcard = cleanupWildcard(locationWildcard);
        this.locationWildcard = locationWildcard;
        myValues.put("location", locationWildcard);
    }

    @Nullable
    public String getTitleWildcard() {
        return titleWildcard;
    }

    public void setTitleWildcard(@NotNull String titleWildcard) {
        if (titleWildcard.isEmpty())
            titleWildcard = "*";
        titleWildcard = cleanupWildcard(titleWildcard);
        this.titleWildcard = titleWildcard;
        myValues.put("title", titleWildcard);
    }

    @Nullable
    public String getDescriptionWildcard() {
        return descriptionWildcard;
    }

    public void setDescriptionWildcard(@NotNull String descriptionWildcard) {
        if (descriptionWildcard.isEmpty())
            descriptionWildcard = "*";
        descriptionWildcard = cleanupWildcard(descriptionWildcard);
        this.descriptionWildcard = descriptionWildcard;
        myValues.put("desc", descriptionWildcard);
    }

    private String cleanupWildcard(String wildcard) {
        String ret = wildcard;
        while (ret.contains("**")) {
            ret = ret.replace("**", "*");
        }
        return ret;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void loadLocalValues(@NotNull ContentValues myValues) throws NullPointerException {
        calendarIdentifier = myValues.getAsLong("identifier");
        locationWildcard = myValues.getAsString("location");
        titleWildcard = myValues.getAsString("title");
        descriptionWildcard = myValues.getAsString("desc");
        calendarName = myValues.getAsString("cal_name");
    }

    public enum AvailabilityType {
        ANY,
        AVAILABLE,
        UNAVAILABLE,
    }

}
