package il.co.kix.minitasker.ui;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Switch;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.SettingsCondition;
import il.co.kix.minitasker.services.ActivityRecognitionIntentService;

/**
 * settings activity
 * <p/>
 * Created by phantom on 29/08/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class SettingsSavableActivity extends SavableConditionFragmentActivity {
    private EditText txtActivityTimeout;
    private Switch swiStatistics;
    private Switch swiActiveProbe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        txtActivityTimeout = (EditText) findViewById(R.id.txtActivityTimeout);
        swiStatistics = (Switch) findViewById(R.id.swiStatistics);
        SettingsCondition condition = (SettingsCondition) getCondition();
        swiStatistics.setChecked(condition.isStatistics());
//        txtActivityTimeout.setText(Integer.toString(condition.getActivity_timeout()));
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        final String delay = db.getPrivateSetting(
                ActivityRecognitionIntentService.KEY_ACTIVITY_DELAY, "30");
        txtActivityTimeout.setText(delay);
        txtActivityTimeout.selectAll();
        swiActiveProbe = (Switch) findViewById(R.id.swiActiveProbe);
        swiActiveProbe.setChecked(db.getPrivateSetting("ACTIVE_PROBE_ENABLED", "0").equals("1"));
    }

    @NotNull
    @Override
    ConditionBase instantiateNewCondition() {
        return new SettingsCondition();
    }

    @Override
    protected void performSaveCondition(ConditionBase c) {
        SettingsCondition condition = (SettingsCondition) c;
        //noinspection ConstantConditions
        String strActivityTimeout = txtActivityTimeout.getText().toString();
        int intActivityTimeout = 0;
        try {
            intActivityTimeout = Integer.valueOf(strActivityTimeout);
            if (intActivityTimeout < 0 || intActivityTimeout > 60 * 60)
                intActivityTimeout = 0;
        } catch (NumberFormatException ignored) {
        }
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        db.setPrivateSetting(ActivityRecognitionIntentService.KEY_ACTIVITY_DELAY,
                "" + intActivityTimeout);
        boolean boolStatistics = swiStatistics.isChecked();
        condition.setActivity_timeout(intActivityTimeout);
        condition.setStatistics(boolStatistics);
        db.setPrivateSetting("ACTIVE_PROBE_ENABLED", swiActiveProbe.isChecked() ? "1" : "0");
    }

    @Override
    boolean isSaveEnabled() {
        return true;
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.activity_settings;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.settings_activity_title;
    }
}
