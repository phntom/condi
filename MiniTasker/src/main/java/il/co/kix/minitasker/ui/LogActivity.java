package il.co.kix.minitasker.ui;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;

/**
 * Created by phantom on 30/11/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class LogActivity extends MiniTaskerActivity
        implements LoaderManager.LoaderCallbacks<SimpleCursorAdapter> {
    private LoaderManager loaderManager;
    @Nullable
    private ListView lstLog = null;
    private View pbrLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstLog = (ListView) findViewById(R.id.lstLog);
        pbrLoading = findViewById(R.id.pbrLoading);

        loaderManager = getLoaderManager();
        assert loaderManager != null;
        reloadData();
    }

    public void reloadData() {
        Loader loader = getLoaderManager().getLoader(0);
        if (loader != null && loader.isReset()) {
            getLoaderManager().restartLoader(0, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        DatabaseHandler db = SingletonDatabase.INSTANCE.db;
        switch (item.getItemId()) {
            case R.id.action_clear:
                assert db != null;
                db.clearLog();
                reloadData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loaderManager != null) {
            loaderManager.destroyLoader(0);
        }
    }

    @Override
    int layoutResourceToInflate() {
        return R.layout.log;
    }

    @Override
    int resourceActivityTitle() {
        return R.string.activity_log_title;
    }

    @NotNull
    @Override
    public Loader<SimpleCursorAdapter> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<SimpleCursorAdapter>(this) {
            @NotNull
            @Override
            public SimpleCursorAdapter loadInBackground() {
                assert SingletonDatabase.INSTANCE.db != null;
                DatabaseHandler db = SingletonDatabase.INSTANCE.db;
                assert db != null;
                Cursor cursor = db.getLogCursor();

                return new SimpleCursorAdapter(
                        getContext(),
                        android.R.layout.simple_list_item_1,
                        cursor,
                        new String[]{"content"},
                        new int[]{android.R.id.text1},
                        0
                );
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<SimpleCursorAdapter> loader, @Nullable SimpleCursorAdapter data) {
        if (data == null || lstLog == null) return;
        {
            SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) lstLog.getAdapter();
            if (oldAdapter != null) {
                Cursor oldCursor = oldAdapter.swapCursor(null);
                if (oldCursor != null) {
                    oldCursor.close();
                }
            }
        }
        lstLog.setAdapter(data);
        lstLog.setVisibility(View.VISIBLE);
        pbrLoading.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<SimpleCursorAdapter> loader) {
        if (lstLog != null) {
            SimpleCursorAdapter oldAdapter = (SimpleCursorAdapter) lstLog.getAdapter();
            if (oldAdapter != null) {
                Cursor oldCursor = oldAdapter.swapCursor(null);
                if (oldCursor != null) {
                    oldCursor.close();
                }
            }
        }
    }
}
