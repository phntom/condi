package il.co.kix.minitasker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Switch;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import il.co.kix.minitasker.AppInfo;
import il.co.kix.minitasker.AppsWithIconsAdapter;
import il.co.kix.minitasker.LoadAppListAdapter;
import il.co.kix.minitasker.MediaActionType;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.actions.LaunchAction;
import il.co.kix.minitasker.actions.MediaAction;
import il.co.kix.minitasker.actions.PackageBaseAction;
import il.co.kix.minitasker.actions.TerminateAction;

/**
 * handles the list of applications to terminate or launch
 * <p/>
 * Created by phantom on 01/06/13.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class NewTaskAppsListFragment extends DialogFragment
        implements LoaderManager.LoaderCallbacks<AppsWithIconsAdapter>,
        DialogInterface.OnClickListener, AdapterView.OnItemClickListener {

    public static final String LAUNCH = "LAUNCH";
    public static final String TERMINATE = "TERMINATE";
    public static final String MUSIC = "MUSIC";
    public static final String SHORTCUT = "SHORTCUT";
    AppsWithIconsAdapter mAdapter;
    @Nullable
    PackageBaseAction action;
    Dialog mDialog;
    @Nullable
    private MiniTaskerActivity caller;
    private ListView listView;
    private View loadingView;
    @Nullable
    private Switch swiRoot = null;
    @Nullable
    private LoaderManager loaderManager;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        caller = (MiniTaskerActivity) getActivity();
        assert caller != null;
        AlertDialog.Builder builder = new AlertDialog.Builder(caller);
        LayoutInflater inflater = caller.getLayoutInflater();
        final String tag = getTag() == null ? "" : getTag();
        final View v = inflater.inflate(
                (tag.equals(TERMINATE) ? R.layout.kill_list_activity : R.layout.list_activity)
                , null);
        assert v != null;
        loadingView = v.findViewById(R.id.pbrLoading);
        listView = (ListView) v.findViewById(R.id.list);

        builder.setView(v);
        if (tag.equals(LAUNCH) || tag.equals(MUSIC) || tag.equals(SHORTCUT)) {
            if (tag.equals(MUSIC)) {
                builder.setTitle(R.string.new_task_title_apps_music);
            } else {
                builder.setTitle(R.string.new_task_title_apps_launch);
            }
            action = new LaunchAction();
            action.setContext(caller);
            action.setPrimary(caller.entity_id);
            if (caller.last_entity_type != null
                    && !caller.last_entity_type.equals(action.getName())) {
                action = new LaunchAction();
                action.setContext(caller);
            }
        } else if (tag.equals(TERMINATE)) {
            builder.setTitle(R.string.new_task_title_apps_terminate);
            swiRoot = (Switch) v.findViewById(R.id.swiRoot);
            action = new TerminateAction();
            action.setContext(caller);
            action.setPrimary(caller.entity_id);
            if (caller.last_entity_type != null
                    && !caller.last_entity_type.equals(action.getName())) {
                action = new TerminateAction();
                action.setContext(caller);
            }
        } else {
            return builder.create();
        }
        action.setTaskId(caller.task_id);
        builder.setNegativeButton(R.string.cancel, this);

        listView.setOnItemClickListener(this);

        loaderManager = getLoaderManager();
        assert loaderManager != null;

        Loader loader = getLoaderManager().getLoader(0);
        if (loader != null && loader.isReset()) {
            getLoaderManager().restartLoader(0, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        }

        mDialog = builder.create();
        return mDialog;
    }

    @Nullable
    @Override
    public Loader<AppsWithIconsAdapter> onCreateLoader(int id, Bundle args) {
//        Log.w("MiniTasker", "onCreateLoader onCreateLoader onCreateLoader");
        assert caller != null;
        boolean isTerminate = getTag() != null && getTag().equals(TERMINATE);
        return new LoadAppListAdapter(caller, isTerminate);
    }

    @Override
    public void onLoadFinished(Loader<AppsWithIconsAdapter> loader, @NotNull AppsWithIconsAdapter data) {
//        Log.w("MiniTasker", "onLoadFinished onLoadFinished onLoadFinished");
        mAdapter = data;
        listView.setAdapter(data);
        loadingView.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        if (swiRoot != null) {
            swiRoot.setVisibility(View.VISIBLE);
            swiRoot.setEnabled(data.isHasRoot());
            if (!data.isHasRoot())
                swiRoot.setChecked(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<AppsWithIconsAdapter> loader) {
//        Log.w("MiniTasker", "onLoaderReset onLoaderReset onLoaderReset");
        listView.setAdapter(null);
    }

    @Override
    public void onClick(@NotNull DialogInterface dialog, int which) {
        assert loaderManager != null;
        loaderManager.destroyLoader(0);
        dialog.dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.w("MiniTasker", "Selected app: "+appIDsList[position]);
        final String tag = getTag() == null ? "" : getTag();
        final AppInfo appInfo = (AppInfo) mAdapter.getItem(position);
        assert appInfo != null;
        final String packageName = appInfo.getPackageName();
        if (packageName == null || action == null) return;
        action.setPackageName(packageName);
        final String packageDisplayName = (appInfo.getPackageDisplayName() == null
                ? "" : appInfo.getPackageDisplayName());

        action.setPackageDisplayName(packageDisplayName);
        if (tag.equals(TERMINATE)) {
            action.setRoot(swiRoot.isChecked());
        }
        assert caller != null;
        action.setStart(caller.is_start);
        action.save();
        if (tag.equals(MUSIC)) {
            MediaAction action2 = new MediaAction();
            action2.setContext(caller);
            action2.setTaskId(caller.task_id);
            action2.setType(MediaActionType.Play);
            action2.setStart(caller.is_start);
            if (action2.save()) {
                action2.saveRestoreCopy();
            }
        }
        if (tag.equals(SHORTCUT)) {
            NewTaskWhenShortcut caller_shortcut = (NewTaskWhenShortcut) caller;
            caller_shortcut.updateApp(packageDisplayName, packageName, (int) action.getPrimary());
        } else {
            caller.entity_id = action.getPrimary();
            caller.last_entity_type = action.getName();
            caller.nextActivityWhen(true);
        }
        mDialog.dismiss();
    }
}