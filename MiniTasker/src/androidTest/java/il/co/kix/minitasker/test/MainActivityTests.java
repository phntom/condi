package il.co.kix.minitasker.test;

import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.ExpandableListView;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.R;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.ui.MainActivity;

import static android.test.ViewAsserts.assertOnScreen;


/**
 * Created by phantom on 06/01/14.
 * MiniTasker Project (c) 2013
 * anton@kix.co.il - http://minitasker.kix.co.il
 */
public class MainActivityTests extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mMainActivity;
    private ExpandableListView mMainList;
    private View mUndoView;
    private View pbrLoading;
    private DatabaseHandler db;

    public MainActivityTests() {
        super(MainActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        String databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        getInstrumentation().getTargetContext().getDatabasePath(databaseName).delete();
        db = new DatabaseHandler(getInstrumentation().getTargetContext(),
                databaseName);
        db.debugMode = true;
        SingletonDatabase.INSTANCE.db = db;

        mMainActivity = getActivity();
        mMainList = (ExpandableListView) mMainActivity.findViewById(R.id.main_view);
        mUndoView = mMainActivity.findViewById(R.id.undo);
        pbrLoading = mMainActivity.findViewById(R.id.pbrLoading);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // Methods whose names are prefixed with test will automatically be run
    public void testMainWindow() {
        assertOnScreen(mMainActivity.getWindow().getDecorView(), mMainList);
        assertOnScreen(mMainActivity.getWindow().getDecorView(), mUndoView);
        assertOnScreen(mMainActivity.getWindow().getDecorView(), pbrLoading);
    }
}
