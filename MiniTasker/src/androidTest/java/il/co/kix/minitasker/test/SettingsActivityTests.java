package il.co.kix.minitasker.test;

import android.content.Intent;
import android.os.Bundle;
import android.test.ActivityUnitTestCase;
import android.widget.EditText;
import android.widget.Switch;

import il.co.kix.minitasker.R;
import il.co.kix.minitasker.ui.SettingsSavableActivity;

import static android.test.ViewAsserts.assertOnScreen;

/**
 * Created by anton on 7/8/2014.
 */
public class SettingsActivityTests extends ActivityUnitTestCase<SettingsSavableActivity> {
    private SettingsSavableActivity mActivity;
    private EditText txtActivityTimeout;
    private Switch swiStatistics;
    private Switch swiActiveProbe;
    private Intent mLaunchIntent;

    public SettingsActivityTests() {
        super(SettingsSavableActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        mLaunchIntent = new Intent(getInstrumentation().getTargetContext(),
                SettingsSavableActivity.class);
    }

    public void setupFields() {
        assertNotNull("setUp failed - activity is null", mActivity);
        txtActivityTimeout = (EditText) mActivity.findViewById(R.id.txtActivityTimeout);
        assertNotNull("txtActivityTimeout is null", txtActivityTimeout);
        swiStatistics = (Switch) mActivity.findViewById(R.id.swiStatistics);
        assertNotNull("swiStatistics is null", swiStatistics);
        swiActiveProbe = (Switch) mActivity.findViewById(R.id.swiActiveProbe);
        assertNotNull("swiActiveProbe is null", swiActiveProbe);
    }

    // Methods whose names are prefixed with test will automatically be run
    public void testWindow() {
        mActivity = startActivity(mLaunchIntent, new Bundle(), null);
        setupFields();
        assertOnScreen(mActivity.getWindow().getDecorView(), txtActivityTimeout);
        assertOnScreen(mActivity.getWindow().getDecorView(), swiStatistics);
        assertOnScreen(mActivity.getWindow().getDecorView(), swiActiveProbe);
        mActivity.finish();
    }

    public void testSave() {
        setActivityContext(getInstrumentation().getTargetContext());
        mActivity = startActivity(mLaunchIntent, new Bundle(), null);
        setupFields();
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                txtActivityTimeout.setText("0");
                swiStatistics.setChecked(false);
                swiActiveProbe.setChecked(false);
                mActivity.performSave();
                mActivity.finish();
            }
        });
    }

    public void testSave2() {
        setActivityContext(getInstrumentation().getTargetContext());
        mActivity = startActivity(mLaunchIntent, new Bundle(), null);
        setupFields();
        assertEquals(txtActivityTimeout.getText().toString(), "0");
        assertFalse(swiStatistics.isChecked());
        assertFalse(swiActiveProbe.isChecked());
    }

    public void testSave3() {
        setActivityContext(getInstrumentation().getTargetContext());
        mActivity = startActivity(mLaunchIntent, new Bundle(), null);
        setupFields();
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                txtActivityTimeout.setText("15");
                swiStatistics.setChecked(true);
                swiActiveProbe.setChecked(true);
                mActivity.performSave();
                mActivity.finish();
            }
        });
    }

    public void testSave4() {
        setActivityContext(getInstrumentation().getTargetContext());
        mActivity = startActivity(mLaunchIntent, new Bundle(), null);
        setupFields();
        assertEquals(txtActivityTimeout.getText().toString(), "15");
//        assertTrue(swiStatistics.isChecked());
//        assertTrue(swiActiveProbe.isChecked());
    }
}