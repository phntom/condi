package il.co.kix.minitasker.test;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.util.Log;

import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;

import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.EntityBase;
import il.co.kix.minitasker.MediaActionType;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.MediaAction;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.conditions.TimeCondition;
import il.co.kix.minitasker.utils.EntityUtils;

/**
 * Created by phantom on 10/8/14.
 * time condition unit testing
 */
@RunWith(MockitoJUnitRunner.class)
public class TimeConditionTests extends InstrumentationTestCase {
    private static final String TAG = "minitaskerTest";
    private DatabaseHandler db;
    private Context context;

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp(): " + getName());

        context = getInstrumentation().getTargetContext();

        String databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        getInstrumentation().getTargetContext().getDatabasePath(databaseName).delete();
        db = new DatabaseHandler(getInstrumentation().getTargetContext(),
                databaseName);
        db.debugMode = true;
        SingletonDatabase.INSTANCE.db = db;

        getInstrumentation().getTargetContext().getCacheDir();

        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        Log.d(TAG, "tearDown(): " + getName());
        super.tearDown();
        waitWhileTasksComplete();
        db.close();
        //noinspection ConstantConditions
        SingletonDatabase.INSTANCE.db = null;
    }

    private void waitWhileTasksComplete() throws InterruptedException {
        while (db.matchConditionCounter > 0 || db.runActionCounter > 0) {
            Thread.sleep(100);
        }
    }

    public static void conditionApplies(boolean trueOnStart,
                                        RangeSet<Integer> ranges, Calendar calendar,
                                        int year, int monthMinusOne, int dayOfMonth, int hour,
                                        int minute, int second) throws InterruptedException {
        calendar.set(year, monthMinusOne, dayOfMonth, hour, minute, second);
        assertTrue(TimeCondition.conditionApplies(trueOnStart, ranges, calendar));
        calendar.set(year, monthMinusOne, dayOfMonth, hour, minute, second);
        assertFalse(TimeCondition.conditionApplies(!trueOnStart, ranges, calendar));

    }

    public void testTimeAppliesNormal() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();

        int start = (int) (TimeUnit.DAYS.toMinutes(4) + TimeUnit.HOURS.toMinutes(14));
        int end = start + 1;
        RangeSet<Integer> ranges = ImmutableRangeSet.of(Range.closedOpen(start, end));

        conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, 9, 13, 59, 0);

        conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, 9, 14, 0, 0);

        conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, 9, 14, 1, 0);

    }

    public void testTimeAppliesInverse() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();

        int start = (int) (TimeUnit.DAYS.toMinutes(4) + TimeUnit.HOURS.toMinutes(14));
        int end = (int) (TimeUnit.DAYS.toMinutes(4) - 1 + 1440);
        RangeSet<Integer> ranges = ImmutableRangeSet.of(Range.closedOpen(start, end));

        conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, 9, 13, 59, 0);

        conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, 9, 14, 0, 0);

        conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, 9, 14, 1, 0);

        conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, 10, 13, 59, 0);

    }

    public void testTimeAppliesSpanning() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();

        int start = (int) (TimeUnit.DAYS.toMinutes(6) + TimeUnit.HOURS.toMinutes(23));
        int end = start + (int) (TimeUnit.HOURS.toMinutes(2));
        RangeSet<Integer> ranges = TreeRangeSet.create();
        ranges.add(Range.closedOpen(start, end));
        ranges.add(Range.closedOpen(start - TimeCondition.minutesInAWeek,
                end - TimeCondition.minutesInAWeek));

        conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, 11, 22, 59, 0);

        conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, 11, 23, 0, 0);

        conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, 12, 0, 0, 0);

        conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, 12, 0, 45, 0);

        conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, 12, 1, 0, 0);

    }

    public void testUpgradeNormalDaily() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();
        RangeSet<Integer> ranges = TreeRangeSet.create();

        int timeStart = 0;
        int timeEnd = 1;
        int timeRepeat = 0;

        TimeCondition.upgradeToRepeatList(calendar, timeStart, timeEnd, timeRepeat, ranges);

        for (int i = 5; i < 5 + 7; i++) {
            conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, i, 0, 0, 0);
            conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, i, 0, 1, 0);
        }

    }

    public void testUpgradeInverseDaily() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();
        RangeSet<Integer> ranges = TreeRangeSet.create();

        int timeStart = 0;
        int timeEnd = -1;
        int timeRepeat = 0;

        TimeCondition.upgradeToRepeatList(calendar, timeStart, timeEnd, timeRepeat, ranges);

        for (int i = 5; i < 5 + 7; i++) {
            conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, i, 0, 0, 0);
            conditionApplies(true, ranges, calendar, 2014, Calendar.OCTOBER, i, 0, 1, 0);
            conditionApplies(false, ranges, calendar, 2014, Calendar.OCTOBER, i + 1, 23, 59, 0);
        }

    }

    public void testScheduleNextNormal() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar2.set(Calendar.MILLISECOND, 0);

        int task_id = db.startNewTask();

        MediaAction action = new MediaAction();
        action.setContext(context);
        action.setPrimary(0);
        action.setTaskId(task_id);
        action.setType(MediaActionType.Pause);
        assertTrue(action.save());
        action.saveRestoreCopy();

        TimeCondition condition = new TimeCondition();
        condition.setContext(context);
        condition.setPrimary(0);
        condition.setTaskId(task_id);
        condition.setParams(1, 2, 0);
        assertTrue(condition.save());
        condition.setTaskEnabled(true);

        calendar.set(2014, Calendar.OCTOBER, 5, 0, 0, 0);
        calendar2.set(2014, Calendar.OCTOBER, 5, 0, 1, 0);

        DatabaseHandler.scheduleNextTimeRangeWhen(db.getWritableDatabase(), calendar, context);
        assertEquals(calendar2.getTimeInMillis(), calendar.getTimeInMillis());

        calendar.set(2014, Calendar.OCTOBER, 5, 0, 1, 0);
        calendar2.set(2014, Calendar.OCTOBER, 5, 0, 2, 0);

        DatabaseHandler.scheduleNextTimeRangeWhen(db.getWritableDatabase(), calendar, context);
        assertEquals(calendar2.getTimeInMillis(), calendar.getTimeInMillis());

    }

    public void testConditionsMatchBothWays() throws InterruptedException, IllegalAccessException, InstantiationException {
        int task_id = db.startNewTask();
        for (Class<? extends EntityBase> conditionClass : EntityUtils.conditions.values()) {
            ConditionBase condition = (ConditionBase) conditionClass.newInstance();
            condition.setContext(context);
            condition.setPrimary(0);
            condition.setTaskId(task_id);
            if (!condition.save()) {
                continue;
            }

            switch (condition.getName()) {
                case "boot":
                case "calendar":
                case "location":
                case "phone":
                case "bluetooth":
                    continue;
            }

            if (condition.conditionApplies(true)) {
                assertFalse(condition.getName() + " conditionApplies matches both ways",
                        condition.conditionApplies(false));
            }
        }
    }


}
