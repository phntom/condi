package il.co.kix.minitasker.test;

import android.database.Cursor;
import android.test.InstrumentationTestCase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.MessageAction;
import il.co.kix.minitasker.conditions.HdmiCondition;

/**
 * Created by anton on 8/7/2014.
 * tests of adding, removing and modifying tasks in the database
 */
public class TasksDbTests extends InstrumentationTestCase {
    private static final String TAG = "CondiTest";
    private String databaseName;
    @NotNull
    private DatabaseHandler db;

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp(): " + getName());
        databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        getInstrumentation().getTargetContext().getDatabasePath(databaseName).delete();
        SingletonDatabase.INSTANCE.db = new DatabaseHandler(getInstrumentation().getTargetContext(),
                databaseName);
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        db.debugMode = true;
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        Log.d(TAG, "tearDown(): " + getName());
        super.tearDown();
        db.close();
    }

    public void testAddTask() {
        int task_id = db.startNewTask();
        Cursor cursor = null;
        try {
            cursor = db.getTasksCursor();
            assertEquals(1, cursor.getCount());
            assertTrue(cursor.moveToFirst());
            assertEquals(task_id, cursor.getLong(cursor.getColumnIndexOrThrow("task_id")));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void testAddAction() {
        for (int repeat = 1; repeat <= 17; repeat++) {
            db.startNewTask();
        }
        int task_id = db.startNewTask();
        MessageAction action = new MessageAction();
        action.setContext(getInstrumentation().getTargetContext());
        action.setPrimary(0);
        action.setTaskId(task_id);
        action.setMsg("hello world");
        assertTrue(action.save());
        action.saveRestoreCopy();
        Cursor cursor = null;
        try {
            cursor = db.getEntityCursor((int) action.getPrimary(), 0);
            assertEquals(6, cursor.getCount());
            assertTrue(cursor.moveToNext());
            assertEquals(0, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(1, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertEquals(action.getPrimary(), cursor.getLong(cursor.getColumnIndexOrThrow("entity_id")));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void testAddCondition() {
        for (int repeat = 1; repeat <= 17; repeat++) {
            db.startNewTask();
        }
        int task_id = db.startNewTask();
        HdmiCondition condition = new HdmiCondition();
        condition.setContext(getInstrumentation().getTargetContext());
        condition.setPrimary(0);
        condition.setTaskId(task_id);
        assertTrue(condition.save());
        Cursor cursor = null;
        try {
            cursor = db.getEntityCursor(0, (int) condition.getPrimary());
            assertEquals(5, cursor.getCount());
            assertTrue(cursor.moveToNext());
            assertEquals(0, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(2, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(4, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(5, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertEquals(condition.getPrimary(), cursor.getLong(cursor.getColumnIndexOrThrow("entity_id")));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void testAddFullTask() {
        for (int repeat = 1; repeat <= 17; repeat++) {
            db.startNewTask();
        }
        int task_id = db.startNewTask();

        MessageAction action = new MessageAction();
        action.setContext(getInstrumentation().getTargetContext());
        action.setPrimary(0);
        action.setTaskId(task_id);
        action.setMsg("hello world");
        assertTrue(action.save());
        action.saveRestoreCopy();

        HdmiCondition condition = new HdmiCondition();
        condition.setContext(getInstrumentation().getTargetContext());
        condition.setPrimary(0);
        condition.setTaskId(task_id);
        assertTrue(condition.save());

        condition.setTaskEnabled(true);

        Cursor cursor = null;
        try {
            cursor = db.getEntityCursor((int) action.getPrimary(), (int) condition.getPrimary());
            assertEquals(7, cursor.getCount());
            assertTrue(cursor.moveToNext());
            assertEquals(0, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(1, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertEquals(action.getPrimary(), cursor.getLong(cursor.getColumnIndexOrThrow("entity_id")));
            assertTrue(cursor.moveToNext());
            assertEquals(2, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(3, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(4, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertTrue(cursor.moveToNext());
            assertEquals(5, cursor.getInt(cursor.getColumnIndexOrThrow("kind")));
            assertEquals(condition.getPrimary(), cursor.getLong(cursor.getColumnIndexOrThrow("entity_id")));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
