package il.co.kix.minitasker.test;

import android.content.ContentResolver;
import android.content.Intent;
import android.provider.Settings;
import android.test.InstrumentationTestCase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.BrightnessAction;
import il.co.kix.minitasker.conditions.BatteryCondition;
import il.co.kix.minitasker.conditions.HdmiCondition;
import il.co.kix.minitasker.receivers.HdmiReceiver;
import il.co.kix.minitasker.receivers.PowerConnectionReceiver;

/**
 * Created by anton on 8/10/2014.
 * here we test running actual tasks and their restore values
 */
public class TaskRunningTests extends InstrumentationTestCase {
    private static final String TAG = "CondiTest";
    private static final int BRIGHTNESS_MAX = 255;
    @NotNull
    private DatabaseHandler db;

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp(): " + getName());
        String databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        getInstrumentation().getTargetContext().getDatabasePath(databaseName).delete();
        SingletonDatabase.INSTANCE.db = new DatabaseHandler(getInstrumentation().getTargetContext(),
                databaseName);
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        db.debugMode = true;
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        waitWhileTasksComplete();
        db.close();
    }

    private void waitWhileTasksComplete() throws InterruptedException {
        while (db.matchConditionCounter > 0 || db.runActionCounter > 0) {
            Thread.sleep(100);
        }
    }


    public void testBrightnessRunTask() throws Settings.SettingNotFoundException, InterruptedException {
        int task_id = db.startNewTask();

        final ContentResolver cr = getInstrumentation().getTargetContext().getContentResolver();
        int previous_level = Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS);
        int target_level = previous_level == BRIGHTNESS_MAX ? 0 : BRIGHTNESS_MAX;

        BrightnessAction action = new BrightnessAction();
        action.setContext(getInstrumentation().getTargetContext());
        action.setPrimary(0);
        action.setTaskId(task_id);
        action.setBrightness(target_level);
        assertTrue(action.save());
        action.saveRestoreCopy();

        HdmiCondition condition = new HdmiCondition();
        condition.setContext(getInstrumentation().getTargetContext());
        condition.setPrimary(0);
        condition.setTaskId(task_id);
        assertTrue(condition.save());

        condition.setTaskEnabled(true);

        final HdmiReceiver receiver = new HdmiReceiver();
        Intent intent = new Intent();
        intent.putExtra("state", true);

        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);

        waitWhileTasksComplete();
        assertEquals(target_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        intent.putExtra("state", false);
        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);

        waitWhileTasksComplete();
        assertEquals(previous_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));
    }

    public void testBrightnessRunTaskResetState() throws Settings.SettingNotFoundException, InterruptedException {
        int task_id = db.startNewTask();

        final ContentResolver cr = getInstrumentation().getTargetContext().getContentResolver();
        int previous_level = Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS);
        int target_level = previous_level == BRIGHTNESS_MAX ? 0 : BRIGHTNESS_MAX;

        BrightnessAction action = new BrightnessAction();
        action.setContext(getInstrumentation().getTargetContext());
        action.setPrimary(0);
        action.setTaskId(task_id);
        action.setBrightness(target_level);
        assertTrue(action.save());
        action.saveRestoreCopy();

        HdmiCondition condition = new HdmiCondition();
        condition.setContext(getInstrumentation().getTargetContext());
        condition.setPrimary(0);
        condition.setTaskId(task_id);
        assertTrue(condition.save());

        condition.setTaskEnabled(true);

        final HdmiReceiver receiver = new HdmiReceiver();
        Intent intent = new Intent();
        intent.putExtra("state", true);
        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);
        waitWhileTasksComplete();
        assertEquals(target_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        db.resetRunningConditions();

        intent.putExtra("state", false);
        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);
        waitWhileTasksComplete();
        assertEquals(previous_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        db.resetRunningConditions();

        intent.putExtra("state", true);
        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);
        waitWhileTasksComplete();
        assertEquals(target_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        db.resetRunningConditions();

        intent.putExtra("state", false);
        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);
        waitWhileTasksComplete();
        assertEquals(previous_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));
    }


    public void testBrightnessOverlappingRunTask() throws Settings.SettingNotFoundException, InterruptedException {
        final ContentResolver cr = getInstrumentation().getTargetContext().getContentResolver();
        int original_level = Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS);
        int target_level = original_level == BRIGHTNESS_MAX ? 0 : BRIGHTNESS_MAX;
        Log.d(this.getName(), "original_level: " + original_level + " target_level: " + target_level);

        int task_id1 = db.startNewTask();
        int task_id2 = db.startNewTask();

        {
            BrightnessAction action = new BrightnessAction();
            action.setContext(getInstrumentation().getTargetContext());
            action.setPrimary(0);
            action.setTaskId(task_id1);
            action.setBrightness(target_level);
            assertTrue(action.save());
            action.saveRestoreCopy();
        }

        {
            HdmiCondition condition = new HdmiCondition();
            condition.setContext(getInstrumentation().getTargetContext());
            condition.setPrimary(0);
            condition.setTaskId(task_id1);
            assertTrue(condition.save());
            condition.setTaskEnabled(true);
        }

        {
            BrightnessAction action = new BrightnessAction();
            action.setContext(getInstrumentation().getTargetContext());
            action.setPrimary(0);
            action.setTaskId(task_id2);
            action.setBrightness(0);
            assertTrue(action.save());
            action.saveRestoreCopy();
        }

        {
            BatteryCondition condition = new BatteryCondition();
            condition.setContext(getInstrumentation().getTargetContext());
            condition.setPrimary(0);
            condition.setTaskId(task_id2);
            assertTrue(condition.save());
            condition.setTaskEnabled(true);
        }


        // start action - brightness max
        final HdmiReceiver hdmiReceiver = new HdmiReceiver();
        Intent hdmiIntent = new Intent();
        hdmiIntent.putExtra("state", true);
        hdmiReceiver.onReceiveBackground(getInstrumentation().getTargetContext(), hdmiIntent);

        waitWhileTasksComplete();
        for (int retry = 5; retry > 0; retry--) {
            if (target_level == Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS))
                break;
            Thread.sleep(1000);
        }
        assertEquals(target_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        // start action brightness min
        final PowerConnectionReceiver powerConnectionReceiver = new PowerConnectionReceiver();
        final Intent batteryLowIntent = new Intent(Intent.ACTION_BATTERY_LOW);
        powerConnectionReceiver.onReceiveBackground(getInstrumentation().getTargetContext(), batteryLowIntent);

        waitWhileTasksComplete();
        for (int retry = 3; retry > 0; retry--) {
            if (0 == Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS))
                break;
            Thread.sleep(1000);
        }
        assertEquals(0, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        // end battery low action - should not restore
        hdmiIntent.putExtra("state", false);
        hdmiReceiver.onReceiveBackground(getInstrumentation().getTargetContext(), hdmiIntent);

        waitWhileTasksComplete();
        for (int retry = 3; retry > 0; retry--) {
            if (0 == Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS))
                break;
            Thread.sleep(1000);
        }
        assertEquals(0, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));

        // end hdmi action - should restore to original
        final Intent batteryOkayIntent = new Intent(Intent.ACTION_BATTERY_OKAY);
        powerConnectionReceiver.onReceiveBackground(getInstrumentation().getTargetContext(), batteryOkayIntent);

        waitWhileTasksComplete();
        for (int retry = 3; retry > 0; retry--) {
            if (original_level == Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS))
                break;
            Thread.sleep(1000);
        }
        assertEquals(original_level, Settings.System.getInt(cr, Settings.System.SCREEN_BRIGHTNESS));
    }
}
