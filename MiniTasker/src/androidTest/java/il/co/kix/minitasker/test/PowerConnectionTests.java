package il.co.kix.minitasker.test;

import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.test.InstrumentationTestCase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Iterator;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.BrightnessAction;
import il.co.kix.minitasker.conditions.ChargerCondition;
import il.co.kix.minitasker.conditions.ConditionBase;
import il.co.kix.minitasker.receivers.PowerConnectionReceiver;
import il.co.kix.minitasker.services.BackgroundService;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by anton on 14/9/2014
 * this test will check the power adapter conditions and low battery
 */
@RunWith(MockitoJUnitRunner.class)
public class PowerConnectionTests extends InstrumentationTestCase {

    private static final String TAG = "CondiTest";
    @NotNull
    private DatabaseHandler db;
    private int task_id;
    private Context context;

    public void testMockitoSanity() {
        //arrange
        Iterator i = mock(Iterator.class);
        when(i.next()).thenReturn("Hello").thenReturn("World");
        //act
        String result = i.next() + " " + i.next();
        //assert
        assertEquals("Hello World", result);
    }

    @Override
    protected void setUp() throws Exception {
        context = getInstrumentation().getTargetContext();
        Log.d(TAG, "setUp(): " + getName());
        String databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        context.getDatabasePath(databaseName).delete();
        SingletonDatabase.INSTANCE.db = new DatabaseHandler(context, databaseName);
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        db.debugMode = true;
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        waitWhileTasksComplete();
        context.stopService(new Intent(context, BackgroundService.class));
        db.close();
    }

    private void waitWhileTasksComplete() throws InterruptedException {
        while (db.matchConditionCounter > 0 || db.runActionCounter > 0) {
            Thread.sleep(100);
        }
    }

    private void addTask() {
        task_id = db.startNewTask();
    }

    private void addAction() {
        addTask();
        BrightnessAction action = new BrightnessAction();
        action.setContext(context);
        action.setPrimary(0);
        action.setBrightness(0);
        action.setTaskId(task_id);
        assertTrue(action.save());
        action.saveRestoreCopy();
    }

    private void addCondition(@NotNull ConditionBase condition) {
        condition.setContext(context);
        condition.setPrimary(0);
        condition.setTaskId(task_id);
    }

    private void saveCondition(@NotNull ConditionBase condition) {
        assertTrue(condition.save());
        condition.setTaskEnabled(true);
    }

    private void sanityChargerState(int state, boolean connect, int charger_type) throws InterruptedException {
        final PowerConnectionReceiver powerConnectionReceiver = new PowerConnectionReceiver();
        final Intent powerPlugged = new Intent(Intent.ACTION_POWER_CONNECTED);
        powerPlugged.putExtra(BatteryManager.EXTRA_PLUGGED, charger_type);
        final Intent powerUnplugged = new Intent(Intent.ACTION_POWER_DISCONNECTED);
        addAction();

        ChargerCondition condition = new ChargerCondition();

        addCondition(condition);
        condition.setState(state);
        saveCondition(condition);

        powerConnectionReceiver.onReceiveBackground(context, connect ? powerPlugged : powerUnplugged);
        waitWhileTasksComplete();
        assertEquals("a single task was ran", 1, db.countLast(db.getWritableDatabase(), LastType.BRIGHTNESS, 0));

        powerConnectionReceiver.onReceiveBackground(context, connect ? powerUnplugged : powerPlugged);
        waitWhileTasksComplete();
        assertEquals("a single task was ran", 0, db.countLast(db.getWritableDatabase(), LastType.BRIGHTNESS, 0));
    }


    public void testPowerAdapterConnectAny() throws InterruptedException {
        sanityChargerState(0, true, BatteryManager.BATTERY_PLUGGED_AC);
    }

    public void testPowerAdapterConnectUsb() throws InterruptedException {
        sanityChargerState(1, true, BatteryManager.BATTERY_PLUGGED_USB);
    }

    public void testPowerAdapterConnectWall() throws InterruptedException {
        sanityChargerState(2, true, BatteryManager.BATTERY_PLUGGED_AC);
    }

    public void testPowerAdapterDisconnectAny() throws InterruptedException {
        sanityChargerState(3, false, BatteryManager.BATTERY_PLUGGED_AC);
    }

    public void testPowerAdapterConnectWireless() throws InterruptedException {
        sanityChargerState(4, true, PowerConnectionReceiver.BATTERY_PLUGGED_WIRELESS);
    }

    public void testPowerAdapterDisconnectAnyNew() throws InterruptedException {
        sanityChargerState(5, false, BatteryManager.BATTERY_PLUGGED_AC);
    }

    public void testPowerAdapterDisconnectUsb() throws InterruptedException {
        sanityChargerState(6, false, BatteryManager.BATTERY_PLUGGED_USB);
    }

    public void testPowerAdapterDisconnectWall() throws InterruptedException {
        sanityChargerState(7, false, BatteryManager.BATTERY_PLUGGED_AC);
    }

    public void testPowerAdapterDisconnectWireless() throws InterruptedException {
        sanityChargerState(8, false, PowerConnectionReceiver.BATTERY_PLUGGED_WIRELESS);
    }
}
