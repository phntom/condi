package il.co.kix.minitasker.test;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.telephony.TelephonyManager;
import android.test.InstrumentationTestCase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.actions.ActionBase;
import il.co.kix.minitasker.actions.PhoneAction;
import il.co.kix.minitasker.conditions.PhoneCondition;
import il.co.kix.minitasker.receivers.PhoneCallReceiver;

/**
 * Created by anton on 8/24/2014.
 * test some silence man
 */
public class PhoneSilenceTests extends InstrumentationTestCase {

    private static final String TAG = "CondiTest";
    private int task_id;
    @NotNull
    private DatabaseHandler db;
    private ActionBase action;
    private PhoneCondition condition;
    private AudioManager audioManager;
    private int max_ring_level;
    private int avg_level;

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp(): " + getName());

        String databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        getInstrumentation().getTargetContext().getDatabasePath(databaseName).delete();
        SingletonDatabase.INSTANCE.db = new DatabaseHandler(getInstrumentation().getTargetContext(),
                databaseName);
        assert SingletonDatabase.INSTANCE.db != null;
        //noinspection ConstantConditions
        db = SingletonDatabase.INSTANCE.db;
        assert db != null;
        db.debugMode = true;

        audioManager = (AudioManager) getInstrumentation().getTargetContext()
                .getSystemService(Context.AUDIO_SERVICE);
        max_ring_level = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        avg_level = max_ring_level / 2;

        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        waitWhileTasksComplete();
        db.close();
    }

    private void waitWhileTasksComplete() throws InterruptedException {
        while (db.matchConditionCounter > 0 || db.runActionCounter > 0) {
            Thread.sleep(100);
        }
    }

    private void createTask(@NotNull String phoneNumber, boolean isPrivate, boolean isUnknown,
                            boolean isInverse) {
        task_id = db.startNewTask();

        action = new PhoneAction();
        action.setContext(getInstrumentation().getTargetContext());
        action.setPrimary(0);
        action.setTaskId(task_id);
        action.setStart(true);

        assertTrue("testing silence action", ((PhoneAction) action).isSilence());
        assertTrue("should suggest creating a restore action", action.save());

        action.saveRestoreCopy();

        addPhoneCondition(phoneNumber, isPrivate, isUnknown, isInverse);

        condition.setTaskEnabled(true);
    }

    private void addPhoneCondition(@NotNull String phoneNumber, boolean isPrivate, boolean isUnknown, boolean isInverse) {
        condition = new PhoneCondition();
        condition.setContext(getInstrumentation().getTargetContext());
        condition.setPrimary(0);
        condition.setTaskId(task_id);

        condition.setPhone(phoneNumber);
        condition.setBlockPrivate(isPrivate);
        condition.setBlockUnrecognized(isUnknown);
        condition.setKind(PhoneCondition.KIND_BLOCK);
        condition.setBlockInverse(isInverse);

        assertTrue(condition.save());
    }

    private void fakeCallStart(String phoneNumber) throws InterruptedException {
        final PhoneCallReceiver receiver = new PhoneCallReceiver();
        Intent intent = new Intent();

        intent.setAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        intent.putExtra(TelephonyManager.EXTRA_STATE, TelephonyManager.EXTRA_STATE_RINGING);
        intent.putExtra(TelephonyManager.EXTRA_INCOMING_NUMBER, phoneNumber);

        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);

        waitWhileTasksComplete();
    }

    private void fakeCallEnd() throws InterruptedException {
        final PhoneCallReceiver receiver = new PhoneCallReceiver();
        Intent intent = new Intent();

        intent.setAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        intent.putExtra(TelephonyManager.EXTRA_STATE, TelephonyManager.EXTRA_STATE_IDLE);

        receiver.onReceiveBackground(getInstrumentation().getTargetContext(), intent);

        waitWhileTasksComplete();
    }

    private void setRingerAverage() {
        audioManager.setStreamVolume(AudioManager.STREAM_RING, avg_level, 0);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        verifyRingerAverage();
    }

    private void verifyRingerAverage() {
        assertEquals("checking ring volume was set",
                avg_level, audioManager.getStreamVolume(AudioManager.STREAM_RING));
        assertEquals("checking ring mode was set",
                AudioManager.RINGER_MODE_NORMAL, audioManager.getRingerMode());
    }


    private void setRingerSilent() {
        audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        verifyRingerSilent();
    }

    private void verifyRingerSilent() {
        assertEquals("checking ring mode was silenced",
                AudioManager.RINGER_MODE_SILENT, audioManager.getRingerMode());
    }

    public void testSilenceSanity() throws InterruptedException {

        createTask("5556666", false, false, false);

        setRingerAverage();

        fakeCallStart("5556666");

        verifyRingerSilent();

        fakeCallEnd();

        verifyRingerAverage();

    }

    public void testSilenceAlreadySilent() throws InterruptedException {

        createTask("5556666", false, false, false);

        setRingerSilent();

        fakeCallStart("5556666");

        verifyRingerSilent();

        fakeCallEnd();

        verifyRingerSilent();

        setRingerAverage();

    }

    public void testSilenceNoMatch() throws InterruptedException {

        createTask("5556666", false, false, false);

        setRingerAverage();

        fakeCallStart("6665555");

        verifyRingerAverage();

        fakeCallEnd();

        verifyRingerAverage();

    }

    public void testSilenceWildcardMatch() throws InterruptedException {
        createTask("555*", false, false, false);

        setRingerAverage();

        fakeCallStart("555");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("5556666");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("5546666");
        verifyRingerAverage();
        fakeCallEnd();
        verifyRingerAverage();

        setRingerAverage();
    }

    public void testBlacklist() throws InterruptedException {

        createTask("5556666", false, false, false);
        createTask("6666", false, false, false);
        createTask("1234", false, false, false);
        createTask("1234199", false, false, false);
        setRingerAverage();

        fakeCallStart("5556666");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("12345");
        verifyRingerAverage();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("1234");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("1234199");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("12");
        verifyRingerAverage();
        fakeCallEnd();
        verifyRingerAverage();
    }

    public void testWhiteList() throws InterruptedException {

        setRingerAverage();
        createTask("5556666", false, false, true);
        addPhoneCondition("1234", false, false, true);

        fakeCallStart("5556666");
        verifyRingerAverage();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("1234199");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();

        fakeCallStart("1234");
        verifyRingerAverage();
        fakeCallEnd();
        verifyRingerAverage();


        fakeCallStart("12");
        verifyRingerSilent();
        fakeCallEnd();
        verifyRingerAverage();
    }

}
