package il.co.kix.minitasker.test;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.test.InstrumentationTestCase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import il.co.kix.minitasker.DatabaseHandler;
import il.co.kix.minitasker.LastType;
import il.co.kix.minitasker.SingletonDatabase;
import il.co.kix.minitasker.StateType;
import il.co.kix.minitasker.actions.ActionBase;
import il.co.kix.minitasker.actions.AirplaneModeAction;
import il.co.kix.minitasker.actions.BluetoothAction;
import il.co.kix.minitasker.actions.BrightnessAction;
import il.co.kix.minitasker.actions.GPSAction;
import il.co.kix.minitasker.actions.MobileDataAction;
import il.co.kix.minitasker.actions.NFCAction;
import il.co.kix.minitasker.actions.OrientationAction;
import il.co.kix.minitasker.actions.StatesAction;
import il.co.kix.minitasker.actions.VolumeControlAction;
import il.co.kix.minitasker.actions.WiFiAction;
import il.co.kix.minitasker.actions.WifiApAction;
import il.co.kix.minitasker.conditions.HdmiCondition;
import il.co.kix.minitasker.receivers.HdmiReceiver;

/**
 * Created by anton on 8/14/2014.
 * a dedicated test for all types of actions that use restore values
 */
@RunWith(MockitoJUnitRunner.class)
public class LastRestoreCounterTests extends InstrumentationTestCase {
    private static final String TAG = "minitaskerTest";
    private int task_id;
    private DatabaseHandler db;

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp(): " + getName());

        String databaseName = DatabaseHandler.DATABASE_NAME + getName();
        //noinspection ResultOfMethodCallIgnored
        getInstrumentation().getTargetContext().getDatabasePath(databaseName).delete();
        db = new DatabaseHandler(getInstrumentation().getTargetContext(),
                databaseName);
        db.debugMode = true;
        SingletonDatabase.INSTANCE.db = db;
//        BackgroundService backgroundService = Mockito.mock(BackgroundService.class);
//        Mockito.stub(backgroundService.onStartCommand((Intent) Mockito.anyObject(), Mockito.anyInt(),
//                Mockito.anyInt())).toReturn(Service.START_STICKY);

        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        Log.d(TAG, "tearDown(): " + getName());
        super.tearDown();
        waitWhileTasksComplete();
        db.close();
        //noinspection ConstantConditions
        SingletonDatabase.INSTANCE.db = null;
    }

    private void waitWhileTasksComplete() throws InterruptedException {
        while (db.matchConditionCounter > 0 || db.runActionCounter > 0) {
            Thread.sleep(100);
        }
    }

    private void createTask() {
        task_id = db.startNewTask();
    }

    private void createCondition() {
        HdmiCondition condition = new HdmiCondition();
        condition.setContext(getInstrumentation().getTargetContext());
        condition.setPrimary(0);
        condition.setTaskId(task_id);
        assertTrue(condition.save());
        condition.setTaskEnabled(true);
    }

    private void runTask(boolean start) {
        final HdmiReceiver hdmiReceiver = new HdmiReceiver();
        Intent hdmiIntent = new Intent();
        hdmiIntent.putExtra("state", start);
        hdmiReceiver.onReceiveBackground(getInstrumentation().getTargetContext(), hdmiIntent);
    }

    private void checkActionRun(@NotNull ActionBase action, @NotNull LastType lastType) throws InterruptedException {
        action.setTaskId(task_id);
        assertTrue(action.save());
        action.saveRestoreCopy();
        createCondition();

        int count = db.countLast(db.getWritableDatabase(), lastType, 0);
        assertEquals("no running tasks", 0, count);

        runTask(true);

        waitWhileTasksComplete();

        if (db.countFlags() > 0) return; // error running action in vm

        assertNotSame("expected last counter to change", count,
                db.countLast(db.getWritableDatabase(), lastType, 0));

        count = db.countLast(db.getWritableDatabase(), lastType, 0);

        assertEquals("a single task was ran", 1, count);

        runTask(false);

        waitWhileTasksComplete();

        count = db.countLast(db.getWritableDatabase(), lastType, 0);

        assertEquals("no running tasks", 0, count);
    }

    private void checkActionPreRun(@NotNull ActionBase action) {
        createTask();
        action.setContext(getInstrumentation().getTargetContext());
        action.setPrimary(0);
    }

    private void checkStatesAction(@NotNull StatesAction action, @NotNull LastType lastType) throws InterruptedException {
        checkActionPreRun(action);
        action.setState(StateType.Toggle);
        checkActionRun(action, lastType);
    }

    public void testWifiAction() throws Exception {
        if (!getInstrumentation().getTargetContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_WIFI)) return;
        if (isAirplaneModeEnabled()) throw new Exception("Airplane Mode must be Off");
        checkStatesAction(new WiFiAction(), LastType.WIFI);
    }

    public void testBluetoothAction() throws InterruptedException {
        checkStatesAction(new BluetoothAction(), LastType.BLUETOOTH);
    }

    protected boolean isAirplaneModeEnabled() {
        String AIRPLANE_MODE_ON;
        int airplane_enabled;
        final ContentResolver cr = getInstrumentation().getTargetContext().getContentResolver();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            //noinspection deprecation
            AIRPLANE_MODE_ON = Settings.System.AIRPLANE_MODE_ON;
            airplane_enabled = Settings.System.getInt(cr, AIRPLANE_MODE_ON, 0);
        } else {
            AIRPLANE_MODE_ON = Settings.Global.AIRPLANE_MODE_ON;
            airplane_enabled = Settings.Global.getInt(cr, AIRPLANE_MODE_ON, 0);
        }
        return airplane_enabled == 1;
    }

    public void testAirplaneModeAction() throws Exception {
        if (isAirplaneModeEnabled()) throw new Exception("Airplane Mode must be Off");
        checkStatesAction(new AirplaneModeAction(), LastType.AIRPLANE);
        Thread.sleep(3000);
        for (int i = 30; i >= 0; i--) {
            if (!isAirplaneModeEnabled()) break;
            Thread.sleep(1000);
        }
        if (isAirplaneModeEnabled()) throw new Exception("Airplane Mode didn't shut down");
    }

    public void testGPSAction() throws InterruptedException {
        checkStatesAction(new GPSAction(), LastType.GPS);
    }

    public void testMobileDataAction() throws Exception {
        if (!getInstrumentation().getTargetContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_TELEPHONY)) return;
        if (isAirplaneModeEnabled()) throw new Exception("Airplane Mode must be Off");
        checkStatesAction(new MobileDataAction(), LastType.MOBILE);
    }

    public void testNFCAction() throws InterruptedException {
        if (!getInstrumentation().getTargetContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_NFC)) return;
        checkStatesAction(new NFCAction(), LastType.NFC);
    }

    public void testOrientationAction() throws InterruptedException {
        checkStatesAction(new OrientationAction(), LastType.ORIENTATION);
    }

//    public void testScanningAction() throws InterruptedException {
//        if (!getInstrumentation().getTargetContext().getPackageManager().hasSystemFeature(
//                PackageManager.FEATURE_WIFI)) return;
//        checkStatesAction(new ScanningAction(), LastType.SCANNING);
//    }

//    public void testSyncAction() throws InterruptedException {
//        checkStatesAction(new ScanningAction(), LastType.SYNC);
//    }

    public void testWifiApAction() throws Exception {
        if (!getInstrumentation().getTargetContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_WIFI)) return;
        if (isAirplaneModeEnabled()) throw new Exception("Airplane Mode must be Off");
        checkStatesAction(new WifiApAction(), LastType.WIFI_AP);
    }

    public void testBrightnessAction() throws InterruptedException {
        BrightnessAction action = new BrightnessAction();
        checkActionPreRun(action);
        action.setBrightness(0);
        checkActionRun(action, LastType.BRIGHTNESS);
    }

    private void checkVolumeAction(boolean vibrate, int activate_position, @NotNull LastType lastType) throws InterruptedException {
        VolumeControlAction action = new VolumeControlAction();
        checkActionPreRun(action);
        action.setVolume_alarm(125);
        action.setVolume_media(125);
        action.setVolume_notification(125);
        action.setVolume_ringtone(125);
        action.setVibrate(vibrate);
        action.setActive(true, activate_position);
        action.setLink_ringtone_notification(false);
        checkActionRun(action, lastType);
    }

    public void testVolumeAlarmAction() throws InterruptedException {
        checkVolumeAction(false, VolumeControlAction.STREAM_ALARM, LastType.VOLUME_ALARM_VOLUME);
    }

    public void testVolumeMediaAction() throws InterruptedException {
        checkVolumeAction(false, VolumeControlAction.STREAM_MEDIA, LastType.VOLUME_MEDIA_VOLUME);
    }

    public void testVolumeNotificationAction() throws InterruptedException {
        checkVolumeAction(false, VolumeControlAction.STREAM_NOTIFICATION, LastType.VOLUME_NOTIFICATION);
    }

    public void testVolumeRingAction() throws InterruptedException {
        checkVolumeAction(false, VolumeControlAction.STREAM_RINGTONE, LastType.VOLUME_RING_VOLUME);
    }

    public void testModeRingAction() throws InterruptedException {
        checkVolumeAction(true, VolumeControlAction.STREAM_RINGTONE, LastType.VOLUME_RING_MODE);
    }

}